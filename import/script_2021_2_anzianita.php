<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
</head>
<body>
	<?php
	ini_set('display_errors', 1);
	error_reporting(E_ALL);

	include_once("pdo.php");
	include_once("../application/helpers/utils_helper.php");
	$db = new db('localhost', 'root', '', 'assistenza2000');

	$change_db = false;

	$f = fopen("users_work.csv", "r");
	while (!feof($f)) {
		$row = fgets($f);
		if ($row != "") {
			list($matr, $cognome, $nome, $data_nascita, $eta, $comune_nascita, $nazionalita, $sesso, $area, $servizio, $cliente, $mansione, $data_assunzione, $mesi_servizio, $mesi_precedenti, $mesi_esper_totale, $anni_esper_totale, $rank_5_anni, $prima_assunzione, $ultima_assunzione, $anno_cessazione, $in_forza, $boh1, $boh2, $boh3, $boh4, $boh5) = explode(";", $row);
			echo '<pre>'.$row.'</pre>';
			$nome = str_replace("  ", " ", trim(trim(trim($nome))));
			$cognome = str_replace("  ", " ", trim(trim(trim($cognome))));
			$eta = calc_age($data_nascita);
			$res = $db->fetch_query("SELECT * FROM users_info WHERE first_name = '".addslashes($nome)."' AND last_name = '".addslashes($cognome)."'");
			// fallback 1
			if (!$res) {
				$q = "SELECT * FROM users_info WHERE first_name LIKE '".substr(addslashes($nome), 0, 3)."%' AND last_name = '".addslashes($cognome)."'";
				echo '<pre>FALLBACK 1: '.$q.'</pre>';
				$res = $db->fetch_query($q);
			}
			// fallback 2
			if (!$res) {
				$q = "SELECT * FROM users_info WHERE first_name = '".addslashes($nome)."' AND last_name LIKE '".substr(addslashes($cognome), 0, -2)."%'";
				echo '<pre>FALLBACK 2: '.$q.'</pre>';
				$res = $db->fetch_query($q);
			}
			// fallback 3
			if (!$res AND (strpos($nome, ' ') OR strpos($cognome, ' '))) {
				$ar_nome = explode(' ', addslashes($nome));
				$ar_cognome = explode(' ', addslashes($cognome));
				if (strlen($ar_cognome[0]) >= 6) {
					$q = "SELECT * FROM users_info WHERE last_name LIKE '%".$ar_cognome[0]."%'";
					echo '<pre>FALLBACK 3: '.$q.'</pre>';
					$res = $db->fetch_query($q);
				} else {
					echo '<pre>NO FALLBACK 3</pre>';
				}
			}
			// fallback 4
			if (!$res) {
				$ar_nome = explode(' ', addslashes($nome));
				$ar_cognome = explode(' ', addslashes($cognome));
				if (strlen($ar_cognome[0]) >= 6) {
					$q = "SELECT * FROM users_info WHERE first_name LIKE '%".$ar_cognome[0]."%'";
					echo '<pre>FALLBACK 4: '.$q.'</pre>';
					$res = $db->fetch_query($q);
				} else {
					echo '<pre>NO FALLBACK 4</pre>';
				}
			}

			if ($res) {
				$res = $res[0];
				echo '<pre>';
				print_r($res);
				echo '</pre>';

				$id_user = $res['id_user'];
			} else {
				$id_user = null;

				echo '<pre style="background: green; padding: 5px; color: #fff;">UTENTE '.$cognome.' - '.$nome.' NON TROVATO - DEVO INSERIRLO</pre>';

				# USERS
				$fields = array(
					'username' => str_replace(" ", "-", $cognome).'-'.str_replace(" ", "-", $nome),
					'password' => md5($cognome),
					'active' => '1',
					'role' => 'user'
				);
				echo '<pre>';
				print_r($fields);
				echo '</pre>';
				if ($change_db)
					$id_user = $db->iquery($fields, 'users');
				
				# USERS_INFO
				if ($id_user) {	// se poco fa ho inserito l'utente
					$fields = array(
						'id_user' => $id_user,
						'first_name' => $nome,
						'last_name' => $cognome,
						'company' => 'Assistenza 2000',
						'email' => null,
						'telephone' => null
					);
					echo '<pre>';
					print_r($fields);
					echo '</pre>';
					if ($change_db)
						$db->iquery($fields, 'users_info');
				}
			}

			if ($id_user) {
				# USERS_INFO
				$fields = [
					'age' => $eta,
					'birth_date' => rvd($data_nascita),
					'birth_city' => $comune_nascita,
					'nationality' => $nazionalita,
					'gender' => $sesso
				];
				echo '<pre>';
				print_r($fields);
				echo '</pre>';
				if ($change_db)
					$db->uquery($fields, 'users_info', array('id_user = '.$id_user));

				# USERS_WORK
				$fields = [
					'id_user' => $id_user,
					'date_hiring' => rvd($data_assunzione),
					'serv_months' => str_replace(",", ".", trim(trim(trim($mesi_servizio)))),
					'prev_months' => str_replace(",", ".", trim(trim(trim($mesi_precedenti)))),
					'tot_months' => str_replace(",", ".", trim(trim(trim($mesi_esper_totale)))),
					'tot_years' => str_replace(",", ".", trim(trim(trim($anni_esper_totale)))),
					'rank' => trim(trim(trim($rank_5_anni))),
					'temp' => trim(trim(trim($rank_5_anni))) == 'Temp' ? 1 : 0,
					'freelance' => preg_match('/.*libero.*/', strtolower($mansione)) ? 1 : 0,
					'trainee' => 0,
					'first_hiring' => trim(trim(trim($prima_assunzione))),
					'last_hiring' => trim(trim(trim($ultima_assunzione))),
					'is_hired' => trim($in_forza) == 'no' ? 0 : 1,
					'year_termination' => trim(trim(trim($anno_cessazione)))
				];
				echo '<pre>';
				print_r($fields);
				echo '</pre>';
				$user_work = $db->fetch_query("SELECT * FROM users_work WHERE id_user = ".$id_user);
				if ($change_db) {
					if (!$user_work)
						$db->iquery($fields, 'users_work');
					else
						$db->uquery($fields, 'users_work', array('id_user = '.$id_user));
				}

				# JOB
				$job_db = $db->fetch_query("SELECT jobs.name FROM jobs JOIN jobs_users ON jobs.id = jobs_users.id_job WHERE jobs_users.id_user = ".$id_user);
				$mansione = strtolower(trim(trim(trim($mansione == 'OSS' ? 'Operatore socio sanitario' : $mansione))));
				if ($job_db) {
					$job_db = strtolower(trim(trim(trim($job_db[0]['name']))));
					echo '<pre>'.$job_db.' <> '.$mansione.'</pre>';
					if ($job_db != $mansione) {
						echo '<pre style="background: orange; padding: 5px; color: #fff;">MANSIONE DA AGGIORNARE</pre>';
						$new_job_db = $db->fetch_query("SELECT * FROM jobs WHERE name = '".$mansione."'");
						echo '<pre>NEW JOB ID: '.$new_job_db[0]['id'].'</pre>';
						if ($change_db)
							$db->query("DELETE FROM jobs_users WHERE id_user = ".$id_user);
						$fields = array(
							'id_user' => $id_user,
							'id_job' => $new_job_db[0]['id']
						);
						echo '<pre>';
						print_r($fields);
						echo '</pre>';
						if ($change_db)
							$db->iquery($fields, 'jobs_users');
					}
				} else {
					$new_job_db = $db->fetch_query("SELECT * FROM jobs WHERE name = '".$mansione."'");
					echo '<pre>NEW JOB ID: '.$new_job_db[0]['id'].'</pre>';
					if ($change_db)
						$db->query("DELETE FROM jobs_users WHERE id_user = ".$id_user);
					$fields = array(
						'id_user' => $id_user,
						'id_job' => $new_job_db[0]['id']
					);
					echo '<pre>';
					print_r($fields);
					echo '</pre>';
					if ($change_db)
						$db->iquery($fields, 'jobs_users');
				}

				# AREA
				echo '<pre style="background: #eee; padding: 5px;">AREA: '.$area.'</pre>';
				$new_area_db = $db->fetch_query("SELECT * FROM areas WHERE name = '".addslashes($area)."'");
				if ($new_area_db) {
					echo '<pre>NEW AREA ID: '.$new_area_db[0]['id'].'</pre>';
					if ($change_db)
						$db->query("DELETE FROM areas_users WHERE id_user = ".$id_user);
					$fields = array(
						'id_user' => $id_user,
						'id_area' => $new_area_db[0]['id']
					);
					echo '<pre>';
					print_r($fields);
					echo '</pre>';
					if ($change_db)
						$db->iquery($fields, 'areas_users');
				} else {
					echo '<pre style="background: #d90000; padding: 5px; color: #fff;">AREA NON TROVATA</pre>';
				}

				# SERVICE
				echo '<pre style="background: #eee; padding: 5px;">SERVIZIO: '.$servizio.'</pre>';
				$new_service_db = $db->fetch_query("SELECT * FROM services WHERE name = '".addslashes($servizio)."'");
				if ($new_service_db) {
					echo '<pre>NEW SERVICE ID: '.$new_service_db[0]['id'].'</pre>';
					if ($change_db)
						$db->query("DELETE FROM services_users WHERE id_user = ".$id_user);
					$fields = array(
						'id_user' => $id_user,
						'id_service' => $new_service_db[0]['id']
					);
					echo '<pre>';
					print_r($fields);
					echo '</pre>';
					if ($change_db)
						$db->iquery($fields, 'services_users');
				} else {
					echo '<pre style="background: #d90000; padding: 5px; color: #fff;">SERVIZIO NON TROVATO</pre>';
				}

				# CUSTOMER
				echo '<pre style="background: #eee; padding: 5px;">CLIENTE: '.$cliente.'</pre>';
				$new_customer_db = $db->fetch_query("SELECT * FROM customers WHERE name = '".addslashes($cliente)."'");
				if ($new_customer_db) {
					echo '<pre>NEW CUSTOMER ID: '.$new_customer_db[0]['id'].'</pre>';
					if ($change_db)
						$db->query("DELETE FROM customers_users WHERE id_user = ".$id_user);
					$fields = array(
						'id_user' => $id_user,
						'id_customer' => $new_customer_db[0]['id']
					);
					echo '<pre>';
					print_r($fields);
					echo '</pre>';
					if ($change_db)
						$db->iquery($fields, 'customers_users');
				} else {
					echo '<pre style="background: #d90000; padding: 5px; color: #fff;">CLIENTE NON TROVATO</pre>';
				}
			}

			echo '<hr style="border-bottom: 5px solid green; margin: 20px 0;">';	
		}
	}	// fine ciclo
	fclose($f);
	?>
</body>
</html>