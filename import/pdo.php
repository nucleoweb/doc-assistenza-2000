<?
# DB CLASS
# A PDO EXTENSION BY ENDRIU DB
# version 0.1 | type BETA
# For any suggestion or information write to: dibbe84@gmail.com

class db extends PDO {
    
	const DBHOST = 'localhost';
	const DBUSR = 'root';
	const DBPSW = '';
	const DBNAME = 'endriu_test';
	const DBDRIVER = 'mysql';
    
	/* 
	Build the PDO connection
	(the connection data can be changed by passing parameters)
	*/
    function __construct($dbhost = NULL, $dbusr = NULL, $dbpsw = NULL, $dbname = NULL, $dbdriver = NULL) {	
		$dsn = ($dbdriver ? $dbdriver : self::DBDRIVER).':dbname='.($dbname ? $dbname : self::DBNAME).';host='.($dbhost ? $dbhost : self::DBHOST);
		try {
			parent::__construct($dsn, ($dbusr ? $dbusr : self::DBUSR), ($dbpsw ? $dbpsw : self::DBPSW));
			$this->query("SET NAMES 'utf8'");
		} catch (PDOException $e) {
			echo 'Connection failed: '.$e->getMessage();
		}
	}
	
	/*
	Executes a query
	*/
	function query($sql) {
		$statement = $this->prepare($sql);
		if ($statement->execute()) {	// execute() returns TRUE / FALSE
			return $statement;
		}
		else {
			return FALSE;
		}
	}
	
	/* 
	Executes the query and returnes the fetched result
	*/
	function fetch_query($sql) {
		$statement = $this->query($sql);
		$res = array();
		while ($line = $statement->fetch(PDO::FETCH_ASSOC)) {
			array_push($res, $line);
		}
		return $res;
	}
	
	/*
	Return the list of table fields 
	(the second parameter - if set to 1 - excludes ids, the primary key "id" and all the foreing keys names like "id_TABLENAME")
	*/
    function getFields($tab, $excludeIds = 0) {
        $sql = "DESCRIBE ".$tab;
        $res = $this->fetch_query($sql);

		$ar_fields = array();
		foreach ($res as $line) {
			if (!$excludeIds OR ($excludeIds AND !preg_match('/^id$/', $line['Field']) AND !preg_match('/^id_.*/', $line['Field']))) {
                $ar_fields[] = $line['Field'];
			}
		}
		return $ar_fields;       
    }
	
	/* 
	Executes an INSERT query 
	$ar => values associative array (field => value)
	$tab => table where inserting data
	$debug => setting debug parameter to 1 prints query instead of executing it
	*/
	function iquery($ar, $tab, $debug = 0) {
		$sql = "INSERT INTO ".$tab." (";
		
		foreach ($ar as $k => $v) {
		  $sql .= $k.",";	
		}
		$sql = substr($sql, 0, -1);
		$sql .= ") VALUES (";
		
		foreach ($ar as $k => $v) {
			$sql .= "'".addslashes($v)."',";
		}
		$sql = substr($sql, 0, -1);
		$sql .= ")";

		if (!$debug) {
			if ($this->query($sql)) 
				return $this->lastInsertId();
			else 
				return FALSE;
		}
		else {			
			echo '<pre>'.$sql.'</pre>';
		}
	}
	
	/* 
	Executes an UPDATE query
	$ar = values associative array (field => value)
	$tab = table where inserting data
	$where = array of conditions (to do the update) - each element of array is a string eg. "id = 5"
	$extra = any other string to append at the end of the query
	$debug => setting debug parameter to 1 prints query instead of executing it
	*/
	function uquery($ar, $tab, $cond = NULL, $extra = NULL, $debug = 0) {
		$sql = "UPDATE ".$tab." SET ";
            
		foreach ($ar as $k => $v) {
			$sql .= $k." = '".addslashes($v)."',";	                  
		}
		$sql = substr($sql, 0, -1);
		if ($cond) {
			$sql .= " WHERE ";
			foreach ($cond as $c) {
				$sql .= $c." AND ";
			}
			$sql = substr($sql, 0, -5);
		}
		if ($extra) 
			$sql .= ' '.$extra;

		if (!$debug) {
			if ($this->query($sql)) 
				return TRUE;
			else 
				return FALSE;
		}
		else {			
			echo '<pre>'.$sql.'</pre>';
		}
	}
	
	/*
	Return the ID of the last inserted record
	(just an alias for the PDO function lastInsertId())
	*/
    function lastID() {
        return $this->lastInsertId();	
    }
	    
}
?>