<?php
class ArubaSMS {

    const SDK_USERNAME = 'Sms58615';
    const SDK_PASSWORD = 'rhueih234$';
    const SENDER_NUM = '3711649736';
    const SENDER_NAME = 'Assistenza2000';

    private $user_key;
    private $session_key;

    function __construct() {
		$this->login();
	}

    private function login() {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://smspanel.aruba.it/API/v1.0/REST/login');
        curl_setopt($ch, CURLOPT_USERPWD, self::SDK_USERNAME.':'.self::SDK_PASSWORD);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

        if ($info['http_code'] != 200) {
            echo('Error! http code: ' . $info['http_code'] . ', body message: ' . $response);
            die();
        } else {
            $values = explode(";", $response);
            // echo('user_key: ' . $values[0]);
            // echo('session_key: ' . $values[1]);
            $this->user_key = $values[0];
            $this->session_key = $values[1];
        }
    }

    public function get_credit() {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://smspanel.aruba.it/API/v1.0/REST/status');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-type: application/json',
            'user_key: '.$this->user_key,
            // Use this when using session key authentication
            'session_key: '.$this->session_key,
            // When using Access Token authentication, use this instead:
            // 'Access_token: UserParam{access_token}'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

        if ($info['http_code'] != 200) {
            echo('Error! http code: ' . $info['http_code'] . ', body message: ' . $response);
            // die();
        } else {
            $obj = json_decode($response);
            return $obj;
        }
    }

    public function send_sms($message, $recipient) {
        $payload = [
            "message_type" => "N",
            "message" => $message,
            "recipient" => [$recipient],
            "sender" => self::SENDER_NUM,
            // "scheduled_delivery_time" => "20161223101010",
            // "order_id" => self::ORDER_ID,
            // "returnCredits" => 'true'
        ];
          
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://smspanel.aruba.it/API/v1.0/REST/sms');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-type: application/json',
            'user_key: '.$this->user_key,
            // Use this when using session key authentication
            'session_key: '.$this->session_key,
            // When using Access Token authentication, use this instead:
            // 'Access_token: UserParam{access_token}'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));
        $response = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        
        if ($info['http_code'] != 201) {
            echo('Error! http code: ' . $info['http_code'] . ', body message: ' . $response);
            // die();
        } else {
            $obj = json_decode($response);
            echo '<pre>';
            print_r($obj);
            echo '</pre>';
            // die();
        }
    }

}
?>