<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once './application/libraries/PHPMailer/Exception.php';
require_once './application/libraries/PHPMailer/PHPMailer.php';
require_once './application/libraries/PHPMailer/SMTP.php';

class Schedule extends CI_Controller {
	
	const F_DOC_SCHEDULE = './tosend.csv';
	const F_MESS_SCHEDULE = './tosend_messages.csv';
	const F_POLL_SCHEDULE = './tosend_polls.csv';
	const NUM_FROM_SCHEDULE = 5;
	
	// https://doc.assistenza2000.it/schedule/send
	
	public function __construct() {
		parent::__construct();
	}
	
	public function send() {
		# 1. INVIO NOTIFICHE DOCUMENTI
		$f = fopen(self::F_DOC_SCHEDULE, "r");
		$new_file_content = "";
		$num_sent = 0;
		$tot_rows = 0;
		$remaining_row = 0;
		while (!feof($f)) {
			$file_row = fgets($f);
			if ($file_row != "") {
				if ($num_sent < 20) {
					$num_sent++;
					list($id_user, $id_doc, $filename, $send_mail, $send_sms) = explode(",", $file_row);
					$this->docs_model->send_doc_notify($id_user, $id_doc, $filename, $send_mail, $send_sms);
				} else {
					$new_file_content .= $file_row;
					$remaining_row++;
				}
				$tot_rows++;
			}
		}
		fclose($f);
		write_file(self::F_DOC_SCHEDULE, $new_file_content);
		
		# 2. INVIO MESSAGGI AGLI UTENTI
		$f = fopen(self::F_MESS_SCHEDULE, "r");
		$new_file_content = "";
		$mess_num_sent = 0;
		$mess_tot_rows = 0;
		$mess_remaining_row = 0;
		while (!feof($f)) {
			$file_row = fgets($f);
			if ($file_row != "") {
				if ($mess_num_sent < 20) {
					$mess_num_sent++;
					list($id_user, $subject, $text, $send_mail, $send_sms, $id_message) = explode(",", $file_row);
					$this->users_model->send_message($id_user, $subject, $text, $send_mail, $send_sms, $id_message);
				} else {
					$new_file_content .= $file_row;
					$mess_remaining_row++;
				}
				$mess_tot_rows++;
			}
		}
		fclose($f);
		write_file(self::F_MESS_SCHEDULE, $new_file_content);

		# 3. INVIO NOTIFICHE SONDAGGI
		$f = fopen(self::F_POLL_SCHEDULE, "r");
		$new_file_content = "";
		$poll_num_sent = 0;
		$poll_tot_rows = 0;
		$poll_remaining_row = 0;
		while (!feof($f)) {
			$file_row = fgets($f);
			if ($file_row != "") {
				if ($poll_num_sent < 20) {
					$poll_num_sent++;
					list($id_user, $id_poll, $title, $send_mail, $send_sms) = explode(",", $file_row);
					$this->polls_model->send_poll_notify($id_user, $id_poll, $title, $send_mail, $send_sms);
				} else {
					$new_file_content .= $file_row;
					$poll_remaining_row++;
				}
				$poll_tot_rows++;
			}
		}
		fclose($f);
		write_file(self::F_POLL_SCHEDULE, $new_file_content);
		
		// INVIO DEL REPORT STATISTICO VIA MAIL
		if ($tot_rows != 0 OR $num_sent != 0 OR $remaining_row != 0 OR $mess_tot_rows != 0 OR $mess_num_sent != 0 OR $mess_remaining_row != 0) {
			$mail = new PHPMailer(true);
			try {
				// $mail->SMTPDebug = 2;
				$mail->isSMTP();
				$mail->Host = 'smtps.aruba.it';
				$mail->CharSet = 'UTF-8';
				$mail->SMTPAuth = true;
				$mail->Username = 'doc-assistenza2000@gruppoyuma.it';
				$mail->Password = 'DOC.lcic$';
				$mail->SMTPSecure = 'ssl';
				$mail->Port = 465;
				$mail->setFrom('info@assistenza2000.it', 'Assistenza 2000');
				$mail->addAddress('dibbe84@gmail.com', 'ADMIN');
				$mail->isHTML(true);
				$mail->Subject = 'Report automatico invii alert schedulati';
				$mail->Body = '<img src="http://www.assistenza2000.it/images/logo.jpg"><br>
										<h1 style="font-size: 20px;">REPORT '.date("d/m/Y").' ORE '.date("H:i").'</h1>
										<table>
										<tr><td style="background: #2A317F; padding: 5px 10px; color: #fff;">Totale notifiche da inviare</td><td style="background: #eee; padding: 5px 10px;">'.$tot_rows.'</td></tr>
										<tr><td style="background: #2A317F; padding: 5px 10px; color: #fff;">Notifiche inviate</td><td style="background: #eee; padding: 5px 10px;">'.$num_sent.'</td></tr>
										<tr><td style="background: #2A317F; padding: 5px 10px; color: #fff;">Notifiche rimaste da inviare</td><td style="background: #eee; padding: 5px 10px;">'.$remaining_row.'</td></tr>
										</table>
										<br>
										<table>
										<tr><td style="background: #7c2a74; padding: 5px 10px; color: #fff;">Totale messaggi da inviare</td><td style="background: #eee; padding: 5px 10px;">'.$mess_tot_rows.'</td></tr>
										<tr><td style="background: #7c2a74; padding: 5px 10px; color: #fff;">Messaggi inviati</td><td style="background: #eee; padding: 5px 10px;">'.$mess_num_sent.'</td></tr>
										<tr><td style="background: #7c2a74; padding: 5px 10px; color: #fff;">Messaggi rimasti da inviare</td><td style="background: #eee; padding: 5px 10px;">'.$mess_remaining_row.'</td></tr>
										</table>';
				$mail->send();
			} catch (Exception $e) {
				echo 'Message could not be sent.';
				echo 'Mailer Error: ' . $mail->ErrorInfo;
			}
		}
	}

	// https://doc.assistenza2000.it/schedule/remind_vote
	public function remind_vote() {
		if (date("H") == "07") {	# faccio questo task solo una volta al giorno all'ora che scelgo in questo IF
			$date_end_tomorrow = date('Y-m-d', strtotime("+1 day"));
			$polls = $this->polls_model->get_polls(null, $date_end_tomorrow);
			$reminder_to_sent = array();
			foreach ($polls as $poll)
				foreach ($poll['users'] as $id_user => $usr_info)
					if (!$usr_info['date_vote']) # se non ha ancora votato per il sondaggio in questione
						$reminder_to_sent[] = array(
							'id_user' => $id_user,
							'id_poll' => $poll['id'],
							'title' => $poll['title'],
							'alert_mail' => $poll['alert_mail'],
							'alert_sms' => $poll['alert_sms']
						);
			foreach ($reminder_to_sent as $reminder_item) {
				if (count($reminder_to_sent) <= self::NUM_FROM_SCHEDULE) {
					$this->polls_model->send_poll_notify($reminder_item['id_user'], $reminder_item['id_poll'], $reminder_item['title'], $reminder_item['alert_mail'], $reminder_item['alert_sms']);
				} else {
					$this->polls_model->schedule_poll_notify($reminder_item['id_user'], $reminder_item['id_poll'], $reminder_item['title'], $reminder_item['alert_mail'], $reminder_item['alert_sms']);
				}
			}
		}
	}

}
