<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once('App.php');
class Ajax extends App {
	
	public function __construct() {
		parent::__construct();
		
		if (!$info = $this->is_logged())
			redirect('/');
	}

    public function getCitiesByService() {
        if ($this->input->post()) {
            $posted = $this->input->post(NULL, TRUE);
            list($service_id, $service_name) = explode('|', $posted['id_service']);
            $users = $this->users_model->get_users('user', ['id_service' => $service_id]);  // mi prendo tutti gli utenti collegati al servizio scelto
            $user_ids = [];
            foreach ($users as $user)   // per ogni utente
                array_push($user_ids, $user['id_user']);    // mi salvo solo l'id
            
            $areas = $this->users_model->getAreasByUserIds($user_ids);
            echo json_encode($areas);
            die();
        }
        return false;
    }

    public function getJobsByLevel() {
        if ($this->input->post()) {
            $posted = $this->input->post(NULL, TRUE);
            list($level_id, $level_name) = explode('|', $posted['id_level']);
            $users = $this->users_model->get_users('user', ['id_level' => $level_id]);  // mi prendo tutti gli utenti collegati al livello scelto
            $user_ids = [];
            foreach ($users as $user)   // per ogni utente
                array_push($user_ids, $user['id_user']);    // mi salvo solo l'id
            
            $jobs = $this->users_model->getJobsByUserIds($user_ids);
            echo json_encode($jobs);
            die();
        }
        return false;
    }

    private function setSessionValue(array $keys, $value) {
        $sessionRef =& $_SESSION;
        foreach ($keys as $key) {
            if (!isset($sessionRef[$key]))
                $sessionRef[$key] = [];
            $sessionRef =& $sessionRef[$key];
        }
        $sessionRef = $value;
    }

    private function unsetSessionValue(array $keys) {
        $sessionRef =& $_SESSION;
        // Traverse the array until the second-to-last key
        foreach ($keys as $i => $key) {
            if (isset($sessionRef[$key])) {
                if ($i === count($keys) - 1)
                    unset($sessionRef[$key]);   // Unset the value at the last key
                else
                    $sessionRef =& $sessionRef[$key];
            } else
                return;
        }
    }    

    public function setSessionData() {
        if ($this->input->post()) {
            $posted = $this->input->post(NULL, TRUE);
            echo '<pre>';
            print_r($posted);
            echo '</pre>';
            if (!strpos($posted['key'], '|')) {
                $_SESSION[$posted['key']] = $posted['val'];
            } else {
                $ar_keys = explode("|", $posted['key']);
                $this::setSessionValue($ar_keys, $posted['val']);
            }

            echo '<pre style="background: red; padding: 20px; color: #fff; font-weight: bold;">';
            print_r($_SESSION);
            echo '</pre>';
        }
        return false;
    }

    public function delSessionData() {
        if ($this->input->post()) {
            $posted = $this->input->post(NULL, TRUE);
            echo '<pre>';
            print_r($posted);
            echo '</pre>';
            if (!strpos($posted['key'], '|')) {
                unset($_SESSION[$posted['key']]);
            } else {
                $ar_keys = explode("|", $posted['key']);
                $this::unsetSessionValue($ar_keys);
            }

            echo '<pre style="background: red; padding: 20px; color: #fff; font-weight: bold;">';
            print_r($_SESSION);
            echo '</pre>';
        }
        return false;
    }

}