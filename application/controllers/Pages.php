<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once('App.php');
class Pages extends App {

	public function __construct() {
		parent::__construct();
		$this->load->model('users_model');
		if (!in_array('logout', $this->uri->segment_array()) AND $info = $this->is_logged() AND $info['role'] == 'admin') {
			redirect('/admin');
		}
	}

	public function index() {		
		# MECCANISMO DI THROTTLING
		if (!$this->users_model->can_login($_SERVER['REMOTE_ADDR'])) {
			$this->load->view("block");
		} else {
			if ($info_user = $this->is_logged()) {
				redirect('/profile');
			} else {
				$data_view['logged'] = 'no';
				$this->render('homepage', NULL, $data_view);
			}
		}
	}
	
	public function profile() {
		if ($info_user = $this->is_logged()) {
			if ($this->users_model->check_psw_age($info_user['id'])) {
				$data_view['info'] = $this->users_model->get_user_info($info_user['id']);
				$doc_cats = $this->cats_model->get_cats();
				foreach ($doc_cats as $cod => $cat) {
					$data_view['docs'][$cat] = $this->docs_model->get_docs($info_user['id'], NULL, NULL, $cod);
				}
				$data_view['polls'] = $this->polls_model->get_polls($info_user['id']);
			} else {
				$data_view['alert'] = array('psw_old' => TRUE);
			}
			$this->render('profile', NULL, $data_view);
		} else {
			redirect('/');
		}
	}

	public function vote($id_poll) {
		if ($info_user = $this->is_logged()) {
			if ($this->input->post()) {
				$posted = $this->input->post(NULL, TRUE);
				$this->polls_model->save_vote($posted);
				redirect('/profile');
			} else {
				$data_view['info'] = $this->users_model->get_user_info($info_user['id']);
				$data_view['poll'] = $this->polls_model->get_poll($id_poll);
				# devo verificare se l'utente è associato al questionario
				if (array_key_exists($info_user['id'], $data_view['poll']['users']))
					$this->render('vote', NULL, $data_view);
				else
					redirect('/profile');
			}
		} else {
			redirect('/');
		}
	}

	public function serve_poll_doc($req_string) {
		if ($info_user = $this->is_logged()) {
			$req_string = base64_decode(urldecode($req_string));
			list($id_poll, $filename) = explode("|", $req_string);
			$this->load->helper('download');
			force_download('./assets/docs/'.$filename, NULL);
		} else {
			redirect('/');
		}
	}
	
	public function download($encoded_id_doc) {
		if ($info_user = $this->is_logged()) {
			$id_doc = explode("_", base64_decode($encoded_id_doc))[0];
			if ($doc_info = $this->docs_model->user_has_doc($info_user['id'], $id_doc)) {
				if ($this->input->post()) {
					$posted = $this->input->post(NULL, TRUE);
					if ($posted['acceptance'] == 'on') {
						$this->log('DWN_DOC', $doc_info['id_doc']);
						$this->docs_model->set_download($info_user['id'], $doc_info['id_doc']);
						$this->load->helper('download');
						force_download('./assets/docs/'.$doc_info['filename'], NULL);
					}
				}
				$data_view['doc_info'] = $doc_info;
				$data_view['id_user'] = $info_user['id'];
				$this->render('download', NULL, $data_view);
			} else {
				redirect('/profile');
			}
		} else {
			redirect('/');
		}
	}
	
	public function change_user_access() {
		if ($info_user = $this->is_logged()) {
			if ($this->input->post()) {
				$posted = $this->input->post(NULL, TRUE);
				$this->users_model->change_access($posted);
				$this->log('PSW_USR', $posted['id_user']);
				redirect('/profile');
			} elseif ($this->is_logged()) {
				$data_view['info'] = $this->users_model->get_user_info($info_user['id']);
				$this->render('form_user_access', NULL, $data_view);
			} else {
				redirect('/profile');
			}
		}
	}
	
}

/*
VECCHIO CODICE CARICAMENTO E ASSOCIAZIONE MANSIONI

$ar_jobs = array();
$f = fopen("./mansioni.csv", 'r');
while (!feof($f)) {
	$row = fgets($f);
	list($cognome, $nome, $mansione) = explode(";", $row);
	$mansione = ucfirst(strtolower($mansione));
	if (!in_array($mansione, $ar_jobs)) {
		array_push($ar_jobs, $mansione);
		$this->db->insert("jobs", array('name' => $mansione));
	}
}
fclose($f);

$f = fopen("./mansioni.csv", 'r');
while (!feof($f)) {
	$row = fgets($f);
	list($cognome, $nome, $mansione) = explode(";", $row);
	$result = $this->db->from("users_info")->where("first_name = '".$nome."' AND last_name = '".$cognome."'")->get();
	if ($result AND $result->num_rows()) {
		$result = $result->row_array();
		$id_user = $result['id_user'];
	}
	else
		$id_user = null;
	$result = $this->db->from("jobs")->where("name = '".$mansione."'")->get();
	if ($result AND $result->num_rows()) {
		$result = $result->row_array();
		$id_job = $result['id'];
	}
	if ($id_user AND $id_job) {
		echo "<p style='color: green;'>".$cognome." | ".$nome." | ".$id_user." | ".$mansione." | ".$id_job."</p>";
		$this->db->insert("jobs_users", array('id_user' => $id_user, 'id_job' => $id_job));
	} else {
		echo "<p style='color: red;'>".$cognome." | ".$nome." | ".$id_user." | ".$mansione." | ".$id_job."</p>";
	}
	
	echo '<pre>';
	print_r($result);
	echo '</pre>';
}
fclose($f);

die();
*/
