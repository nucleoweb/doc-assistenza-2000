<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App extends CI_Controller {
	
	const SESS_VAR_NAME = '2G67423NCG4GHVIN';
	
	private function verifyReCaptcha($recaptchaCode) {
		$postdata = http_build_query(["secret"=>"6LfaPdMUAAAAAOW1-1Neh1hzHAf2fCim2EFPlw__","response"=>$recaptchaCode]);
		$opts = ['http' =>
			[
				'method'  => 'POST',
				'header'  => 'Content-type: application/x-www-form-urlencoded',
				'content' => $postdata
			]
		];
		$context  = stream_context_create($opts);
		$result = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
		$check = json_decode($result);
		// echo '<pre>';
		// print_r($check);
		// echo '</pre>';
		return $check->success;
	}
	
	public function __construct() {
		parent::__construct();
	}
	
	protected function log($action, $id_resource = NULL) {
		if ($info_user = $this->is_logged() OR $action == "TIN_USR") {
			$fields = array(
				'id_user' => (isset($info_user['id']) ? $info_user['id'] : '000'),
				'action' => $action,
				'id_resource' => $id_resource,
				'date_action' => date("Y-m-d H:i:s"),
				'ip_from' => $_SERVER['REMOTE_ADDR']
			);
			$log_file = fopen("./assets/security_log.txt", 'a');
			fwrite($log_file, implode(";", $fields)."\n");
			fclose($log_file);
			if ($action == "TIN_USR")	// loggo su DB solo i tentativi di accesso perché mi servono per evitare il brute force
				$this->db->insert('logs', $fields);
		} else {
			redirect('/');
		}
	}
	
	protected function render($page, $namespace = NULL, $data_view = NULL) {
		$content = $this->load->view((($namespace) ? $namespace.'/' : '').$page, $data_view, TRUE);
		$logged = $this->is_logged();
		$fields = array(
			'logged_info' => @$this->users_model->get_user_info($logged['id']),
			'content' => $content
		);
		$this->load->view('template', $fields);
	}
	
	public function login() {
		if (!$this->users_model->can_login($_SERVER['REMOTE_ADDR'])) {
			$this->load->view("block");
		} else {
			if ($this->input->post()) {		
				$this->log('TIN_USR', NULL);
				$posted = $this->input->post();
				$posted = $this->security->xss_clean($posted);
				
				if (!empty($posted['captcha_token']) AND $this->verifyReCaptcha($posted['captcha_token'])) {
					if ($user_info = $this->users_model->login($posted['usr'], $posted['psw'])) {
						$_SESSION[self::SESS_VAR_NAME] = base64_encode($user_info['id_user'].'_'.$user_info['role'].'_'.time());
						$this->log('LOG_USR', $user_info['id_user']);
						if ($user_info['role'] == 'admin')
							redirect('/admin');
						else
							redirect('/profile');
					}
				}
			}
			redirect('/');
		}
	}
	
	public function logout() {
		$user_info = base64_decode($_SESSION[self::SESS_VAR_NAME]);
		list($id_user, $role, $login_time) = explode("_", $user_info);
		$this->log('ESC_USR', $id_user);
		unset($_SESSION[self::SESS_VAR_NAME]);
		redirect('/');
	}

	protected function is_logged() {
		if (!empty($_SESSION[self::SESS_VAR_NAME])) {
			$sess_data = base64_decode($this->session->userdata(self::SESS_VAR_NAME));
			$sess_info = explode("_", $sess_data);
			$info = array(
				'id' => $sess_info[0],
				'role' => $sess_info[1],
			);
			return $info;
		}
		return FALSE;
	}
	
}
