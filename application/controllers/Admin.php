<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once('./lib/dompdf/autoload.inc.php');
use Dompdf\Dompdf;
use Dompdf\Options;

require_once './lib/fpdf/fpdf.php'; // Carica FPDF
require_once './lib/fpdi/autoload.php'; // Poi FPDI
use setasign\Fpdi\Fpdi;

include_once('App.php');
class Admin extends App {
	
	const F_DOC_FTP = './pdf_upload/';
	const PAYSHEET_PDF_DIR = './upload_paysheet/';
	const IMPORT_DIR = './assets/import/';
	
	// const FILE_UPLOAD_ERRORS = array(
		// 0 => 'There is no error, the file uploaded with success',
		// 1 => 'The uploaded file exceeds the upload_max_filesize directive in php.ini',
		// 2 => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
		// 3 => 'The uploaded file was only partially uploaded',
		// 4 => 'No file was uploaded',
		// 6 => 'Missing a temporary folder',
		// 7 => 'Failed to write file to disk.',
		// 8 => 'A PHP extension stopped the file upload.'
	// );
	// 
	public function __construct() {
		parent::__construct();
		$config['upload_path'] = './assets/docs/';
		$config['allowed_types'] = 'doc|docx|pdf|xls|xlsx|odt|zip|7z|rar|ppt|txt|csv';
		$config['max_size'] = '20480';
		$this->load->library('upload', $config);
		
		if (!$info = $this->is_logged())
			redirect('/');
		if (!in_array('logout', $this->uri->segment_array()) AND $info = $this->is_logged() AND $info['role'] == 'user')
			redirect('/profile');
	}
	
	public function index() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$data_view['docs'] = $this->docs_model->get_docs(NULL, $posted['year'], $posted['month'], $posted['cat']);
			$data_view['year'] = $posted['year'];
			$data_view['month'] = $posted['month'];
			$data_view['cat'] = $posted['cat'];
		} else {
			$data_view['docs'] = $this->docs_model->get_docs();
			$data_view['year'] = date("Y");
			$data_view['month'] = date("n");
		}
		$data_view['years'] = $this->docs_model->get_years();
		$data_view['month_names'] = $this->config->item('month_names');
		$data_view['doc_cats'] = $this->cats_model->get_cats();	
		$data_view['users'] = $this->users_model->get_users();
		$data_view['jobs'] = $this->jobs_model->get_jobs(TRUE);
		$data_view['areas'] = $this->areas_model->get_areas(TRUE);
		$data_view['services'] = $this->services_model->get_services(TRUE);
		$data_view['customers'] = $this->customers_model->get_customers(TRUE);
		$data_view['levels'] = $this->levels_model->get_levels(TRUE);
		$this->render('homepage', 'admin', $data_view);
	}
	
	### DOCS

	private function clean_folder($folderPath) {
		if (is_dir($folderPath)) { // Ensure the folder exists
			$files = glob($folderPath . '/*'); // Get all files and folders in the folder
			foreach ($files as $file) {
				if (is_file($file)) {
					unlink($file); // Delete the file
				} elseif (is_dir($file)) {
					// Recursively delete subdirectories and their contents
					$this->clean_folder($file);
					rmdir($file); // Remove the empty directory
				}
			}
		}
	}

	public function upload_paysheets() {
		$this->clean_folder(self::PAYSHEET_PDF_DIR);

		if ($this->input->post()) {
			$config['upload_path'] = self::PAYSHEET_PDF_DIR.date('Ymd_Hi').'/';
			$config['allowed_types'] = 'pdf';
			$config['max_size'] = '40960';
			// $this->load->library('upload', $config);
    		$this->upload->initialize($config);

			if (!is_dir($config['upload_path']))
				mkdir($config['upload_path'], 0777, true);

			$posted = $this->input->post(NULL, TRUE);

			if (!empty($_FILES['pdf_paysheets']['name'])) {
				if ($this->upload->do_upload('pdf_paysheets')) {
					$upload_data = $this->upload->data();
					
					$full_path = $upload_data['full_path'];
					$filename = $upload_data['file_name'];

					include('./lib/pdfparser/alt_autoload.php-dist');
					$parser = new \Smalot\PdfParser\Parser();
					$pdf = $parser->parseFile($full_path);
					$pages = $pdf->getPages();

					$persons = [];

					foreach ($pages as $i => $page) {
						$page_text = $pdf->getPages()[$i]->getText();
						// echo '<hr>'.$page_text;
						$fiscal_code = preg_match('/[A-Z]{6}[0-9]{2}[A-Z]{1}[0-9]{2}[A-Z]{1}[0-9]{3}[A-Z]{1}/', $page_text, $matches);
						if ($fiscal_code === 0) // non è stato trovato alcun codice fiscale nella pagina
							continue;
						$fiscal_code = $matches[0];
						// $fiscal_code = 'DBNNDR84R02H769W'; // DEBUG
						// echo '<pre style="background: #eee; padding: 10px;">'.$fiscal_code.'</pre>';

						$period = preg_match('/(gennaio|febbraio|marzo|aprile|maggio|giugno|luglio|agosto|settembre|ottobre|novembre|dicembre)\s\d{4}/i', $page_text, $matches);
						$period = str_replace(' ', '_', $matches[0]);
						// echo '<pre style="background: #eee; padding: 10px;">'.$period.'</pre>';

						$ar_key = $fiscal_code.'_'.strtoupper($period);

						if (!isset($persons[$ar_key]))
							$persons[$ar_key] = [$i];
						else
							$persons[$ar_key][] = $i;
					}
					
					if (!count($persons)) { // non è stato trovato alcun codice fiscale in nessuna pagina
						$error = 'Nessun codice fiscale trovato nei documenti caricati';
						die();
					} else {
						$this->clean_folder(self::F_DOC_FTP);

						try {
							foreach ($persons as $ar_key => $page_indexes) {
								list($fcode, $month, $year) = explode('_', $ar_key);

								$u = $this->users_model->get_by_fiscode($fcode);

								$pdf = new Fpdi();
								$pdf->setSourceFile($full_path);
								
								foreach ($page_indexes as $p) {
									$p++; // gli indici sono 0-based mentre le pagine del PDF vengono considerate 1-based
									$page_tpl = $pdf->importPage($p);
									$pdf->AddPage();
									$pdf->useTemplate($page_tpl);
								}

								$pdf->Output(self::F_DOC_FTP.$fcode."_".strtoupper($u['last_name'])."_".strtoupper($u['first_name'])."_Buste_paga.pdf", 'F');
							}
							redirect('/admin/fetch_doc');
						} catch (Exception $e) {
							$error = $e->getMessage();
							echo '<pre style="background: #f00; color: #fff; padding: 10px; font-weight: bold; font-size: 18px;">';
							print_r($error);
							echo '</pre>';
							echo '<a style="background: #2A317F; color: #fff; font-weight: 600; padding: 10px; text-decoration: none; text-transform: uppercase; font-family: Verdana;" href="/admin/upload_paysheets">Riprova</a>';
							die();
						}
					}

					die();

				} else {
					$error = $this->upload->display_errors();
					echo '<pre>';
					print_r($error);
					echo '</pre>';
					die();
				}
			}
		} else {
			$this->render('form_paysheet', 'admin', []);
		}
	}
	
	public function fetch_doc() {
		if ($this->input->post()) {		// devo eseguire l'operazione di assegnazione documenti
			$this->log('FTC_DOC', null);
			$posted = $this->input->post(NULL, TRUE);	
			$ar_result = array();
			$dir = scandir(self::F_DOC_FTP);
			$schedule = ((count($dir) <= 20) ? FALSE : TRUE);
			foreach ($dir as $f) {
				$tmp_result = array();
				if ($f != '.' AND $f != '..') {
					preg_match('/^([A-Za-z0-9]{12,20})\_(.+)$/', $f, $matches);
					$cod_fiscale = $matches[1];
					$tmp_result['filename'] = $f;
					if ($user_found = $this->users_model->get_by_fiscode($cod_fiscale)) {
						$tmp_result['user_name'] = $user_found['first_name'].' '.$user_found['last_name'];
						$tmp_result['user_id'] = $user_found['id_user'];
						if ($id_doc = $this->docs_model->assoc_doc($f, $user_found['id_user'], $posted, $schedule)) {
							$this->log('ASS_DOC', $id_doc);
							$tmp_result['response'] = 'OK';
						} else {
							$tmp_result['response'] = 'KO';
						}
					} else {
						$tmp_result['user_name'] = 'NULL';
						$tmp_result['user_id'] = 'NULL';
						$tmp_result['response'] = 'KO';
					}
					$ar_result[] = $tmp_result;
				}
			}
			$data_view['result'] = $ar_result;
			$this->render('fetch_report', 'admin', $data_view);
		} else {		// faccio vedere la schermata per iniziare l'operazione
			$ar_result = array();
			$dir = scandir(self::F_DOC_FTP);
			foreach ($dir as $f) {
				$tmp_result = array();
				if ($f != '.' AND $f != '..') {
					preg_match('/^([A-Za-z0-9]{12,20})\_(.+)$/', $f, $matches);
					$cod_fiscale = $matches[1];
					$tmp_result['filename'] = $f;
					$tmp_result['link'] = $this->config->item('base_url').str_replace('./', '', self::F_DOC_FTP).$f;
					if ($user_found = $this->users_model->get_by_fiscode($cod_fiscale)) {
						$tmp_result['user_name'] = $user_found['first_name'].' '.$user_found['last_name'];
						$tmp_result['user_id'] = $user_found['id_user'];
					} else {
						$tmp_result['user_name'] = 'NULL';
						$tmp_result['user_id'] = 'NULL';
					}
				$ar_result[] = $tmp_result;
				}
			}
			$data_view['docs_to_assoc'] = $ar_result;
			$data_view['doc_cats'] = $this->cats_model->get_cats();
			$this->render('fetch_doc', 'admin', $data_view);
		}
	}
	
	public function deassoc_doc() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			if ($this->docs_model->deassoc_doc($posted['from'], $posted['to'])) {
				$this->log('DEA_DOC', null);
				redirect('/admin');
			}
		} else {
			$data_view = array();
			$this->render('deassoc_doc', 'admin', $data_view);
		}
	}
	
	public function insert_doc() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);		// qui per ogni campo ho un array di valori
			$num_files = count($_FILES['filedoc']['name']);
			for ($i = 0; $i < $num_files; $i++) {
				if (!empty($_FILES['filedoc']['name'][$i])) {
					$_FILES['file']['name'] = $_FILES['filedoc']['name'][$i];
					$_FILES['file']['type'] = $_FILES['filedoc']['type'][$i];
					$_FILES['file']['tmp_name'] = $_FILES['filedoc']['tmp_name'][$i];
					$_FILES['file']['error'] = $_FILES['filedoc']['error'][$i];
					$_FILES['file']['size'] = $_FILES['filedoc']['size'][$i];

					if ($this->upload->do_upload('file')) {
						$upload_data = $this->upload->data();
						$single_file = array(
							"name" => $posted['name'][$i],
							"cat" => $posted['cat'][$i],
							"note" => $posted['note'][$i],
							"filename" => $upload_data['file_name'],
							"users" => $posted['users'],
							"alert_mail" => $posted['alert_mail'],
							"alert_sms" => $posted['alert_sms'],
						);
						$id_doc = $this->docs_model->save_doc($single_file);
						$this->log('INS_DOC', $id_doc);
					} else {
						$error = $this->upload->display_errors();
						echo '<pre>';
						print_r($error);
						echo '</pre>';
						die();
					}
				}
			}
			redirect('admin');
			/*
			if ($this->upload->do_upload('filedoc')) {
				$upload_data = $this->upload->data();
				$posted['filename'] = $upload_data['file_name'];				
				$id_doc = $this->docs_model->save_doc($posted);
				$this->log('INS_DOC', $id_doc);
				redirect('admin');
			} else {
				$error = $this->upload->display_errors();
				echo '<pre>';
				print_r($error);
				echo '</pre>';
				die();
			}
			*/
		} else {
			$data_view['users'] = $this->users_model->get_users('user');
			$data_view['jobs'] = $this->jobs_model->get_jobs(TRUE);
			$data_view['doc_cats'] = $this->cats_model->get_cats();
			$data_view['areas'] = $this->areas_model->get_areas(TRUE);
			$data_view['services'] = $this->services_model->get_services(TRUE);
			$data_view['customers'] = $this->customers_model->get_customers(TRUE);
			$data_view['levels'] = $this->levels_model->get_levels(TRUE);
			$this->render('form_doc', 'admin', $data_view);
		}
	}
	
	public function edit_doc($id = NULL) {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			if ($_FILES['filedoc']['error'] != 4) {
				if ($_FILES['filedoc']['error'] == 0) {
					if ($this->upload->do_upload('filedoc')) {
						$upload_data = $this->upload->data();
						$posted['filename'] = $upload_data['file_name'];
					} else {
						$error = $this->upload->display_errors();
						echo '<pre>';
						print_r($error);
						echo '</pre>';
						die();
					}
				} else {
					echo self::FILE_UPLOAD_ERRORS[$_FILES['filedoc']['error']];
					die();
				}
			}
			$this->docs_model->update_doc($posted);
			$this->log('UPD_DOC', $posted['id_doc']);
			redirect('/admin');
		} elseif (!empty($id)) {
			$data_view['users'] = $this->users_model->get_users('user');
			$data_view['jobs'] = $this->jobs_model->get_jobs(TRUE);
			$data_view['info'] = $this->docs_model->get_doc($id);
			$data_view['areas'] = $this->areas_model->get_areas(TRUE);
			$data_view['services'] = $this->services_model->get_services(TRUE);
			$data_view['customers'] = $this->customers_model->get_customers(TRUE);
			$data_view['levels'] = $this->levels_model->get_levels(TRUE);
			$data_view['doc_cats'] = $this->cats_model->get_cats();
			$this->render('form_doc', 'admin', $data_view);
		} else {
			redirect('/admin');
		}
	}

	public function print_doc_info($id) {
		$data_view['docinfo'] = $this->docs_model->get_doc($id, TRUE);
		$this->load->view('admin/print_doc_info', $data_view);
	}

	public function save_doc_info($id) {
		$data_view['docinfo'] = $this->docs_model->get_doc($id, TRUE);		
		$output = $this->load->view('admin/save_doc_info', $data_view, TRUE);

		header('Access-Control-Allow-Origin: *'); //to get data from firefox addon
		header('Content-Type: application/pdf');
		header('Content-Disposition: attachment');
		// header('filename: file.pdf');

		$options = new Options();
		$options->set('isRemoteEnabled', true);
		$options->setIsFontSubsettingEnabled(true);
		$dompdf = new Dompdf($options);
		$dompdf->loadHtml($output);
		$dompdf->setPaper('A4', 'portrait');
		$dompdf->render();
		$dompdf->stream("DOCAss2000_".$data_view['docinfo']['id_doc']."_".str_replace('.pdf', '', $data_view['docinfo']['filename'])."_fileinfo.pdf");
		die();
		
	}
	
	public function serve_doc($req_string) {
		$req_string = base64_decode(urldecode($req_string));
		list($id_doc, $filename) = explode("|", $req_string);
		$this->load->helper('download');
		force_download('./assets/docs/'.$filename, NULL);
	}
	
	public function delete_doc($id) {
		$this->docs_model->delete_doc($id);
		$this->log('DEL_DOC', $id);
		redirect('/admin');
	}
	
	### MESSAGES
	
	public function write_to_users() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$this->users_model->send_messages($posted);
			$this->log('SND_MSG', NULL);
			redirect('/admin');
		} else {
			$data_view['users'] = $this->users_model->get_users('user');
			$data_view['jobs'] = $this->jobs_model->get_jobs(TRUE);
			$data_view['areas'] = $this->areas_model->get_areas(TRUE);
			$data_view['services'] = $this->services_model->get_services(TRUE);
			$data_view['customers'] = $this->customers_model->get_customers(TRUE);
			$data_view['levels'] = $this->levels_model->get_levels(TRUE);
			$data_view['sms_left'] = $this->users_model->get_sms_left();
			$this->render('write_to_users', 'admin', $data_view);
		}
	}
	
	public function list_messages() {
		$data_view['messages'] = $this->users_model->list_messages(TRUE);
		$this->render('list_messages', 'admin', $data_view);
	}
	
	public function del_mess($id) {
		$this->users_model->del_message($id);
		redirect('/admin/list_messages');
	}
	
	### USERS
	
	public function insert_user() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$new_user_id = $this->users_model->save_user($posted);
			$this->log('INS_USR', $new_user_id);
			redirect('/admin');
		} else {
			$data_view['jobs'] = $this->jobs_model->get_jobs(TRUE);
			$data_view['areas'] = $this->areas_model->get_areas(TRUE);
			$data_view['customers'] = $this->customers_model->get_customers(TRUE);
			$data_view['levels'] = $this->levels_model->get_levels(TRUE);
			$data_view['services'] = $this->services_model->get_services(TRUE);
			$this->render('form_user', 'admin', $data_view);
		}
	}
	
	public function edit_user($id = NULL) {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$this->users_model->update_user($posted);
			$this->log('UPD_USR', $posted['id_user']);
			redirect('/admin');
		} elseif (!empty($id)) {
			$data_view['info'] = $this->users_model->get_user_info($id);
			if ($data_view['info']) {
				$data_view['docs'] = $this->docs_model->get_docs($id);
				$data_view['jobs'] = $this->jobs_model->get_jobs(TRUE);
				$data_view['areas'] = $this->areas_model->get_areas(TRUE);
				$data_view['customers'] = $this->customers_model->get_customers(TRUE);
				$data_view['levels'] = $this->levels_model->get_levels(TRUE);
				$data_view['services'] = $this->services_model->get_services(TRUE);
				$this->render('form_user', 'admin', $data_view);
			} else {
				echo '<p>Utente non trovato.<br><a href="/admin">Torna alla homepage</a<</p>';
			}
		} else {
			redirect('/admin');
		}
	}
	
	public function change_user_access($id = NULL) {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$this->users_model->change_access($posted);
			$this->log('PSW_USR', $posted['id_user']);
			redirect('/admin/edit_user/'.$posted['id_user']);
		} elseif (!empty($id)) {
			$data_view['info'] = $this->users_model->get_user_info($id);
			$this->render('form_user_access', 'admin', $data_view);
		} else {
			redirect('/admin');
		}
	}
	
	public function delete_user($id) {
		$this->users_model->delete_user($id);
		$this->log('DEL_USR', $id);
		redirect('/admin');
	}

	public function report() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$data_view['data'] = $this->reports_model->get_data($posted);
			$this->render('/report/'.$posted['type'], 'admin', $data_view);
		} else {
			$this->render('/report/select', 'admin');
		}
	}

	public function calc_serv_months() {
		$data_view = null;
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$data_view = [
				'date_start' => $posted['date_start'],
				'date_end' => $posted['date_end'],
				'serv_months' => $this->users_model->calc_serv_months($posted['date_start'], $posted['date_end'])
			];
		}
		$this->render('/calc_serv_months', 'admin', $data_view);
	}

	public function goToPlatform() {
		$login_page_content = file_get_contents("https://www.assistenza2000.com/admin/login.php");
		preg_match('/input type="hidden" name="token" value="(.*)"/', $login_page_content, $matches);
		if (!empty($matches[1])) {
			$data_view['token'] = $matches[1];
			$this->render('/goToPlatform', 'admin', $data_view);
		} else {
			die("Impossibile recuperare il token di accesso ECM");
		}
	}
	
	### CATEGORIES
	
	public function view_cats() {
		$data_view['cats'] = $this->cats_model->get_cats(TRUE);
		$this->render('cats_list', 'admin', $data_view);
	}
	
	public function insert_cat() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$id_cat = $this->cats_model->insert_cat($posted);
			$this->log('INS_CAT', $id_cat);
		}
		redirect('/admin/view_cats');
	}
	
	public function del_cat() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$this->cats_model->del_cat($posted);
			$this->log('DEL_CAT', $posted['id_cat_to_del']);
		}
		redirect('/admin/view_cats');
	}
	
	public function edit_cat($id = NULL) {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$this->cats_model->update_cat($posted);
			$this->log('UPD_CAT', $posted['id_cat']);
			redirect('/admin/view_cats');
		} elseif (!empty($id)) {
			$data_view['cat'] = $this->cats_model->get_cat($id);
			$this->render('form_cat', 'admin', $data_view);
		} else {
			redirect('/admin/view_cats');
		}
	}
	
	### JOBS
	
	public function view_jobs() {
		$data_view['jobs'] = $this->jobs_model->get_jobs(TRUE);
		$this->render('jobs_list', 'admin', $data_view);
	}
	
	public function insert_job() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$id_job = $this->jobs_model->insert_job($posted);
			$this->log('INS_JOB', $id_job);
		}
		redirect('/admin/view_jobs');
	}
	
	public function del_job() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$this->jobs_model->del_job($posted);
			$this->log('DEL_JOB', $posted['id_job_to_del']);
		}
		redirect('/admin/view_jobs');
	}
	
	public function edit_job($id = NULL) {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$this->jobs_model->update_job($posted);
			$this->log('UPD_JOB', $posted['id_job']);
			redirect('/admin/view_jobs');
		} elseif (!empty($id)) {
			$data_view['job'] = $this->jobs_model->get_job($id);
			$this->render('form_job', 'admin', $data_view);
		} else {
			redirect('/admin/view_jobs');
		}
	}

	### AREAS
	
	public function view_areas() {
		$data_view['areas'] = $this->areas_model->get_areas(TRUE);
		$this->render('areas_list', 'admin', $data_view);
	}
	
	public function insert_area() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$id_area = $this->areas_model->insert_area($posted);
			$this->log('INS_AREA', $id_area);
		}
		redirect('/admin/view_areas');
	}
	
	public function del_area() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$this->areas_model->del_area($posted);
			$this->log('DEL_AREA', $posted['id_area_to_del']);
		}
		redirect('/admin/view_areas');
	}
	
	public function edit_area($id = NULL) {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$this->areas_model->update_area($posted);
			$this->log('UPD_AREA', $posted['id_area']);
			redirect('/admin/view_areas');
		} elseif (!empty($id)) {
			$data_view['area'] = $this->areas_model->get_area($id);
			$this->render('form_area', 'admin', $data_view);
		} else {
			redirect('/admin/view_areas');
		}
	}

	### SERVICES
	
	public function view_services() {
		$data_view['services'] = $this->services_model->get_services(TRUE);
		$this->render('services_list', 'admin', $data_view);
	}
	
	public function insert_service() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$id_service = $this->services_model->insert_service($posted);
			$this->log('INS_SERVICE', $id_service);
		}
		redirect('/admin/view_services');
	}
	
	public function del_service() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$this->services_model->del_service($posted);
			$this->log('DEL_SERVICE', $posted['id_service_to_del']);
		}
		redirect('/admin/view_services');
	}
	
	public function edit_service($id = NULL) {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$this->services_model->update_service($posted);
			$this->log('UPD_SERVICE', $posted['id_service']);
			redirect('/admin/view_services');
		} elseif (!empty($id)) {
			$data_view['service'] = $this->services_model->get_service($id);
			$this->render('form_service', 'admin', $data_view);
		} else {
			redirect('/admin/view_services');
		}
	}

	### CUSTOMERS
	
	public function view_customers() {
		$data_view['customers'] = $this->customers_model->get_customers(TRUE);
		$this->render('customers_list', 'admin', $data_view);
	}
	
	public function insert_customer() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$id_customer = $this->customers_model->insert_customer($posted);
			$this->log('INS_CUSTOMER', $id_customer);
		}
		redirect('/admin/view_customers');
	}
	
	public function del_customer() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$this->customers_model->del_customer($posted);
			$this->log('DEL_CUSTOMER', $posted['id_customer_to_del']);
		}
		redirect('/admin/view_customers');
	}
	
	public function edit_customer($id = NULL) {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$this->customers_model->update_customer($posted);
			$this->log('UPD_CUSTOMER', $posted['id_customer']);
			redirect('/admin/view_customers');
		} elseif (!empty($id)) {
			$data_view['customer'] = $this->customers_model->get_customer($id);
			$this->render('form_customer', 'admin', $data_view);
		} else {
			redirect('/admin/view_customers');
		}
	}

	### LEVELS
	
	public function view_levels() {
		$data_view['levels'] = $this->levels_model->get_levels(TRUE);
		$this->render('levels_list', 'admin', $data_view);
	}
	
	public function insert_level() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$id_level = $this->levels_model->insert_level($posted);
			$this->log('INS_LEVEL', $id_level);
		}
		redirect('/admin/view_levels');
	}
	
	public function del_level() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$this->levels_model->del_level($posted);
			$this->log('DEL_LEVEL', $posted['id_level_to_del']);
		}
		redirect('/admin/view_levels');
	}
	
	public function edit_level($id = NULL) {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$this->levels_model->update_level($posted);
			$this->log('UPD_LEVEL', $posted['id_level']);
			redirect('/admin/view_levels');
		} elseif (!empty($id)) {
			$data_view['level'] = $this->levels_model->get_level($id);
			$this->render('form_level', 'admin', $data_view);
		} else {
			redirect('/admin/view_levels');
		}
	}
	
	### POLLS
	
	public function list_polls() {
		$data_view['polls'] = $this->polls_model->get_polls();
		$this->render('list_polls', 'admin', $data_view);
	}
	
	public function insert_poll() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			if (!empty($_FILES['filedoc']['name'])) {
				if ($this->upload->do_upload('filedoc')) {
					$upload_data = $this->upload->data();
					$posted["filename"] = $upload_data['file_name'];
				} else {
					$error = $this->upload->display_errors();
					echo '<pre>';
					print_r($error);
					echo '</pre>';
					die();
				}
			}
			$new_poll_id = $this->polls_model->save_poll($posted);
			$this->log('INS_POL', $new_poll_id);
			redirect('/admin/list_polls');
		} else {
			$data_view['users'] = $this->users_model->get_users('user');
			$data_view['jobs'] = $this->jobs_model->get_jobs(TRUE);
			$data_view['areas'] = $this->areas_model->get_areas(TRUE);
			$data_view['services'] = $this->services_model->get_services(TRUE);
			$data_view['customers'] = $this->customers_model->get_customers(TRUE);
			$data_view['levels'] = $this->levels_model->get_levels(TRUE);
			$this->render('form_poll', 'admin', $data_view);
		}
	}
	
	public function edit_poll($id = NULL) {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			if ($_FILES['filedoc']['error'] != 4) {
				if ($_FILES['filedoc']['error'] == 0) {
					if ($this->upload->do_upload('filedoc')) {
						$upload_data = $this->upload->data();
						$posted['filename'] = $upload_data['file_name'];
					} else {
						$error = $this->upload->display_errors();
						echo '<pre>';
						print_r($error);
						echo '</pre>';
						die();
					}
				} else {
					echo self::FILE_UPLOAD_ERRORS[$_FILES['filedoc']['error']];
					die();
				}
			}
			$this->polls_model->update_poll($posted);
			$this->log('UPD_POL', $posted['id']);
			redirect('/admin/list_polls');
		} elseif (!empty($id)) {
			$data_view['info'] = $this->polls_model->get_poll($id);
			$data_view['users'] = $this->users_model->get_users('user');
			$data_view['jobs'] = $this->jobs_model->get_jobs(TRUE);
			$data_view['areas'] = $this->areas_model->get_areas(TRUE);
			$data_view['services'] = $this->services_model->get_services(TRUE);
			$data_view['customers'] = $this->customers_model->get_customers(TRUE);
			$data_view['levels'] = $this->levels_model->get_levels(TRUE);
			$this->render('form_poll', 'admin', $data_view);
		} else {
			redirect('/admin/list_polls');
		}
	}

	public function view_poll($id) {
		$data_view['poll'] = $this->polls_model->get_poll($id);
		$this->render('view_poll', 'admin', $data_view);
	}

	public function export_poll($id) {
		$data_view['poll'] = $this->polls_model->get_poll($id);
		$output = $this->load->view('admin/export_poll', $data_view, true);
		// echo $output;
		$this->log('EXP_POL', $id);

		$options = new Options();
		$options->set('isRemoteEnabled', true);
		$dompdf = new Dompdf($options);
		$dompdf->loadHtml($output);
		$dompdf->setPaper('A4', 'portrait');
		$dompdf->render();
		$dompdf->stream();
		die();
	}

	public function clone_poll($id) {
		$this->polls_model->clone_poll($id);
		$this->log('CLN_POL', $id);
		redirect('/admin/list_polls');
	}
	
	public function delete_poll($id) {
		$this->polls_model->delete_poll($id);
		$this->log('DEL_POL', $id);
		redirect('/admin/list_polls');
	}
	
	### EXPORT
	
	public function export() {
		$users = $this->users_model->get_users('user');
		$this->log('EXP_USR', null);
		header('Content-Type: application/csv');
		header('Content-Disposition: attachment; filename=anagrafica_dipendenti.csv');
		header('Pragma: no-cache');
		echo 'id;nome;cognome;genere;comune nascita;data nascita;eta;nazionalita;indirizzo residenza;citta residenza;email;telefono;codice fiscale;username;utente attivo;in forza;data registrazione;data ultimo cambio psw'."\n";
		foreach ($users as $u) {
			echo $u['id_user'].';'.$u['first_name'].';'.$u['last_name'].';'.$u['gender'].';'.$u['birth_city'].';'.$u['birth_date'].';'.$u['age'].';'.$u['nationality'].';'.$u['address'].';'.$u['city'].';'.$u['email'].';'.$u['telephone'].';'.$u['fiscal_code'].';'.$u['username'].';'.($u['active'] ? 'Si' : 'No').';'.($u['is_hired'] ? 'Si' : 'No').';'.$u['date_creation'].';'.$u['date_pswchange']."\n";
		}
		die();
	}

	public function export_by_level() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);

			$this->log('EXP_USR', null);

			header('Content-Type: application/csv');
			header('Content-Disposition: attachment; filename=anagrafica_dipendenti_per_livello.csv');
			header('Pragma: no-cache');

			foreach ($posted['level_list'] as $id_level) {
				list($level_id, $level_name) = explode('|', $id_level);

				$filters = ['id_level' => $level_id];
				if (isset($posted['only_hired']))
					$filters['is_hired'] = 1;
				if (isset($posted['only_active']))
					$filters['active'] = 1;
				$users = $this->users_model->get_users('user', $filters);
				echo $level_name."\n";
				echo 'id;nome;cognome;genere;comune nascita;data nascita;eta;nazionalita;indirizzo residenza;citta residenza;email;telefono;codice fiscale;username;utente attivo;in forza;data registrazione;data ultimo cambio psw'."\n";
				foreach ($users as $u) {
					echo $u['id_user'].';'.$u['first_name'].';'.$u['last_name'].';'.$u['gender'].';'.$u['birth_city'].';'.$u['birth_date'].';'.$u['age'].';'.$u['nationality'].';'.$u['address'].';'.$u['city'].';'.$u['email'].';'.$u['telephone'].';'.$u['fiscal_code'].';'.$u['username'].';'.($u['active'] ? 'Si' : 'No').';'.($u['is_hired'] ? 'Si' : 'No').';'.$u['date_creation'].';'.$u['date_pswchange']."\n";
				}
				echo "\n";
			}

			die();
		} else {
			$data_view['levels'] = $this->levels_model->get_levels(TRUE);
			$this->render('export_by_level', 'admin', $data_view);
		}
	}

	public function export_by_service() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);

			$this->log('EXP_USR', null);

			header('Content-Type: application/csv');
			header('Content-Disposition: attachment; filename=anagrafica_dipendenti_per_servizio.csv');
			header('Pragma: no-cache');

			foreach ($posted['service_list'] as $id_service) {
				list($service_id, $service_name) = explode('|', $id_service);

				$filters = ['id_service' => $service_id];
				if (isset($posted['only_hired']))
					$filters['is_hired'] = 1;
				if (isset($posted['only_active']))
					$filters['active'] = 1;
				$users = $this->users_model->get_users('user', $filters);
				echo $service_name."\n";
				echo 'id;nome;cognome;genere;comune nascita;data nascita;eta;nazionalita;indirizzo residenza;citta residenza;email;telefono;codice fiscale;username;utente attivo;in forza;data registrazione;data ultimo cambio psw'."\n";
				foreach ($users as $u) {
					echo $u['id_user'].';'.$u['first_name'].';'.$u['last_name'].';'.$u['gender'].';'.$u['birth_city'].';'.$u['birth_date'].';'.$u['age'].';'.$u['nationality'].';'.$u['address'].';'.$u['city'].';'.$u['email'].';'.$u['telephone'].';'.$u['fiscal_code'].';'.$u['username'].';'.($u['active'] ? 'Si' : 'No').';'.($u['is_hired'] ? 'Si' : 'No').';'.$u['date_creation'].';'.$u['date_pswchange']."\n";
				}
				echo "\n";
			}

			die();
		} else {
			$data_view['services'] = $this->services_model->get_services(TRUE);
			$this->render('export_by_service', 'admin', $data_view);
		}
	}

	public function export_by_service_and_city() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);

			$this->log('EXP_USR', null);

			header('Content-Type: application/csv');
			header('Content-Disposition: attachment; filename=anagrafica_dipendenti_per_servizio_e_citta.csv');
			header('Pragma: no-cache');

			list($service_id, $service_name) = explode('|', $posted['service']);

			foreach ($posted['area_list'] as $id_area) {
				list($area_id, $area_name) = explode('|', $id_area);

				$filters = ['id_service' => $service_id, 'id_area' => $area_id];
				if (isset($posted['only_hired']))
					$filters['is_hired'] = 1;
				if (isset($posted['only_active']))
					$filters['active'] = 1;
				$users = $this->users_model->get_users('user', $filters);
				echo $service_name.' - '.$area_name."\n";
				echo 'id;nome;cognome;genere;comune nascita;data nascita;eta;nazionalita;indirizzo residenza;citta residenza;email;telefono;codice fiscale;username;utente attivo;in forza;data registrazione;data ultimo cambio psw'."\n";
				foreach ($users as $u) {
					echo $u['id_user'].';'.$u['first_name'].';'.$u['last_name'].';'.$u['gender'].';'.$u['birth_city'].';'.$u['birth_date'].';'.$u['age'].';'.$u['nationality'].';'.$u['address'].';'.$u['city'].';'.$u['email'].';'.$u['telephone'].';'.$u['fiscal_code'].';'.$u['username'].';'.($u['active'] ? 'Si' : 'No').';'.($u['is_hired'] ? 'Si' : 'No').';'.$u['date_creation'].';'.$u['date_pswchange']."\n";
				}
				echo "\n";
			}

			die();
		} else {
			$data_view['services'] = $this->services_model->get_services(TRUE);
			$this->render('export_by_service_and_city', 'admin', $data_view);
		}
	}

	public function export_by_job() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);

			$this->log('EXP_USR', null);

			header('Content-Type: application/csv');
			header('Content-Disposition: attachment; filename=anagrafica_dipendenti_per_mansione.csv');
			header('Pragma: no-cache');

			foreach ($posted['job_list'] as $id_job) {
				list($job_id, $job_name) = explode('|', $id_job);

				$filters = ['id_job' => $job_id];
				if (isset($posted['only_hired']))
					$filters['is_hired'] = 1;
				if (isset($posted['only_active']))
					$filters['active'] = 1;
				$users = $this->users_model->get_users('user', $filters);
				echo $job_name."\n";
				echo 'id;nome;cognome;genere;comune nascita;data nascita;eta;nazionalita;indirizzo residenza;citta residenza;email;telefono;codice fiscale;username;utente attivo;in forza;data registrazione;data ultimo cambio psw'."\n";
				foreach ($users as $u) {
					echo $u['id_user'].';'.$u['first_name'].';'.$u['last_name'].';'.$u['gender'].';'.$u['birth_city'].';'.$u['birth_date'].';'.$u['age'].';'.$u['nationality'].';'.$u['address'].';'.$u['city'].';'.$u['email'].';'.$u['telephone'].';'.$u['fiscal_code'].';'.$u['username'].';'.($u['active'] ? 'Si' : 'No').';'.($u['is_hired'] ? 'Si' : 'No').';'.$u['date_creation'].';'.$u['date_pswchange']."\n";
				}
				echo "\n";
			}

			die();
		} else {
			$data_view['jobs'] = $this->jobs_model->get_jobs(TRUE);
			$this->render('export_by_job', 'admin', $data_view);
		}
	}
	
	public function export_by_level_and_job() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);

			$this->log('EXP_USR', null);

			header('Content-Type: application/csv');
			header('Content-Disposition: attachment; filename=anagrafica_dipendenti_per_livello_e_mansione.csv');
			header('Pragma: no-cache');

			list($level_id, $level_name) = explode('|', $posted['level']);

			foreach ($posted['job_list'] as $id_job) {
				list($job_id, $job_name) = explode('|', $id_job);

				$filters = ['id_level' => $level_id, 'id_job' => $job_id];
				if (isset($posted['only_hired']))
					$filters['is_hired'] = 1;
				if (isset($posted['only_active']))
					$filters['active'] = 1;
				$users = $this->users_model->get_users('user', $filters);
				echo $level_name.' - '.$job_name."\n";
				echo 'id;nome;cognome;genere;comune nascita;data nascita;eta;nazionalita;indirizzo residenza;citta residenza;email;telefono;codice fiscale;username;utente attivo;in forza;data registrazione;data ultimo cambio psw'."\n";
				foreach ($users as $u) {
					echo $u['id_user'].';'.$u['first_name'].';'.$u['last_name'].';'.$u['gender'].';'.$u['birth_city'].';'.$u['birth_date'].';'.$u['age'].';'.$u['nationality'].';'.$u['address'].';'.$u['city'].';'.$u['email'].';'.$u['telephone'].';'.$u['fiscal_code'].';'.$u['username'].';'.($u['active'] ? 'Si' : 'No').';'.($u['is_hired'] ? 'Si' : 'No').';'.$u['date_creation'].';'.$u['date_pswchange']."\n";
				}
				echo "\n";
			}

			die();
		} else {
			$data_view['levels'] = $this->levels_model->get_levels(TRUE);
			$this->render('export_by_level_and_job', 'admin', $data_view);
		}
	}

	### BILANCI - TIPI DOCUMENTO

	public function view_doctypes() {
		$data_view['doctypes'] = $this->doctypes_model->get_doctypes(TRUE);
		$this->render('doctypes_list', 'admin', $data_view);
	}

	public function edit_doctype($id = NULL) {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$this->doctypes_model->update_doctype($posted);
			$this->log('UPD_DOCTYPE', $posted['id_doctype']);
			redirect('/admin/view_doctypes');
		} elseif (!empty($id)) {
			$data_view['doctype'] = $this->doctypes_model->get_doctype($id);
			$this->render('form_doctype', 'admin', $data_view);
		} else {
			redirect('/admin/view_doctypes');
		}
	}

	### BILANCI - CENTRI DI COSTO
	
	public function view_costcenters() {
		$data_view['costcenters'] = $this->costcenters_model->get_costcenters(TRUE);
		$this->render('costcenters_list', 'admin', $data_view);
	}
	
	public function insert_costcenter() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$id_costcenter = $this->costcenters_model->insert_costcenter($posted);
			$this->log('INS_COSTCENTER', $id_costcenter);
		}
		redirect('/admin/view_costcenters');
	}
	
	public function del_costcenter() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$this->costcenters_model->del_costcenter($posted);
			$this->log('DEL_COSTCENTER', $posted['id_costcenter_to_del']);
		}
		redirect('/admin/view_costcenters');
	}
	
	public function edit_costcenter($id = NULL) {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$this->costcenters_model->update_costcenter($posted);
			$this->log('UPD_COSTCENTER', $posted['id_costcenter']);
			redirect('/admin/view_costcenters');
		} elseif (!empty($id)) {
			$data_view['costcenter'] = $this->costcenters_model->get_costcenter($id);
			$this->render('form_costcenter', 'admin', $data_view);
		} else {
			redirect('/admin/view_costcenters');
		}
	}

	### BILANCI - CATEGORIE DI COSTO
	
	public function view_costcats() {
		$data_view['costcats'] = $this->costcats_model->get_costcats(TRUE);
		$this->render('costcats_list', 'admin', $data_view);
	}
	
	public function insert_costcat() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$id_costcat = $this->costcats_model->insert_costcat($posted);
			$this->log('INS_COSTCAT', $id_costcat);
		}
		redirect('/admin/view_costcats');
	}
	
	public function del_costcat() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$this->costcats_model->del_costcat($posted);
			$this->log('DEL_COSTCAT', $posted['id_costcat_to_del']);
		}
		redirect('/admin/view_costcats');
	}
	
	public function edit_costcat($id = NULL) {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$this->costcats_model->update_costcat($posted);
			$this->log('UPD_COSTCAT', $posted['id_costcat']);
			redirect('/admin/view_costcats');
		} elseif (!empty($id)) {
			$data_view['costcat'] = $this->costcats_model->get_costcat($id);
			$this->render('form_costcat', 'admin', $data_view);
		} else {
			redirect('/admin/view_costcats');
		}
	}

	### BILANCI - FORNITORI
	
	public function view_suppliers() {
		$data_view['suppliers'] = $this->suppliers_model->get_suppliers(TRUE);
		$data_view['costcats'] = $this->costcats_model->get_costcats();
		$data_view['costcenters'] = $this->costcenters_model->get_costcenters();
		$this->render('suppliers_list', 'admin', $data_view);
	}
	
	public function insert_supplier() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$id_supplier = $this->suppliers_model->insert_supplier($posted);
			$this->log('INS_SUPPLIER', $id_supplier);
		}
		redirect('/admin/view_suppliers');
	}
	
	public function del_supplier() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$this->suppliers_model->del_supplier($posted);
			$this->log('DEL_SUPPLIER', $posted['id_supplier_to_del']);
		}
		redirect('/admin/view_suppliers');
	}
	
	public function edit_supplier($id = NULL) {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$this->suppliers_model->update_supplier($posted);
			$this->log('UPD_SUPPLIER', $posted['id_supplier']);
			redirect('/admin/view_suppliers');
		} elseif (!empty($id)) {
			$data_view['supplier'] = $this->suppliers_model->get_supplier($id);
			$data_view['costcats'] = $this->costcats_model->get_costcats();
			$data_view['costcenters'] = $this->costcenters_model->get_costcenters();
			$this->render('form_supplier', 'admin', $data_view);
		} else {
			redirect('/admin/view_suppliers');
		}
	}

	### BILANCI - CLIENTI
	
	public function view_bcustomers() {
		$data_view['customers'] = $this->bcustomers_model->get_customers(TRUE);
		$data_view['costcenters'] = $this->costcenters_model->get_costcenters();
		$this->render('bcustomers_list', 'admin', $data_view);
	}
	
	public function insert_bcustomer() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$id_customer = $this->bcustomers_model->insert_customer($posted);
			$this->log('INS_BCUSTOMER', $id_customer);
		}
		redirect('/admin/view_bcustomers');
	}
	
	public function del_bcustomer() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$this->bcustomers_model->del_customer($posted);
			$this->log('DEL_BCUSTOMER', $posted['id_customer_to_del']);
		}
		redirect('/admin/view_bcustomers');
	}
	
	public function edit_bcustomer($id = NULL) {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$this->bcustomers_model->update_customer($posted);
			$this->log('UPD_BCUSTOMER', $posted['id_customer']);
			redirect('/admin/view_bcustomers');
		} elseif (!empty($id)) {
			$data_view['customer'] = $this->bcustomers_model->get_customer($id);
			$data_view['costcenters'] = $this->costcenters_model->get_costcenters();
			$this->render('form_bcustomer', 'admin', $data_view);
		} else {
			redirect('/admin/view_bcustomers');
		}
	}

	### BILANCI - BANDI/FINANZIAMENTI
	
	public function view_financings() {
		$data_view['financings'] = $this->financings_model->get_financings(TRUE);
		$data_view['costcenters'] = $this->costcenters_model->get_costcenters();
		$this->render('financings_list', 'admin', $data_view);
	}
	
	public function insert_financing() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$id_financing = $this->financings_model->insert_financing($posted);
			$this->log('INS_FINANCING', $id_financing);
		}
		redirect('/admin/view_financings');
	}
	
	public function del_financing() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$this->financings_model->del_financing($posted);
			$this->log('DEL_FINANCING', $posted['id_financing_to_del']);
		}
		redirect('/admin/view_financings');
	}
	
	public function edit_financing($id = NULL) {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$this->financings_model->update_financing($posted);
			$this->log('UPD_FINANCING', $posted['id_financing']);
			redirect('/admin/view_financings');
		} elseif (!empty($id)) {
			$data_view['financing'] = $this->financings_model->get_financing($id);
			$data_view['costcenters'] = $this->costcenters_model->get_costcenters();
			$this->render('form_financing', 'admin', $data_view);
		} else {
			redirect('/admin/view_financings');
		}
	}

	### BILANCI - RICAVI

	public function view_revenues() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$data_view['filter'] = $posted;
			$_SESSION['filter']['revenues'] = $posted;
		} else {
			if (!empty($_SESSION['filter']['revenues']))
				$data_view['filter'] = $_SESSION['filter']['revenues'];
			$data_view['doctypes'] = $this->doctypes_model->get_doctypes();
		}

		# Controllo che i filtri non siano tutti vuoti; se è così, imposto quelli standard per il mese attuale
		$filter_empty = true;
		if (!empty($_SESSION['filter']['revenues']))
			foreach ($_SESSION['filter']['revenues'] as $filter_item)
				if ($filter_item != "")
					$filter_empty = false;
		if ($filter_empty) {
			$data_view['filter']['filter_period_month'] = date('n');
			$data_view['filter']['filter_period_year'] = date('Y');
			$_SESSION['filter']['revenues'] = $data_view['filter'];
		}

		$data_view['revenues'] = $this->revenues_model->get_revenues(TRUE, (!empty($data_view['filter']) ? $data_view['filter'] : null));
		$data_view['find_total'] = 0;
		foreach ($data_view['revenues'] as $fatt)
			foreach ($fatt['detail'] as $det)
				if ($fatt['doctype']['type'] == 'minus')
					$data_view['find_total'] -= $det['amount'];
				else
					$data_view['find_total'] += $det['amount'];
		$data_view['find_total'] = round($data_view['find_total'], 2);
		$data_view['customers'] = $this->bcustomers_model->get_customers(TRUE);
		$data_view['costcenters'] = $this->costcenters_model->get_costcenters(TRUE);
		$data_view['ret'] = $this->session->flashdata('ret');
		$this->render('revenues_list', 'admin', $data_view);
	}

	public function insert_revenue() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$id_revenue = $this->revenues_model->insert_revenue($posted);
			$this->log('INS_REVENUE', $id_revenue);
		}
		redirect('/admin/view_revenues');
	}
	
	public function del_revenue() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$this->revenues_model->del_revenue($posted);
			$this->log('DEL_REVENUE', $posted['id_revenue_to_del']);
		}
		redirect('/admin/view_revenues');
	}
	
	public function edit_revenue($id = NULL) {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$this->revenues_model->update_revenue($posted);
			$this->log('UPD_REVENUE', $posted['id_revenue']);
			redirect('/admin/view_revenues');
		} elseif (!empty($id)) {
			$data_view['revenue'] = $this->revenues_model->get_revenue($id);
			$data_view['customers'] = $this->bcustomers_model->get_customers(TRUE);
			$data_view['costcenters'] = $this->costcenters_model->get_costcenters();
			$this->render('form_revenue', 'admin', $data_view);
		} else {
			redirect('/admin/view_revenues');
		}
	}

	### BILANCI - COSTI FORNITORI

	public function view_suppliercosts() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$data_view['filter'] = $posted;
			$_SESSION['filter']['suppliercosts'] = $posted;
		} else {
			if (!empty($_SESSION['filter']['suppliercosts']))
				$data_view['filter'] = $_SESSION['filter']['suppliercosts'];
			$data_view['doctypes'] = $this->doctypes_model->get_doctypes();
		}

		# Controllo che i filtri non siano tutti vuoti; se è così, imposto quelli standard per il mese attuale
		$filter_empty = true;
		if (!empty($_SESSION['filter']['suppliercosts']))
			foreach ($_SESSION['filter']['suppliercosts'] as $filter_item)
				if ($filter_item != "")
					$filter_empty = false;
		if ($filter_empty) {
			$data_view['filter']['filter_period_month'] = date('n');
			$data_view['filter']['filter_period_year'] = date('Y');
			$_SESSION['filter']['suppliercosts'] = $data_view['filter'];
		}

		$data_view['suppliercosts'] = $this->suppliercosts_model->get_suppliercosts(TRUE, (!empty($data_view['filter']) ? $data_view['filter'] : null));
		$data_view['find_total'] = 0;
		foreach ($data_view['suppliercosts'] as $fatt)
			foreach ($fatt['detail'] as $det)
				if ($fatt['doctype']['type'] == 'minus')
					$data_view['find_total'] -= $det['amount'];
				else
					$data_view['find_total'] += $det['amount'];
		$data_view['find_total'] = round($data_view['find_total'], 2);
		$data_view['suppliers'] = $this->suppliers_model->get_suppliers(TRUE);
		$data_view['costcenters'] = $this->costcenters_model->get_costcenters(TRUE);
		$data_view['ret'] = $this->session->flashdata('ret');
		$this->render('suppliercosts_list', 'admin', $data_view);
	}

	public function insert_suppliercost() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$id_suppliercost = $this->suppliercosts_model->insert_suppliercost($posted);
			$this->log('INS_SUPPLIERCOST', $id_suppliercost);
		}
		redirect('/admin/view_suppliercosts');
	}
	
	public function del_suppliercost() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$this->suppliercosts_model->del_suppliercost($posted);
			$this->log('DEL_SUPPLIERCOST', $posted['id_suppliercost_to_del']);
		}
		redirect('/admin/view_suppliercosts');
	}
	
	public function edit_suppliercost($id = NULL) {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$this->suppliercosts_model->update_suppliercost($posted);
			$this->log('UPD_SUPPLIERCOST', $posted['id_suppliercost']);
			redirect('/admin/view_suppliercosts');
		} elseif (!empty($id)) {
			$data_view['suppliercost'] = $this->suppliercosts_model->get_suppliercost($id);
			$data_view['suppliers'] = $this->suppliers_model->get_suppliers(TRUE);
			$data_view['costcenters'] = $this->costcenters_model->get_costcenters();
			$data_view['costcats'] = $this->costcats_model->get_costcats();
			$this->render('form_suppliercost', 'admin', $data_view);
		} else {
			redirect('/admin/view_suppliercosts');
		}
	}

	### BILANCI - COSTI DIPENDENTI

	public function view_usercosts() {
		if ($this->input->post()) { // ho impostato dei filtri e fatto una ricerca
			$posted = $this->input->post(NULL, TRUE);
			$data_view['filter'] = $posted;
			$_SESSION['filter']['usercosts'] = $posted;
		} else {	// no ho fatto una ricerca specifica
			if (!empty($_SESSION['filter']['usercosts'])) 	// se ho dei filtri in sessione
				$data_view['filter'] = $_SESSION['filter']['usercosts'];			
		}

		# Controllo che i filtri non siano tutti vuoti; se è così, imposto quelli standard per il mese attuale
		$filter_empty = true;
		if (!empty($_SESSION['filter']['usercosts']))
			foreach ($_SESSION['filter']['usercosts'] as $filter_item)
				if ($filter_item != "")
					$filter_empty = false;
		if ($filter_empty) {
			$data_view['filter']['filter_period_month'] = date('n');
			$data_view['filter']['filter_period_year'] = date('Y');
			$_SESSION['filter']['usercosts'] = $data_view['filter'];
		}

		$data_view['usercosts'] = $this->usercosts_model->get_usercosts(TRUE, (!empty($data_view['filter']) ? $data_view['filter'] : null));
		$data_view['find_total'] = 0;
		foreach ($data_view['usercosts'] as $fatt)
			foreach ($fatt['detail'] as $det)
				if (!empty($fatt['doctype']) AND $fatt['doctype']['type'] == 'minus')
					$data_view['find_total'] -= $det['amount'];
				else
					$data_view['find_total'] += $det['amount'];
		$data_view['find_total'] = round($data_view['find_total'], 2);
		$data_view['users'] = $this->users_model->get_users('user');
		$data_view['costcenters'] = $this->costcenters_model->get_costcenters();
		$data_view['ret'] = $this->session->flashdata('ret');
		$this->render('usercosts_list', 'admin', $data_view);
	}

	public function insert_usercost() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$id_usercost = $this->usercosts_model->insert_usercost($posted);
			$this->log('INS_USERCOST', $id_usercost);
		}
		redirect('/admin/view_usercosts');
	}
	
	public function del_usercost() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$this->usercosts_model->del_usercost($posted);
			$this->log('DEL_USERCOST', $posted['id_usercost_to_del']);
		}
		redirect('/admin/view_usercosts');
	}
	
	public function edit_usercost($id = NULL) {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);
			$this->usercosts_model->update_usercost($posted);
			$this->log('UPD_USERCOST', $posted['id_usercost']);
			redirect('/admin/view_usercosts');
		} elseif (!empty($id)) {
			$data_view['usercost'] = $this->usercosts_model->get_usercost($id);
			$data_view['users'] = $this->users_model->get_users('user');
			$data_view['costcenters'] = $this->costcenters_model->get_costcenters();
			$this->render('form_usercost', 'admin', $data_view);
		} else {
			redirect('/admin/view_usercosts');
		}
	}

	### BILANCI - DELETE

	public function delete_balance_data() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);

			$ret = ['status' => 'ko'];

			if ($posted['data_type']) {
				switch ($posted['data_type']) {
					case 'usercosts':
						$op_result = $this->usercosts_model->del_usercosts($posted);
					break;
					case 'revenues':
						$op_result = $this->revenues_model->del_revenues($posted);
					break;
					case 'suppliercosts':
						$op_result = $this->suppliercosts_model->del_suppliercosts($posted);
					break;
				}

				if ($op_result['trans_status'] !== FALSE) {	// transaction cancellazione andata bene
					$ret = [
						'status' => 'ok',
						'deleted_rows' => $op_result['deleted_rows'],
						// 'deleted_detail_rows' => $deleted_detail_rows
					];
				}	
			}

			$this->session->set_flashdata('ret', $ret);

			redirect('/admin/delete_balance_data');
		} else {
			$data_view = [
				'ret' => $this->session->flashdata('ret'),
				'revenues_imports' => $this->revenues_model->get_last_imports(),
				'suppliercosts_imports' => $this->suppliercosts_model->get_last_imports(),
				'usercosts_imports' => $this->usercosts_model->get_last_imports()
			];
			$this->render('delete_balance_data', 'admin', $data_view);
		}
	}

	### BILANCI - IMPORT

	private function empty_dir($dir_to_empty) {
		$ff = @scandir($dir_to_empty);
		if ($ff and count($ff))
			foreach ($ff as $f)
				if ($f != "." AND $f != "..")
					unlink($dir_to_empty.$f);
	}

	private function import_archive($uploaded_file) {
		$this->empty_dir($this::IMPORT_DIR);

		$zip = new ZipArchive;
		if ($zip->open($uploaded_file) === TRUE) {
			$zip->extractTo($this::IMPORT_DIR);
			$zip->close();
		} else {
			echo "<h1>Impossibile estrarre l'archivio</h1>";
		}
	}

	private function get_xml_sum_data($xml, $field, $filename = null) {
		// echo '<pre>';
		// print_r($xml->DatiBeniServizi);
		// echo '</pre>';

		switch ($field) {
			case 'aliquota_iva':
				if (isset($xml->DatiBeniServizi->DatiRiepilogo[0]) && isset($xml->DatiBeniServizi->DatiRiepilogo[1]))
					return 'Varie';
				else
					return @(float)$xml->DatiBeniServizi->DatiRiepilogo->AliquotaIVA;
			break;
			case 'imponibile':
				if (isset($xml->DatiBeniServizi->DatiRiepilogo[0]) && isset($xml->DatiBeniServizi->DatiRiepilogo[1])) {
					$imponibile = 0;
					foreach ($xml->DatiBeniServizi->DatiRiepilogo as $line)
						$imponibile += @(float)$line->ImponibileImporto;
					return $imponibile;
				} else
					return @(float)$xml->DatiBeniServizi->DatiRiepilogo->ImponibileImporto;
			break;
			case 'imposta':
				if (isset($xml->DatiBeniServizi->DatiRiepilogo[0]) && isset($xml->DatiBeniServizi->DatiRiepilogo[1])) {
					$imposta = 0;
					foreach ($xml->DatiBeniServizi->DatiRiepilogo as $line)
						$imposta += @(float)$line->Imposta;
					return $imposta;
				} else
					return @(float)$xml->DatiBeniServizi->DatiRiepilogo->Imposta;
			break;
			case 'esigibilita_iva':
				if (isset($xml->DatiBeniServizi->DatiRiepilogo[0]) && isset($xml->DatiBeniServizi->DatiRiepilogo[1]))
					return 'Varie';
				else
					return @(string)$xml->DatiBeniServizi->DatiRiepilogo->EsigibilitaIVA;
			break;
			default:
				return null;
			break;
		}
	}

	private function extract_period_from_invoice($invoice_content, $xml_content) {
		$months_indexes = [
			'gennaio' => 1,
			'gen' => 1,
			'febbraio' => 2, 
			'feb' => 2, 
			'marzo' => 3, 
			'mar' => 3, 
			'aprile' => 4, 
			'apr' => 4, 
			'maggio' => 5, 
			'mag' => 5, 
			'giugno' => 6, 
			'giu' => 6, 
			'luglio' => 7, 
			'lug' => 7, 
			'agosto' => 8, 
			'ago' => 8, 
			'settembre' => 9, 
			'set' => 9, 
			'ottobre' => 10, 
			'ott' => 10, 
			'novembre' => 11, 
			'nov' => 11, 
			'dicembre' => 12,
			'dic' => 12
		];

		$details_content = (string)$xml_content->FatturaElettronicaBody->DatiBeniServizi;
		
		if (preg_match('/(gennaio|gen|febbraio|feb|marzo|mar|aprile|apr|maggio|mag|giugno|giu|luglio|lug|agosto|ago|settembre|set|ottobre|ott|novembre|nov|dicembre|dic)\s([0-9]{4})/', $invoice_content, $matches)) {
			$period_month = $months_indexes[$matches[1]];
			$period_year = $matches[2];
		} elseif (preg_match('/([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{2,4})/', $details_content, $matches)) {	// formati dd/mm/yyyy e varianti (anno o mese con 1 o 2 cifre e anno con 2 o 4 cifre)
			$period_month = ltrim($matches[2], '0');	// se il mese è composto da 2 cifre e la prima è uno 0, lo tronco (ad es. "08" diventa "8")
			$period_year = strlen($matches[3]) == 2 ? '20'.$matches[3] : $matches[3];	// se l'anno è composto da 2 cifre, aggiungo il '20' davanti (ad es. "24" diventa "2024")
		} else if (preg_match('/([0-9]{4})\/([0-9]{1,2})\/([0-9]{1,2})/', $details_content, $matches)) {	// formati yyyy-mm-dd e varianti (anno o mese con 1 o 2 cifre)
			$period_month = ltrim($matches[2], '0');	// se il mese è composto da 2 cifre e la prima è uno 0, lo tronco (ad es. "08" diventa "8")
			$period_year = $matches[1];
		} else {
			$period_month = null;
			$period_year = null;
		}

		return [$period_month, $period_year];
	}

	public function import_revenues() {
		if (count($_FILES)) {
			$this->import_archive($_FILES['package']['tmp_name']);
			$ff = scandir($this::IMPORT_DIR);

			$ret = [
				'alert_type' => 'import_ok',
				'entries' => 0,
				'customers_ext' => 0,
				'customers_ins' => 0,
				'customers_not' => 0,
				'customers_not_list' => '',
				'revenues_ext' => 0,
				'revenues_ext_list' => '',
				'revenues_ins' => 0,
				'revenues_not' => 0,
				'revenues_not_list' => '',
			];

			foreach ($ff as $f) {	// per ogni fattura importata
				if ($f != "." AND $f != "..") {	// se è effettivamente un file
					$ret['entries']++;
					// leggo il contenuto grezzo del file
					$invoice_content = strtolower(file_get_contents($this::IMPORT_DIR.$f));
					// leggo il contenuto XML
					$xml_content = simplexml_load_file($this::IMPORT_DIR.$f) or die("Impossibile leggere XML del file: ".$f);
					// cerco il periodo di riferimento
					$ar_period = $this->extract_period_from_invoice($invoice_content, $xml_content);
					$period_month = $ar_period[0];
					$period_year = $ar_period[1];
					// setto l'array con i dati del cliente
					$xml_dati_cliente = $xml_content->FatturaElettronicaHeader->CessionarioCommittente;
					$dati_cliente = [
						'denominazione' => @(string)$xml_dati_cliente->DatiAnagrafici->Anagrafica->Denominazione,
						'nome' => @(string)$xml_dati_cliente->DatiAnagrafici->Anagrafica->Nome,
						'cognome' => @(string)$xml_dati_cliente->DatiAnagrafici->Anagrafica->Cognome,
						'piva' => @(string)$xml_dati_cliente->DatiAnagrafici->IdFiscaleIVA->IdCodice,
						'codfisc' => @(string)$xml_dati_cliente->DatiAnagrafici->CodiceFiscale,
						'indirizzo' => @(string)$xml_dati_cliente->Sede->Indirizzo,
						'civico' => @(string)$xml_dati_cliente->Sede->NumeroCivico,
						'cap' => @(string)$xml_dati_cliente->Sede->CAP,
						'comune' => @(string)$xml_dati_cliente->Sede->Comune,
						'prov' => @(string)$xml_dati_cliente->Sede->Provincia,
						'nazione' => @(string)$xml_dati_cliente->Sede->Nazione,
						'telefono' => @(string)$xml_dati_cliente->Contatti->Telefono,
						'email' => @(string)$xml_dati_cliente->Contatti->Email
					];
					// controllo se il cliente esiste - se non esiste lo creo
					$customer = $this->bcustomers_model->get_customer(NULL, $dati_cliente['piva'], $dati_cliente['codfisc']);
					if (!$customer) {
						if ($id_customer = $this->bcustomers_model->insert_customer($dati_cliente)) {
							$customer = $this->bcustomers_model->get_customer($id_customer);
							$ret['customers_ins']++;
						} else {
							$ret['customers_not']++;
							$ret['customers_not_list'] .= $dati_cliente['denominazione'].' - '.$dati_cliente['nome'].' '.$dati_cliente['cognome']."<br>";
						}
					} else {
						$id_customer = $customer['id'];
						$ret['customers_ext']++;
					}
					// setto l'array con i dati della fattura
					$xml_dati_fattura = $xml_content->FatturaElettronicaBody;
					$dati_fattura = [
						'id_customer' => $id_customer,
						// 'period_month' => @explode('-', (string)$xml_dati_fattura->DatiGenerali->DatiGeneraliDocumento->Data)[1],
						// 'period_year' => @explode('-', (string)$xml_dati_fattura->DatiGenerali->DatiGeneraliDocumento->Data)[0],
						'period_month' => $period_month,
						'period_year' => $period_year,
						'tipo_documento' => @(string)$xml_dati_fattura->DatiGenerali->DatiGeneraliDocumento->TipoDocumento,
						'divisa' => @(string)$xml_dati_fattura->DatiGenerali->DatiGeneraliDocumento->Divisa,
						'data' => @(string)$xml_dati_fattura->DatiGenerali->DatiGeneraliDocumento->Data,
						'numero' => @preg_replace('/\s/', '', (string)$xml_dati_fattura->DatiGenerali->DatiGeneraliDocumento->Numero),
						'importo_totale' => @(float)$xml_dati_fattura->DatiGenerali->DatiGeneraliDocumento->ImportoTotaleDocumento,
						'aliquota_iva' => $this::get_xml_sum_data($xml_dati_fattura, 'aliquota_iva', $f),
						'arrotondamento' => null,
						'imponibile' => $this::get_xml_sum_data($xml_dati_fattura, 'imponibile', $f),
						'imposta' => $this::get_xml_sum_data($xml_dati_fattura, 'imposta', $f),
						'esigibilita_iva' => $this::get_xml_sum_data($xml_dati_fattura, 'esigibilita_iva', $f),
						'filename' => $f
					];
					// controllo se la fattura esiste - se non esiste la registro
					$revenue = $this->revenues_model->get_revenue(NULL, $dati_fattura['numero'], $id_customer, $dati_fattura['data'], $dati_fattura['importo_totale']);
					if (!$revenue) {
						if ($this->revenues_model->insert_revenue($dati_fattura)) {
							$ret['revenues_ins']++;
						} else {
							// echo '<pre style="background: red; color: #fff; font-weight: 600;">RICAVO FALLITO</pre>';
							$ret['revenues_not']++;
							$ret['revenues_not_list'] .= '<b>FATT.</b> '.$dati_fattura['numero'].' <b>DEL</b> '.rvd($dati_fattura['data']).' - <b>ID CLIENTE</b> '.$dati_fattura['id_customer'].' - <b>IMPORTO</b> '.$dati_fattura['importo_totale']."€<br>";
						}
					} else {
						// echo '<pre style="background: red; color: #fff; font-weight: 600;">RICAVO DUPLICATO</pre>';
						$ret['revenues_ext']++;
						$ret['revenues_ext_list'] .= '<b>FATT.</b> '.$dati_fattura['numero'].' <b>DEL</b> '.rvd($dati_fattura['data']).' - <b>ID CLIENTE</b> '.$dati_fattura['id_customer'].' - <b>IMPORTO</b> '.$dati_fattura['importo_totale']."€<br>";
					}
				}
			}

			$this->session->set_flashdata('ret', $ret);
			redirect('/admin/view_revenues');
		} else {
			$this->render('import_revenues', 'admin', []);
		}
	}

	public function import_suppliercosts() {
		if (count($_FILES)) {
			$this->import_archive($_FILES['package']['tmp_name']);
			$ff = scandir($this::IMPORT_DIR);

			$ret = [
				'alert_type' => 'import_ok',
				'entries' => 0,
				'suppliers_ext' => 0,
				'suppliers_ins' => 0,
				'suppliers_not' => 0,
				'suppliers_not_list' => '',
				'suppliercosts_ext' => 0,
				'suppliercosts_ext_list' => '',
				'suppliercosts_ins' => 0,
				'suppliercosts_not' => 0,
				'suppliercosts_not_list' => '',
			];

			foreach ($ff as $f) {	// per ogni fattura importata
				if ($f != "." AND $f != "..") {	// se è effettivamente un file
					$ret['entries']++;
					// leggo il contenuto grezzo del file
					$invoice_content = strtolower(file_get_contents($this::IMPORT_DIR.$f));
					// leggo il contenuto XML
					$xml_content = simplexml_load_file($this::IMPORT_DIR.$f) or die("Impossibile leggere XML del file: ".$f);
					// cerco il periodo di riferimento
					$ar_period = $this->extract_period_from_invoice($invoice_content, $xml_content);
					$period_month = $ar_period[0];
					$period_year = $ar_period[1];
					// setto l'array con i dati del fornitore
					$xml_dati_fornitore = $xml_content->FatturaElettronicaHeader->CedentePrestatore;
					$dati_fornitore = [
						'denominazione' => @(string)$xml_dati_fornitore->DatiAnagrafici->Anagrafica->Denominazione,
						'nome' => @(string)$xml_dati_fornitore->DatiAnagrafici->Anagrafica->Nome,
						'cognome' => @(string)$xml_dati_fornitore->DatiAnagrafici->Anagrafica->Cognome,
						'piva' => @(string)$xml_dati_fornitore->DatiAnagrafici->IdFiscaleIVA->IdCodice,
						'codfisc' => @(string)$xml_dati_fornitore->DatiAnagrafici->CodiceFiscale,
						'indirizzo' => @(string)$xml_dati_fornitore->Sede->Indirizzo,
						'civico' => @(string)$xml_dati_fornitore->Sede->NumeroCivico,
						'cap' => @(string)$xml_dati_fornitore->Sede->CAP,
						'comune' => @(string)$xml_dati_fornitore->Sede->Comune,
						'prov' => @(string)$xml_dati_fornitore->Sede->Provincia,
						'nazione' => @(string)$xml_dati_fornitore->Sede->Nazione,
						'telefono' => @(string)$xml_dati_fornitore->Contatti->Telefono,
						'email' => @(string)$xml_dati_fornitore->Contatti->Email
					];
					// controllo se il fornitore esiste - se non esiste lo creo
					$supplier = $this->suppliers_model->get_supplier(NULL, $dati_fornitore['piva'], $dati_fornitore['codfisc']);
					if (!$supplier) {	// se il fornitore non esiste
						if ($id_supplier = $this->suppliers_model->insert_supplier($dati_fornitore)) {
							$supplier = $this->suppliers_model->get_supplier($id_supplier);
							$ret['suppliers_ins']++;
						} else {
							$ret['suppliers_not']++;
							$ret['suppliers_not_list'] .= $dati_fornitore['denominazione'].' - '.$dati_fornitore['nome'].' '.$dati_fornitore['cognome']."<br>";
						}
					} else {	// se il fornitore è già censito
						// aggiorno le sue informazioni
						$update_data = $dati_fornitore;
						foreach (['piva','codfisc'] as $k)
							unset($dati_fornitore[$k]);
						$update_data['id_supplier'] = $supplier['id'];
						$this->suppliers_model->update_supplier($update_data);
						// recupero il suo ID
						$id_supplier = $supplier['id'];
						$ret['suppliers_ext']++;
					}
					// setto l'array con i dati della fattura
					$xml_dati_fattura = $xml_content->FatturaElettronicaBody;
					$dati_fattura = [
						'id_supplier' => $id_supplier,
						// 'period_month' => @explode('-', (string)$xml_dati_fattura->DatiGenerali->DatiGeneraliDocumento->Data)[1],
						// 'period_year' => @explode('-', (string)$xml_dati_fattura->DatiGenerali->DatiGeneraliDocumento->Data)[0],
						'period_month' => $period_month,
						'period_year' => $period_year,
						'tipo_documento' => @(string)$xml_dati_fattura->DatiGenerali->DatiGeneraliDocumento->TipoDocumento,
						'divisa' => @(string)$xml_dati_fattura->DatiGenerali->DatiGeneraliDocumento->Divisa,
						'data' => @(string)$xml_dati_fattura->DatiGenerali->DatiGeneraliDocumento->Data,
						'numero' => @preg_replace('/\s/', '', (string)$xml_dati_fattura->DatiGenerali->DatiGeneraliDocumento->Numero),
						'importo_totale' => @(float)$xml_dati_fattura->DatiGenerali->DatiGeneraliDocumento->ImportoTotaleDocumento,
						'aliquota_iva' => $this::get_xml_sum_data($xml_dati_fattura, 'aliquota_iva', $f),
						'arrotondamento' => null,
						'imponibile' => $this::get_xml_sum_data($xml_dati_fattura, 'imponibile', $f),
						'imposta' => $this::get_xml_sum_data($xml_dati_fattura, 'imposta', $f),
						'esigibilita_iva' => $this::get_xml_sum_data($xml_dati_fattura, 'esigibilita_iva', $f),
						'filename' => $f
					];
					// controllo se la fattura esiste - se non esiste la registro
					$suppliercost = $this->suppliercosts_model->get_suppliercost(NULL, $dati_fattura['numero'], $id_supplier, $dati_fattura['data'], $dati_fattura['importo_totale']);
					if (!$suppliercost) {
						if ($this->suppliercosts_model->insert_suppliercost($dati_fattura)) {
							$ret['suppliercosts_ins']++;
						} else {
							// echo '<pre style="background: red; color: #fff; font-weight: 600;">COSTO FORNITORE FALLITO</pre>';
							$ret['suppliercosts_not']++;
							$ret['suppliercosts_not_list'] .= '<b>FATT.</b> '.$dati_fattura['numero'].' <b>DEL</b> '.rvd($dati_fattura['data']).' - <b>ID FORN.</b> '.$dati_fattura['id_supplier'].' - <b>IMPORTO</b> '.$dati_fattura['importo_totale']."€<br>";
						}
					} else {
						// echo '<pre style="background: red; color: #fff; font-weight: 600;">COSTO FORNITORE DUPLICATO</pre>';
						$ret['suppliercosts_ext']++;
						$ret['suppliercosts_ext_list'] .= '<b>FATT.</b> '.$dati_fattura['numero'].' <b>DEL</b> '.rvd($dati_fattura['data']).' - <b>ID FORN.</b> '.$dati_fattura['id_supplier'].' - <b>IMPORTO</b> '.$dati_fattura['importo_totale']."€<br>";
					}
				}
			}

			// echo '<pre>';
			// print_r($ret);
			// echo '</pre>';
			// die();

			$this->session->set_flashdata('ret', $ret);
			redirect('/admin/view_suppliercosts');
		} else {
			$this->render('import_suppliercosts', 'admin', []);
		}
	}

	public function import_usercosts() {
		if (count($_FILES)) {
			$ret = [
				'alert_type' => 'import_ok',
				'entries' => 0,
				'usercosts_ext' => 0,
				'usercosts_ins' => 0,
				'usercosts_not' => 0,
				'usercosts_not_list' => '',
			];

			include_once('./application/helpers/import_usercosts_data.php');

			// die();
			$this->session->set_flashdata('ret', $ret);
			redirect('/admin/view_usercosts');
		} else {
			$this->render('import_usercosts', 'admin', []);
		}
	}

	### BILANCI - EXPORT

	public function export_balance() {
		if ($this->input->post()) {
			include_once('./application/helpers/export_balance.php');
		} else {
			$data_view = [];
			$this->render('export_balance', 'admin', $data_view);
		}
	}

	### BILANCI - REPORT

	public function report_balance() {
		if ($this->input->post()) {
			$posted = $this->input->post(NULL, TRUE);

			$data_view['data'] = $this->reports_balance_model->get_data($posted);
			$this->render('/report/balance/view_report', 'admin', $data_view);
		} else {
			$data_view['costcenters'] = $this->costcenters_model->get_costcenters();
			$this->render('/report/balance/create_report', 'admin', $data_view);
		}
	}

	public function export_report() {
		if ($this->input->post()) {
			include_once('./application/helpers/export_report.php');
		} else {
			redirect('/admin/report_balance');
		}
	}

	### DUMMY =========================================================================

	public function dummy() {
		/*
		Questo è un metodo che serve a fare operazioni di recovery o alignment dei dati, a uso e consumo del tecnico che mantiene la piattaforma
		*/
		// if ($_SERVER['REMOTE_ADDR'] !== "79.42.66.96")
			die();
		// echo '<h1>Bentornato Maestro</h1>';
		// $result = $this->db->select('*')->from('docs_users')->join('docs', 'docs_users.id_doc = docs.id_doc')->where("name LIKE '%dicembre%' AND created_at LIKE '2018-12-%'")->get()->result_array();
		// foreach ($result as $row) {
			// echo '<pre>';
			// print_r($row);
			// echo '</pre>';
			// $this->db->delete('docs_users', array('id_doc' => $row['id_doc'], 'id_user' => $row['id_user']));
		// }
		
		// $result = $this->db->query("SELECT id_doc, name FROM docs WHERE id_doc NOT IN (SELECT id_doc FROM docs_users)");
		// foreach ($result->result() as $row) {
			// echo '<pre>';
			// print_r($row);
			// echo '</pre>';
			// $this->db->delete('docs', array('id_doc' => $row->id_doc));
		// }
	}
	
}
