<?php
namespace PhpOffice;

require_once('./lib/phpspreadsheet/PhpOffice/autoload.php');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;

$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader("Xlsx");
$spreadsheet = $reader->load($_FILES['package']['tmp_name']);
$worksheet = $spreadsheet->getActiveSheet();
$rows = $worksheet->toArray();

/*
Array
(
    [0] => Codice azienda
    [1] => Codice fiscale azienda
    [2] => Denominazione
    [3] => Codice dipendente
    [4] => Codice fiscale dipendente
    [5] => Cognome
    [6] => Nome
    [7] => Data assunzione
    [8] => Data cessazione
    [9] => DATA ELABORAZIONE
    [10] => Mese da
    [11] => Anno da
    [12] => Mese a
    [13] => Anno a
    [14] => Codice indirizzamento
    [15] => Descrizione indirizzamento
    [16] => Costo retribuzione
    [17] => Costo contributivo
    [18] => Costo inail
    [19] => Ore ordinarie
    [20] => Ore straordinarie
    [21] => Totale costo
    [22] => Totale ore
    [23] => Costo medio
    [24] => Descrizione tipologia ripartizione 1
    [25] => Codice ripartizione 1
    [26] => Descrizione ripartizione 1
)
*/

function convertXlsDateToMySql($date) {
    if ($date != "") {
        $new_date = \DateTime::createFromFormat('n/j/Y', $date);
        return $new_date->format('Y-m-d');
    }
    return null;
}

foreach ($rows as $row_idx => $row_data) { // key is the row count(starts from 0)
    // echo '<pre>';
    // print_r($row_data);
    // echo '</pre>';
    // foreach ($row_data as $col_idx => $col_val) {};

    if ($row_data[21] != 0 AND !empty($row_data[25])) {   // se sto analizando una riga dove è presente un totale retribuzione
        $ret['entries']++;
        if (!$this->usercosts_model->get_usercost(null, $row_data[4], convertXlsDateToMySql($row_data[9]), $row_data[12], $row_data[13], (!empty($row_data[25]) ? $row_data[25] : null), $row_data[3])) {
            // Recupero le info sul profilo dell'utente
            $user_data = $this->db->from('users_info')->where("fiscal_code = '".$row_data[4]."'")->get()->row_array();
            // Aggiorno il costo orario dell'utente in questione
            if ($user_data) {
                $userwork_data = [
                    'hourly_cost' => $row_data[23]
                ];
                $this->users_model->update_fields($userwork_data, 'users_work', 'id_user = '.$user_data['id_user']);
            }
            // Cerco se il centro di costo che devo inserire esiste
            $cc_data = $this->db->from('b_costcenters')->where("id = ".$row_data[25])->get();
            // Importo il costo dipendente
            $usercost_data = [
                'id_user' => ($user_data ? $user_data['id_user'] : NULL),
                'id_costcenter' => $cc_data ? $row_data[25] : NULL,
                'cod_dipendente' => $row_data[3],
                'codfisc' => $row_data[4],
                'nome' => $row_data[6],
                'cognome' => $row_data[5],
                'data_assunzione' => convertXlsDateToMySql($row_data[7]),
                'data_cessazione' => convertXlsDateToMySql($row_data[8]),
                'data_elaborazione' => convertXlsDateToMySql($row_data[9]),
                'period_month' => $row_data[12],
                'period_year' => $row_data[13],
                'amount' => $row_data[21],
                'hours' => $row_data[22],
                'hourly_cost' => $row_data[22] > 0 ? ((float)$row_data[21] / (float)$row_data[22]) : $row_data[23],
                // 'hourly_cost' => $row_data[23],
            ];
            if ($this->usercosts_model->insert_usercost($usercost_data))
                $ret['usercosts_ins']++;
            else {
                $ret['usercosts_not']++;
                $ret['usercosts_not_list'] .= $row_data[6].' '.$row_data[5].' - '.$row_data[21]."€<br>";
            }
        } else {
            $ret['usercosts_ext']++;
        }
    }
    if ($ret['entries'] == 0) { // se non sono riuscito ad importare neanche una riga, probabilmente il file non è conforme
        $ret = [
            'alert_type' => 'import_ko',
            'error' => '<b>File non conforme</b>.<br>Controlla tutti i dati, in particolare la colonna del Totale retribuzione e quella con l\'id del centro di costo'
        ];
    }
};

// die();