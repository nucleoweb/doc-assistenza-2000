<?php
namespace PhpOffice;

require_once('./lib/phpspreadsheet/PhpOffice/autoload.php');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;

$data = json_decode(base64_decode($_POST['data']));

# INIT SPREADSHEET
$spreadsheet = new Spreadsheet();
$numsheet = 0;

$sheetIndex = $spreadsheet->getIndex(
    $spreadsheet->getSheetByName('Worksheet')
);
$spreadsheet->removeSheetByIndex($sheetIndex);

# INIT SHEET
$numsheet++;
$sheetname = 'REPORT';
$newsheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, $sheetname);
$spreadsheet->addSheet($newsheet, $numsheet);
$sheet = $spreadsheet->getSheetByName($sheetname);

$numrow = 2;
$lettercol = 'A';

# LAYOUT
$sheet->getColumnDimension('A')->setWidth(20);
foreach (range('A', 'ZZ') as $letter)
    $sheet->getColumnDimension($letter)->setWidth(20);

# CONTENT
foreach ($data as $year => $months) {
    foreach ($months as $month => $centers) {

        $sheet->setCellValue('A'.$numrow, $year.' '.monthNameByNum($month));
        $sheet->getStyle('A'.$numrow)->getFont()->setBold(true);
        $sheet->getStyle('A'.$numrow)->getFont()->setSize(18);

        $numrow += 2;

        $lettercol = 'B';
        foreach ($centers as $center => $sums) {
            $sheet->setCellValue($lettercol.$numrow, ($center == 'x' ? 'TOTALE' : $center));
            $sheet->getStyle($lettercol.$numrow)->getAlignment()->setWrapText(true);
            $sheet->getStyle($lettercol.$numrow)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            $sheet->getStyle($lettercol.$numrow)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle($lettercol.$numrow)->getFont()->setBold(true);
            $sheet->getStyle($lettercol.$numrow)->getFont()->setSize(9);
            $lettercol++;
        }
        $numrow++;
        
        $sheet->setCellValue('A'.$numrow, 'RICAVI');
        $sheet->getStyle('A'.$numrow)->getFont()->setBold(true);
        $lettercol = 'B';
        foreach ($centers as $center => $sums) {
            $sheet->getStyle($lettercol.$numrow)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $sheet->setCellValue($lettercol.$numrow, $sums->revenues);
            // $sheet->getStyle($lettercol.$numrow)->getNumberFormat()->setFormatCode('€ #,##0;€ -#,##0');
            $lettercol++;
        }
        $numrow++;

        $sheet->setCellValue('A'.$numrow, 'COSTI DIPENDENTI');
        $sheet->getStyle('A'.$numrow)->getFont()->setBold(true);
        $lettercol = 'B';
        foreach ($centers as $center => $sums) {
            $sheet->getStyle($lettercol.$numrow)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $sheet->setCellValue($lettercol.$numrow, $sums->usercosts);
            // $sheet->getStyle($lettercol.$numrow)->getNumberFormat()->setFormatCode('€ #,##0;€ -#,##0');
            $lettercol++;
        }
        $numrow++;
            
        $sheet->setCellValue('A'.$numrow, 'COSTI FORNITORI');
        $sheet->getStyle('A'.$numrow)->getFont()->setBold(true);
        $lettercol = 'B';
        foreach ($centers as $center => $sums) {
            $sheet->getStyle($lettercol.$numrow)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $sheet->setCellValue($lettercol.$numrow, $sums->suppliercosts);
            // $sheet->getStyle($lettercol.$numrow)->getNumberFormat()->setFormatCode('€ #,##0;€ -#,##0');
            $lettercol++;
        }
        $numrow++;
             
        $sheet->setCellValue('A'.$numrow, 'SALDO');
        $sheet->getStyle('A'.$numrow)->getFont()->setBold(true);
        $lettercol = 'B';
        foreach ($centers as $center => $sums) {
            $tot = $sums->revenues - ($sums->usercosts + $sums->suppliercosts);
            $sheet->getStyle($lettercol.$numrow)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $sheet->getStyle($lettercol.$numrow)->applyFromArray(['fill' => ['fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID, 'startColor' => ['argb' => ($tot >= 0 ? 'FFEAF7C5' : 'FFF9D9D1')]]]);
            $sheet->setCellValue($lettercol.$numrow, '='.$lettercol.($numrow-3).'-SUM('.$lettercol.($numrow-2).':'.$lettercol.($numrow-1).')');
            $lettercol++;
        }
        $numrow++;

        $numrow += 3;
    }
}

# OUTPUT FILE
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="'.date("Y-m-d_His").'_report_bilancio_Assistenza2000.xlsx"');
header('Cache-Control: max-age=0');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$writer = new Xlsx($spreadsheet);
// ob_end_flush();
$writer->save('php://output');
?>