<?php
function rvd($date) {
    $date = trim(trim(trim($date)));
    if (strpos($date, "-")) // formato YYYY-MM-DD
        return implode("/", array_reverse(explode("-", $date)));
    return implode("-", array_reverse(explode("/", $date)));
}

function calc_age($birth_date) {
    if (strpos($birth_date, "/"))
        $birth_date = rvd($birth_date);
    $d1 = new DateTime();
    $d2 = new DateTime($birth_date);
    $diff = $d2->diff($d1);
    return $diff->y;
}

function monthNameByNum($i) {
    $month_names = ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre'];
    return $month_names[($i-1)];
}

function isWithinInterval($month, $year, $from_month, $from_year, $to_month, $to_year) {
    // Convert all variables to a comparable format (e.g., year * 12 + month)
    $currentDate = $year * 12 + $month;
    $startDate = $from_year * 12 + $from_month;
    $endDate = $to_year * 12 + $to_month;

    // Check if the current date is within the interval
    return ($currentDate >= $startDate && $currentDate <= $endDate);
}

function getMonthDiff($from_month, $from_year, $to_month, $to_year) {
    $startDate = $from_year * 12 + $from_month;
    $endDate = $to_year * 12 + $to_month;
    return $endDate - $startDate + 1;
}

function generateMonthsArray($dateRange) {
    $result = [];
    
    $currentYear = $dateRange['from_year'];
    $currentMonth = $dateRange['from_month'];
    $endYear = $dateRange['to_year'];
    $endMonth = $dateRange['to_month'];
    
    while ($currentYear < $endYear || ($currentYear == $endYear && $currentMonth <= $endMonth)) {
        $result[] = ['month' => $currentMonth, 'year' => $currentYear];
        
        // Move to the next month
        $currentMonth++;
        if ($currentMonth > 12) {
            $currentMonth = 1;
            $currentYear++;
        }
    }
    
    return $result;
}
?>