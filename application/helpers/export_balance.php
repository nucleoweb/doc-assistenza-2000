<?php
namespace PhpOffice;

require_once('./lib/phpspreadsheet/PhpOffice/autoload.php');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Chart\Chart;
use PhpOffice\PhpSpreadsheet\Chart\DataSeries;
use PhpOffice\PhpSpreadsheet\Chart\DataSeriesValues;
use PhpOffice\PhpSpreadsheet\Chart\Legend as ChartLegend;
use PhpOffice\PhpSpreadsheet\Chart\PlotArea;
use PhpOffice\PhpSpreadsheet\Chart\Title;
// use PhpOffice\PhpSpreadsheet\Chart\Axis;

ini_set("display_errors", 1);
error_reporting(E_ALL);

$start_time = microtime(true);

# FUNCTIONS
function exec_time($start_time) {
    $actual_time = microtime(true);
    $diff = $actual_time - $start_time;
    if ($diff < 60)
        return  round($diff)." secondi";
    else
        return floor($diff / 60)." minuti e ".round($diff % 60)." secondi";
}

function getMaxCol($num) {
    $result = '';
    while ($num > 0) {
        $num--;  // Decrease by 1 because Excel column index starts from 1 (not 0)
        $remainder = $num % 26;
        $result = chr(65 + $remainder) . $result;  // chr(65) is 'A'
        $num = floor($num / 26);
    }
    return $result;
}

# SETTINGS
$style = true;
$time_debug = false;

# INIT SPREADSHEET
$spreadsheet = new Spreadsheet();
$numsheet = 0;

$sheetIndex = $spreadsheet->getIndex(
    $spreadsheet->getSheetByName('Worksheet')
);
$spreadsheet->removeSheetByIndex($sheetIndex);

# DATA FROM DATABASE
$cats = $this->db->from("b_costcats")->where('id != 1000')->get()->result_array();
$centers = $this->db->from("b_costcenters")->where('id != 1000')->get()->result_array();
// Recupero tutti i bandi/finanziamenti che hanno come competenza almeno una parte dell'anno che sto considerando
$all_financings = $this->financings_model->getFinancingsByYear($_POST['year']);

$id_cat_costo_personale = 15;

if ($time_debug) echo "<pre style='color: #d90000; font-weight: 600;'>TEMPO STEP 1: ".exec_time($start_time)."</pre>";

# CICLO SUI MESI DELL'ANNO - OGNI MESE È UN FOGLIO
for ($m = 1; $m <= 12; $m++) {
    if ($time_debug) echo "<pre style='color: #d90000; font-weight: 600;'>TEMPO MESE ".monthNameByNum($m).": ".exec_time($start_time)."</pre>";

    # INIT SHEET
    $numsheet++;
    $sheetname = monthNameByNum($m);
    $newsheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, $sheetname);
    // $spreadsheet->addSheet($newsheet, $numsheet);
    $spreadsheet->addSheet($newsheet);
    $sheet = $spreadsheet->getSheetByName($sheetname);

    # LAYOUT
    $sheet->getColumnDimension('A')->setWidth(60);

    # STYLE
    if ($style) {
        $greybold = [
            'font' => ['bold' => true, 'size' => 12],
            'fill' => ['fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID, 'startColor' => ['argb' => 'FFEEEEEE']],
            'borders' => ['allBorders' => ['borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN, 'color' => ['argb' => 'FF000000']]]
        ];
        $sheet->getStyle('A1:A100')->applyFromArray(['alignment' => ['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT]]);
        $sheet->getStyle('A10:ZZ10')->applyFromArray($greybold);
    }

    # DATA BY MONTH
    // $financings = $this->financings_model->getFinancingsByMonthYear($m, $_POST['year']);
    $financings = $all_financings;
    $m_costcenters = $this->costcenters_model->getCostCentersByMonthYear($m, $_POST['year']);

    # CONTENT
    # COLONNA A
    if ($time_debug) echo "<pre style='color: #d90000; font-weight: 600;'>TEMPO MESE ".monthNameByNum($m)." - INIZIO COLONNA A: ".exec_time($start_time)."</pre>";
    $sheet->setCellValue('A6', "Centri di costo");
    $sheet->setCellValue('A7', "Numero buste");
    $sheet->setCellValue('A8', "Località");
    if ($style) $sheet->getStyle('A9')->applyFromArray(['font' => ['bold' => true, 'size' => 18]]);
    $sheet->setCellValue('A9', "SERVIZI");
    if ($style) $sheet->getStyle('A9')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
    $sheet->setCellValue('A10', "RICAVI");
    $sheet->setCellValue('A11', "Costo del personale");

    $numrow = 12;
    foreach ($financings as $fin) {
        $sheet->setCellValue('A'.$numrow, $fin['name']);
        if ($style) $sheet->getStyle('A'.$numrow)->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);
        $numrow++;
    }

    foreach ($cats as $cat) {
        if ($cat['id'] != $id_cat_costo_personale) {
            $sheet->setCellValue('A'.$numrow, $cat['name']);
            $numrow++;
        }
    }

    if ($style) $sheet->getStyle('A'.$numrow.':ZZ'.$numrow)->applyFromArray($greybold);
    $sheet->setCellValue('A'.$numrow, "TOTALE COSTI");
    $numrow++;
    if ($style) $sheet->getStyle('A'.$numrow.':ZZ'.$numrow)->applyFromArray($greybold);
    $sheet->setCellValue('A'.$numrow, "MARGINE OPERATIVO LORDO");
    $numrow++;
    if ($style) $sheet->getStyle('A'.$numrow.':ZZ'.$numrow)->applyFromArray($greybold);
    $sheet->setCellValue('A'.$numrow, "M.O.L. IN %");

    # COLONNA B
    if ($time_debug) echo "<pre style='color: #d90000; font-weight: 600;'>TEMPO MESE ".monthNameByNum($m)." - INIZIO COLONNA B: ".exec_time($start_time)."</pre>";
    $sheet->setCellValue('B5', "Tot. paghe");
    $sheet->setCellValue('B7', '=SUM(D7:ZZ7)');
    if ($style) $sheet->getStyle('B9')->applyFromArray($greybold);
    if ($style) $sheet->getStyle('B9')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
    if ($style) $sheet->getStyle('B9')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    $sheet->setCellValue('B9', "TOTALE");
    $sheet->setCellValue('B10', '=SUM(D10:ZZ10)');

    $sheet->getColumnDimension('B')->setWidth(15);
    if ($style) $sheet->getStyle('B10:B'.(10+count($financings)+count($cats)+2))->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

    $sheet->setCellValue('B11', '=SUM(D11:ZZ11)');

    $numrow = 12;
    foreach ($financings as $fin) {
        $sheet->setCellValue('B'.$numrow, '=SUM(D'.$numrow.':ZZ'.$numrow.')');
        if ($style) $sheet->getStyle('B'.$numrow)->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);
        $numrow++;
    }

    foreach ($cats as $cat) {
        if ($cat['id'] != $id_cat_costo_personale) {
            $sheet->setCellValue('B'.$numrow, '=SUM(D'.$numrow.':ZZ'.$numrow.')');
            $numrow++;
        }
    }

    $sheet->setCellValue('B'.$numrow, '=SUM(B11:B'.($numrow-1).')');    // TOTALE COSTI
    $numrow++;
    $sheet->setCellValue('B'.$numrow, '=B10-B'.($numrow-1));    // MARGINE OPERATIVO LORDO
    $numrow++;
    $sheet->setCellValue('B'.$numrow, '=IFERROR(ROUND(B'.($numrow-1).'/B10,2),0)');    // M.O.L. IN %
    if ($style) $sheet->getStyle('B'.$numrow)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE_00);

    # COLONNA C
    if ($time_debug) echo "<pre style='color: #d90000; font-weight: 600;'>TEMPO MESE ".monthNameByNum($m)." - INIZIO COLONNA C: ".exec_time($start_time)."</pre>";
    $sheet->setCellValue('C5', '=B7');    // TOT PAGHE
    $sheet->setCellValue('C8', "%");    // TOT PAGHE
    if ($style) $sheet->getStyle('C10:C'.(10+count($financings)+count($cats)+3))->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE_00);
    $sheet->setCellValue('C10', '=B10/B10');
    $sheet->setCellValue('C11', '=B11/B10');

    $numrow = 12;
    foreach ($financings as $fin) {
        $sheet->setCellValue('C'.$numrow, '=B'.$numrow.'/B10');
        if ($style) $sheet->getStyle('C'.$numrow)->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);
        $numrow++;
    }

    foreach ($cats as $cat) {
        if ($cat['id'] != $id_cat_costo_personale) {
            $sheet->setCellValue('C'.$numrow, '=B'.$numrow.'/B10');
            $numrow++;
        }
    }

    $sheet->setCellValue('C'.$numrow, '=B'.$numrow.'/B10');
    $numrow++;
    $sheet->setCellValue('C'.$numrow, '=B'.$numrow.'/B10');

    # COLONNE DALLA D IN POI
    if ($time_debug) echo "<pre style='color: #d90000; font-weight: 600;'>TEMPO MESE ".monthNameByNum($m)." - INIZIO COLONNA D: ".exec_time($start_time)."</pre>";
    if ($style) {
        $sheet->getStyle('D8:ZZ9')->getAlignment()->setWrapText(true);
        $sheet->getStyle('D8:ZZ9')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->getStyle('D8:ZZ9')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D8:ZZ9')->getFont()->setBold(true);
        $sheet->getStyle('D8:ZZ9')->getFont()->setSize(8);
        $sheet->getStyle('D10:ZZ'.(10+count($financings)+count($cats)+2))->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
    }

    $letter = 'D';

    $costcenters = $this->costcenters_model->get_real_costcenters();
    foreach ($costcenters as $index => $center) {
        if ($time_debug) echo "<pre style='color: #333; font-weight: 600;'>TEMPO MESE ".monthNameByNum($m)." - COLONNA D - CENTRO DI COSTO ".($index+1).": ".exec_time($start_time)."</pre>";
        // Calcolo il num di buste per il centro di costo
        $num_buste = $this->usercosts_model->getNumBusteByCC($center['id'], $m, $_POST['year']);
        $tot_buste = $this->usercosts_model->getNumBusteByMonthYear($m, $_POST['year']);

        if ($style) $sheet->getColumnDimension($letter)->setWidth(13);
        $sheet->setCellValue($letter.'6', $center['id']);
        $sheet->setCellValue($letter.'7', $num_buste);
        $sheet->setCellValue($letter.'8', $center['location']);
        $sheet->setCellValue($letter.'9', $center['name']);
        if ($time_debug) echo "<pre style='color: green; font-weight: 600;'>TEMPO MESE ".monthNameByNum($m)." - COLONNA D - CENTRO DI COSTO ".($index+1)." - INIT COMPLETATO: ".exec_time($start_time)."</pre>";

        ############ RICAVI DEL MESE ############
                    $tot = $this->revenues_model->calcCCsumByMonthYear($center['id'], $m, $_POST['year'], $num_buste, $tot_buste);
                    // Inserisco il totale nella casella
                    $sheet->setCellValue($letter.'10', (float)$tot);
                    if ($time_debug) echo "<pre style='color: green; font-weight: 600;'>TEMPO MESE ".monthNameByNum($m)." - COLONNA D - CENTRO DI COSTO ".($index+1)." - RICAVI COMPLETATI: ".exec_time($start_time)."</pre>";

        ############ COSTI PERSONALE DEL MESE ############
                    $tot = $this->usercosts_model->calcCCsumByMonthYear($center['id'], $m, $_POST['year'], $num_buste, $tot_buste);
                    // Inserisco il totale nella casella
                    $sheet->setCellValue($letter.'11', (float)$tot);
                    if ($time_debug) echo "<pre style='color: green; font-weight: 600;'>TEMPO MESE ".monthNameByNum($m)." - COLONNA D - CENTRO DI COSTO ".($index+1)." - COSTI PERSONALE COMPLETATI: ".exec_time($start_time)."</pre>";

        ############ BANDI - FINANZIAMENTI DEL MESE ############
                    $numrow = 12;
                    foreach ($financings as $fin) {
                        $tot = $this->financings_model->calcCCsumByMonthYear($fin, $center['id'], $m, $_POST['year'], $num_buste, $tot_buste);
                        $sheet->setCellValue($letter.$numrow, (float)-1*$tot);
                        if ($style) $sheet->getStyle($letter.$numrow)->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_RED);
                        $numrow++;
                    }
                    if ($time_debug) echo "<pre style='color: green; font-weight: 600;'>TEMPO MESE ".monthNameByNum($m)." - COLONNA D - CENTRO DI COSTO ".($index+1)." - FINANZIAMENTI COMPLETATI: ".exec_time($start_time)."</pre>";

        ############ COSTI FORNITORI DEL MESE ############
                    foreach ($cats as $cat) {   // per ogni categoria di costo
                        if ($cat['id'] != $id_cat_costo_personale) {    // se la categoria non è quella "Costo del Personale" che viene gestita separatamente
                            $tot = $this->suppliercosts_model->calcCCsumByMonthYear($center['id'], $m, $_POST['year'], $cat['id'], $num_buste, $tot_buste);
                            // Inserisco il totale nella casella
                            $sheet->setCellValue($letter.$numrow, (float)$tot);
                            $numrow++;
                        }
                    }
                    if ($time_debug) echo "<pre style='color: green; font-weight: 600;'>TEMPO MESE ".monthNameByNum($m)." - COLONNA D - CENTRO DI COSTO ".($index+1)." - COSTI FORNITORE: ".exec_time($start_time)."</pre>";

        // Somme e percentuali
        $sheet->setCellValue($letter.$numrow, '=SUM('.$letter.'11:'.$letter.($numrow-1).')');    // TOTALE COSTI
        $numrow++;
        $sheet->setCellValue($letter.$numrow, '='.$letter.'10-'.$letter.($numrow-1));    // MARGINE OPERATIVO LORDO
        $numrow++;
        $sheet->setCellValue($letter.$numrow, '=IFERROR(ROUND('.$letter.($numrow-1).'/'.$letter.'10,2),0)');    // M.O.L. IN %
        if ($style) $sheet->getStyle($letter.$numrow)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE_00);

        $letter++;
    }
}

if ($time_debug) echo "<pre style='color: #d90000; font-weight: 600;'>TEMPO FINE REPORT MENSILI: ".exec_time($start_time)."</pre>";

########## AGGIUNGO IL FOGLIO "TOTALI PER SERVIZIO"
            $numsheet++;
            $sheetname = "Totali per servizio";
            $newsheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, $sheetname);
            $spreadsheet->addSheet($newsheet);
            $sheet = $spreadsheet->getSheetByName($sheetname);

            # COLONNA C
            $sheet->setCellValue('C6', 'LOCALITÀ');
            $sheet->setCellValue('C7', "SERVIZI");
            $sheet->setCellValue('C8', 'RICAVI');
            $sheet->setCellValue('C9', 'TOTALE COSTI');
            $sheet->setCellValue('C10', 'MARGINE OPERATIVO LORDO');
            $sheet->setCellValue('C11', 'M.O.L. IN %');

            # COLONNA D
            $sheet->setCellValue('D7', 'TOTALE');
            $sheet->setCellValue('D8', '=SUM(E8:BZ8)');        // Ricavi
            $sheet->setCellValue('D9', '=SUM(E9:BZ9)');        // Totale costi
            $sheet->setCellValue('D10', '=D8-D9');             // Margine operativo lordo
            $sheet->setCellValue('D11', '=IFERROR(ROUND(D10/D8,2),0)');   // M.O.L. in %

            # COLONNE DALLA E IN POI
            $letter = 'E';
            $ref_letter = 'D';
            $row_total_costs = 10+count($financings)+count($cats)+1;
            foreach ($costcenters as $center) {
                if ($style) $sheet->getColumnDimension($letter)->setWidth(13);
                $sheet->setCellValue($letter.'6', $center['location']);
                $sheet->setCellValue($letter.'7', $center['name']);
                $sheet->setCellValue($letter.'8', '=Gennaio!'.$ref_letter.'10+Febbraio!'.$ref_letter.'10+Marzo!'.$ref_letter.'10+Aprile!'.$ref_letter.'10+Maggio!'.$ref_letter.'10+Giugno!'.$ref_letter.'10+Luglio!'.$ref_letter.'10+Agosto!'.$ref_letter.'10+Settembre!'.$ref_letter.'10+Ottobre!'.$ref_letter.'10+Novembre!'.$ref_letter.'10+Dicembre!'.$ref_letter.'10');
                $sheet->setCellValue($letter.'9', '=Gennaio!'.$ref_letter.$row_total_costs.'+Febbraio!'.$ref_letter.$row_total_costs.'+Marzo!'.$ref_letter.$row_total_costs.'+Aprile!'.$ref_letter.$row_total_costs.'+Maggio!'.$ref_letter.$row_total_costs.'+Giugno!'.$ref_letter.$row_total_costs.'+Luglio!'.$ref_letter.$row_total_costs.'+Agosto!'.$ref_letter.$row_total_costs.'+Settembre!'.$ref_letter.$row_total_costs.'+Ottobre!'.$ref_letter.$row_total_costs.'+Novembre!'.$ref_letter.$row_total_costs.'+Dicembre!'.$ref_letter.$row_total_costs);
                $sheet->setCellValue($letter.'10', '=+'.$letter.'8-'.$letter.'9');
                $sheet->setCellValue($letter.'11', '=IFERROR(ROUND('.$letter.'10/'.$letter.'8,2),0)');
                if ($style) $sheet->getStyle($letter.'11')->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE_00);
                $letter++;
                $ref_letter++;
            }

            # STYLE
            if ($style) {
                $sheet->getStyle('E6:ZZ7')->getAlignment()->setWrapText(true);
                $sheet->getStyle('E6:ZZ7')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $sheet->getStyle('E6:ZZ7')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('E6:ZZ7')->getFont()->setBold(true);
                $sheet->getStyle('E6:ZZ7')->getFont()->setSize(8);
                $sheet->getStyle('C6:C11')->applyFromArray($greybold);
                $sheet->getStyle('C7')->applyFromArray(['font' => ['bold' => true, 'size' => 18]]);
                $sheet->getStyle('C7')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $sheet->getStyle('D7')->applyFromArray(['font' => ['bold' => true, 'size' => 14]]);
                $sheet->getStyle('D7')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('D7')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $sheet->getColumnDimension('A')->setWidth(1);
                $sheet->getColumnDimension('B')->setWidth(1);
                $sheet->getColumnDimension('C')->setWidth(30);
                $sheet->getColumnDimension('D')->setWidth(15);
                $sheet->getStyle('D8:ZZ10')->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $sheet->getStyle('D11:ZZ11')->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE_00);
            }

            if ($time_debug) echo "<pre style='color: #d90000; font-weight: 600;'>TEMPO FINE TOTALI PER SERVIZIO: ".exec_time($start_time)."</pre>";

########## AGGIUNGO IL FOGLIO "TOTALI"
            $numsheet++;
            $sheetname = "Totali";
            $newsheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, $sheetname);
            $spreadsheet->addSheet($newsheet);
            $sheet = $spreadsheet->getSheetByName($sheetname);

            # COLONNA C
            $sheet->setCellValue('C7', 'RICAVI');
            $sheet->setCellValue('C8', 'TOTALE COSTI');
            $sheet->setCellValue('C9', 'MARGINE OPERATIVO LORDO');
            $sheet->setCellValue('C10', 'M.O.L. IN %');

            # COLONNE DALLA D ALLA O
            $row_total_costs = 10+count($financings)+count($cats)+1;
            $letter = 'D';
            for ($m = 1; $m <= 12; $m++) {
                $sheet->setCellValue($letter.'6', substr(monthNameByNum($m), 0, 3).'-'.substr($_POST['year'], 2));  // nome mese
                $sheet->setCellValue($letter.'7', '='.monthNameByNum($m).'!B10');  // RICAVI
                $sheet->setCellValue($letter.'8', '='.monthNameByNum($m).'!B'.$row_total_costs);  // TOTALE COSTI
                $sheet->setCellValue($letter.'9', '='.$letter.'7-'.$letter.'8');  // MARGINE OPERATIVO LORDO
                $sheet->setCellValue($letter.'10', '=IFERROR(ROUND('.$letter.'9/'.$letter.'7,2),0)');  // MARGINE OPERATIVO LORDO
                $sheet->getColumnDimension($letter)->setWidth(13);
                $letter++;
            }

            # TOTALI ANNO
            $sheet->setCellValue('P6', 'Tot anno '.$_POST['year']);
            $sheet->setCellValue('P7', '=SUM(D7:O7)');
            $sheet->setCellValue('P8', '=SUM(D8:O8)');
            $sheet->setCellValue('P9', '=SUM(D9:O9)');
            $sheet->setCellValue('P10', '=+P9/P7');

            # STYLE
            if ($style) {
                $sheet->getColumnDimension('A')->setWidth(1);
                $sheet->getColumnDimension('B')->setWidth(1);
                $sheet->getColumnDimension('C')->setWidth(30);
                $sheet->getColumnDimension('P')->setWidth(20);
                $sheet->getStyle('D6:P6')->applyFromArray($greybold);
                $sheet->getStyle('C7:C10')->applyFromArray($greybold);
                $sheet->getStyle('D7:ZZ9')->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $sheet->getStyle('D10:ZZ10')->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE_00);
            }

            # GRAFICO ANNO
            // Define the data series
            $dataSeriesLabels = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, $sheetname.'!$C$7', null, 1), // Revenues
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, $sheetname.'!$C$8', null, 1), // Costs
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, $sheetname.'!$C$9', null, 1), // Gross Operating Margin
                // new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, $sheetname.'!$C$10', null, 1), // G.O.M. (%)
            ];

            $xAxisTickValues = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, $sheetname.'!$D$6:$P$6', null, 13), // Months + Total
            ];

            $dataSeriesValues = [
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_NUMBER, $sheetname.'!$D$7:$P$7', null, 13), // Revenues data
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_NUMBER, $sheetname.'!$D$8:$P$8', null, 13), // Costs data
                new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_NUMBER, $sheetname.'!$D$9:$P$9', null, 13), // Gross Operating Margin data
                // new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_NUMBER, $sheetname.'!$D$10:$P$10', null, 13), // G.O.M. (%) data
            ];

            // Build the DataSeries
            $series = new DataSeries(
                DataSeries::TYPE_BARCHART,        // plotType
                DataSeries::GROUPING_CLUSTERED,   // plotGrouping
                range(0, count($dataSeriesValues) - 1), // plotOrder
                $dataSeriesLabels,                // plotLabel
                $xAxisTickValues,                 // plotCategory
                $dataSeriesValues                 // plotValues
            );

            // Set additional options
            $series->setPlotDirection(DataSeries::DIRECTION_COL);
            // $series->setPlotDirection(DataSeries::DIRECTION_BAR);

            // Create the plot area
            $plotArea = new PlotArea(null, [$series]);

            // Define the chart legend
            $legend = new ChartLegend(ChartLegend::POSITION_BOTTOM, null, false);

            // Define the chart axis
            // $xAxis = new Axis();
            // $yAxis = new Axis();

            $title = new Title('Grafico andamento annuo');
            $xAxisLabel = new Title('Periodo');
            $yAxisLabel = new Title('Importo');

            // Create the chart
            $chart = new Chart(
                'sample_chart',                   // chart name
                $title,   // chart title
                $legend,                          // legend
                $plotArea,                        // plotArea
                true,                             // plot visible
                DataSeries::EMPTY_AS_GAP,                                // displayBlanksAs
                $xAxisLabel,                // X-axis title
                $yAxisLabel             // Y-axis title
            );

            // Set the position where the chart will appear
            $chart->setTopLeftPosition('C13');
            $chart->setBottomRightPosition('Q42');

            // Add the chart to the worksheet
            $sheet->addChart($chart);

if ($time_debug) die("HO FINITO - DEVO FARE L'OUTPUT");

# OUTPUT FILE
if (0) {
    ob_start();
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename="'.date("Y-m-d").'_bilancio_di_periodo_'.$_POST['year'].'_Assistenza2000.xlsx"');
    header('Cache-Control: max-age=0');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    try {
        $writer = new Xlsx($spreadsheet);
        $writer->setIncludeCharts(true);
        $writer->save('php://output');
    } catch (Exception $e) {
        echo 'Errore durante l\'output del file: ',  $e->getMessage();
    }
    ob_end_flush();
} else {
    $temp_file = tempnam(sys_get_temp_dir(), 'temp_excel_file.xlsx');
    $writer = new Xlsx($spreadsheet);
    $writer->setIncludeCharts(true);
    $writer->save($temp_file);

    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename="'.date("Y-m-d").'_bilancio_di_periodo_'.$_POST['year'].'_Assistenza2000.xlsx"');
    header('Cache-Control: max-age=0');

    readfile($temp_file);
    unlink($temp_file);
}
?>