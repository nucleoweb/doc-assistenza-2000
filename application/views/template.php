<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<!--[if lt IE 7]><html class="ie ie6 lte9 lte8 lte7"><![endif]-->
<!--[if IE 7]><html class="ie ie7 lte9 lte8 lte7"><![endif]-->
<!--[if IE 8]><html class="ie ie8 lte9 lte8"><![endif]-->
<!--[if IE 9]><html class="ie ie9 lte9"><![endif]-->
<!--[if gt IE 9]><html><![endif]-->
<!--[if !IE]><!--><html><!--<![endif]-->
	<head>
	<title>Assistenza 2000 | Area documentale</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
	<meta charset="utf-8">
	<meta name="robots" content="noindex,nofollow">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<meta http-equiv="Content-Security-Policy" content="frame-ancestors 'none'">
	<meta http-equiv="X-Frame-Options" content="deny">

	<link rel="stylesheet" type="text/css" href="/assets/css/normalize.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/ivory.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/style.css">
	
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"> 

	<script type="text/javascript" src="/assets/js/jquery-3.7.0.min.js"></script>
	<script type="text/javascript" src="/assets/js/list.js"></script>
	
	<link rel="stylesheet" type="text/css" href="/assets/js/form-validator/theme-default.css">
	<script type="text/javascript" src="/assets/js/form-validator/jquery.form-validator.js"></script>
	<script type="text/javascript" src="/assets/js/form-validator/it.js"></script>
	
	<link rel="stylesheet" type="text/css" href="/assets/lib/jqueryui/jquery-ui.css">
	<script type="text/javascript" src="/assets/lib/jqueryui/jquery-ui.js"></script>
	<script type="text/javascript" src="/assets/lib/jqueryui/datepicker-it.js"></script>
	<link rel="stylesheet" type="text/css" href="/assets/lib/jqueryui/jquery-ui-timepicker-addon.css">
	<script type="text/javascript" src="/assets/lib/jqueryui/jquery-ui-timepicker-addon.js"></script>
	
	<script type="text/javascript" src="/assets/js/jquery.matchHeight.js"></script>

	<script src="/assets/lib/sweetalert2/sweetalert2.min.js"></script>
	<link rel="stylesheet" href="/assets/lib/sweetalert2/sweetalert2.min.css">

	<script src="/assets/lib/tippy/popper.min.js"></script>
	<script src="/assets/lib/tippy/tippy-bundle.umd.min.js"></script>
</head>
<body class="home">
	<div class="g1200" id="wrapper">
		<header class="row" id="testata">
			<div class="c4"><img id="logo" src="/assets/gfx/logo.jpg"></div>
			<div class="c8">
				&copy;2017-<?=date("Y")?> Assistenza 2000<br>
				Vai al sito &rarr; <a href="http://www.assistenza2000.it/" target="_blank">www.assistenza2000.it</a><br>
				<span class="mini">Gestione tecnica piattaforma: <a href="https://www.nucleoweb.it" target="_blank">Nucleo Web</a></span>
			</div>
		</header>
		<?php if (!empty($logged_info)) { ?>
		<div class="row" id="topbar">
			<div class="c6 first_col">Sei collegato come: &nbsp;<i class="fa fa-cog"></i> <b><?=$logged_info['first_name']." ".$logged_info['last_name']?></b></div>
			<div class="c6 second_col"><a href="/logout" class="btn" id="logout_btn"><i class="fa fa-sign-out"></i> Logout</a></div>
		</div>
		<?php } ?>
		<div id="content">
			<?php
			if (!empty($logged_info) AND $logged_info['username'] == 'admin') {
				$this->load->view('admin/menu');
			}
			?>
			<?php echo $content; ?>
		</div>
		<footer>
			<div class="row">
				<div class="c6 first">Piattaforma ottimizzata per la consultazione con i browser Google Chrome e Mozilla Firefox</div>
				<div class="c6 last">Per segnalare eventuali problematiche nell'utilizzo scrivere a <a href="mailto:info@assistenza2000.it">info@assistenza2000.it</a></div>
			</div>
		</footer>
	</div>
	
	<script>
	$(document).ready(function() {
		$(".datepicker").datepicker({
			changeMonth: true,
            changeYear: true,
			yearRange: 'c-50:c'
		});

		$('.balance_add_btn_link').click(function() {
			$('.balances_list_row > div.main').toggleClass('shrinked');
			$('.balances_list_row > div.side').toggleClass('expanded');
		});
	});
	</script>
</body>
</html>