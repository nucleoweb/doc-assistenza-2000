<?php
if (date("Y-m-d") < $poll['date_start'] OR $poll['date_end'] < date("Y-m-d"))
    header("Location: /profile");
?>

<div class="row space-bot">
	<div class="c12">
		<a href="/profile" class="back">Torna al tuo profilo</a>
	</div>
</div>
<h1>Vota sondaggio</h1>
<?php
// echo '<pre>';
// print_r($poll);
// echo '</pre>';

$date_vote = null;
foreach ($poll['users'] as $id_user => $usr_data)
    if ($id_user == $info['id_user'])
        $date_vote = $usr_data['date_vote'];

if (!$date_vote) {  # se non hai ancora votato
    ?>
    <div class="row">
        <div class="c6">
            <h3><?=$poll['title']?></h3>
            <p><?=$poll['info']?></p>
            <br>
            <?php if (!empty($poll['filename'])) { ?>
                <fieldset>
                    <p><i class="fa fa-file-o"></i> <b>Clicca per visualizzare l'allegato:</b> <a href="/serve_poll_doc/<?=$poll['req_string']?>" target="_blank"><?=$poll['filename']?></p></a>
                </fieldset>
            <?php } ?>
        </div>
        <div class="c6">
            <h5>Esprimi il tuo voto</h5>
            <hr>
            <form action="" method="POST" id="vote_form">
                <input type="hidden" name="id_poll" value="<?=$poll['id']?>">
                <input type="hidden" name="id_user" value="<?=$info['id_user']?>">
                <?php
                foreach ($poll['questions'] as $qst) {
                    echo '<h5>'.$qst['title'].'</h5>';
                    if ($qst['type'] == 'C') {  // domanda a risposta chiusa
                        echo '<div class="qst_item" data-qstid="'.$qst['id'].'">';
                        foreach ($poll['options'][$qst['id']] as $opt) {
                            echo '<div class="opt_wrapper">
                                    <input type="radio" name="opt_'.$qst['id'].'" id="opt_'.$qst['id'].'_'.$opt['id'].'" value="'.$opt['id'].'"> <label for="opt_'.$qst['id'].'_'.$opt['id'].'">'.$opt['title'].'</label>
                                </div>';
                        }
                        echo '</div>';
                    } else {    // domanda a risposta aperta
                        echo '<div class="qst_item" data-qstid="'.$qst['id'].'">';
                        echo '<div class="txt_wrapper"><textarea class="poll_textarea" id="txt_'.$qst['id'].'" name="txt_'.$qst['id'].'" placeholder="Scrivi qui"></textarea></div>';
                        echo '</div>';
                    }
                }
                ?>
                <br class="clear">
                <div class="infobox alert">
                    <?php if (0) { ?>
                        <?php if ($poll['anonymous']) { ?>
                            Il voto espresso sarà conteggiato ai fini dell'esito finale del sondaggio, in forma del tutto anonima. Non verrà salvata in alcun modo l'associazione tra il tuo utente e il voto espresso, ma verrà registrata soltanto la data in cui hai votato per il sondaggio, in modo da poter raccogliere informazioni sul numero di votanti, su chi non ha votato o deve ancora esprimere il suo voto e assicurarci che ogni persona possa votare soltanto una volta per ogni sondaggio in cui è coinvolta.<br>
                        <?php } else { ?>
                            Questo è un sondaggio non anonimo. Verrà registrato sulla piattaforma che tu, attraverso il tuo account, hai espresso una certa preferenza; questo dato sarà visibile soltanto agli amministratori.<br>
                        <?php } ?>
                        Una volta espressa la tua preferenza, non potrai più visualizzare l'opzione scelta sulla piattaforma, ma ti verrà inviata un'e-mail come ricevuta del voto espresso, indicando il titolo del sondaggio e quale preferenza hai espresso. Il voto, una volta inviato, non può essere cambiato; eventuali eccezioni a questa regola verranno comunicate dall'azienda.<br>
                        Conserva l'e-mail di conferma del voto: qualora dovesse sorgere la necessità di una verifica o riconteggio dei voti, ti verrà chiesto di mostrarla a testimonianza della preferenza espressa.<br>
                        Se riscontri un qualunque malfunzionamento del sistema di votazione o della correttezza della registrazione del tuo voto, contatta i tuoi riferimenti aziendali.
                    <?php } ?>

                    Consulta la guida al voto: <a style="background: #fff; padding: 5px 10px;" target="_blank" href="/Guida_al_voto_nei_sondaggi_Assistenza_2000.pdf">Clicca qui per visualizzare la guida</a><br><hr><br>
                    Scarica e leggi l'informativa: <a style="background: #fff; padding: 5px 10px;" target="_blank" href="/Info_lavoratori_sondaggi_e_voti_via_web.pdf">Clicca qui per scaricare il PDF</a>
                    <br><br>
                    <input type="checkbox" name="info_acceptance" id="info_acceptance"> <label for="info_acceptance">Confermo di aver letto integralmente l'informativa</label>
                    <br><br>
                    <input type="checkbox" name="acceptance" id="poll_acceptance"> <label for="poll_acceptance">Consento a partecipare al sondaggio/votazione</label>
                    <br><br>
                    <a href="#" class="btn" style="background: #fff;" id="vote_btn">Invia il tuo voto</a>
                </div>
            </form>
        </div>
    </div>
    <?php
} else {    # se invece hai già votato
    echo '<p>Hai già votato per questo sondaggio in data '.implode("/", array_reverse(explode("-", $date_vote))).'</p>';
}
?>

<script>
function check_votes() {
    voted = true;
    $('.qst_item').each(function() {
        qst_id = $(this).data('qstid');
        if ($(this).find("input[name='opt_"+qst_id+"']").length)
            if (!$(this).find("input[name='opt_"+qst_id+"']:checked").val())
                voted = false;
        if ($(this).find('.poll_textarea').length)
            if (!$(this).find('.poll_textarea').val())
                voted = false;
    });
    return voted;
}

$(document).ready(function() {
	$("#poll_acceptance, #info_acceptance").click(function() {
		if ($("#poll_acceptance").is(':checked') && $("#info_acceptance").is(':checked'))
			$("div.infobox.alert a.btn").fadeIn();
		else
			$("div.infobox.alert a.btn").hide();
	});
	
	$("#vote_btn").click(function() {
		if ($("#poll_acceptance").is(':checked') && $("#info_acceptance").is(':checked') && check_votes()) {
            if (confirm("Sei sicuro della tua scelta? Non potrai cambiare i tuoi voti una volta inviato"))
			    $("#vote_form").submit();
            else
                return false;
        }
        else {
            alert("Devi rispondere a tutte le domande e accettare le condizioni prima di inviare il tuo voto");
            return false;
        }
	});
});
</script>