<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php if ($logged == "no") { ?>
	<style>
	#logout_btn { display: none; }
	</style>
<?php } ?>

<script src="https://www.google.com/recaptcha/api.js?render=6LfaPdMUAAAAAN0SJQjvXyGPpQOL3aTlota8eKtX"></script>

<h1>Benvenuto nell'area documentale</h1>
<div class="row">
	<div class="c6" id="login_box">
		<h4>Inserisci i tuoi dati per accedere</h4>
		<form action="/login" method="POST">
			<input type="hidden" id="captcha_token" name="captcha_token" value="" required>
			<input type="text" name="usr" autocomplete="off" placeholder="Username">
			<input type="password" name="psw" autocomplete="off" placeholder="Password">
			<button type="submit" class="blue">ACCEDI</button>
		</form>
	</div>
	<div class="c6"></div>
</div>

<script>
$(document).ready(function() {
	grecaptcha.ready(function() {
		grecaptcha.execute('6LfaPdMUAAAAAN0SJQjvXyGPpQOL3aTlota8eKtX', {action: 'homepage'}).then(function(token) {
		   $("#captcha_token").val(token);
		});
	});
});
</script>