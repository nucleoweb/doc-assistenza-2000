<div class="row space-bot">
	<div class="c12">
		<?php if ($info['role'] == 'admin') { ?>
			<a href="/admin/edit_user/<?=$info['id_user']?>" class="back">Torna al profilo dell'utente</a>
		<?php } else { ?>
			<a href="/profile" class="back">Torna al tuo profilo</a>
		<?php } ?>
	</div>
</div>
<h1><i class="fa fa-lock"></i> Modifica accesso utente</h1>
<form class="admin" action="/change_user_access" method="POST">
	<div class="row">
		<div class="c6 accessdata_block">
			<p>Stai modificando i dati d'accesso per l'utente: <b><?=$info['first_name'].' '.$info['last_name']?></b></p>
			<br>
			<input type="hidden" name="id_user" value="<?=$info['id_user']?>">
			<input type="hidden" name="email" value="<?=$info['email']?>">
			<input type="hidden" name="first_name" value="<?=$info['first_name']?>">
			<input type="hidden" name="last_name" value="<?=$info['last_name']?>">
			<input type="hidden" name="send_data" value="1">
			<input type="text" name="username" placeholder="Username" value="<?=$info['username']?>" autocomplete="off" data-validation="length" data-validation-length="5-30">
			<input type="password" name="password_confirmation" placeholder="Password" value="" autocomplete="off" data-validation="strength" data-validation-strength="2" data-validation-length="8-20"> <a href="#" class="show_psw" data-fieldname="password_confirmation"><i class="fa fa-eye"></i></a>
			<input type="password" name="password" placeholder="Ripeti password" value="" autocomplete="off"  data-validation="confirmation"> <a href="#" class="show_psw" data-fieldname="password"><i class="fa fa-eye"></i></a>
			<br>
			<button type="submit" class="blue">Salva</button>
		</div>
		<div class="c6">
			<div class="infobox">
				Ricorda di salvare la tua password in quanto d'ora in poi non sarà più visibile in chiaro ma potrà solo essere modificata.<br>	
			</div>
		</div>
	</div>
</form>

<style>
input[type=password], input[type=text] {
	clear: none !important;
	display: inline-block !important;
	width: 80% !important;
}
a.show_psw {
		background: #eee;
		border: 1px solid #ddd;
		display: inline-block;
		height: 34px;
		line-height: 34px;
		text-align: center;
		width: 35px;
}
a.show_psw i.fa {
	margin: 0 auto;
}
</style>

<script>
$(document).ready(function() {
	$(".show_psw").click(function() {
		$("input[name='"+$(this).data('fieldname')+"']").attr('type', 'text');
		return false;
	});
});

$.validate({
	errorMessagePosition: 'top',
	modules: 'security',
	onModulesLoaded: function() {
		var optionalConfig = {
		  fontSize: '12pt',
		  padding: '4px',
		  bad: 'Pessimo',
		  weak: 'Debole',
		  good: 'Buono',
		  strong: 'Forte'
    };
    $('input[name="password_confirmation"]').displayPasswordStrength(optionalConfig);
  }
});
</script>