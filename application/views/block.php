<div style="background: #eee; padding: 40px; font-size: Tahoma; font-size: 24px !important; line-height: 28px !important;">
	<img id="logo" src="/assets/gfx/logo.jpg">
	<br>
	<p>È stato superato il limite di tentativi di login dal tuo indirizzo IP.<br>Devi aspettare prima di poter riprovare.</p>
	<a href="/" class="btn" style=" font-size: 16px !important; text-decoration: none; background: #2a317f; color: #fff !important; padding: 8px 20px; text-transform: uppercase;">Torna alla schermata di login</a>
</div>