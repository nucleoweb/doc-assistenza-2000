<h1><i class="fa fa-question-circle"></i> Storico sondaggi</h1>
<a class="btn green" href="/admin/insert_poll">Crea sondaggio</a>
<a style="background: #0085C6; color: #fff; display: inline-block; margin-left: 10px; height: 34px; line-height: 26px; padding: 5px 10px;" target="_blank" href="/Guida_al_voto_nei_sondaggi_Assistenza_2000.pdf">Consulta la guida al voto</a>
<a style="background: #0085C6; color: #fff; display: inline-block; margin-left: 10px; height: 34px; line-height: 26px; padding: 5px 10px;" target="_blank" href="/Info_lavoratori_sondaggi_e_voti_via_web.pdf">Scarica e leggi l'informativa</a>
<br><br>
<?php
// echo '<pre>';
// print_r($polls);
// echo '</pre>';

if (!count($polls)) {
	echo '<p>Non sono presenti sondaggi al momento</p>';
} else {
	?>
	<table>
		<tr>
			<th>ID</th>
			<th>Data inizio</th>
			<th>Data fine</th>
			<th>Titolo</th>
			<th>Stato</th>
			<th>Anonimo</th>
			<th>N. domande</th>
			<th>Num. utenti</th>
			<th>Voti ricevuti</th>
			<th>Affluenza</th>
			<th></th>
		</tr>
		<?php foreach ($polls as $p) { ?>
		<tr>
			<td style="width: 60px; text-align: center;"><?=$p['id']?></td>
			<td style="width: 120px; text-align: center;"><?=implode("/", array_reverse(explode("-", $p['date_start'])))?></td>
			<td style="width: 120px; text-align: center;"><?=implode("/", array_reverse(explode("-", $p['date_end'])))?></td>
			<td><?=$p['title']?></td>
			<td style="width: 160px; text-align: center; font-weight: bold;">
				<?php
				$stato = null;
				$state_color = '#333';
				if (date("Y-m-d") < $p['date_start']) {
					$stato = 'Programmato';
					$state_color = '#0085C6';
				}
				elseif ($p['date_start'] <= date("Y-m-d") AND date("Y-m-d") <= $p['date_end']) {
					$stato = 'Aperto';
					$state_color = '#51A351';
				}
				else {
					$stato = 'Chiuso';
					$state_color = '#D90000';
				}
				echo '<span style="color: '.$state_color.';">'.$stato.'</span>';
				?>
			</td>
			<td style="width: 120px; text-align: center;">
				<?=($p['anonymous'] ? 'Sì' : 'No')?>
			</td>
			<td style="width: 120px; text-align: center;">
				<?=count($p['questions'])?>
			</td>
			<td style="width: 120px; text-align: center;">
				<?=@count($p['users'])?>
			</td>
			<td style="width: 120px; text-align: center;">
				<?=$p['num_voted']?>
			</td>
			<td style="width: 120px; text-align: center;">
				<?php
					if ($stato == 'Programmato' OR count($p['users']) == 0)
						echo '-';
					else {
						$perc = round(($p['num_voted'] / count($p['users'])) * 100);
						echo $perc.'%';
					}
				?>
			</td>
			<td class="actions" style="width: 120px; text-align: center;">
				<?php if (date("Y-m-d") < $p['date_start']) { ?>
					<a href="/admin/edit_poll/<?=$p['id']?>" title="Modifica sondaggio"><i class="fa fa-pencil"></i></a>
					<a href="/admin/delete_poll/<?=$p['id']?>" title="Elimina sondaggio" onclick="return confirm('Sei sicuro di voler eliminare questo sondaggio?');"><i class="fa fa-trash"></i></a>
				<?php } ?>
				<?php if (date("Y-m-d") >= $p['date_start']) { ?>
					<a href="/admin/view_poll/<?=$p['id']?>" title="Guarda risultati sondaggio"><i class="fa fa-search"></i></a>
					<a href="/admin/export_poll/<?=$p['id']?>" title="Esporta in PDF"><i class="fa fa-file-pdf-o"></i></a>
				<?php } ?>
					<a href="/admin/clone_poll/<?=$p['id']?>" title="Clona sondaggio" onclick="return confirm('Sei sicuro di voler clonare questo sondaggio?');"><i class="fa fa-files-o"></i></a>
			</td>
		</tr>
		<?php } ?>
	</table>
	<?php
}
?>