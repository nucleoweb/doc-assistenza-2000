<div class="row space-bot">
	<div class="c12">
		<a href="/admin" class="back home">Torna all'homepage</a>
		<a href="/admin/view_doctypes" class="back">Torna ai tipi documento</a>
	</div>
</div>
<h1><i class="fa fa-file-text" aria-hidden="true"></i> Modifica tipo documento</h1>
<form class="admin detail_form" action="/admin/edit_doctype" method="POST">
	<input type="hidden" name="id_doctype" value="<?=@$doctype['id']?>">
	<label>Cod</label>
	<input type="text" name="cod" placeholder="Codice tipo documento" value="<?=@htmlentities($doctype['cod'])?>" data-validation="required">
	<label>Description</label>
	<input type="text" name="description" placeholder="Descrizione tipo documento" value="<?=@$doctype['description']?>">
	<label>Tipo operazione</label>
	<select name="type" required>
		<option value="plus" <?=($doctype['type'] == 'plus' ? 'selected' : '')?>>PLUS</option>
		<option value="minus" <?=($doctype['type'] == 'minus' ? 'selected' : '')?>>MINUS</option>
	</select>
	<br>
	<button type="submit" class="blue">Salva</button>
</form>