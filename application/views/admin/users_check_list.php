<h4><i class="fa fa-chain"></i> Associa gli utenti</h4>
<div id="users_list">
    <input type="checkbox" name="select_all" id="select_all"> <label for="select_all" style="color: #2A317F; font-weight: 600;">Seleziona tutti gli utenti</label> | 
    <label style="color: #2A317F; font-weight: 600;">Filtra: </label>
    <select id="user_check_filter" name="user_check_filter" style="width: 180px; display: inline-block;">
        <option value="">- Tutti gli utenti -</option>
        <option value="yes">Selezionati</option>
        <option value="no">Non selezionati</option>
    </select>
    <div id="users_filter_bar">
        <div id="users_filters_wrapper">
            <select class="list_filter" id="user_type_filter" name="user_type_filter">
                <option value="">- Tutti gli stati -</option>
                <option value="active">Attivi sulla piattaforma</option>
                <option value="not_active">Non attivi</option>
            </select>
            <select class="list_filter" id="user_role_filter" name="user_role_filter">
                <option value="">- Tutti i ruoli -</option>
                <option value="user">Utente</option>
                <option value="admin">Amministratore</option>
            </select>
            <select class="list_filter" id="user_job_filter" name="user_job_filter">
                <option value="">- Tutte le mansioni -</option>
                <?php
                foreach ($jobs as $job)
                    echo '<option value="'.$job['name'].'">'.$job['name'].'</option>';
                ?>
            </select>
            <select class="list_filter" id="user_level_filter" name="user_level_filter">
                <option value="">- Tutti i livelli -</option>
                <?php
                foreach ($levels as $level)
                    echo '<option value="'.$level['name'].'">'.$level['name'].'</option>';
                ?>
            </select>
            <select class="list_filter" id="user_area_filter" name="user_area_filter">
                <option value="">- Tutte le aree -</option>
                <?php
                foreach ($areas as $area)
                    echo '<option value="'.$area['name'].'">'.$area['name'].'</option>';
                ?>
            </select>
            <select class="list_filter" id="user_service_filter" name="user_service_filter">
                <option value="">- Tutti i servizi -</option>
                <?php
                foreach ($services as $serv)
                    echo '<option value="'.$serv['name'].'">'.$serv['name'].'</option>';
                ?>
            </select>
            <select class="list_filter" id="user_customer_filter" name="user_customer_filter">
                <option value="">- Tutti i clienti -</option>
                <?php
                foreach ($customers as $cust)
                    echo '<option value="'.$cust['name'].'">'.$cust['name'].'</option>';
                ?>
            </select>
            <select class="list_filter" id="user_freelance_filter" name="user_freelance_filter">
                <option value="">- Dipendenti e non -</option>
                <option value="0">Dipendenti</option>
                <option value="1">Liberi professionisti</option>
            </select>
            <select class="list_filter" id="user_rank_filter" name="user_rank_filter">
                <option value="">- Tutti i rank -</option>
                <option value="Senior">Senior</option>
                <option value="Junior">Junior</option>
                <option value="Temp">Temp</option>
            </select>
        </div>
    </div>
    <form class="admin"><input type="text" class="search" id="txt_search" placeholder="Cerca utente"></form>
    <div class="users_report_bar"><i class="fa fa-search"></i> <span id="users_found"></span> utenti visualizzati <div class="sep"></div> <i class="fa fa-check"></i> <span id="users_checked"></span> utenti selezionati</div>
    <ul class="list">
        <?php 					
        foreach ($users as $u) {
            echo '<li class="user_item '.(($u['role'] == "admin") ? 'admin' : '').' '.((!$u['active']) ? 'not_active' : '').'" data-state="'.(($u['active']) ? 'active' : 'not_active').'" data-job="'.$u['job'].'" data-check="'.((!empty($info['users']) AND array_key_exists($u['id_user'], $info['users'])) ? 'yes' : 'no').'" data-role="'.$u['role'].'" data-service="'.$u['service'].'" data-area="'.$u['area'].'" data-customer="'.$u['customer'].'" data-level="'.$u['level'].'" data-rank="'.$u['rank'].'" data-freelance="'.$u['freelance'].'">';
            ?>
                <input type="checkbox" class="check_user" name="users[]" value="<?=$u['id_user'] ?>" id="user_list_<?=$u['id_user'] ?>" <?=((!empty($info['users']) AND array_key_exists($u['id_user'], $info['users'])) ? 'checked' : '')?>>
                <b><label class="name" for="user_list_<?=$u['id_user'] ?>"><?=$u['last_name'].' '.$u['first_name'] ?></label></b>
                <span class="job"><i class="fa fa-briefcase"></i> <?=($u['job'] ? $u['job'] : '<i>n.d.</i>')?></span>
                <span class="service"><i class="fa fa-ambulance"></i> <?=($u['service'] ? $u['service'] : '<i>n.d.</i>')?></span>
                <?php
                if (isset($docs_list)) { 
                    if (!empty($info['users']) AND array_key_exists($u['id_user'], $info['users'])) {
                        if (!empty($info['users'][$u['id_user']]['last_download'])) {
                            echo '<i class="fa fa-cloud-download"></i> Download: <span class="date">'.date("d/m/Y - H:i", strtotime($info['users'][$u['id_user']]['last_download'])).'</span>';
                        } else {
                            echo '<i class="fa fa-cloud-download"></i> non scaricato';
                        }
                    }
                }
                ?>
            </li>
            <?php 
        }
        ?>
    </ul>
    <ul class="pagination"></ul>
</div>

<script>
function check_filter(item) {
	if ($("#user_check_filter").val() && $("#user_check_filter").val() != item.check)
		return false;
	if ($("#user_type_filter").val() && $("#user_type_filter").val() != item.state)
		return false;
	if ($("#user_job_filter").val() && $("#user_job_filter").val() != item.job)
		return false;
    if ($("#user_area_filter").val() && $("#user_area_filter").val() != item.area)
		return false;
	if ($("#user_service_filter").val() && $("#user_service_filter").val() != item.service)
		return false;
	if ($("#user_customer_filter").val() && $("#user_customer_filter").val() != item.customer)
		return false;
	if ($("#user_level_filter").val() && $("#user_level_filter").val() != item.level)
		return false;
    if ($("#user_freelance_filter").val() && $("#user_freelance_filter").val() != item.freelance)
		return false;
	if ($("#user_rank_filter").val() && $("#user_rank_filter").val() != item.rank)
		return false;
	return true;
}

function reindexList() {
	page_selected = 1;
	if ($('ul.pagination li.active').length)
		page_selected = $('ul.pagination li.active a').html();
	console.log("PAGE SELECTED: "+page_selected);

	qs = $('#txt_search').val();
	$('#txt_search').val(' ');
	usersList.search(); 
	usersList.filter();
	usersList.show(0, 10000);
	usersList.reIndex();
	$("#users_checked").html($(".user_item[data-check='yes']").length);
	$('#txt_search').val(qs);
	usersList.search(qs);
	usersList.filter(function(item) {
		return check_filter(item.values());
	});

	start_index = (page_selected - 1) * 30 + 1;
	console.log("START INDEX: "+start_index);
	usersList.show(start_index, 30);
}

$(document).ready(function() {
	$("#select_all").click(function() {
		if ($("#select_all").prop('checked') == true) {
			usersList.show(0, 10000);
			$("input.check_user").prop('checked', true);
			$(".user_item").attr("data-check", "yes");
			usersList.show(0, 30);
		} else {
			usersList.show(0, 10000);
			$("input.check_user").prop('checked', false);
			$(".user_item").attr("data-check", "no");
			usersList.show(0, 30);
		}
		reindexList();
	});
	
	$("#user_check_filter, #user_type_filter, #user_job_filter, #user_area_filter, #user_service_filter, #user_customer_filter, #user_level_filter, #user_freelance_filter, #user_rank_filter").change(function() {
		// usersList.filter();
		usersList.filter(function(item) {
			return check_filter(item.values());
		});
	});
	
	$(document).on('change', ".check_user", function() {
		if ($(this).prop('checked') == true)
			$(this).closest(".user_item").attr("data-check", "yes");
		else
			$(this).closest(".user_item").attr("data-check", "no");
		reindexList();
	});
});

var options = {
    valueNames: [
		'name',
		{ data: ['state'] },
		{ data: ['job'] },
		{ data: ['check'] },
        { data: ['area'] },
		{ data: ['service'] },
		{ data: ['customer'] },
		{ data: ['level'] },
        { data: ['freelance'] },
		{ data: ['rank'] }
	],
	page: 30,
	pagination: true
};
var usersList = new List('users_list', options);

usersList.on('updated', function() {
	$("#users_found").html(usersList.matchingItems.length);
});
$(function() {
	$("#users_found").html(usersList.matchingItems.length);
	reindexList();
});

$('.admin_users_list_form').submit(function() {
    <?php if (isset($polls_list)) { ?>
        if ($("#alert_sms").prop("checked") == true && $("#message_char_counter").hasClass("ko")) {
            alert("Hai ecceduto il limite di caratterti per il messaggio; invia il tuo messaggio solo tramite e-mail oppure controlla e riprova");
            return false;
        }
    <?php } ?>
	$('#txt_search').val(' ');
	usersList.search(); 
	usersList.filter();
	usersList.show(0, 10000);
	return true;
});

$(function() {
<?php if (!empty($info)) { ?>
	$("#user_check_filter option[value='yes']").prop('selected', true).trigger("change");
<?php } ?>
});
</script>