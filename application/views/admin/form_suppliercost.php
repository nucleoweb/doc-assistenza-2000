<div class="row space-bot">
	<div class="c12">
		<a href="/admin" class="back home">Torna all'homepage</a>
		<a href="/admin/view_suppliercosts" class="back">Torna ai costi fornitore</a>
	</div>
</div>
<h1><i class="fa fa-eur" aria-hidden="true"></i> Modifica costo fornitore</h1>
<form class="admin detail_form" action="/admin/edit_suppliercost" method="POST">
	<input type="hidden" name="id_suppliercost" value="<?=@$suppliercost['id']?>">
	<label>Fornitore</label>
	<select name="id_supplier">
		<option value="">- Seleziona un fornitore -</option>
		<?php
		foreach ($suppliers as $s)
			echo '<option value="'.$s['id'].'" '.($s['id'] == $suppliercost['id_supplier'] ? 'selected' : '').'>'.($s['denominazione'] ? $s['denominazione'] : $s['cognome'].' '.$s['nome']).'</option>';
		?>
	</select>
	<div class="row" style="padding: 20px; background: #f6f6f6;">
		<div class="c12 first">
			<label><b>Periodo di competenza</b></label><br><br>
			<select name="period_month" style="width: 45%; float: left; clear: none; margin: 0 10px 0 0;"><?php for ($m = 1; $m <= 12; $m++) { echo '<option value="'.$m.'" '.($m == $suppliercost['period_month'] ? 'selected' : '').'>'.monthNameByNum($m).'</option>'; } ?></select>
			<select name="period_year" style="width: 45%; float: left; clear: none; margin: 0 10px 0 0;"><?php for ($y = 2020; $y <= ((int)date('Y')+5); $y++) { echo '<option value="'.$y.'" '.($y == $suppliercost['period_year'] ? 'selected' : '').'>'.$y.'</option>'; } ?></select>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="c3">
			<label>Tipo documento</label>
			<input type="text" name="tipo_documento" placeholder="Tipo documento" value="<?=@$suppliercost['tipo_documento']?>">
		</div>
		<div class="c3">
			<label>Divisa</label>
			<input type="text" name="divisa" placeholder="Divisa" value="<?=@$suppliercost['divisa']?>">
		</div>
		<div class="c3">
			<label>Data documento</label>
			<input type="date" name="data" placeholder="Data" value="<?=@$suppliercost['data']?>">
		</div>
		<div class="c3">
			<label>Numero documento</label>
			<input type="text" name="numero" placeholder="Numero documento" value="<?=@$suppliercost['numero']?>">
		</div>
	</div>
	<div class="row">
		<div class="c4">
			<label>Importo totale</label>
			<input type="number" step="0.01" name="importo_totale" id="cost_amount" placeholder="Importo costo fornitore" value="<?=@$suppliercost['importo_totale']?>">
		</div>
		<div class="c4">
			<label>Imponibile <span id="calculated_amount_by_sum"></span></label>
			<input type="number" step="0.01" name="imponibile" id="cost_imponibile" placeholder="Importo imponibile" value="<?=@$suppliercost['imponibile']?>">
		</div>
		<div class="c4">
			<label>Imposta</label>
			<input type="number" step="0.01" name="imposta" id="cost_imposta" placeholder="Importo imposta" value="<?=@$suppliercost['imposta']?>">
		</div>
	</div>
	<div class="amount_detail_box">
		<label>
			<b>Suddividi costo</b>
			<a href="#" id="add_row" style="margin-left: 30px;"><i class="fa fa-plus" aria-hidden="true" style="margin: 0;"></i> Aggiungi riga dettaglio</a>
		</label><br><br>
		<div class="row detail_title_row">
			<div class="num_col"></div>
			<div class="c3">Centro di costo</div>
			<div class="c3">Cat. di costo</div>
			<div class="c1">Mese</div>
			<div class="c1">Anno</div>
			<div class="c2">Importo</div>
			<div class="c1"></div>
		</div>
		<?php
		if (!empty($suppliercost)) {
			foreach ($suppliercost['detail'] as $k => $row) {
				?>
				<div class="row amount_detail_row">
					<div class="row_number_wrapper">
						<span class="row_number"><?=($k+1)?></span>
					</div>
					<div class="c3">
						<select name="detail[id_costcenter][]">
							<option value="">- Scegli centro di costo -</option>
							<?php
							foreach ($costcenters as $cc) {
								echo '<option value="'.$cc['id'].'" '.($cc['id'] == $row['id_costcenter'] ? 'selected' : '').'>'.$cc['name'].($cc['location'] ? ' - '.$cc['location'] : '').'</option>';
							}
							?>
						</select>
					</div>
					<div class="c3">
						<select name="detail[id_costcat][]">
							<option value="">- Scegli categoria di costo -</option>
							<?php
							foreach ($costcats as $cat) {
								echo '<option value="'.$cat['id'].'" '.($cat['id'] == $row['id_costcat'] ? 'selected' : '').'>'.$cat['name'].'</option>';
							}
							?>
						</select>
					</div>
					<div class="c1">
						<select name="detail[period_month][]">
							<?php for ($m = 1; $m <= 12; $m++) { echo '<option value="'.$m.'" '.($m == $row['period_month'] ? 'selected' : '').'>'.monthNameByNum($m).'</option>'; } ?>
						</select>
					</div>
					<div class="c1">
						<select name="detail[period_year][]">
							<?php for ($y = (int)(date('Y')-1); $y <= ((int)date('Y')+5); $y++) { echo '<option value="'.$y.'" '.($y == $row['period_year'] ? 'selected' : '').'>'.$y.'</option>'; } ?>
						</select>
					</div>
					<div class="c2">
						<input type="number" step="0.01" name="detail[amount][]" class="amount" placeholder="Importo" value="<?=$row['amount']?>">
					</div>
					<div class="c1">
						<a href="#" class="btn darkblue clone_row"><i class="fa fa-clone" aria-hidden="true"></i></a>
						<a href="#" class="btn red remove_row"><i class="fa fa-times" aria-hidden="true"></i></a>
					</div>
				</div>
				<?php
			}	
		}
		?>
	</div>
	<!--
	<input type="text" name="aliquota_iva" placeholder="Aliquota IVA" value="<?=@$suppliercost['aliquota_iva']?>">
	<input type="text" name="arrotondamento" placeholder="Arrotondamento" value="<?=@$suppliercost['arrotondamento']?>">
	<input type="text" name="esigibilita_iva" placeholder="Esigiblità IVA" value="<?=@$suppliercost['esigibilita_iva']?>">
	-->
	<br>
	<a href="#" class="btn blue" id="submit_btn">Salva</a>
</form>

<script>
function check_sum_details() {
	// Prendo il valore totale del costo
	var amount = parseFloat($('#cost_imponibile').val());
	// Calcolo la somma di tutte le voci di dettaglio
	var total = 0;
	$('.amount').each(function() {
		total += parseFloat($(this).val());
	});
	$('#calculated_amount_by_sum').html('Calcolato: '+total.toFixed(2));
	if (amount != total) {
		$('.amount').css('background', '#eda8a8');

		// let diff = total - amount;

		// Swal.fire({
		// 	icon: 'warning',
		// 	title: 'Difformità nei valori',
		// 	html: "La somma delle voci di dettaglio non è uguale all'importo totale del costo fornitore.<br>"+
		// 			"Costo fornitore: <b>"+amount+"</b><br>"+
		// 			"Totale dettaglio: <b>"+total+"</b><br>"+
		// 			"Differenza: <b>"+diff+"€</b>.<br>"+
		// 			"Per favore controlla."
		// });
	} else {
		$('.amount').css('background', '#c1eaa6');
	}
	return false;
}

function renumber_rows() {
	let numrow = 1;
	$('.amount_detail_box .amount_detail_row').each(function() {
		$(this).find('.row_number').html(numrow);
		numrow++;
	});
	return false;
}

function add_row() {
	let new_row = $('.amount_detail_box .amount_detail_row').last().clone();
	new_row.find('input').val('');
	new_row.find('select').val('');
	new_row.find('.amount').val(0);
	$('.amount_detail_box').append(new_row);
	renumber_rows();
	check_sum_details();
	return false;
}

function is_valid() {
	var is_valid = true;
	$('.amount_detail_box input, .amount_detail_box select').each(function() {
		if (!$(this).val() || $(this).val() == 0 || $(this).val() == "0")
			is_valid = false;
	});
	return is_valid;
}

$(document).ready(function() {
	$('#add_row').click(function() {
		add_row();
		return false;
	});

	$('#submit_btn').click(function() {
		if (is_valid()) {
			$('.detail_form').submit();
		} else {
			Swal.fire({
				icon: 'warning',
				title: 'Controlla i dati inseriti',
				html: 'Alcuni dati non sono stati compilati o sono inesatti. Controlla e riprova per favore.'
			});
		}			
		return false;
	});
});

$(document).on('change', 'input.amount', function() {
	check_sum_details();
	return false;
});

$(document).on('click', '.clone_row', function() {
	$(this).closest('.amount_detail_row').clone().appendTo('.amount_detail_box');
	renumber_rows();
	check_sum_details();
	return false;
});

$(document).on('click', '.remove_row', function() {
	$(this).closest('.amount_detail_row').remove();
	renumber_rows();
	check_sum_details();
	return false;
});

$(function() {
	check_sum_details();
	return false;
});
</script>