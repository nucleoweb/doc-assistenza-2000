<div class="row space-bot">
	<div class="c12">
		<a href="/admin" class="back">Torna all'homepage</a>
	</div>
</div>
<h1><i class="fa fa-briefcase"></i> Modifica mansione</h1>
<form class="admin" action="/admin/edit_job" method="POST">
	<?php if (!empty($job)) { ?>
		<input type="hidden" name="id_job" value="<?=@$job['id']?>">
	<?php } ?>
	<input type="text" name="name" placeholder="Nome mansione" value="<?=@$job['name']?>" data-validation="required">
	<br>
	<button type="submit" class="blue">Salva</button>
</form>