<h1><i class="fa fa-building" aria-hidden="true"></i> Gestione clienti</h1>
<div class="row">
	<div class="c9">
		<table>
		<tr>
			<th style="width: 80px;">ID</th>
			<th>Nome</th>
			<th style="width: 180px;">P.IVA</th>
			<th style="width: 180px;">Cod.fiscale</th>
			<th>Centro di costo</th>
			<th style="width: 80px;"></th>
		</tr>
		<?php foreach ($customers as $customer) { ?>
			<tr style="<?=(!$customer['id_costcenter'] ? 'background: #fff9d3;' : '')?>">
				<td style="text-align: center;"><?=$customer['id']?></td>
				<td><?=($customer['denominazione'] ? $customer['denominazione'] : $customer['cognome'].' '.$customer['nome'])?></td>
				<td style="text-align: center;"><?=$customer['piva']?></td>
				<td style="text-align: center;"><?=$customer['codfisc']?></td>
				<td style="text-align: center;"><?=($customer['costcenter'] ? $customer['costcenter']['name'].' - '.$customer['costcenter']['location'] : '')?></td>
				<td class="actions">
					<a href="/admin/edit_bcustomer/<?=$customer['id']?>"><i class="fa fa-pencil"></i></a>
					<a class="del_customer" data-idcustomer="<?=$customer['id']?>" data-namecustomer="<?=htmlentities(($customer['denominazione'] ? $customer['denominazione'] : $customer['nome'].' '.$customer['cognome']))?>" href="#"><i class="fa fa-trash"></i></a>
				</td>
			</tr>
		<?php } ?>
		</table>
	</div>
	<div class="c3" style="padding-left: 40px;">
		<h2>Inserisci cliente</h2>
		<form class="admin detail_form" action="/admin/insert_bcustomer" method="POST">
			<label>Centro di costo</label>
			<select name="id_costcenter">
				<option value="">- Seleziona un centro di costo -</option>
				<?php
				foreach ($costcenters as $center)
					echo '<option value="'.$center['id'].'" '.($center['id'] == $supplier['id_costcenter'] ? 'selected' : '').'>'.$center['name'].' - '.$center['location'].'</option>';
				?>
			</select>
			<label>Ragione sociale</label>
			<input type="text" name="denominazione" placeholder="Denominazione cliente" value="<?=@$info['denominazione']?>">
			<label>Nome</label>
			<input type="text" name="nome" placeholder="Nome cliente" value="<?=@$info['nome']?>">
			<label>Cognome</label>
			<input type="text" name="cognome" placeholder="Cognome cliente" value="<?=@$info['cognome']?>">
			<label>P.IVA</label>
			<input type="text" name="piva" placeholder="P.IVA cliente" value="<?=@$info['piva']?>">
			<label>Codice fiscale</label>
			<input type="text" name="codfisc" placeholder="Cod.fiscale cliente" value="<?=@$info['codfisc']?>">
			<label>Indirizzo</label>
			<input type="text" name="indirizzo" placeholder="Indirizzo cliente" value="<?=@$info['indirizzo']?>">
			<label>Num. civico</label>
			<input type="text" name="civico" placeholder="Civico cliente" value="<?=@$info['civico']?>">
			<label>CAP</label>
			<input type="text" name="cap" placeholder="CAP cliente" value="<?=@$info['cap']?>">
			<label>Città</label>
			<input type="text" name="comune" placeholder="Comune cliente" value="<?=@$info['comune']?>">
			<label>Provincia (sigla)</label>
			<input type="text" name="prov" maxLength="2" placeholder="Provincia cliente" value="<?=@$info['prov']?>">
			<label>Nazione</label>
			<input type="text" name="nazione" placeholder="Nazione cliente" value="<?=@$info['nazione']?>">
			<label>Telefono</label>
			<input type="text" name="telefono" placeholder="Telefono cliente" value="<?=@$info['telefono']?>">
			<label>Indirizzo e-mail</label>
			<input type="text" name="email" placeholder="E-mail cliente" value="<?=@$info['email']?>">
			<br>
			<button type="submit" class="blue">Salva</button>
		</form>
	</div>
</div>

<div id="overlay"></div>
<div class="form_del_wrapper">
	<form id="del_customer_form" class="admin" action="/admin/del_bcustomer" method="POST">
		<input name="id_customer_to_del" id="id_customer_to_del" type="hidden" value="">
		<h4>Stai per cancellare il cliente [ <span id="customer_to_del"></span> ]</h4>
		<a id="del_customer_close" class="btn orange" href="#">Annulla</a>
		<a id="del_customer_proceed" class="btn green" href="#" style="float: right;">Elimina cliente</a>
	</form>
</div>

<script>
$.validate({
	errorMessagePosition: 'top'
});

$(document).ready(function() {
	$(".del_customer").click(function() {
		idcustomer = $(this).data("idcustomer");
		namecustomer = $(this).data("namecustomer");
		$("#overlay, .form_del_wrapper").fadeIn();
		$("#id_customer_to_del").val(idcustomer);
		$("#customer_to_del").html(namecustomer);
		$("#customer_to_assoc option").show();
		$("#customer_to_assoc option[value='"+idcustomer+"']").hide();
	});
	
	$("#del_customer_proceed").click(function() {
		// if ($("#customer_to_assoc").val()) {
			$("#del_customer_form").submit();
		// } else {
			// alert("Devi selezionare un servizio a cui associare gli utenti!");
		// }
	});
	
	$("#del_customer_close").click(function() {
		$("#overlay, .form_del_wrapper").fadeOut();
	});
});
</script>