<h1><i class="fa fa-building" aria-hidden="true"></i> Gestione fornitori</h1>
<div class="row">
	<div class="c9">
		<table>
		<tr>
			<th style="width: 80px;">ID</th>
			<th>Nome</th>
			<th style="width: 180px;">P.IVA</th>
			<th style="width: 180px;">Cod.fiscale</th>
			<th>Centro di costo</th>
			<th>Cat. di costo</th>
			<th style="width: 80px;"></th>
		</tr>
		<?php foreach ($suppliers as $supplier) { ?>
			<tr style="<?=((!$supplier['id_costcenter'] OR !$supplier['id_costcat']) ? 'background: #fff9d3;' : '')?>">
				<td style="text-align: center;"><?=$supplier['id']?></td>
				<td><?=($supplier['denominazione'] ? $supplier['denominazione'] : $supplier['nome'].' '.$supplier['cognome'])?></td>
				<td style="text-align: center;"><?=$supplier['piva']?></td>
				<td style="text-align: center;"><?=$supplier['codfisc']?></td>
				<td style="text-align: center;"><?=($supplier['costcenter'] ? $supplier['costcenter']['name'].' - '.$supplier['costcenter']['location'] : '')?></td>
				<td style="text-align: center;"><?=($supplier['costcat'] ? $supplier['costcat']['name'] : '')?></td>
				<td class="actions">
					<a href="/admin/edit_supplier/<?=$supplier['id']?>" data-tippy-content="Vai al fornitore"><i class="fa fa-pencil"></i></a>
					<a class="del_supplier" data-idsupplier="<?=$supplier['id']?>" data-namesupplier="<?=htmlentities(($supplier['denominazione'] ? $supplier['denominazione'] : $supplier['nome'].' '.$supplier['cognome']))?>" href="#" data-tippy-content="Cancella il fornitore"><i class="fa fa-trash"></i></a>
				</td>
			</tr>
		<?php } ?>
		</table>
	</div>
	<div class="c3" style="padding-left: 40px;">
		<h2>Inserisci fornitore</h2>
		<form class="admin detail_form" action="/admin/insert_supplier" method="POST">
			<label>Categoria di costo</label>
			<select name="id_costcat">
				<option value="">- Seleziona una categoria di costo -</option>
				<?php
				foreach ($costcats as $cat)
					echo '<option value="'.$cat['id'].'">'.$cat['name'].'</option>';
				?>
			</select>
			<label>Centro di costo</label>
			<select name="id_costcenter">
				<option value="">- Seleziona un centro di costo -</option>
				<?php
				foreach ($costcenters as $center)
					echo '<option value="'.$center['id'].'">'.$center['name'].' - '.$center['location'].'</option>';
				?>
			</select>
			<label>Ragione sociale</label>
			<input type="text" name="denominazione" placeholder="Denominazione fornitore" value="<?=@$info['denominazione']?>">
			<label>Nome</label>
			<input type="text" name="nome" placeholder="Nome fornitore" value="<?=@$info['nome']?>">
			<label>Cognome</label>
			<input type="text" name="cognome" placeholder="Cognome fornitore" value="<?=@$info['cognome']?>">
			<label>P.IVA</label>
			<input type="text" name="piva" placeholder="P.IVA fornitore" value="<?=@$info['piva']?>">
			<label>Codice fiscale</label>
			<input type="text" name="codfisc" placeholder="Cod.fiscale fornitore" value="<?=@$info['codfisc']?>">
			<label>Indirizzo</label>
			<input type="text" name="indirizzo" placeholder="Indirizzo fornitore" value="<?=@$info['indirizzo']?>">
			<label>Num. civico</label>
			<input type="text" name="civico" placeholder="Civico fornitore" value="<?=@$info['civico']?>">
			<label>CAP</label>
			<input type="text" name="cap" placeholder="CAP fornitore" value="<?=@$info['cap']?>">
			<label>Città</label>
			<input type="text" name="comune" placeholder="Comune fornitore" value="<?=@$info['comune']?>">
			<label>Provincia (sigla)</label>
			<input type="text" name="prov" maxLength="2" placeholder="Provincia fornitore" value="<?=@$info['prov']?>">
			<label>Nazione</label>
			<input type="text" name="nazione" placeholder="Nazione fornitore" value="<?=@$info['nazione']?>">
			<label>Telefono</label>
			<input type="text" name="telefono" placeholder="Telefono fornitore" value="<?=@$info['telefono']?>">
			<label>Indirizzo e-mail</label>
			<input type="text" name="email" placeholder="E-mail fornitore" value="<?=@$info['email']?>">
			<br>
			<button type="submit" class="blue">Salva</button>
		</form>
	</div>
</div>

<div id="overlay"></div>
<div class="form_del_wrapper">
	<form id="del_supplier_form" class="admin" action="/admin/del_supplier" method="POST">
		<input name="id_supplier_to_del" id="id_supplier_to_del" type="hidden" value="">
		<h4>Stai per cancellare il fornitore [ <span id="supplier_to_del"></span> ]</h4>
		<a id="del_supplier_close" class="btn orange" href="#">Annulla</a>
		<a id="del_supplier_proceed" class="btn green" href="#" style="float: right;">Elimina fornitore</a>
	</form>
</div>

<script>
$.validate({
	errorMessagePosition: 'top'
});

$(function() {
	tippy('[data-tippy-content]');
});

$(document).ready(function() {
	$(".del_supplier").click(function() {
		idsupplier = $(this).data("idsupplier");
		namesupplier = $(this).data("namesupplier");
		$("#overlay, .form_del_wrapper").fadeIn();
		$("#id_supplier_to_del").val(idsupplier);
		$("#supplier_to_del").html(namesupplier);
		$("#supplier_to_assoc option").show();
		$("#supplier_to_assoc option[value='"+idsupplier+"']").hide();
	});
	
	$("#del_supplier_proceed").click(function() {
		// if ($("#supplier_to_assoc").val()) {
			$("#del_supplier_form").submit();
		// } else {
			// alert("Devi selezionare un servizio a cui associare gli utenti!");
		// }
	});
	
	$("#del_supplier_close").click(function() {
		$("#overlay, .form_del_wrapper").fadeOut();
	});
});
</script>