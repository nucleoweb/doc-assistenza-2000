<div class="row space-bot">
	<div class="c12">
		<a href="/admin" class="back">Torna all'homepage</a>
	</div>
</div>
<h1><i class="fa fa-file-text-o"></i> Report associazione documenti</h1>
<h2>Documenti esaminati: <?=count($result)?></h2>
<?php
foreach ($result as $row) {
	?>
	<table>
	<tr><td>Nome file</td><td><b><?=$row['filename']?></b></td></tr>
	<tr><td>Nome utente</td><td><?=$row['user_name']?></td></tr>
	<tr><td>ID utente</td><td><?=$row['user_id']?></td></tr>
	<tr><td>Esito</td><td style="color: <?=(($row['response'] == 'OK') ? 'green' : 'red')?>;"><b><?=$row['response']?></b></td></tr>
	</table>
	<br>
	<?php
}
?>

