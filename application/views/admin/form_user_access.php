<div class="row space-bot">
	<div class="c12">
		<a href="/admin/edit_user/<?=$info['id_user']?>" class="back">Torna al profilo dell'utente</a>
	</div>
</div>
<h1><i class="fa fa-lock"></i> Modifica accesso utente</h1>
<form class="admin" action="/admin/change_user_access" method="POST">
	<div class="row">
		<div class="c6 accessdata_block">
			<p>Stai modificando i dati d'accesso per l'utente: <b><?=$info['first_name'].' '.$info['last_name']?></b></p>
			<br>
			<input type="hidden" name="id_user" value="<?=$info['id_user']?>">
			<input type="hidden" name="email" value="<?=$info['email']?>">
			<input type="hidden" name="first_name" value="<?=$info['first_name']?>">
			<input type="hidden" name="last_name" value="<?=$info['last_name']?>">
			<input type="text" name="username" placeholder="Username" value="<?=$info['username']?>" data-validation="length" data-validation-length="5-30">
			<input type="password" name="password_confirmation" placeholder="Password" value="" data-validation="strength" data-validation-strength="2" data-validation-length="8-20"> <a href="#" class="show_psw" data-fieldname="password_confirmation"><i class="fa fa-eye"></i></a>
			<input type="password" name="password" placeholder="Ripeti password" value=""  data-validation="confirmation"> <a href="#" class="show_psw" data-fieldname="password"><i class="fa fa-eye"></i></a>
			<br>
			<button type="submit" class="blue">Salva</button>
		</div>
		<div class="c6">
			<div class="infobox">
				Ricorda di salvare la password per questo utente in quanto d'ora in poi non sarà più visibile in chiaro ma potrà solo essere modificata.<br>
				<br>
				<p>Vuoi inviare un'e-mail con i dati d'accesso all'utente?</p>
				<input type="radio" name="send_data" value="1" id="send_data_1" checked> <label for="send_data_1">Sì</label>
				&nbsp; &nbsp; &nbsp;
				<input type="radio" name="send_data" value="0" id="send_data_0"> <label for="send_data_0">No</label>
			</div>
		</div>
	</div>
</form>

<style>
input[type=text] {
	clear: none !important;
	display: inline-block !important;
	width: 80% !important;
}
</style>

<script>
$(document).ready(function() {
	$(".show_psw").click(function() {
		$("input[name='"+$(this).data('fieldname')+"']").attr('type', 'text');
		return false;
	});
});

$.validate({
	errorMessagePosition: 'top',
	modules: 'security',
	onModulesLoaded: function() {
		var optionalConfig = {
		  fontSize: '12pt',
		  padding: '4px',
		  bad: 'Pessimo',
		  weak: 'Debole',
		  good: 'Buono',
		  strong: 'Forte'
    };
    $('input[name="password_confirmation"]').displayPasswordStrength(optionalConfig);
  }
});
</script>