<h1><i class="fa fa-ambulance"></i> Gestione clienti</h1>
<div class="row">
	<div class="c6">
		<table>
		<tr>
			<th>ID</th>
			<th>Nome</th>
			<th>Num. utenti attivi</th>
			<th>Num. utenti totali</th>
			<th></th>
		</tr>
		<?php foreach ($customers as $customer) { ?>
			<tr>
				<td><?=$customer['id']?></td>
				<td><?=$customer['name']?></td>
				<td><?=$customer['num_users_active']?></td>
				<td><?=$customer['num_users']?></td>
				<td class="actions">
					<a href="/admin/edit_customer/<?=$customer['id']?>"><i class="fa fa-pencil"></i></a>
					<a class="del_customer" data-idcustomer="<?=$customer['id']?>" data-namecustomer="<?=$customer['name']?>" href="#"><i class="fa fa-trash"></i></a>
				</td>
			</tr>
		<?php } ?>
		</table>
	</div>
	<div class="c6" style="padding-left: 40px;">
		<h2>Inserisci nuovo cliente</h2>
		<form class="admin" action="/admin/insert_customer" method="POST">
			<input type="text" name="name" placeholder="Nome cliente" value="<?=@$info['name']?>" data-validation="required">
			<br>
			<button type="submit" class="blue">Salva</button>
		</form>
	</div>
</div>

<div id="overlay"></div>
<div class="form_del_wrapper">
	<form id="del_customer_form" class="admin" action="/admin/del_customer" method="POST">
		<input name="id_customer_to_del" id="id_customer_to_del" type="hidden" value="">
		<h4>Stai per cancellare il cliente [ <span id="customer_to_del"></span> ]</h4>
		<hr>
		Seleziona un cliente a cui associare gli utenti (se presenti):
		<br><br>
		<select name="customer_to_assoc" id="customer_to_assoc">
			<option value="">- Seleziona un cliente -</option>
			<?php foreach ($customers as $customer) { ?>
				<option value="<?=$customer['id']?>"><?=$customer['name']?></option>
			<?php } ?>
		</select>
		<br><br>
		<a id="del_customer_close" class="btn orange" href="#">Annulla</a>
		<a id="del_customer_proceed" class="btn green" href="#" style="float: right;">Elimina cliente</a>
	</form>
</div>

<script>
$.validate({
	errorMessagePosition: 'top'
});

$(document).ready(function() {
	$(".del_customer").click(function() {
		idcustomer = $(this).data("idcustomer");
		namecustomer = $(this).data("namecustomer");
		$("#overlay, .form_del_wrapper").fadeIn();
		$("#id_customer_to_del").val(idcustomer);
		$("#customer_to_del").html(namecustomer);
		$("#customer_to_assoc option").show();
		$("#customer_to_assoc option[value='"+idcustomer+"']").hide();
	});
	
	$("#del_customer_proceed").click(function() {
		if ($("#customer_to_assoc").val()) {
			$("#del_customer_form").submit();
		} else {
			alert("Devi selezionare un cliente a cui associare gli utenti!");
		}
	});
	
	$("#del_customer_close").click(function() {
		$("#overlay, .form_del_wrapper").fadeOut();
	});
});
</script>