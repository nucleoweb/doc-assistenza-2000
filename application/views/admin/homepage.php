<h1>Area amministrazione piattaforma</h1>
<div class="row">
	<div class="c6">
		<h4><i class="fa fa-file-text-o"></i> Documenti inseriti</h4>
		<form action="" method="POST" id="doc_filter_form">
			<select name="year">
				<?php foreach ($years as $y) { echo '<option value="'.$y.'" '.(($y == $year) ? 'selected' : '').'>'.$y.'</option>'; } ?>
			</select>
			<select name="month">
				<?php for ($m = 1; $m <= 12; $m++) { echo '<option value="'.$m.'" '.(($m == $month) ? 'selected' : '').'>'.$month_names[$m].'</option>'; } ?>
			</select>
			<select name="cat">
				<option value="">- Tutte le categorie -</option>
				<?php foreach ($doc_cats as $cod => $cat_name) { echo '<option value="'.$cod.'" '.(($cod == $cat) ? 'selected' : '').'>'.$cat_name.'</option>'; } ?>
			</select>
			<button type="submit" class="btn green">Filtra</button>
		</form>
		
		<?php if (count($docs)) { ?>
			<div id="docs_list" style="margin-top: -12px;">
				<form class="admin"><input type="text" class="search" placeholder="Cerca documento"></form>
				<ul class="list">
					<?php
					foreach ($docs as $d) {
						$cat = ucfirst(strtolower(str_replace("_", " ", ($d['cat']))));
						echo '<li>
									<b><a class="name" title="'.$d['filename'].'" href="/admin/edit_doc/'.$d['id_doc'].'" style="font-size: 15px;">'.ucfirst(str_replace("_", " ",$d['name'])).'</a></b>
									<i class="fa fa-user"></i> N. utenti: <span class="date">'.count($d['users_assigned']).'</span><br>
									<i class="fa fa-tag"></i> Categoria: <span class="date">'.((strlen($cat) <= 16) ? $cat : substr($cat, 0, 16)."..").'</span><br>
									<i class="fa fa-upload"></i> Upload: <span class="date">'.date("d/m/Y", strtotime($d['date_upload'])).'</span><br>';
						if (isset($d['users_assigned']) AND count($d['users_assigned']) == 1) {
							$usr_assigned = reset($d['users_assigned']);
							$datetime = $usr_assigned['last_download'];
							if ($datetime)
								echo '<i class="fa fa-cloud-download"></i> Download: <span class="date">'.date("d/m/Y", strtotime($datetime)).'</span><br>';
						}
						// echo '<i class="fa fa-file"></i> <span class="filename"></span>';
						echo '</li>';
					}
					?>
				</ul>
				<ul class="pagination"></ul>
			</div>
		<?php } else { ?>
			<p>Non sono presenti documenti nella piattaforma per il filtro selezionato</p>
		<?php } ?>
	</div>
	<div class="c6">
		<h4><i class="fa fa-user"></i> Utenti presenti</h4>
		<?php if (count($users)) { ?>
			<div id="users_list">
				<div id="users_filter_bar">
					<label style="">Filtra: </label><br>
					<div id="users_filters_wrapper">
						<select class="list_filter" id="user_type_filter" name="user_type_filter">
							<option value="">- Tutti gli stati -</option>
							<option value="active">Attivi sulla piattaforma</option>
							<option value="not_active">Non attivi</option>
						</select>
						<select class="list_filter" id="user_role_filter" name="user_role_filter">
							<option value="">- Tutti i ruoli -</option>
							<option value="user">Utente</option>
							<option value="admin">Amministratore</option>
						</select>
						<select class="list_filter" id="user_job_filter" name="user_job_filter">
							<option value="">- Tutte le mansioni -</option>
							<?php
							foreach ($jobs as $job)
								echo '<option value="'.$job['name'].'">'.$job['name'].'</option>';
							?>
						</select>
						<select class="list_filter" id="user_level_filter" name="user_level_filter">
							<option value="">- Tutti i livelli -</option>
							<?php
							foreach ($levels as $level)
								echo '<option value="'.$level['name'].'">'.$level['name'].'</option>';
							?>
						</select>
						<select class="list_filter" id="user_area_filter" name="user_area_filter">
							<option value="">- Tutte le aree -</option>
							<?php
							foreach ($areas as $area)
								echo '<option value="'.$area['name'].'">'.$area['name'].'</option>';
							?>
						</select>
						<select class="list_filter" id="user_service_filter" name="user_service_filter">
							<option value="">- Tutti i servizi -</option>
							<?php
							foreach ($services as $serv)
								echo '<option value="'.$serv['name'].'">'.$serv['name'].'</option>';
							?>
						</select>
						<select class="list_filter" id="user_customer_filter" name="user_customer_filter">
							<option value="">- Tutti i clienti -</option>
							<?php
							foreach ($customers as $cust)
								echo '<option value="'.$cust['name'].'">'.$cust['name'].'</option>';
							?>
						</select>
						<select class="list_filter" id="user_freelance_filter" name="user_freelance_filter">
							<option value="">- Dipendenti e non -</option>
							<option value="0">Dipendenti</option>
							<option value="1">Liberi professionisti</option>
						</select>
						<select class="list_filter" id="user_rank_filter" name="user_rank_filter">
							<option value="">- Tutti i rank -</option>
							<option value="Senior">Senior</option>
							<option value="Junior">Junior</option>
							<option value="Temp">Temp</option>
						</select>
					</div>
				</div>
				<form class="admin"><input type="text" id="txt_search" class="search" placeholder="Cerca utente"></form>
				<div class="users_report_bar"><i class="fa fa-search"></i> <span id="users_found"></span> utenti visualizzati</div>
				<ul class="list users_list">
					<?php
					foreach ($users as $u) {
						echo '<li class="'.(($u['role'] == "admin") ? 'admin' : '').' '.((!$u['active']) ? 'not_active' : '').'" data-state="'.(($u['active']) ? 'active' : 'not_active').'" data-role="'.$u['role'].'" data-job="'.$u['job'].'" data-service="'.$u['service'].'" data-area="'.$u['area'].'" data-customer="'.$u['customer'].'" data-level="'.$u['level'].'" data-rank="'.$u['rank'].'" data-freelance="'.$u['freelance'].'">
									<b><a class="name" href="/admin/edit_user/'.$u['id_user'].'">'.$u['last_name'].' '.$u['first_name'].'</a></b>
									<span class="role"><i class="fa fa-key"></i> ';
							switch ($u['role']) {
								case 'admin':
									echo 'Amministratore';
								break;
								case 'user':
									echo 'Utente';
								break;
								default:
									echo $u['role'];
								break;
							}
							echo '</span>
							<span class="job"><i class="fa fa-briefcase"></i> '.($u['job'] ? $u['job'] : '<i>n.d.</i>').'</span>
							<span class="service"><i class="fa fa-ambulance"></i> '.($u['service'] ? $u['service'] : '<i>n.d.</i>').'</span>
						</li>';
					}
					?>
				</ul>
				<ul class="pagination"></ul>
			</div>
		<?php } else { ?>
			<p>Non sono presenti utenti nella piattaforma</p>
		<?php } ?>
	</div>
</div>

<script>
function check_filter(item) {
	if ($("#user_type_filter").val() && $("#user_type_filter").val() != item.state)
		return false;
	if ($("#user_role_filter").val() && $("#user_role_filter").val() != item.role)
		return false;
	if ($("#user_job_filter").val() && $("#user_job_filter").val() != item.job)
		return false;
	if ($("#user_area_filter").val() && $("#user_area_filter").val() != item.area)
		return false;
	if ($("#user_service_filter").val() && $("#user_service_filter").val() != item.service)
		return false;
	if ($("#user_customer_filter").val() && $("#user_customer_filter").val() != item.customer)
		return false;
	if ($("#user_level_filter").val() && $("#user_level_filter").val() != item.level)
		return false;
	if ($("#user_freelance_filter").val() && $("#user_freelance_filter").val() != item.freelance)
		return false;
	if ($("#user_rank_filter").val() && $("#user_rank_filter").val() != item.rank)
		return false;
	return true;
}

$(document).ready(function() {
	$("#user_type_filter, #user_role_filter, #user_job_filter, #user_area_filter, #user_service_filter, #user_customer_filter, #user_level_filter, #user_freelance_filter, #user_rank_filter").change(function() {
		// usersList.filter();
		usersList.filter(function(item) {
			return check_filter(item.values());
		});
	});
});

var options = {
    valueNames: ['name', 'date', 'filename'],
	page: 30,
	pagination: true
};
var docsList = new List('docs_list', options);

var options = {
    valueNames: [
		'name',
		{ data: ['role'] },
		{ data: ['state'] },
		{ data: ['job'] },
		{ data: ['area'] },
		{ data: ['service'] },
		{ data: ['customer'] },
		{ data: ['level'] },
		{ data: ['freelance'] },
		{ data: ['rank'] }
	],
	page: 30,
	pagination: true
};
var usersList = new List('users_list', options);

usersList.on('updated', function() {
	$("#users_found").html(usersList.matchingItems.length);
});
$(function() {
	$("#users_found").html(usersList.matchingItems.length);
});
</script>