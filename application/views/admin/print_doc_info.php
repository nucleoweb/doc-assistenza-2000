<html>
    <head>
        <style>
            * { font-family: Verdana; font-size: 8px; }
            table td { border: 1px solid #aaa; padding: 2px 10px; }
            table > tr > td.label { background: #eee; vertical-align: top; }
            table table td { border: 1px solid #aaa; padding: 2px 5px; }
        </style>
    </head>
    <body>
        <table>
            <tr><td class="label"><b>ID</b></td><td><?=$docinfo['id_doc']?></td></tr>
            <tr><td class="label"><b>Nome</b></td><td><?=$docinfo['name']?></td></tr>
            <tr><td class="label"><b>Nome file</b></td><td><?=$docinfo['filename']?></td></tr>
            <tr><td class="label"><b>Categoria</b></td><td><?=$docinfo['cat']?></td></tr>
            <tr><td class="label"><b>Data caricamento</b></td><td><?=implode("/", array_reverse(explode("-", substr($docinfo['date_upload'], 0, 10)))).' '.substr($docinfo['date_upload'], 11, 5)?></td></tr>
            <tr><td class="label"><b>Note</b></td><td><?=$docinfo['note']?></td></tr>
            <tr><td class="label"><b>Num. utenti</b></td><td><?=count($docinfo['users'])?></td></tr>
            <tr><!--<td><b>Elenco utenti</b></td>--><td colspan="2">
                <table>
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>Download</th>
                        <th></th>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>Download</th>
                    </tr>
                    <?php
                    $c = 0;
                    foreach ($docinfo['users'] as $id => $user) { 
                        ?>
                        <?=(($c % 2) == 0 ? '<tr>' : '')?>
                            <td class="label"><?=$id?></td>
                            <td><?=$user['last_name'].' '.$user['first_name']?></td>
                            <td><?=implode("/", array_reverse(explode("-", substr($user['last_download'], 0, 10)))).' '.substr($user['last_download'], 11, 5)?></td>
                            <?php $c++; ?>
                        <?=(($c % 2) == 0 ? '</tr>' : '<td style="width: 50px; border: 0;"></td>')?>
                        <?php
                    }
                    ?>
                    <?=(($c % 2) != 0 ? '</tr>' : '')?>
                </table>
            </td></tr>
        </table>

        <script>
        window.print();
        </script>
    </body>
</html>