<?php if (!empty($ret)) { ?>
	<script>
	$(function() {
		<?php if ($ret['alert_type'] == 'import_ok') { ?>
			Swal.fire({
				icon: 'success',
				title: 'Importazione completata',
				html: '<b>Record esaminati</b>: <?=$ret['entries']?><hr>'+
						'<b>Costi dipendente esistenti</b>: <?=$ret['usercosts_ext']?><br>'+
						'<b>Costi dipendente inseriti</b>: <?=$ret['usercosts_ins']?><br>'+
						'<b>Costi dipendente falliti</b>: <?=$ret['usercosts_not']?><br>'+
						'<hr><b>Costi dipendente falliti:</b><br><?=($ret['usercosts_not_list'] != "" ? $ret['usercosts_not_list'] : '-')?>',
			});
		<?php } elseif ($ret['alert_type'] == 'import_ko') { ?>
			Swal.fire({
				icon: 'error',
				title: 'Importazione fallita',
				html: "<?=$ret['error']?>"
			});
		<?php } ?>
	});
	</script>
<?php } ?>

<?php
// echo '<pre>';
// print_r($usercosts);
// echo '</pre>';
?>

<h1 class="balance_list_title">
	<i class="fa fa-eur" aria-hidden="true"></i> Gestione costi dipendente
	<a href="#" class="balance_add_btn_link"><i class="fa fa-plus" aria-hidden="true"></i> Aggiungi costo dipendente</a>
</h1>
<div class="row balances_list_row">
	<div class="c9 main">
		<div class="row filter_row">
			<form action="/admin/view_usercosts" method="POST">
				<div class="c2">
					<label>Nome dipendente</label>
					<input type="text" class="filter_control" name="filter_name" value="<?=@isset($filter['filter_name']) ? $filter['filter_name'] : '' ?>">
				</div>
				<div class="c2">
					<label>Periodo comp. (mese)</label>
					<select class="filter_control" name="filter_period_month">
						<option value="">- Tutti i mesi -</option>
						<?php for ($m = 1; $m <= 12; $m++) { echo '<option value="'.$m.'" '.($filter['filter_period_month'] == $m ? 'selected' : '').'>'.monthNameByNum($m).'</option>'; } ?>
					</select>
				</div>
				<div class="c2">
					<label>Periodo comp. (anno)</label>
					<select class="filter_control" name="filter_period_year">
						<option value="">- Scegli l'anno -</option>
						<?php for ($y = 2020; $y <= ((int)date('Y')+5); $y++) { echo '<option value="'.$y.'" '.($filter['filter_period_year'] == $y ? 'selected' : '').'>'.$y.'</option>'; } ?>
					</select>
				</div>
				<div class="c2">
					<label>Centro di costo</label>
					<select class="filter_control" name="filter_costcenter">
						<option value="">- Tutti i centri di costo -</option>
						<?php foreach ($costcenters as $cc) { 
							echo '<option value="'.$cc['id'].'" '.($filter['filter_costcenter'] == $cc['id'] ? 'selected' : '').'>'.$cc['name'].' - '.$cc['location'].'</option>'; 
						} ?>
					</select>
				</div>
				<div class="c2">
					<label>Data creazione</label>
					<input type="date" class="filter_control" name="filter_date" value="<?=@isset($filter['filter_date']) ? $filter['filter_date'] : '' ?>">
				</div>
				<div class="c2">
					<label class="checkbox" for="filter_problems"><input type="checkbox" class="filter_control" name="filter_problems" id="filter_problems" <?=(!empty($filter['filter_problems']) ? 'checked' : '')?>> Solo con anomalie</label>
				</div>
				<div class="c12 filter_action_wrapper">
					<button type="submit" class="btn green"><i class="fa fa-search" aria-hidden="true"></i> Cerca</button>
					<a href="#" class="btn blue form_reset_btn" style="margin: 8px 0 0 0;"><i class="fa fa-ban" aria-hidden="true"></i> Azzera filtri</a>
					<a href="/admin/import_usercosts" class="btn yellow form_import_btn" style="margin: 8px 0 0 0;"><i class="fa fa-upload" aria-hidden="true"></i> Importa</a>
				</div>
			</form>
		</div>
		<div class="row">
			<div class="c12">
				Sono stati trovati <b class="numres"><?=count($usercosts)?></b> risultati con i filtri attuali - Totale importi <b class="numres"><?=number_format($find_total, 2, ",", ".")?> €</b>
			</div>
		</div>
		<div class="row" style="margin-top: 20px;">
			<div class="c12">
				<table class="tab_list">
				<tr class="title_row">
					<th style="width: 50px;">Info</th>
					<th style="width: 150px;">Periodo</th>
					<th style="width: 220px;">Centro costo</th>
					<th>Utente</th>
					<th style="width: 120px;">Importo</th>
					<th style="width: 100px;">Num. ore</th>
					<th style="width: 130px;">Costo orario</th>
					<th style="width: 100px;">Data elab.</th>
					<th style="width: 50px;">Dett.</th>
					<th style="width: 100px;">Creato il</th>
					<th style="width: 110px;"></th>
				</tr>
				<?php foreach ($usercosts as $usercost) { 
					$missing_costcenter = false;
					$sum_is_ok = true;
					$sum = 0;
					$det_periods = [];
					$det_costcenters = [];
					$no_periods = false;
					foreach ($usercost['detail'] as $det) {
						if (!$det['id_costcenter'])
							$missing_costcenter = true;
						else {
							if (!in_array($det['id_costcenter'], $det_costcenters))
								$det_costcenters[] = $det['id_costcenter'];
						}
						if ($det['period_month'] AND $det['period_year']) {
							$p = monthNameByNum($det['period_month']).' '.$det['period_year'];
							if (!in_array($p, $det_periods))
								$det_periods[] = $p;
						} else {
							$no_periods = true;
						}
						$sum += $det['amount'];
					}

					if (count($det_costcenters)) {
						if (count($det_costcenters) > 1)
							$cc_info = 'Vari';
						else {
							$tmp_cc = $this->costcenters_model->get_costcenter($det_costcenters[0]);
							$cc_info = $tmp_cc['name'];
							if ($tmp_cc['location'] && !strpos(strtolower($cc_info), trim(strtolower(preg_replace('/\(.+\)/', '', $tmp_cc['location'])))))
								$cc_info .= ' '.$tmp_cc['location'];
						}
					} else {
						$cc_info = '';
					}

					if (round($sum, 2) != round($usercost['amount'], 2))
						$sum_is_ok = false;
					?>
					<tr <?=(($missing_costcenter OR !$sum_is_ok OR $no_periods) ? 'style="background: #fff9d3;" class="danger"' : '')?>">
						<td style="text-align: center; font-size: 18px;">
							<?php if ($missing_costcenter OR !$sum_is_ok OR $no_periods) { ?>
							<i class="fa fa-info-circle cost_info_icon" id="cost_info_icon_<?=$usercost['id']?>" aria-hidden="true"></i>
							<div class="cost_info_tooltip" style="display: none;">
								<table style="border: 0; box-shadow: none;">
									<?php if ($missing_costcenter OR !$sum_is_ok OR $no_periods) { ?>
										<tr>
											<td style="border: 0; margin: 0; padding: 0; color: #f45d8a;"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i><b>Problema:</b></td>
											<td style="border: 0; margin: 0; padding: 0 0 0 10px;">
												<?php
												if ($missing_costcenter)
													echo 'Centro di costo mancante<br>';
												if (!$sum_is_ok)
													echo 'Somma dettagli errata<br>';
												if ($no_periods)
													echo 'Periodo di riferimento mancante<br>';
												?>
											</td>
										</tr>
									<?php } ?>
								</table>
							</div>
							<?php } ?>
						</td>
						<td style="text-align: center;"><?=(count($det_periods) ? (count($det_periods) > 1 ? 'Vari' : $det_periods[0]) : '')?></td>
						<td style="text-align: center; font-size: 13px; line-height: 1.2;"><?=$cc_info?></td>
						<td><?=$usercost['utente']?></td>
						<td style="text-align: right;"><?=number_format($usercost['amount'], 2, ",", ".")?> €</td>
						<td style="text-align: center;"><?=number_format($usercost['hours'], 2, ",", ".")?></td>
						<td style="text-align: right;"><?=number_format($usercost['hourly_cost'], 2, ",", ".")?> €</td>
						<td><?=rvd($usercost['data_elaborazione'])?></td>
						<td style="text-align: center;"><?=count($usercost['detail'])?></td>
						<td><?=rvd(substr($usercost['created_at'], 0, 10))?></td>
						<td class="actions">
							<a href="/admin/edit_user/<?=$usercost['id_user']?>" data-tippy-content="Vai al dipendente"><i class="fa fa-user"></i></a>
							<a href="/admin/edit_usercost/<?=$usercost['id']?>" data-tippy-content="Modifica voce di costo"><i class="fa fa-pencil"></i></a>
							<a class="del_usercost" data-idusercost="<?=$usercost['id']?>" data-nameusercost="<?=htmlentities($usercost['nome'].' '.$usercost['cognome'])?>" href="#" data-tippy-content="Elimina voce di costo"><i class="fa fa-trash"></i></a>
						</td>
					</tr>
				<?php } ?>
				</table>
			</div>
		</div>
	</div>
	<div class="c3 side">
		<h2>Nuovo costo dipendente</h2><br>
		<form class="admin detail_form" action="/admin/insert_usercost" method="POST">
			<label>Dipendente</label>
			<select name="id_user">
				<option value="">- Seleziona un dipendente -</option>
				<?php
				foreach ($users as $u)
					echo '<option value="'.$u['id_user'].'">'.$u['last_name'].' '.$u['first_name'].'</option>';
				?>
			</select>
			<label>Centro di costo</label>
			<select name="id_costcenter">
				<option value="">- Seleziona un centro di costo -</option>
				<?php
				foreach ($costcenters as $center)
					echo '<option value="'.$center['id'].'">'.$center['name'].' - '.$center['location'].'</option>';
				?>
			</select>
			<div class="row" style="padding: 20px; background: #f6f6f6;">
				<div class="c12 first">
					<label><b>Periodo di competenza</b></label><br>
					<select name="period_month" style="width: 45%; float: left; clear: none; margin: 0 10px 0 0;">
						<option value="">- Scegli il mese -</option>
						<?php for ($m = 1; $m <= 12; $m++) { echo '<option value="'.$m.'">'.monthNameByNum($m).'</option>'; } ?>
					</select>
					<select name="period_year" style="width: 45%; float: left; clear: none; margin: 0 10px 0 0;">
						<option value="">- Scegli l'anno -</option>
						<?php for ($y = 2020; $y <= ((int)date('Y')+5); $y++) { echo '<option value="'.$y.'">'.$y.'</option>'; } ?>
					</select>
				</div>
			</div>
			<br>
			<label>Importo retribuzione</label>
			<input type="number" step="0.01" name="amount" placeholder="Importo retribuzione" value="">
			<label>Num. ore lavorate</label>
			<input type="number" step="0.01" name="hours" placeholder="Ore lavorate" value="">
			<label>Costo orario</label>
			<input type="number" step="0.01" name="hourly_cost" placeholder="Costo orario" value="">
			<br>
			<button type="submit" class="blue">Salva</button>
		</form>
	</div>
</div>

<div id="overlay"></div>
<div class="form_del_wrapper">
	<form id="del_usercost_form" class="admin" action="/admin/del_usercost" method="POST">
		<input name="id_usercost_to_del" id="id_usercost_to_del" type="hidden" value="">
		<h4>Stai per cancellare il costo dipendente [ <span id="usercost_to_del"></span> ]</h4>
		<a id="del_usercost_close" class="btn orange" href="#">Annulla</a>
		<a id="del_usercost_proceed" class="btn green" href="#" style="float: right;">Elimina costo dipendente</a>
	</form>
</div>

<script>
$(function() {
	handleProblematicRowsVisibility();

	tippy('[data-tippy-content]');

	$('.cost_info_icon').each(function() {
		var template = $(this).closest('td').find('.cost_info_tooltip').html();
		tippy('#'+$(this).attr('id'), {
			content(reference) {
				return template;
			},
			allowHTML: true
		});
	});
});

$.validate({
	errorMessagePosition: 'top'
});

function handleProblematicRowsVisibility() {
	if ($('#filter_problems').is(':checked')) {
		$('.tab_list tr:not(.title_row)').hide();
		$('.tab_list tr.danger').show();
		$.ajax({
			url: '/ajax/setSessionData',
			data: 'key=filter|usercosts|filter_problems&val=1',
			method: 'POST',
			success: function() {}
		});
	} else {
		$('.tab_list tr').show();
		$.ajax({
			url: '/ajax/delSessionData',
			data: 'key=filter|usercosts|filter_problems',
			method: 'POST',
			success: function() {}
		});
	}
}

$(document).ready(function() {
	$('#filter_problems').change(function() {
		handleProblematicRowsVisibility();
	});

	$(".del_usercost").click(function() {
		idusercost = $(this).data("idusercost");
		nameusercost = $(this).data("nameusercost");
		$("#overlay, .form_del_wrapper").fadeIn();
		$("#id_usercost_to_del").val(idusercost);
		$("#usercost_to_del").html(nameusercost);
		$("#usercost_to_assoc option").show();
		$("#usercost_to_assoc option[value='"+idusercost+"']").hide();
	});
	
	$("#del_usercost_proceed").click(function() {
		// if ($("#usercost_to_assoc").val()) {
			$("#del_usercost_form").submit();
		// } else {
			// alert("Devi selezionare un servizio a cui associare gli utenti!");
		// }
	});
	
	$("#del_usercost_close").click(function() {
		$("#overlay, .form_del_wrapper").fadeOut();
	});

	$('.form_reset_btn').click(function() {
		$(this).closest('form').find('input').val("");
		$(this).closest('form').find('select').val("");
		$(this).closest('form').find('input[type=checkbox]').prop('checked', false);
	});
});
</script>