<h1><i class="fa fa-bars"></i> Storico messaggi</h1>
<?php
// echo '<pre>';
// print_r($messages);
// echo '</pre>';
?>
<div class="row">
	<div class="c12">
		<table id="mess_table">
			<thead>
				<tr>
					<th>Data</th>
					<th>Oggetto</th>
					<th>Invio mail</th>
					<th>Invio sms</th>
					<th>Num. utenti</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach ($messages as $m) {
					?>
					<tr>
						<td style="width: 150px;"><span style="display: none;"><?=$m['date_creation']?></span><?=implode("/", array_reverse(explode("-", substr($m['date_creation'], 0, 10))))." ".substr($m['date_creation'], 11, 5)?></td>
						<td>
							<?=$m['subject']?>
							<div class="mess_info" id="mess_info_<?=$m['id']?>">
								<p><b>Testo:</b> <?=$m['body']?></p>
								<p><b>Destinatari:</b> 
									<?php
									$str_dest = "";
									foreach ($m['recipients'] as $rec) {
										$str_dest .= $rec['last_name']." ".$rec['first_name'].", ";
									}
									echo trim($str_dest, ", ");
									?>
								</p>
							</div>
						</td>
						<td style="width: 100px; text-align: center;"><?=($m['send_mail'] ? 'Sì' : 'No')?></td>
						<td style="width: 100px; text-align: center;"><?=($m['send_sms'] ? 'Sì' : 'No')?></td>
						<td style="width: 100px; text-align: center;"><?=$m['num_users']?></td>
						<td class="actions" style="width: 60px; text-align: center;">
							<a href="#" class="mess_info_open" data-id="<?=$m['id']?>"><i class="fa fa-plus"></i></a>
							<a class="del_mess" data-idmess="<?=$m['id']?>" href="#"><i class="fa fa-trash"></i></a>
						</td>
					</tr>
					<?php
				}
				?>
			</tbody>
			<tfoot></tfoot>
		</table>
	</div>
</div>

<link rel="stylesheet" type="text/css" href="/lib/datatable/datatables.min.css"/>
 <script type="text/javascript" src="/lib/datatable/datatables.min.js"></script>

<script>
$(document).ready(function() {
    $('#mess_table').DataTable({
        "language": {
            "lengthMenu": "Mostra _MENU_ risultati per pagina",
            "zeroRecords": "Non ho trovato nessun risultato",
            "info": "Pagina _PAGE_ di _PAGES_",
            "infoEmpty": "Nessun risultato disponibile",
            "infoFiltered": "(filtrati da _MAX_ risultati totali)"
        }
    });
	
	$(document).on('click', ".del_mess", function() {
		idmess = $(this).data("idmess");
		if (confirm('Sei sicuro di voler cancellare questo messaggio?'))
			window.location.href="/admin/del_mess/"+idmess;
		return false;
	});
	
	$(document).on('click', '.mess_info_open', function() {
		console.log("ho aperto le info sul messaggio");
		mess_id = $(this).data("id");
		$(".mess_info").hide();
		$("#mess_info_"+mess_id).fadeIn();
	});
});
</script>