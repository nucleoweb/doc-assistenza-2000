<div class="row space-bot">
	<div class="c12">
		<a href="/admin" class="back home">Torna all'homepage</a>
		<a href="/admin/view_usercosts" class="back">Torna ai costi dipendente</a>
	</div>
</div>
<h1><i class="fa fa-eur" aria-hidden="true"></i> Modifica costo dipendente</h1>
<form class="admin detail_form" action="/admin/edit_usercost" method="POST">
	<input type="hidden" name="id_usercost" value="<?=@$usercost['id']?>">
	<label>Dipendente</label>
	<select name="id_user">
		<option value="">- Seleziona un dipendente -</option>
		<?php
		foreach ($users as $u)
			echo '<option value="'.$u['id_user'].'" '.($u['id_user'] == $usercost['id_user'] ? 'selected' : '').'>'.$u['last_name'].' '.$u['first_name'].'</option>';
		?>
	</select>
	<select name="id_costcenter">
		<option value="">- Seleziona un centro di costo -</option>
		<?php
		foreach ($costcenters as $center)
			echo '<option value="'.$center['id'].'" '.($center['id'] == $usercost['id_costcenter'] ? 'selected' : '').'>'.$center['name'].' - '.$center['location'].'</option>';
		?>
	</select>
	<div class="row">
		<div class="c4">
			<label>Codice fiscale</label>
			<input type="text" name="codfisc" placeholder="Codice fiscale" value="<?=@$usercost['codfisc']?>">
		</div>
		<div class="c4">
			<label>Nome</label>
			<input type="text" name="nome" placeholder="Nome" value="<?=@$usercost['nome']?>">
		</div>
		<div class="c4">
			<label>Cognome</label>
			<input type="text" name="cognome" placeholder="Cognome" value="<?=@$usercost['cognome']?>">
		</div>
	</div>
	<div class="row">
		<div class="c4">
			<label>Data assunzione</label>
			<input type="date" name="data_assunzione" placeholder="Data assunzione" value="<?=@$usercost['data_assunzione']?>">
		</div>
		<div class="c4">
			<label>Data cessazione</label>
			<input type="date" name="data_cessazione" placeholder="Data cessazione" value="<?=@$usercost['data_cessazione']?>">
		</div>
		<div class="c4">
			<label>Data elaborazione</label>
			<input type="date" name="data_elaborazione" placeholder="Data elaborazione" value="<?=@$usercost['data_elaborazione']?>">
		</div>
	</div>
	<div class="row" style="padding: 20px; background: #f6f6f6;">
		<div class="c12 first">
			<label><b>Periodo di competenza</b></label><br><br>
			<select name="period_month" style="width: 45%; float: left; clear: none; margin: 0 10px 0 0;"><?php for ($m = 1; $m <= 12; $m++) { echo '<option value="'.$m.'" '.($m == $usercost['period_month'] ? 'selected' : '').'>'.monthNameByNum($m).'</option>'; } ?></select>
			<select name="period_year" style="width: 45%; float: left; clear: none; margin: 0 10px 0 0;"><?php for ($y = 2020; $y <= ((int)date('Y')+5); $y++) { echo '<option value="'.$y.'" '.($y == $usercost['period_year'] ? 'selected' : '').'>'.$y.'</option>'; } ?></select>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="c4">
			<label>Importo retribuzione <span id="calculated_amount_by_sum"></span></label>
			<input type="number" step="0.0000000001" name="amount" placeholder="Importo retribuzione" id="cost_retribuzione" value="<?=@$usercost['amount']?>">
		</div>
		<div class="c4">
			<label>Num. ore lavorate</label>
			<input type="number" step="0.01" name="hours" placeholder="Ore lavorate" value="<?=@$usercost['hours']?>">
		</div>
		<div class="c4">
			<label>Costo orario</label>
			<input type="number" step="0.0000000001" name="hourly_cost" id="hourly_cost" placeholder="Costo orario" value="<?=@$usercost['hourly_cost']?>" readonly>
		</div>
	</div>

	<div class="amount_detail_box">
		<label>
			<b>Suddividi costo</b>
			<a href="#" id="add_row" style="margin-left: 30px;"><i class="fa fa-plus" aria-hidden="true" style="margin: 0;"></i> Aggiungi riga dettaglio</a>
		</label><br><br>
		<div class="row detail_title_row">
			<div class="num_col"></div>
			<div class="c2">Centro di costo</div>
			<div class="c1">Mese</div>
			<div class="c1">Anno</div>
			<div class="c2">Importo</div>
			<div class="c2">Num. ore</div>
			<div class="c2">Costo orario</div>
			<div class="c1"></div>
		</div>
		<?php
		if (!empty($usercost)) {
			foreach ($usercost['detail'] as $k => $row) {
				?>
				<div class="row amount_detail_row">
					<div class="row_number_wrapper">
						<span class="row_number"><?=($k+1)?></span>
					</div>
					<div class="c2">
						<select name="detail[id_costcenter][]">
							<option value="">- Scegli centro di costo -</option>
							<?php
							foreach ($costcenters as $cc) {
								echo '<option value="'.$cc['id'].'" '.($cc['id'] == $row['id_costcenter'] ? 'selected' : '').'>'.$cc['name'].($cc['location'] ? ' - '.$cc['location'] : '').'</option>';
							}
							?>
						</select>
					</div>
					<div class="c1">
						<select name="detail[period_month][]">
							<?php for ($m = 1; $m <= 12; $m++) { echo '<option value="'.$m.'" '.($m == $row['period_month'] ? 'selected' : '').'>'.monthNameByNum($m).'</option>'; } ?>
						</select>
					</div>
					<div class="c1">
						<select name="detail[period_year][]">
							<?php for ($y = (int)(date('Y')-1); $y <= ((int)date('Y')+5); $y++) { echo '<option value="'.$y.'" '.($y == $row['period_year'] ? 'selected' : '').'>'.$y.'</option>'; } ?>
						</select>
					</div>
					<div class="c2">
						<input type="number" step="0.0000000001" name="detail[amount][]" class="amount" placeholder="Importo" value="<?=$row['amount']?>" readonly>
					</div>
					<div class="c2">
						<input type="number" step="0.01" name="detail[hours][]" class="hours" placeholder="Num. ore" value="<?=$row['hours']?>">
					</div>
					<div class="c2">
						<input type="number" step="0.0000000001" name="detail[hourly_cost][]" class="hourly_cost" placeholder="Costo orario" value="<?=$row['hourly_cost']?>">
					</div>
					<div class="c1">
						<a href="#" class="btn darkblue clone_row"><i class="fa fa-clone" aria-hidden="true"></i></a>
						<a href="#" class="btn red remove_row"><i class="fa fa-times" aria-hidden="true"></i></a>
					</div>
				</div>
				<?php
			}	
		}
		?>
	</div>
	<br>
	<a href="#" class="btn blue" id="submit_btn">Salva</a>
</form>

<script>
function check_sum_details() {
	// Prendo il valore totale del costo
	var amount = parseFloat($('#cost_retribuzione').val()).toFixed(2);
	// Calcolo la somma di tutte le voci di dettaglio
	var total = 0;
	$('.amount').each(function() {
		total += parseFloat($(this).val());
	});
	$('#calculated_amount_by_sum').html('Calcolato: '+total.toFixed(2));
	if (amount != total.toFixed(2)) {
		$('.amount').css('background', '#eda8a8');

		// let diff = total - amount;

		// Swal.fire({
		// 	icon: 'warning',
		// 	title: 'Difformità nei valori',
		// 	html: "La somma delle voci di dettaglio non è uguale all'importo totale del costo fornitore.<br>"+
		// 			"Costo fornitore: <b>"+amount+"</b><br>"+
		// 			"Totale dettaglio: <b>"+total+"</b><br>"+
		// 			"Differenza: <b>"+diff+"€</b>.<br>"+
		// 			"Per favore controlla."
		// });
	} else {
		$('.amount').css('background', '#c1eaa6');
	}
	return false;
}

function renumber_rows() {
	let numrow = 1;
	$('.amount_detail_box .amount_detail_row').each(function() {
		$(this).find('.row_number').html(numrow);
		numrow++;
	});
	return false;
}

function calculate_detail_amounts() {
	$('.amount_detail_box .amount_detail_row').each(function() {
		let calculated_total = parseFloat($(this).find('.hours').val()) * parseFloat($(this).find('.hourly_cost').val());
		$(this).find('.amount').val(calculated_total.toFixed(10));
	});
	check_sum_details();
}

function add_row() {
	let new_row = $('.amount_detail_box .amount_detail_row').last().clone();
	new_row.find('input').val('');
	new_row.find('select').val('');
	new_row.find('.hours').val(0);
	new_row.find('.hourly_cost').val($('#hourly_cost').val());
	$('.amount_detail_box').append(new_row);
	renumber_rows();
	calculate_detail_amounts();
	check_sum_details();
	return false;
}

function is_valid() {
	var is_valid = true;
	$('.amount_detail_box input, .amount_detail_box select').each(function() {
		if (!$(this).val() || $(this).val() == 0 || $(this).val() == "0")
			is_valid = false;
	});
	return is_valid;
}

$(document).ready(function() {
	$('#add_row').click(function() {
		add_row();
		return false;
	});

	$('#submit_btn').click(function() {
		if (is_valid()) {
			$('.detail_form').submit();
		} else {
			Swal.fire({
				icon: 'warning',
				title: 'Controlla i dati inseriti',
				html: 'Alcuni dati non sono stati compilati o sono inesatti. Controlla e riprova per favore.'
			});
		}			
		return false;
	});
});

$(document).on('change', 'input.amount', function() {
	check_sum_details();
	return false;
});

$(document).on('change', 'input.hours', function() {
	calculate_detail_amounts();
	return false;
});

$(document).on('click', '.clone_row', function() {
	$(this).closest('.amount_detail_row').clone().appendTo('.amount_detail_box');
	renumber_rows();
	check_sum_details();
	return false;
});

$(document).on('click', '.remove_row', function() {
	$(this).closest('.amount_detail_row').remove();
	renumber_rows();
	check_sum_details();
	return false;
});

$(function() {
	check_sum_details();
	return false;
});
</script>