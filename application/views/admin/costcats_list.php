<h1><i class="fa fa-folder-open" aria-hidden="true"></i> Gestione categorie di costo</h1>
<div class="row">
	<div class="c9">
		<table>
		<tr>
			<th>ID</th>
			<th>Nome</th>
			<th style="width: 80px;"></th>
		</tr>
		<?php foreach ($costcats as $costcat) { ?>
			<tr>
				<td><?=$costcat['id']?></td>
				<td><?=$costcat['name']?></td>
				<td class="actions">
					<a href="/admin/edit_costcat/<?=$costcat['id']?>"><i class="fa fa-pencil"></i></a>
					<a class="del_costcat" data-idcostcat="<?=$costcat['id']?>" data-namecostcat="<?=htmlentities($costcat['name'])?>" href="#"><i class="fa fa-trash"></i></a>
				</td>
			</tr>
		<?php } ?>
		</table>
	</div>
	<div class="c3" style="padding-left: 40px;">
		<h2>Inserisci categoria di costo</h2>
		<form class="admin" action="/admin/insert_costcat" method="POST">
			<input type="text" name="name" placeholder="Nome categoria di costo" value="<?=@$info['name']?>">
			<input type="text" name="note" placeholder="Note categoria di costo" value="<?=@$info['note']?>">
			<br>
			<button type="submit" class="blue">Salva</button>
		</form>
	</div>
</div>

<div id="overlay"></div>
<div class="form_del_wrapper">
	<form id="del_costcat_form" class="admin" action="/admin/del_costcat" method="POST">
		<input name="id_costcat_to_del" id="id_costcat_to_del" type="hidden" value="">
		<h4>Stai per cancellare la categoria di costo [ <span id="costcat_to_del"></span> ]</h4>
		<a id="del_costcat_close" class="btn orange" href="#">Annulla</a>
		<a id="del_costcat_proceed" class="btn green" href="#" style="float: right;">Elimina categoria di costo</a>
	</form>
</div>

<script>
$.validate({
	errorMessagePosition: 'top'
});

$(document).ready(function() {
	$(".del_costcat").click(function() {
		idcostcat = $(this).data("idcostcat");
		namecostcat = $(this).data("namecostcat");
		$("#overlay, .form_del_wrapper").fadeIn();
		$("#id_costcat_to_del").val(idcostcat);
		$("#costcat_to_del").html(namecostcat);
		$("#costcat_to_assoc option").show();
		$("#costcat_to_assoc option[value='"+idcostcat+"']").hide();
	});
	
	$("#del_costcat_proceed").click(function() {
		// if ($("#costcat_to_assoc").val()) {
			$("#del_costcat_form").submit();
		// } else {
			// alert("Devi selezionare un servizio a cui associare gli utenti!");
		// }
	});
	
	$("#del_costcat_close").click(function() {
		$("#overlay, .form_del_wrapper").fadeOut();
	});
});
</script>