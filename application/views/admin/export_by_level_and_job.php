<h1><i class="fa fa-external-link" aria-hidden="true"></i> Esporta utenti per livello e mansione</h1>

<form action="" method="POST">
    <div class="row">
        <div class="c12">
            <h3>Seleziona il livello</h3>
            <select name="level" id="level">
                <option value="">- Scegli un livello -</option>
                <?php foreach ($levels as $lev) { ?>
                    <option value="<?=$lev['id']?>|<?=$lev['name']?>"><?=$lev['name']?> (<?=$lev['num_users']?> utenti)</option>
                <?php } ?>
            </select>           
        </div>
    </div>
    <div class="row space-top" id="jobs_row">
        <div class="c12">            
            <ul class="check_list"></ul>
        </div>
    </div>
    <div class="row space-top" id="only_active_check_row">
        <div class="c12">            
            <label for="only_hired_check"><input type="checkbox" name="only_hired" value="1" id="only_hired_check"> Esporta solo utenti attualmente in forza</label>
        </div>
        <div class="c12 space-top">            
            <label for="only_active_check"><input type="checkbox" name="only_active" value="1" id="only_active_check"> Esporta solo utenti attivi</label>
        </div>
    </div>
    <div class="row space-top" id="submit_row">
		<div class="c12">
			<button type="submit" class="blue">Esporta</button>
        </div>
    </div>
</form>

<style>
#submit_row, #only_active_check_row {
    display: none;
}
</style>

<script>
$(document).ready(function() {
    $("#level").change(function() {
        $('#jobs_row ul.check_list').html('');
        $('#submit_row').hide();
        $('#only_active_check_row').hide();
        $.ajax({
            url: '/ajax/getJobsByLevel',
            method: 'POST',
            data: 'id_level='+$(this).val(),
            dataType: 'json',
            success: function(ar_jobs) {
                ar_jobs.forEach((job) => {
                    $('#jobs_row ul.check_list').append('<li><label for="job_'+job.id+'"><input type="checkbox" name="job_list[]" value="'+job.id+'|'+job.name+'" id="job_'+job.id+'"> '+job.name+'</label></li>');
                });
                $('#only_active_check_row').show();
                $('#submit_row').show();
            }
        });
    });
});
</script>