<h1><i class="fa fa-calculator"></i> Calcola mesi di servizio</h1>
<?php if (!empty($serv_months)) { ?>
    <p>I mesi di servizio dal <b><?=$date_start?></b> al <b><?=$date_end?></b> sono: <b><?=$serv_months?></b></p>
    <br>
    <a class="btn blue" href="/admin/calc_serv_months">Effettua un altro calcolo</a>
<?php } else { ?>
    <form class="admin" action="/admin/calc_serv_months" method="POST">
        <div class="row">
            <div class="c6">
                <label class="txtInput">Data di assunzione</label>
                <input type="text" name="date_start" class="datepicker">
            </div>
            <div class="c6">
                <label class="txtInput">Data di interruzione rapporto</label>
                <input type="text" name="date_end" class="datepicker">
            </div>
        </div>
        <div class="row">
            <div class="c12">
                <button type="submit" class="blue">Calcola</button>
            </div>
        </div>
    </form>
<?php } ?>