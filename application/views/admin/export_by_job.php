<h1><i class="fa fa-external-link" aria-hidden="true"></i> Esporta utenti per mansione</h1>
<h3>Seleziona le mansioni da esportare</h3>

<form action="" method="POST">
    <div class="row">
        <div class="c12">
            <ul class="check_list">
            <?php foreach ($jobs as $job) { ?>
                <li>
                    <label for="job_<?=$job['id']?>">
                        <input type="checkbox" name="job_list[]" value="<?=$job['id']?>|<?=$job['name']?>" id="job_<?=$job['id']?>"> <?=$job['name']?> 
                    </label>
                    (<?=$job['num_users']?> utenti)
                </li>
            <?php } ?>
            </ul>
        </div>
    </div>
    <div class="row space-top" id="only_active_check_row">
        <div class="c12">            
            <label for="only_hired_check"><input type="checkbox" name="only_hired" value="1" id="only_hired_check"> Esporta solo utenti attualmente in forza</label>
        </div>
        <div class="c12 space-top">            
            <label for="only_active_check"><input type="checkbox" name="only_active" value="1" id="only_active_check"> Esporta solo utenti attivi</label>
        </div>
    </div>
    <div class="row space-top" id="submit_row">
		<div class="c12">
			<button type="submit" class="blue">Esporta</button>
        </div>
    </div>
</form>
