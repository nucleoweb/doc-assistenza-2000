<div class="row space-bot">
	<div class="c12">
		<a href="/admin" class="back">Torna all'homepage</a>
	</div>
</div>
<h1><i class="fa fa-handshake-o"></i> Modifica cliente</h1>
<form class="admin" action="/admin/edit_customer" method="POST">
	<?php if (!empty($customer)) { ?>
		<input type="hidden" name="id_customer" value="<?=@$customer['id']?>">
	<?php } ?>
	<input type="text" name="name" placeholder="Nome cliente" value="<?=@$customer['name']?>" data-validation="required">
	<br>
	<button type="submit" class="blue">Salva</button>
</form>