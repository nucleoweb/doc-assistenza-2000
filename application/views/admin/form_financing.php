<div class="row space-bot">
	<div class="c12">
		<a href="/admin" class="back home">Torna all'homepage</a>
		<a href="/admin/view_financings" class="back">Torna ai bandi/contributi</a>
	</div>
</div>
<h1><i class="fa fa-eur" aria-hidden="true"></i> Modifica bando/contributo</h1>
<form class="admin detail_form" action="/admin/edit_financing" method="POST">
	<input type="hidden" name="id_financing" value="<?=@$financing['id']?>">
	<label>Denominazione</label>
	<input type="text" name="name" placeholder="Denominazione contributo" value="<?=@$financing['name']?>">
	<label>Descrizione</label>
	<textarea name="description" placeholder="Descrizione contributo"><?=@$financing['description']?></textarea>
	<label>Importo totale</label>
	<input type="number" step="0.01" name="amount" placeholder="Importo contributo" value="<?=@$financing['amount']?>">
	<div class="row" style="padding: 20px; background: #f6f6f6;">
		<div class="c6 first">
			<label><b>Inizio periodo</b></label><br><br>
			<select name="from_month" style="width: 45%; float: left; clear: none; margin: 0 10px 0 0;"><?php for ($m = 1; $m <= 12; $m++) { echo '<option value="'.$m.'" '.($m == $financing['from_month'] ? 'selected' : '').'>'.monthNameByNum($m).'</option>'; } ?></select>
			<select name="from_year" style="width: 45%; float: left; clear: none; margin: 0 10px 0 0;"><?php for ($y = 2020; $y <= ((int)date('Y')+5); $y++) { echo '<option value="'.$y.'" '.($y == $financing['from_year'] ? 'selected' : '').'>'.$y.'</option>'; } ?></select>
		</div>
		<div class="c6 last">
			<label><b>Fine periodo</b></label><br><br>
			<select name="to_month" style="width: 45%; float: left; clear: none; margin: 0 10px 0 0;"><?php for ($m = 1; $m <= 12; $m++) { echo '<option value="'.$m.'" '.($m == $financing['to_month'] ? 'selected' : '').'>'.monthNameByNum($m).'</option>'; } ?></select>
			<select name="to_year" style="width: 45%; float: left; clear: none; margin: 0 10px 0 0;"><?php for ($y = 2020; $y <= ((int)date('Y')+5); $y++) { echo '<option value="'.$y.'" '.($y == $financing['to_year'] ? 'selected' : '').'>'.$y.'</option>'; } ?></select>
		</div>
	</div>
	<br>
	<label style="margin-bottom: 10px;"><b>Collega i centri di costo</b></label>
	<?php foreach ($costcenters as $cc) { ?>
		<div><label class="list" for="cc_<?=$cc['id']?>"><input type="checkbox" id="cc_<?=$cc['id']?>" name="cc[]" value="<?=$cc['id']?>" <?=(in_array($cc['id'], json_decode($financing['costcenters'])) ? 'checked' : '')?> /> <?=$cc['name']?></label></div>
	<?php } ?>
	<br>
	<button type="submit" class="blue">Salva</button>
</form>