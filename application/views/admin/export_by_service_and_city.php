<h1><i class="fa fa-external-link" aria-hidden="true"></i> Esporta utenti per servizio e città</h1>

<form action="" method="POST">
    <div class="row">
        <div class="c12">
            <h3>Seleziona il servizio</h3>
            <select name="service" id="service">
                <option value="">- Scegli un servizio -</option>
                <?php foreach ($services as $serv) { ?>
                    <option value="<?=$serv['id']?>|<?=$serv['name']?>"><?=$serv['name']?> (<?=$serv['num_users']?> utenti)</option>
                <?php } ?>
            </select>           
        </div>
    </div>
    <div class="row space-top" id="areas_row">
        <div class="c12">            
            <ul class="check_list"></ul>
        </div>
    </div>
    <div class="row space-top" id="only_active_check_row">
        <div class="c12">            
            <label for="only_hired_check"><input type="checkbox" name="only_hired" value="1" id="only_hired_check"> Esporta solo utenti attualmente in forza</label>
        </div>
        <div class="c12 space-top">            
            <label for="only_active_check"><input type="checkbox" name="only_active" value="1" id="only_active_check"> Esporta solo utenti attivi</label>
        </div>
    </div>
    <div class="row space-top" id="submit_row">
		<div class="c12">
			<button type="submit" class="blue">Esporta</button>
        </div>
    </div>
</form>

<style>
#submit_row, #only_active_check_row {
    display: none;
}
</style>

<script>
$(document).ready(function() {
    $("#service").change(function() {
        $('#areas_row ul.check_list').html('');
        $('#submit_row').hide();
        $('#only_active_check_row').hide();
        $.ajax({
            url: '/ajax/getCitiesByService',
            method: 'POST',
            data: 'id_service='+$(this).val(),
            dataType: 'json',
            success: function(ar_areas) {
                // console.log(ar_areas);
                ar_areas.forEach((area) => {
                    $('#areas_row ul.check_list').append('<li><label for="area_'+area.id+'"><input type="checkbox" name="area_list[]" value="'+area.id+'|'+area.name+'" id="area_'+area.id+'"> '+area.name+'</label></li>');
                });
                $('#only_active_check_row').show();
                $('#submit_row').show();
            }
        });
    });
});
</script>