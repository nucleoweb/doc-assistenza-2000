<div class="row">
	<div class="c12" id="admin_menubar">
		<ul class="menu">
			<li>
				<a class="btn" href="/admin"><i class="fa fa-home"></i> Homepage</a>
			</li>
			<li>
				<a class="btn" href="#"><i class="fa fa-table"></i> Anagrafiche</a>
				<ul class="submenu">
					<li><a class="btn" href="/admin/view_jobs"><i class="fa fa-briefcase"></i> Gestisci mansioni</a></li>
					<li><a class="btn" href="/admin/view_levels"><i class="fa fa-sort"></i> Gestisci livelli</a></li>
					<li><a class="btn" href="/admin/view_areas"><i class="fa fa-map-marker"></i> Gestisci aree</a></li>
					<li><a class="btn" href="/admin/view_services"><i class="fa fa-ambulance"></i> Gestisci servizi</a></li>
					<li><a class="btn" href="/admin/view_customers"><i class="fa fa-handshake-o"></i> Gestisci clienti</a></li>
				</ul>
			</li>
			<li>
				<a class="btn" href="#"><i class="fa fa-users"></i> Dipendenti</a>
				<ul class="submenu">
					<li><a class="btn" href="/admin/insert_user"><i class="fa fa-user"></i> Inserisci utente</a></li>
					<li><a class="btn" href="/admin/report"><i class="fa fa-bar-chart"></i> Visualizza report</a></li>
					<li class="have_submenu"><a class="btn" href="#"><i class="fa fa-external-link"></i> Esporta utenti</a>
						<ul class="submenu lv2">
							<li><a class="btn" href="/admin/export" target="_blank"><i class="fa fa-users"></i> Esporta tutti</a></li>
							<li><a class="btn" href="/admin/export_by_level"><i class="fa fa-sort"></i> Esporta per livello</a></li>
							<li><a class="btn" href="/admin/export_by_job"><i class="fa fa-briefcase"></i> Esporta per mansione</a></li>
							<li><a class="btn" href="/admin/export_by_level_and_job"><i class="fa fa-briefcase"></i> Esporta per livello e mansione</a></li>
							<li><a class="btn" href="/admin/export_by_service"><i class="fa fa-ambulance"></i> Esporta per servizio</a></li>
							<li><a class="btn" href="/admin/export_by_service_and_city"><i class="fa fa-building"></i> Esporta per servizio e città</a></li>
						</ul>
					</li>
					<li><a class="btn" href="/admin/calc_serv_months"><i class="fa fa-calculator"></i> Calcola mesi servizio</a></li>
					<li style="background: #ffe3b7 !important;"><a class="btn" href="/admin/goToPlatform" target="_blank"><i class="fa fa-database"></i> Apri piattaforma ECM</a></li>
				</ul>
			</li>
			<li>
				<a class="btn" href="#"><i class="fa fa-file-text-o"></i> Documenti</a>
				<ul class="submenu">
					<li><a class="btn" href="/admin/insert_doc"><i class="fa fa-file-text-o"></i> Carica documento</a></li>
					<li><a class="btn" href="/admin/fetch_doc"><i class="fa fa-link"></i> Associa documenti</a></li>
					<li><a class="btn" href="/admin/deassoc_doc"><i class="fa fa-chain-broken"></i> Rimuovi associazioni</a></li>
					<li><a class="btn" href="/admin/view_cats"><i class="fa fa-folder-open"></i> Gestisci categorie</a></li>
					<li><a class="btn" href="/admin/upload_paysheets"><i class="fa fa-upload"></i> Carica PDF buste paga</a></li>
				</ul>
			</li>
			<li>
				<a class="btn" href="#"><i class="fa fa-envelope"></i> Messaggi</a>
				<ul class="submenu">
					<li><a class="btn" href="/admin/write_to_users"><i class="fa fa-envelope"></i> Invia messaggio</a></li>
					<li><a class="btn" href="/admin/list_messages"><i class="fa fa-bars"></i> Storico messaggi</a></li>
				</ul>
			</li>
			<li>
				<a class="btn" href="#"><i class="fa fa-question-circle"></i> Sondaggi</a>
				<ul class="submenu">
					<li><a class="btn" href="/admin/list_polls"><i class="fa fa-question-circle"></i> Gestisci sondaggi</a></li>
				</ul>
			</li>
			<li>
				<a class="btn" href="#"><i class="fa fa-line-chart"></i> Bilanci</a>
				<ul class="submenu">
					<li class="have_submenu"><a class="btn" href="#"><i class="fa fa-list"></i> Gestisci anagrafiche</a>
						<ul class="submenu lv2 left">
							<li><a class="btn" href="/admin/view_costcenters"><i class="fa fa-money"></i> Gestisci centri di costo</a></li>
							<li><a class="btn" href="/admin/view_costcats"><i class="fa fa-folder-open"></i> Gestisci categorie di costo</a></li>
							<li><a class="btn" href="/admin"><i class="fa fa-users"></i> Gestisci dipendenti</a></li>
							<li><a class="btn" href="/admin/view_suppliers"><i class="fa fa-building"></i> Gestisci fornitori</a></li>
							<li><a class="btn" href="/admin/view_bcustomers"><i class="fa fa-building"></i> Gestisci clienti</a></li>
							<li><a class="btn" href="/admin/view_doctypes"><i class="fa fa-file-text"></i> Gestisci tipi documento</a></li>
						</ul>
					</li>
					<li class="have_submenu"><a class="btn" href="#"><i class="fa fa-upload"></i> Importa dati</a>
						<ul class="submenu lv2 left">
							<li><a class="btn" href="/admin/import_revenues"><i class="fa fa-upload"></i> Importa ricavi</a></li>
							<li><a class="btn" href="/admin/import_suppliercosts"><i class="fa fa-upload"></i> Importa costi fornitori</a></li>
							<li><a class="btn" href="/admin/import_usercosts"><i class="fa fa-upload"></i> Importa costi dipendenti</a></li>
						</ul>
					</li>
					<li class="have_submenu"><a class="btn" href="#"><i class="fa fa-eur"></i> Gestisci entrate / uscite</a>
						<ul class="submenu lv2 left">
							<li><a class="btn" href="/admin/view_financings"><i class="fa fa-eur"></i> Gestisci bandi - contributi</a></li>
							<li><a class="btn" href="/admin/view_revenues"><i class="fa fa-eur"></i> Gestisci ricavi</a></li>
							<li><a class="btn" href="/admin/view_suppliercosts"><i class="fa fa-eur"></i> Gestisci costi fornitori</a></li>
							<li><a class="btn" href="/admin/view_usercosts"><i class="fa fa-eur"></i> Gestisci costi dipendenti</a></li>
						</ul>
					</li>
					<li class="have_submenu"><a class="btn" href="#"><i class="fa fa-external-link"></i> Esporta / Report</a>
						<ul class="submenu lv2 left">
							<li><a class="btn" href="/admin/export_balance"><i class="fa fa-external-link"></i> Esporta bilancio</a></li>
							<li><a class="btn" href="/admin/report_balance"><i class="fa fa-bar-chart"></i> Crea report</a></li>
						</ul>
					</li>
					<li><a class="btn" href="/admin/delete_balance_data"><i class="fa fa-trash"></i> Elimina dati</a></li>
				</ul>
			</li>
		</ul>
	</div>
</div>