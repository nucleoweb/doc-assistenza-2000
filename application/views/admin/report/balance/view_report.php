<div class="row space-bot">
	<div class="c12">
		<a href="/admin" class="back home">Torna all'homepage</a>
		<a href="/admin/report_balance" class="back">Crea un report diverso</a>
	</div>
</div>

<h1 style="margin: 25px 0;"><i class="fa fa-bar-chart"></i> Report bilanci</h1>

<div class="row">
    <div class="c12">
        <form action="/admin/export_report" method="POST">
            <input type="hidden" name="data" value="<?=base64_encode(json_encode($data))?>">
            <button type="submit"><i class="fa fa-file-excel-o" style="font-size: 20px;"></i> Exporta in XLS</button>
        </form>
    </div>
</div>

<div class="row">
    <div class="c12">
        <?php
        foreach ($data as $year => $months) {
            foreach ($months as $month => $centers) {

                echo '<h2 style="padding-top: 20px; border-top: 1px dotted #555;">'.$year.' '.monthNameByNum($month).'</h2>';

                echo '<div style="width: 100%; overflow-x: scroll; margin-bottom: 40px;">';
                    echo '<table style="border: 0;">';
                        echo '<tr>';
                            echo '<th style="width: 200px;"></th>';
                            foreach ($centers as $center => $sums) {
                                echo '<th style="font-weight: 600; color: #111; font-size: 12px; line-height: 16px; min-width: 150px;">'.($center == 'x' ? 'TOTALE' : $center).'</th>';
                            }
                        echo '</tr>';
                        echo '<tr>';
                            echo '<td style="font-weight: 600; color: #111; min-width: 160px;">RICAVI</td>';
                            foreach ($centers as $center => $sums) {
                                echo '<td>'.number_format($sums['revenues'], 2, ",", ".").' €</td>';
                            }
                        echo '</tr>';
                        echo '<tr>';
                            echo '<td style="font-weight: 600; color: #111; min-width: 160px;">COSTI DIPENDENTI</td>';
                            foreach ($centers as $center => $sums) {
                                echo '<td>'.number_format($sums['usercosts'], 2, ",", ".").' €</td>';
                            }
                        echo '</tr>';
                        echo '<tr>';
                            echo '<td style="font-weight: 600; color: #111;">COSTI FORNITORI</td>';
                            foreach ($centers as $center => $sums) {
                                echo '<td>'.number_format($sums['suppliercosts'], 2, ",", ".").' €</td>';
                                
                            }
                        echo '</tr>';
                        echo '<tr>';
                            echo '<td style="font-weight: 600; color: #111; min-width: 160px; background: #eee;">SALDO</td>';
                            foreach ($centers as $center => $sums) {
                                $tot = $sums['revenues'] - ($sums['usercosts'] + $sums['suppliercosts']);
                                echo '<td style="font-weight: 600; color: #111; background: '.($tot >= 0 ? '#eaf7c5' : '#f9d9d1').';">'.number_format($tot, 2, ",", ".").' €</td>';
                                
                            }
                        echo '</tr>';
                    echo '</table>';
                echo '</div>';
            }
        }
        ?>
    </div>
</div>