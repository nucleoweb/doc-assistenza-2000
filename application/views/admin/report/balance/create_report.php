<h1><i class="fa fa-bar-chart"></i> Report bilanci</h1>
<div class="row">
    <div class="c6">
        <!-- <p>Seleziona il tipo di report che vuoi visualizzare e i parametri aggiuntivi.</p>
        <br> -->
        <form class="admin" action="/admin/report_balance" method="POST">
            <div class="row" style="margin-top: 20px;">
                <div class="12">
                    <label class="txtInput"><b>Inizio periodo</b></label><br>
					<select name="from_month" style="width: 45%; float: left; clear: none; margin: 0 10px 0 0;" required>
						<option value="">- Scegli il mese -</option>
						<?php for ($m = 1; $m <= 12; $m++) { echo '<option value="'.$m.'">'.monthNameByNum($m).'</option>'; } ?>
					</select>
					<select name="from_year" style="width: 45%; float: left; clear: none; margin: 0 10px 0 0;" required>
						<option value="">- Scegli l'anno -</option>
						<?php for ($y = 2020; $y <= ((int)date('Y')+5); $y++) { echo '<option value="'.$y.'">'.$y.'</option>'; } ?>
					</select>
                </div>
            </div>
            <div class="row" style="margin-top: 20px;">
                <div class="c12">
                    <label class="txtInput"><b>Fine periodo</b></label><br>
					<select name="to_month" style="width: 45%; float: left; clear: none; margin: 0 10px 0 0;" required>
						<option value="">- Scegli il mese -</option>
						<?php for ($m = 1; $m <= 12; $m++) { echo '<option value="'.$m.'">'.monthNameByNum($m).'</option>'; } ?>
					</select>
					<select name="to_year" style="width: 45%; float: left; clear: none; margin: 0 10px 0 0;" required>
						<option value="">- Scegli l'anno -</option>
						<?php for ($y = 2020; $y <= ((int)date('Y')+5); $y++) { echo '<option value="'.$y.'">'.$y.'</option>'; } ?>
					</select>
                </div>
            </div>
            <div class="row" style="margin-top: 20px;">
                <div class="c12">
                    <label class="txtInput"><b>Centri di costo</b></label><br>
                    <div style="background: #eee; padding: 5px; border-radius: 5px;"><label for="cc_x"><input type="checkbox" id="cc_x" name="cc[]" value="x" /> <b style="color: #d90000; font-size: 18px;">COMPLESSIVO</b></label></div>
                    <hr>
                    <a href="#" id="select_all_cc" style="background: #111756; padding: 5px 10px; color: #fff; border-radius: 5px;">
                        <i class="fa fa-check-square-o" aria-hidden="true"></i> Seleziona tutti i centri di costo singoli
                    </a>
                    <br><br>
                    <?php
                    foreach ($costcenters as $cc)
                        if ($cc['id'] != 1000)
                            echo '<div style="margin-bottom: 5px;"><label for="cc_'.$cc['id'].'"><input type="checkbox" class="cc_check" id="cc_'.$cc['id'].'" name="cc[]" value="'.$cc['id'].'" /> '.$cc['name'].' - '.$cc['location'].'</label></div>';
                    ?>
                </div>
            </div>
            <div class="row" style="margin-top: 20px;">
                <div class="c12">
                    <button type="submit" class="blue">Visualizza report</button>            
                </div>
            </div>
        </form>
    </div>
    <div class="c6"></div>
</div>

<script>
    $(document).ready(function() {
        $('#cc_x').click(function() {
            $('.cc_check').prop("checked", false);
        });

        $('.cc_check').click(function() {
            $('#cc_x').prop("checked", false);
        });

        $('#select_all_cc').click(function() {
            $('#cc_x').prop("checked", false);
            $('.cc_check').prop("checked", true);
        });
    });
</script>