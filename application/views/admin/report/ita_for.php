<?php
// echo '<pre>';
// print_r($data);
// echo '</pre>';
?>
<h1><i class="fa fa-bar-chart"></i> Report Italiani / stranieri</h1>
<div class="row">
    <div class="c6">
        <table class="report_tab">
            <tr>
                <th colspan="3">Nazionalità</th>
            </tr>
            <tr>
                <td>ITALIANI</td>
                <td><?=$data['ita']?></td>
                <td><?=number_format(($data['ita']/($data['ita']+$data['for'])*100), 1)?>%</td>
            </tr>
            <tr>
                <td>STRANIERI</td>
                <td><?=$data['for']?></td>
                <td><?=number_format(($data['for']/($data['ita']+$data['for'])*100), 1)?>%</td>
            </tr>
            <tr class="closing_row">
                <td>TOTALE</td>
                <td><?=$data['ita']+$data['for']?></td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="c6">
        <div id="chart_div"></div>

        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript">
            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(drawChart);

            function drawChart() {
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Topping');
                data.addColumn('number', 'Slices');
                data.addRows([
                    ['Italiani', <?=$data['ita']?>],
                    ['Stranieri', <?=$data['for']?>],
                ]);

                var options = {'title':'Nazionalità', 'width':'100%', 'height':600};
                var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
                chart.draw(data, options);
            }
        </script>
    </div>
</div>