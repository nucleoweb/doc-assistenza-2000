<?php
// echo '<pre>';
// print_r($data);
// echo '</pre>';
// die();
?>
<h1><i class="fa fa-bar-chart"></i> Fasce d'età</h1>
<div class="row">
    <div class="c6">
        <table class="report_tab">
            <tr>
                <th colspan="3">Fasce d'età</th>
            </tr>
            <?php
            foreach ($data as $label => $num) { 
                if ($label != 'tot') {
                    ?>
                    <tr>
                        <td><?=$label?></td>
                        <td><?=$num?></td>
                        <td><?=number_format(($num/$data['tot']*100), 1)?>%</td>
                    </tr>
                    <?php
                }
            }
            ?>
            <tr class="closing_row">
                <td>TOTALE</td>
                <td><?=$data['tot']?></td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="c6">
        <div id="chart_div"></div>

        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript">
            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(drawChart);

            function drawChart() {
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Topping');
                data.addColumn('number', 'Slices');
                data.addRows([
                    <?php
                    foreach ($data as $label => $num) {
                        if ($label != 'tot') {
                            ?>
                            ["<?=$label?>", <?=$num?>],
                            <?php
                        }
                    }
                    ?>
                ]);

                var options = {'title':'Fasce d\'età', 'width':'100%', 'height':600};
                var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
                chart.draw(data, options);
            }
        </script>
    </div>
</div>