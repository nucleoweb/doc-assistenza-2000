<h1><i class="fa fa-bar-chart"></i> Report utenti</h1>
<div class="row">
    <div class="c6">
        <p>Seleziona il tipo di report che vuoi visualizzare e i parametri aggiuntivi.</p>
        <br>
        <form class="admin" action="/admin/report" method="POST">
            <div class="row">
                <div class="12">
                    <label class="txtInput">Tipo di report</label>
                    <select name="type">
                        <option value="">- Scegli tipo di report -</option>
                        <option value="ita_for">Italiani / stranieri</option>
                        <option value="fasce_eta">Fasce d'età</option>
                    </select>

                    <label class="txtInput">Data di riferimento</label>
                    <input type="text" name="ref_date" class="datepicker" value="<?=date('d/m/Y')?>">
                </div>
            </div>
            <div id="report_filter_block">
                <p>Filtra in base alle opzioni:</p>
                <div class="row">
                    <div class="c6 first">
                        <label class="txtInput">In forza</label>
                        <select name="is_hired">
                            <option value="">- Tutti gli utenti -</option>
                            <option value="1">Solo attualmente in forza</option>
                            <option value="0">Solo non in forza</option>
                        </select>
                    </div>
                    <div class="c6 last">
                        <label class="txtInput">Liberi professionisti</label>
                        <select name="freelance">
                            <option value="">- Tutti gli utenti -</option>
                            <option value="1">Solo liberi professionisti</option>
                            <option value="0">Non liberi professionisti</option>
                        </select>
                    </div>
                </div>
            </div>
            <button type="submit" class="blue">Visualizza report</button>            
        </form>
    </div>
    <div class="c6">
        <p>Note</p>
        <ul id="report_notes_list">
            <li>Il report "Italiani / stranieri" si basa sul campo "Straniero" della scheda utente e non sul campo "Nazionalità"</li>
            <li>Il campo "Data di riferimento" viene considerato solo sui report inerenti l'anzianità dei dipendenti</li>
        </ul>
    </div>
</div>