<?php if (!empty($ret)) { ?>
	<script>
	$(function() {
		<?php if ($ret['status'] == 'ok') { ?>
			Swal.fire({
				icon: 'success',
				title: 'Cancellazione completata',
				html: '<b>Voci eliminate</b>: <?=$ret['deleted_rows']?><br>'
			});
		<?php } else { ?>
			Swal.fire({
				icon: 'error',
				title: 'Errore',
				html: 'Si è verificato un errore durante la cancellazione'
			});
		<?php } ?>
	});
	</script>
<?php } ?>

<div class="row space-bot">
	<div class="c12">
		<a href="/admin" class="back">Torna all'homepage</a>
	</div>
</div>
<h1><i class="fa fa-trash" aria-hidden="true"></i> Elimina dati di bilancio</h1><br>
<form class="admin detail_form" action="/admin/delete_balance_data" method="POST" style="max-width: 600px;">
	<label>Tipo di dati</label>
	<select name="data_type" id="data_type" required>
		<option value="">- Seleziona una tipologia -</option>
		<option value="revenues">Ricavi</option>
		<option value="suppliercosts">Costi fornitori</option>
		<option value="usercosts">Costi dipendente</option>
	</select>
	<label>Periodo comp. (mese)</label>
	<select name="period_month" id="period_month">
		<option value="">- Tutti i mesi -</option>
		<?php for ($m = 1; $m <= 12; $m++) { echo '<option value="'.$m.'">'.monthNameByNum($m).'</option>'; } ?>
	</select>
	<label>Periodo comp. (anno)</label>
	<select name="period_year" id="period_year">
		<option value="">- Scegli l'anno -</option>
		<?php for ($y = 2020; $y <= ((int)date('Y')+5); $y++) { echo '<option value="'.$y.'">'.$y.'</option>'; } ?>
	</select>
	<label>Data di caricamento</label>
	<input type="date" name="upload_date" id="upload_date">
	<br>
	<a href="#" class="btn blue" id="submit_btn">Cancella dati</a>
</form>
<div class="row" style="margin-top: 60px;">
	<hr>
	<div class="c12">
		<p style="background: #eee; padding: 10px; font-size: 16px; font-weight: 600;">Qui sotto sono elencati gli ultimi 30 caricamenti per ogni tipologia di dato, col relativo numero di record presenti; <span style="color: #d90000;">clicca sulla data per eliminare tutti i dati importati</span>, ti verrà chiesta conferma.</p>
	</div>
</div>
<div class="row">
	<div class="c4">
		<h2>Caricamento ricavi</h2>
		<ul>
		<?php foreach ($revenues_imports as $d => $num) { ?>
			<li style="border-bottom: 1px dotted #aaa; padding-bottom: 10px; margin-bottom: 10px;">
				<a href="#" class="import_date_to_delete" data-type="revenues" data-val="<?=$d?>" style="font-size: 18px;">
					<i class="fa fa-trash"></i> <?=rvd($d)?> 
					<span style="background: #eee; padding: 5px 10px 5px 5px; border-radius: 5px; display: inline-block; margin-left: 10px; color: #111;"><i class="fa fa-list"></i> <?=$num?> record</span>
				</a>
			</li>
		<?php } ?>
		</ul>
	</div>
	<div class="c4">
		<h2>Caricamento costi fornitori</h2>
		<ul>
		<?php foreach ($suppliercosts_imports as $d => $num) { ?>
			<li style="border-bottom: 1px dotted #aaa; padding-bottom: 10px; margin-bottom: 10px;">
				<a href="#" class="import_date_to_delete" data-type="suppliercosts" data-val="<?=$d?>" style="font-size: 18px;">
					<i class="fa fa-trash"></i> <?=rvd($d)?> 
					<span style="background: #eee; padding: 5px 10px 5px 5px; border-radius: 5px; display: inline-block; margin-left: 10px; color: #111;"><i class="fa fa-list"></i> <?=$num?> record</span>
				</a>
			</li>
		<?php } ?>
		</ul>
	</div>
	<div class="c4">
		<h2>Caricamento costi dipendenti</h2>
		<ul>
		<?php foreach ($usercosts_imports as $d => $num) { ?>
			<li style="border-bottom: 1px dotted #aaa; padding-bottom: 10px; margin-bottom: 10px;">
				<a href="#" class="import_date_to_delete" data-type="usercosts" data-val="<?=$d?>" style="font-size: 18px;">
					<i class="fa fa-trash"></i> <?=rvd($d)?> 
					<span style="background: #eee; padding: 5px 10px 5px 5px; border-radius: 5px; display: inline-block; margin-left: 10px; color: #111;"><i class="fa fa-list"></i> <?=$num?> record</span>
				</a>
			</li>
		<?php } ?>
		</ul>
	</div>
</div>

<script>
$(document).ready(function() {
	$('#submit_btn').click(function() {
		if ($('#data_type').val()) {
			if ((!$('#period_month').val() && !$('#period_year').val()) || ($('#period_month').val() && $('#period_year').val())) {
				Swal.fire({
					icon: 'question',
					title: 'Sei sicuro?',
					html: "Sei davvero sicuro di voler cancellare i dati dei <b>"+$('#data_type').find(":selected").text().toLowerCase()+"</b> con competenza <b>"+($('#period_month').val() && $('#period_year').val() ? $('#period_month').val()+'/'+$('#period_year').val() : 'TUTTE')+"</b>, caricati in data <b>"+($('#upload_date').val() ? $('#upload_date').val().split('-').reverse().join('/') : 'TUTTE')+"</b>?<br><span style='color: #d90000; font-weight: 600;'>L'operazione non è reversibile!</span>",
					showCancelButton: true,
					confirmButtonText: '<span style="color: #fff;">Sì, procedi</span>',
					cancelButtonText: '<span style="color: #fff;">No, ho cambiato idea</span>',
					confirmButtonColor: "#d90000",
					cancelButtonColor: "#2A317F",
				}).then((result) => {
					if (result.isConfirmed) {
						$('.detail_form').submit();
					}
				});
			} else {
				Swal.fire({
					icon: 'warning',
					title: 'Valori mancanti',
					html: "Se vuoi cancellare per <b>competenza</b>, devi scegliere sia il mese che l'anno"
				});
			}
		} else {
			Swal.fire({
				icon: 'warning',
				title: 'Valori mancanti',
				html: "Devi scegliere almeno la <b>tipologia dei dati</b> da cancellare"
			});
		}
		return false;
	});

	$('.import_date_to_delete').click(function() {
		$('#data_type').val($(this).data('type'));
		$('#upload_date').val($(this).data('val'));
		$('#submit_btn').click();
	})
});
</script>