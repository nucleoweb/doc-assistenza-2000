<h1><i class="fa fa-question-circle"></i> Guarda risultati sondaggio</h1>
<h3><?=$poll['title']?></h3>
<br>
<p id="poll_info_text"><?=$poll['info']?></p>
<br>

<?php 
// echo '<pre>';
// print_r($poll);
// echo '</pre>';
?>

<table>
	<tr>
		<th>ID</th>
		<th>Data inizio</th>
		<th>Data fine</th>
		<!-- <th>Titolo</th> -->
		<th>Stato</th>
		<th>Anonimo</th>
		<th>Num. utenti</th>
		<th>Voti ricevuti</th>
		<th>Affluenza</th>
	</tr>
	<tr>
		<td style="width: 60px; text-align: center;"><?=$poll['id']?></td>
		<td style="width: 120px; text-align: center;"><?=implode("/", array_reverse(explode("-", $poll['date_start'])))?></td>
		<td style="width: 120px; text-align: center;"><?=implode("/", array_reverse(explode("-", $poll['date_end'])))?></td>
		<!-- <td><?=$poll['title']?></td> -->
		<td style="width: 160px; text-align: center; font-weight: bold;">
			<?php
			$stato = null;
			$state_color = '#333';
			if (date("Y-m-d") < $poll['date_start']) {
				$stato = 'Programmato';
				$state_color = '#0085C6';
			}
			elseif ($poll['date_start'] <= date("Y-m-d") AND date("Y-m-d") <= $poll['date_end']) {
				$stato = 'Aperto';
				$state_color = '#51A351';
			}
			else {
				$stato = 'Chiuso';
				$state_color = '#D90000';
			}
			echo '<span style="color: '.$state_color.';">'.$stato.'</span>';
			?>
		</td>
		<td style="width: 120px; text-align: center;">
			<?=($poll['anonymous'] ? 'Sì' : 'No')?>
		</td>
		<td style="width: 120px; text-align: center;">
			<?=count($poll['users'])?> &nbsp; <a href="#" id="view_users_list"><i class="fa fa-search"></i></a>
		</td>
		<td style="width: 120px; text-align: center;">
			<?=$poll['num_voted']?> &nbsp; <a href="#" id="view_users_voted"><i class="fa fa-search"></i></a>
		</td>
		<td style="width: 120px; text-align: center;">
			<?php
				if ($stato == 'Programmato' OR count($poll['users']) == 0)
					echo '-';
				else {
					$perc = round(($poll['num_voted'] / count($poll['users'])) * 100);
					echo $perc.'%';
				}
			?>
		</td>
	</tr>
</table>
<br><br>

<div id="poll_users_list_wrapper">
	<h5>Utenti coinvolti:</h5>
	<?php
	$users_str = '';
	foreach ($poll['users'] as $u)
		$users_str .= $u['name'].', ';
	echo trim($users_str, ', ');
	?>
</div>

<div id="poll_users_voted_wrapper">
	<h5>Hanno votato:</h5>
	<?php
	$users_str = '';
	foreach ($poll['users'] as $u)
		if ($u['date_vote'])
			$users_str .= $u['name'].', ';
	echo trim($users_str, ', ');
	?>
</div>

<?php 
foreach ($poll['questions'] as $qst) {
	echo '<h5>'.$qst['title'].'</h5>';
	if ($qst['type'] == 'C') {
		?>
		<table>
			<tr>
				<th>Opzione</th>
				<th>Num. voti</th>
				<th>Percentuale</th>
			</tr>
			<?php
			foreach ($poll['options'][$qst['id']] as $opt) {
				$perc_vote = 0;
				if ($poll['num_voted'] > 0)
					$perc_vote = round(($opt['votes'] / $poll['num_voted']) * 100);
				echo '<tr class="opt_stat_wrapper">
						<td>'.$opt['title'].'</td>
						<td style="width: 120px; text-align: center;">'.$opt['votes'].'</td>
						<td>
							<div class="perc_bar" style="background: green; display: block; height: 20px; float: left; width: '.(($poll['num_voted'] > 0) ? 'calc('.$perc_vote.'%'.($perc_vote > 90 ? ' - 50px' : '').')' : '0').'";></div>
							<span style="margin-left: 10px;">'.(($poll['num_voted'] > 0) ? $perc_vote.'%' : '0%').'</span>
						</td>
					</tr>';
			}
			?>
		</table>
		<br>
		<?php
	} else {
		?>
		<table>
			<tr>
				<th>Risposte</th>
			</tr>
			<?php
			if (isset($poll['answers'][$qst['id']])) {
				foreach ($poll['answers'][$qst['id']] as $ans) {
					echo '<tr class="ans_stat_wrapper">
							<td>'.$ans['answer'].'</td>
						</tr>';
				}
			}
			?>
		</table>
		<br>
		<?php
	}
}
?>

<?php if (!$poll['anonymous']) { ?>
	<div class="sect_panel full">
		<h4><i class="fa fa-user"></i> Preferenze degli utenti</h4>
		<?php if (isset($poll['users_voted'])) { ?>
			<table>
				<?php 
				foreach ($poll['users_voted'] as $usr) {
					echo '<tr class="opt_stat_wrapper">
							<td style="width: 25%;">'.$usr['first_name'].' '.$usr['last_name'].'</td>
							<td>';
					foreach ($usr['answers'] as $q => $a)
						echo '<p style="margin: 0; padding: 0; font-size: 14px; line-height: 1.2;"><b>'.$q.'</b>: '.$a.'</p>';
					echo '</td>
							</tr>';
				}
				?>
			</table>
		<?php } else { ?>
			<p>Non è stato possibile recuperare la lista delle preferenze espresse dagli utenti</p>
		<?php } ?>
	</div>
<?php } ?>

<hr>
<a style="float: left;" href="/admin/delete_poll/<?=$poll['id']?>" class="btn red" onclick="return confirm('Sei sicuro di voler eliminare questo sondaggio?');">Elimina</a>
<a style="float: right;" href="/admin/export_poll/<?=$poll['id']?>" class="btn blue">Esporta in PDF</a>

<script>
$(document).ready(function() {
	$('#view_users_list').click(function() {
		$('#poll_users_list_wrapper').toggle();
	})

	$('#view_users_voted').click(function() {
		$('#poll_users_voted_wrapper').toggle();
	})
});
</script>