<div class="row space-bot">
	<div class="c12">
		<a href="/admin" class="back">Torna all'homepage</a>
	</div>
</div>
<h1><i class="fa fa-folder-open"></i> Modifica categoria</h1>
<form class="admin" action="/admin/edit_cat" method="POST">
	<?php if (!empty($cat)) { ?>
		<input type="hidden" name="id_cat" value="<?=@$cat['id_cat']?>">
	<?php } ?>
	<input type="text" name="code" placeholder="Codice categoria" value="<?=@$cat['code']?>" data-validation="custom" data-validation-regexp="^([A-Z_]{3,})$">
	<input type="text" name="name" placeholder="Nome categoria" value="<?=@$cat['name']?>" data-validation="required">
	<br>
	<button type="submit" class="blue">Salva</button>
</form>
<p><b>N.B.:</b> Il codice categoria accetta solo lettere maiuscole e trattini bassi [ _ ]</p>