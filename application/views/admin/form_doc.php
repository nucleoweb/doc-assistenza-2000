<h1><i class="fa fa-file-text-o"></i> <?=((!empty($info)) ? 'Modifica documento' : 'Inserisci un nuovo documento')?></h1>
<form class="admin admin_users_list_form" id="edit_doc_form" action="<?=((!empty($info)) ? '/admin/edit_doc' : '/admin/insert_doc')?>" method="POST" enctype="multipart/form-data">
	<div class="row">
		<div class="c6">	
			<?php if (!empty($info)) { # STO EDITANDO UN SINGOLO FILE ?>
					<input type="hidden" name="id_doc" value="<?=@$info['id_doc']?>">
					<input type="text" name="name" placeholder="Nome documento" value="<?=@$info['name']?>" data-validation="required">
					<fieldset>
						<!--<p><i class="fa fa-file-o"></i> File attuale: <a href="/assets/docs/<?=$info['filename']?>" target="_blank"><?=$info['filename']?></p></a>-->
						<p><i class="fa fa-file-o"></i> File attuale: <a href="/admin/serve_doc/<?=$info['req_string']?>" target="_blank"><?=$info['filename']?></p></a>
					</fieldset>
					<input type="file" name="filedoc" <?=((empty($info)) ? 'data-validation="required"' : '')?>>
					<select name="cat" data-validation="required">
						<option value="">- Scegli una categoria -</option>
						<?php foreach ($doc_cats as $cod => $cat) { ?>
							<option value="<?=$cod?>" <?=((!empty($info)) ? (($cod == $info['cat']) ? 'selected' : '') : '' )?>><?=$cat?></option>
						<?php } ?>
					</select>
					<textarea name="note" placeholder="Note sul documento"><?=@$info['note']?></textarea>
			<?php } else { # STO CARICANDO DEI NUOVI FILE (UNO O PIU') ?>
					<div id="fileinsert_zone">
						<div class="filedoc_wrapper">
							<input type="text" name="name[]" placeholder="Nome documento" value="<?=@$info['name']?>" data-validation="required">
							<input type="file" name="filedoc[]" <?=((empty($info)) ? 'data-validation="required"' : '')?>>
							<select name="cat[]" data-validation="required">
								<option value="">- Scegli una categoria -</option>
								<?php foreach ($doc_cats as $cod => $cat) { ?>
									<option value="<?=$cod?>" <?=((!empty($info)) ? (($cod == $info['cat']) ? 'selected' : '') : '' )?>><?=$cat?></option>
								<?php } ?>
							</select>
							<textarea name="note[]" placeholder="Note sul documento"><?=@$info['note']?></textarea>
						</div>
					</div>
					<a href="#" class="btn" id="addfile_btn"><i class="fa fa-plus" style="margin-right: 5px;"></i> Aggiungi file</a>
					<br>
					
					<div class="filedoc_wrapper" id="filedoc_wrapper_template" style="display: none;">
						<input type="text" name="name[]" placeholder="Nome documento" value="<?=@$info['name']?>" data-validation="required">
						<input type="file" name="filedoc[]" <?=((empty($info)) ? 'data-validation="required"' : '')?>>
						<select name="cat[]" data-validation="required">
							<option value="">- Scegli una categoria -</option>
							<?php foreach ($doc_cats as $cod => $cat) { ?>
								<option value="<?=$cod?>" <?=((!empty($info)) ? (($cod == $info['cat']) ? 'selected' : '') : '' )?>><?=$cat?></option>
							<?php } ?>
						</select>
						<textarea name="note[]" placeholder="Note sul documento"><?=@$info['note']?></textarea>
					</div>
			<?php } ?>
			
			
			<br>
			<?php if (empty($info) OR 1) { ?>
				<div id="alert_box">
					<h4><i class="fa fa-bell"></i> Notifiche</h4>
					<input name="alert_mail" type="checkbox" id="alert_mail" <?=(isset($info['alert_mail']) ? ($info['alert_mail'] ? 'checked' : '') : 'checked')?>> <label for="alert_mail">Invia notifica del documento via e-mail</label>
					<br><br>
					<input name="alert_sms" type="checkbox" id="alert_sms" <?=(isset($info['alert_sms']) ? ($info['alert_sms'] ? 'checked' : '') : 'checked')?>> <label for="alert_sms">Invia notifica del documento via sms</label>
				</div>
			<?php } ?>
			<button type="submit" class="blue">Salva</button>
			<?php if (!empty($info)) { ?>
				<a style="float: right;" href="/admin/delete_doc/<?=$info['id_doc']?>" class="btn red" onclick="return confirm('Sei sicuro di voler eliminare questo documento?');">Elimina</a>
				<a style="float: right; margin-right: 10px;" href="/admin/print_doc_info/<?=$info['id_doc']?>" target="_blank" class="btn blue"><i class="fa fa-print" aria-hidden="true" style="font-size: 18px; margin-right: 5px;"></i> Stampa info</a>
				<a style="float: right; margin-right: 10px;" href="/admin/save_doc_info/<?=$info['id_doc']?>" class="btn blue"><i class="fa fa-file-text" aria-hidden="true" style="font-size: 18px; margin-right: 5px;"></i> Salva info</a>
			<?php } ?>
		</div>
		<div class="c6">
			<?php $this->load->view('admin/users_check_list.php', ['docs_list' => 'yes']); ?>
		</div>
	</div>
</form>

<script>
$(document).ready(function() {
	$("#addfile_btn").click(function() {
		code = $("#filedoc_wrapper_template").clone().removeAttr("id").show();
		console.log(code);
		$("#fileinsert_zone").append(code);
	});
});

$.validate({
	errorMessagePosition: 'top'
});
</script>