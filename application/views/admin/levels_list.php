<h1><i class="fa fa-sort"></i> Gestione livelli</h1>
<div class="row">
	<div class="c6">
		<table>
		<tr>
			<th>ID</th>
			<th>Nome</th>
			<th>Num. utenti attivi</th>
			<th>Num. utenti totali</th>
			<th></th>
		</tr>
		<?php foreach ($levels as $level) { ?>
			<tr>
				<td><?=$level['id']?></td>
				<td><?=$level['name']?></td>
				<td><?=$level['num_users_active']?></td>
				<td><?=$level['num_users']?></td>
				<td class="actions">
					<a href="/admin/edit_level/<?=$level['id']?>"><i class="fa fa-pencil"></i></a>
					<a class="del_level" data-idlevel="<?=$level['id']?>" data-namelevel="<?=$level['name']?>" href="#"><i class="fa fa-trash"></i></a>
				</td>
			</tr>
		<?php } ?>
		</table>
	</div>
	<div class="c6" style="padding-left: 40px;">
		<h2>Inserisci nuovo livello</h2>
		<form class="admin" action="/admin/insert_level" method="POST">
			<input type="text" name="name" placeholder="Nome livello" value="<?=@$info['name']?>" data-validation="required">
			<br>
			<button type="submit" class="blue">Salva</button>
		</form>
	</div>
</div>

<div id="overlay"></div>
<div class="form_del_wrapper">
	<form id="del_level_form" class="admin" action="/admin/del_level" method="POST">
		<input name="id_level_to_del" id="id_level_to_del" type="hidden" value="">
		<h4>Stai per cancellare il livello [ <span id="level_to_del"></span> ]</h4>
		<hr>
		Seleziona un livello a cui associare gli utenti (se presenti):
		<br><br>
		<select name="level_to_assoc" id="level_to_assoc">
			<option value="">- Seleziona un livello -</option>
			<?php foreach ($levels as $level) { ?>
				<option value="<?=$level['id']?>"><?=$level['name']?></option>
			<?php } ?>
		</select>
		<br><br>
		<a id="del_level_close" class="btn orange" href="#">Annulla</a>
		<a id="del_level_proceed" class="btn green" href="#" style="float: right;">Elimina livello</a>
	</form>
</div>

<script>
$.validate({
	errorMessagePosition: 'top'
});

$(document).ready(function() {
	$(".del_level").click(function() {
		idlevel = $(this).data("idlevel");
		namelevel = $(this).data("namelevel");
		$("#overlay, .form_del_wrapper").fadeIn();
		$("#id_level_to_del").val(idlevel);
		$("#level_to_del").html(namelevel);
		$("#level_to_assoc option").show();
		$("#level_to_assoc option[value='"+idlevel+"']").hide();
	});
	
	$("#del_level_proceed").click(function() {
		if ($("#level_to_assoc").val()) {
			$("#del_level_form").submit();
		} else {
			alert("Devi selezionare un livello a cui associare gli utenti!");
		}
	});
	
	$("#del_level_close").click(function() {
		$("#overlay, .form_del_wrapper").fadeOut();
	});
});
</script>