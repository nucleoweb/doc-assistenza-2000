<h1><i class="fa fa-eur" aria-hidden="true"></i> Gestione bandi/contributi</h1>
<div class="row">
	<div class="c6">
		<table class="tab_list">
		<tr>
			<th>ID</th>
			<th>Nome</th>
			<th>Importo</th>
			<th>Periodo</th>
			<th>Centri costo</th>
			<th></th>
		</tr>
		<?php foreach ($financings as $financing) { ?>
			<tr>
				<td style="text-align: center;"><?=$financing['id']?></td>
				<td style="text-align: center;"><?=$financing['name']?></td>
				<td style="text-align: right;"><?=number_format($financing['amount'], 2, ",", ".")?> €</td>
				<td style="text-align: center;"><?=monthNameByNum($financing['from_month']).'-'.$financing['from_year'].' / '.monthNameByNum($financing['to_month']).'-'.$financing['to_year']?></td>
				<td style="text-align: center;"><?=count(json_decode($financing['costcenters']))?></td>
				<td class="actions">
					<a href="/admin/edit_financing/<?=$financing['id']?>"><i class="fa fa-pencil"></i></a>
					<a class="del_financing" data-idfinancing="<?=$financing['id']?>" data-namefinancing="<?=htmlentities($financing['name'])?>" href="#"><i class="fa fa-trash"></i></a>
				</td>
			</tr>
		<?php } ?>
		</table>
	</div>
	<div class="c6" style="padding-left: 40px;">
		<h2>Inserisci nuovo contributo</h2>
		<form class="admin detail_form" action="/admin/insert_financing" method="POST">
			<label>Nome</label>
			<input type="text" name="name" placeholder="Denominazione contributo" value="<?=@$info['name']?>">
			<label>Descrizione</label>
			<textarea name="description" placeholder="Descrizione contributo"><?=@$info['description']?></textarea>
			<label>Importo contributo</label>
			<input type="number" step="0.01" name="amount" placeholder="Importo contributo" value="<?=@$info['amount']?>">
			<div class="row" style="padding: 20px; background: #f6f6f6;">
				<div class="c6 first">
					<label><b>Inizio periodo</b></label><br>
					<select name="from_month" style="width: 45%; float: left; clear: none; margin: 0 10px 0 0;">
						<option value="">- Scegli il mese -</option>
						<?php for ($m = 1; $m <= 12; $m++) { echo '<option value="'.$m.'">'.monthNameByNum($m).'</option>'; } ?>
					</select>
					<select name="from_year" style="width: 45%; float: left; clear: none; margin: 0 10px 0 0;">
						<option value="">- Scegli l'anno -</option>
						<?php for ($y = 2020; $y <= ((int)date('Y')+5); $y++) { echo '<option value="'.$y.'">'.$y.'</option>'; } ?>
					</select>
				</div>
				<div class="c6 last">
					<label><b>Fine periodo</b></label><br>
					<select name="to_month" style="width: 45%; float: left; clear: none; margin: 0 10px 0 0;">
						<option value="">- Scegli il mese -</option>
						<?php for ($m = 1; $m <= 12; $m++) { echo '<option value="'.$m.'">'.monthNameByNum($m).'</option>'; } ?>
					</select>
					<select name="to_year" style="width: 45%; float: left; clear: none; margin: 0 10px 0 0;">
						<option value="">- Scegli l'anno -</option>
						<?php for ($y = 2020; $y <= ((int)date('Y')+5); $y++) { echo '<option value="'.$y.'">'.$y.'</option>'; } ?>
					</select>
				</div>
			</div>
			<br>
			<label style="margin-bottom: 10px;"><b>Collega i centri di costo</b></label>
			<?php foreach ($costcenters as $cc) { ?>
				<div><label for="cc_<?=$cc['id']?>"><input type="checkbox" id="cc_<?=$cc['id']?>" name="cc[]" value="<?=$cc['id']?>" /> <?=$cc['name']?></label></div>
			<?php } ?>
			<br>
			<button type="submit" class="blue">Salva</button>
		</form>
	</div>
</div>

<div id="overlay"></div>
<div class="form_del_wrapper">
	<form id="del_financing_form" class="admin" action="/admin/del_financing" method="POST">
		<input name="id_financing_to_del" id="id_financing_to_del" type="hidden" value="">
		<h4>Stai per cancellare il contributo [ <span id="financing_to_del"></span> ]</h4>
		<a id="del_financing_close" class="btn orange" href="#">Annulla</a>
		<a id="del_financing_proceed" class="btn green" href="#" style="float: right;">Elimina contributo</a>
	</form>
</div>

<script>
$.validate({
	errorMessagePosition: 'top'
});

$(document).ready(function() {
	$(".del_financing").click(function() {
		idfinancing = $(this).data("idfinancing");
		namefinancing = $(this).data("namefinancing");
		$("#overlay, .form_del_wrapper").fadeIn();
		$("#id_financing_to_del").val(idfinancing);
		$("#financing_to_del").html(namefinancing);
		$("#financing_to_assoc option").show();
		$("#financing_to_assoc option[value='"+idfinancing+"']").hide();
	});
	
	$("#del_financing_proceed").click(function() {
		// if ($("#financing_to_assoc").val()) {
			$("#del_financing_form").submit();
		// } else {
			// alert("Devi selezionare un servizio a cui associare gli utenti!");
		// }
	});
	
	$("#del_financing_close").click(function() {
		$("#overlay, .form_del_wrapper").fadeOut();
	});
});
</script>