<html>
<head>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"> 
	<link rel="stylesheet" type="text/css" href="<?=$this->config->item('base_url')?>assets/css/normalize.css">
	<link rel="stylesheet" type="text/css" href="<?=$this->config->item('base_url')?>assets/css/ivory.css">
	<link rel="stylesheet" type="text/css" href="<?=$this->config->item('base_url')?>assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?=$this->config->item('base_url')?>assets/css/pdf.css">
</head>
<body>
	<table id="pdf_title_table">
		<tr>
			<td width="25%">
				<img style="max-width: 120px !important" src="<?=$this->config->item('base_url')?>assets/gfx/logo.jpg">
			</td>
			<td width="75%">
				<h1>Risultati sondaggio</h1>
				<h3><?=$poll['title']?></h3>
			</td>
		</tr>
	</table>
	<br>
	<table>
		<tr>
			<!--<th>ID</th>-->
			<th>Data inizio</th>
			<th>Data fine</th>
			<!-- <th>Titolo</th> -->
			<th>Stato</th>
			<th>Anonimo</th>
			<th>Num. utenti</th>
			<th>Voti ricevuti</th>
			<th>Affluenza</th>
		</tr>
		<tr>
			<!--<td style="text-align: center;"><?=$poll['id']?></td>-->
			<td style="text-align: center;"><?=implode("/", array_reverse(explode("-", $poll['date_start'])))?></td>
			<td style="text-align: center;"><?=implode("/", array_reverse(explode("-", $poll['date_end'])))?></td>
			<!-- <td><?=$poll['title']?></td> -->
			<td style="text-align: center;">
				<?php
				$stato = null;
				$state_color = '#333';
				if (date("Y-m-d") < $poll['date_start']) {
					$stato = 'Programmato';
					$state_color = '#0085C6';
				}
				elseif ($poll['date_start'] <= date("Y-m-d") AND date("Y-m-d") <= $poll['date_end']) {
					$stato = 'Aperto';
					$state_color = '#51A351';
				}
				else {
					$stato = 'Chiuso';
					$state_color = '#D90000';
				}
				echo '<span style="color: '.$state_color.';">'.$stato.'</span>';
				?>
			</td>
			<td style="text-align: center;">
				<?=($poll['anonymous'] ? 'Sì' : 'No')?>
			</td>
			<td style="text-align: center;">
				<?=count($poll['users'])?>
			</td>
			<td style="text-align: center;">
				<?=$poll['num_voted']?>
			</td>
			<td style="text-align: center;">
				<?php
					if ($stato == 'Programmato' OR count($poll['users']) == 0)
						echo '-';
					else {
						$perc = round(($poll['num_voted'] / count($poll['users'])) * 100);
						echo $perc.'%';
					}
				?>
			</td>
		</tr>
	</table>
	<br>
	<p class="poll_info"><?=$poll['info']?></p>
	<br>
	<?php 
	foreach ($poll['questions'] as $qst) {
		echo '<h5>'.$qst['title'].'</h5>';
		if ($qst['type'] == 'C') {
			?>
			<table>
				<tr>
					<th>Opzione</th>
					<th>Num. voti</th>
					<th colspan="2">Percentuale</th>
				</tr>
				<?php
				foreach ($poll['options'][$qst['id']] as $opt) {
					echo '<tr class="opt_stat_wrapper">
							<td width="50%">'.$opt['title'].'</td>
							<td width="10%" style="text-align: center;">'.$opt['votes'].'</td>
							<td width="10%" style="text-align: center;">'.(($poll['num_voted'] > 0) ? round(($opt['votes'] / $poll['num_voted']) * 100).'%' : '0%').'</td>
							<td width="30%">
								<div class="perc_bar" style="background: green; display: block; height: 10px; float: left; width: '.(($poll['num_voted'] > 0) ? round(($opt['votes'] / $poll['num_voted']) * 100).'%' : '0').'";>&nbsp;</div>
								<!--<span style="margin-left: 10px;">'.(($poll['num_voted'] > 0) ? round(($opt['votes'] / $poll['num_voted']) * 100).'%' : '0%').'</span>-->
							</td>
						</tr>';
				}
				?>
			</table>
			<?php
		} else {
			?>
			<table>
				<tr>
					<th>Risposte</th>
				</tr>
				<?php
				if (isset($poll['answers'][$qst['id']])) {
					foreach ($poll['answers'][$qst['id']] as $ans) {
						echo '<tr class="ans_stat_wrapper">
								<td>'.$ans['answer'].'</td>
							</tr>';
					}
				}
				?>
			</table>
			<?php
		}
	}
	?>

	<?php if (!$poll['anonymous']) { ?>
		<div class="sect_panel full">
			<h4><i class="fa fa-user"></i> Preferenze degli utenti</h4>
			<?php if (isset($poll['users_voted'])) { ?>
				<table>
					<?php 
					foreach ($poll['users_voted'] as $usr) {
						echo '<tr class="opt_stat_wrapper">
								<td style="width: 25%;">'.$usr['first_name'].' '.$usr['last_name'].'</td>
								<td>';
						foreach ($usr['answers'] as $q => $a)
							echo '<p style="margin: 0; padding: 0; font-size: 14px; line-height: 1.2;"><b>'.$q.'</b>: '.$a.'</p>';
						echo '</td>
								</tr>';
					}
					?>
				</table>
			<?php } else { ?>
				<p>Non è stato possibile recuperare la lista delle preferenze espresse dagli utenti</p>
			<?php } ?>
		</div>
	<?php } ?>
</body>
</html>