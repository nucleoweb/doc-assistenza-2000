<h1><i class="fa fa-map-marker"></i> Gestione aree</h1>
<div class="row">
	<div class="c6">
		<table>
		<tr>
			<th>ID</th>
			<th>Nome</th>
			<th>Num. utenti attivi</th>
			<th>Num. utenti totali</th>
			<th></th>
		</tr>
		<?php foreach ($areas as $area) { ?>
			<tr>
				<td><?=$area['id']?></td>
				<td><?=$area['name']?></td>
				<td><?=$area['num_users_active']?></td>
				<td><?=$area['num_users']?></td>
				<td class="actions">
					<a href="/admin/edit_area/<?=$area['id']?>"><i class="fa fa-pencil"></i></a>
					<a class="del_area" data-idarea="<?=$area['id']?>" data-namearea="<?=$area['name']?>" href="#"><i class="fa fa-trash"></i></a>
				</td>
			</tr>
		<?php } ?>
		</table>
	</div>
	<div class="c6" style="padding-left: 40px;">
		<h2>Inserisci nuova area</h2>
		<form class="admin" action="/admin/insert_area" method="POST">
			<input type="text" name="name" placeholder="Nome area" value="<?=@$info['name']?>" data-validation="required">
			<br>
			<button type="submit" class="blue">Salva</button>
		</form>
	</div>
</div>

<div id="overlay"></div>
<div class="form_del_wrapper">
	<form id="del_area_form" class="admin" action="/admin/del_area" method="POST">
		<input name="id_area_to_del" id="id_area_to_del" type="hidden" value="">
		<h4>Stai per cancellare l'area [ <span id="area_to_del"></span> ]</h4>
		<hr>
		Seleziona un'area a cui associare gli utenti (se presenti):
		<br><br>
		<select name="area_to_assoc" id="area_to_assoc">
			<option value="">- Seleziona un'area -</option>
			<?php foreach ($areas as $area) { ?>
				<option value="<?=$area['id']?>"><?=$area['name']?></option>
			<?php } ?>
		</select>
		<br><br>
		<a id="del_area_close" class="btn orange" href="#">Annulla</a>
		<a id="del_area_proceed" class="btn green" href="#" style="float: right;">Elimina area</a>
	</form>
</div>

<script>
$.validate({
	errorMessagePosition: 'top'
});

$(document).ready(function() {
	$(".del_area").click(function() {
		idarea = $(this).data("idarea");
		namearea = $(this).data("namearea");
		$("#overlay, .form_del_wrapper").fadeIn();
		$("#id_area_to_del").val(idarea);
		$("#area_to_del").html(namearea);
		$("#area_to_assoc option").show();
		$("#area_to_assoc option[value='"+idarea+"']").hide();
	});
	
	$("#del_area_proceed").click(function() {
		if ($("#area_to_assoc").val()) {
			$("#del_area_form").submit();
		} else {
			alert("Devi selezionare un'area a cui associare gli utenti!");
		}
	});
	
	$("#del_area_close").click(function() {
		$("#overlay, .form_del_wrapper").fadeOut();
	});
});
</script>