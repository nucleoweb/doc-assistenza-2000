<h1><i class="fa fa-question-circle"></i> <?=((!empty($info)) ? 'Modifica sondaggio' : 'Inserisci nuovo sondaggio')?></h1>
<form class="admin admin_users_list_form" id="edit_poll_form" action="<?=((!empty($info)) ? '/admin/edit_poll' : '/admin/insert_poll')?>" method="POST" enctype="multipart/form-data">
	<div class="row">
		<div class="c6">
			<?php if (!empty($info)) { ?>
				<input type="hidden" name="id" value="<?=@$info['id']?>">
			<?php } ?>
			<input type="text" name="title" placeholder="Oggetto sondaggio" value="<?=@$info['title']?>" data-validation="required">
			<textarea name="info" placeholder="Info sul sondaggio"><?=@$info['info']?></textarea>
			<?php if (!empty($info) AND !empty($info['filename'])) { ?>
			<fieldset>
				<p><i class="fa fa-file-o"></i> File attuale: <a href="/admin/serve_doc/<?=$info['req_string']?>" target="_blank"><?=$info['filename']?></p></a>
			</fieldset>
			<?php } ?>
			<input type="file" name="filedoc">
			<div class="row" style="width: 97.5%; margin: 0;">
				<div class="c6 first">
					<input type="text" name="date_start" class="datepicker" placeholder="Data avvio" value="<?=@implode("/", array_reverse(explode("-", $info['date_start'])))?>" data-validation="required">
				</div>
				<div class="c6 last">
					<input type="text" name="date_end" class="datepicker" placeholder="Data scadenza" value="<?=@implode("/", array_reverse(explode("-", $info['date_end'])))?>" data-validation="required">
				</div>
				<p><b>N.B.:</b> il sondaggio inizia alla mezzanotte del giorno iniziale e termina alle 23:59:59 del giorno finale</p>
			</div>
			
			<div class="sect_panel" id="opt_panel">
				<h4><i class="fa fa-check-circle"></i> Inserisci le domande del questionario</h4>
				<a href="#" class="btn addquestion_btn" id="addquestion_closed_btn" data-type="closed"><i class="fa fa-plus" style="margin-right: 5px;"></i> Aggiungi domanda chiusa</a>
				<a href="#" class="btn addquestion_btn" id="addquestion_open_btn" data-type="open"><i class="fa fa-plus" style="margin-right: 5px;"></i> Aggiungi domanda aperta</a>
				<br>
				<div id="questions_wrapper"></div>
			</div>

			<div class="sect_panel">
				<h4><i class="fa fa-user-secret"></i> Opzione anonimato</h4>
				<input type="checkbox" name="anonymous" id="anonymous" <?=(isset($info['anonymous']) ? ($info['anonymous'] ? 'checked' : '') : 'checked')?>> <label for="anonymous">Rendi anonime le votazioni del sondaggio</label>
			</div>

			<?php if (empty($info) OR 1) { ?>
				<div id="alert_box">
					<h4><i class="fa fa-bell"></i> Notifiche</h4>
					<input name="alert_mail" type="checkbox" id="alert_mail" <?=(isset($info['alert_mail']) ? ($info['alert_mail'] ? 'checked' : '') : 'checked')?>> <label for="alert_mail">Invia notifica del sondaggio via e-mail</label>
					<br><br>
					<input name="alert_sms" type="checkbox" id="alert_sms" <?=(isset($info['alert_sms']) ? ($info['alert_sms'] ? 'checked' : '') : 'checked')?>> <label for="alert_sms">Invia notifica del sondaggio via sms</label>
				</div>
			<?php } ?>
			<button type="submit" class="blue">Salva</button>
			<?php if (!empty($info)) { ?>
				<a style="float: right;" href="/admin/delete_poll/<?=$info['id']?>" class="btn red" onclick="return confirm('Sei sicuro di voler eliminare questo sondaggio?');">Elimina</a>
			<?php } ?>
		</div>
		<div class="c6">
			<?php $this->load->view('admin/users_check_list.php'); ?>
		</div>
	</div>
</form>

<script>
$(document).ready(function() {
	/* AGGIUNTA NUOVA DOMANDA */
	$(".addquestion_btn").click(function() {
		type = $(this).data('type');
		num_q = 0;
		if ($('.qst_item').length)
			num_q = $('.qst_item').last().data('num');
		num_q++;
		/*
		if (type == 'closed') {		// domanda a risposta chiusa
			$("#questions_wrapper").append('<div class="qst_item" data-num="'+num_q+'">'+
				'<div class="qst_row"><input type="text" class="qst_title" name="question_'+num_q+'"><a href="#" class="qst_del"><i class="fa fa-trash"></i></a></div>'+
				'<p><i class="fa fa-check-circle"></i> Inserisci le opzioni da votare</p>'+
				'<a href="#" class="btn addoption_btn"><i class="fa fa-plus" style="margin-right: 5px;"></i> Aggiungi opzione</a>'+
				'<div class="options_wrapper"></div>'+
				'<a href="#" class="qst_clone" title="Clona questa domanda"><i class="fa fa-copy"></i></a>'+
			'</div>');
		} else {	// domanda a risposta aperta
			$("#questions_wrapper").append('<div class="qst_item" data-num="'+num_q+'">'+
				'<div class="qst_row"><input type="text" class="qst_title" name="question_'+num_q+'"><a href="#" class="qst_del"><i class="fa fa-trash"></i></a></div>'+
				'<a href="#" class="qst_clone" title="Clona questa domanda"><i class="fa fa-copy"></i></a>'+
			'</div>');
		}
		*/
		qst_str = '<div class="qst_item" data-num="'+num_q+'">'+
					'<div class="qst_row"><input type="text" class="qst_title" name="question_'+num_q+'"><a href="#" class="qst_del"><i class="fa fa-trash"></i></a></div>';
		if (type == 'closed') {		// domanda a risposta chiusa - aggiungo la parte delle opzioni
			qst_str += '<p><i class="fa fa-check-circle"></i> Inserisci le opzioni da votare</p>'+
						'<a href="#" class="btn addoption_btn"><i class="fa fa-plus" style="margin-right: 5px;"></i> Aggiungi opzione</a>'+
						'<div class="options_wrapper"></div>';
		}
		qst_str += '<a href="#" class="qst_clone" title="Clona questa domanda"><i class="fa fa-copy"></i></a>'+
					'</div>';
		$("#questions_wrapper").append(qst_str);
		return false;
	});
});

/* ELIMINAZIONE DOMANDA */
$(document).on('click', '.qst_del', function() {
	$(this).closest(".qst_item").remove();
	return false;
});

/* INSERIMENTO OPZIONE DOM. RISP. CHIUSA */
$(document).on('click', '.addoption_btn', function() {
	qst_item = $(this).closest('.qst_item');
	qst_item.find(".options_wrapper").append('<div class="opt_row"><input type="text" class="qst_opt_title" name="question_'+qst_item.data('num')+'_options[]" placeholder="Inserisci il nome dell\'opzione"><a href="#" class="opt_del"><i class="fa fa-trash"></i></a></div>');
	return false;
});

/* ELIMINAZIONE OPZIONE DOM. RISP. CHIUSA */
$(document).on('click', '.opt_del', function() {
	$(this).closest(".opt_row").remove();
	return false;
});

/* CLONAZIONE DOMANDA */
$(document).on('click', '.qst_clone', function() {
	num_q = $('.qst_item').last().data('num');	// mi prendo il numero dell'ultima domanda PRIMA di clonare
	num_q++;
	qst_cloned = $(this).closest(".qst_item").clone();
	qst_cloned.data('num', num_q);
	qst_cloned.find('.qst_title').attr('name', 'question_'+num_q);
	qst_cloned.find('.qst_opt_title').each(function() {
		$(this).attr('name', 'question_'+num_q+'_options[]');
	});
	$("#questions_wrapper").append(qst_cloned);
	return false;
});

<?php if (!empty($info) AND isset($info['questions']) AND count($info['questions']) AND isset($info['options']) AND count($info['options'])) { ?>
	$(function() {
		<?php foreach ($info['questions'] as $qst) { ?>
			q_str = `<div class="qst_item" data-num="<?=$qst['id']?>">
				<div class="qst_row"><input type="text" class="qst_title" name="question_<?=$qst['id']?>" value="<?=$qst['title']?>"><a href="#" class="qst_del"><i class="fa fa-trash"></i></a></div>
				<p><i class="fa fa-check-circle"></i> Inserisci le opzioni da votare</p>
				<a href="#" class="btn addoption_btn"><i class="fa fa-plus" style="margin-right: 5px;"></i> Aggiungi opzione</a>
				<div class="options_wrapper">`;
			<?php 
			if (isset($info['options'][$qst['id']]))
				foreach ($info['options'][$qst['id']] as $opt) { ?>
				q_str += `<div class="opt_row"><input type="text" class="qst_opt_title" name="question_<?=$qst['id']?>_options[]" placeholder="Inserisci il nome dell\'opzione" value="<?=addslashes($opt['title'])?>"><a href="#" class="opt_del"><i class="fa fa-trash"></i></a></div>`;
			<?php } ?>
			q_str += `</div><a href="#" class="qst_clone" title="Clona questa domanda"><i class="fa fa-copy"></i></a></div>`;
			$("#questions_wrapper").append(q_str);
		<?php } ?>
	});
<?php } ?>

/* RICHIESTA CONFERMA SALVATAGGIO SONDAGGIO */
$('#edit_poll_form').submit(function() {
	return confirm("Sei sicuro di voler creare/modificare questo sondaggio? Assicurati di aver controllato bene le informazioni. Una volta passata la data d'inizio non potrai piu modificare le opzioni per cui far votare gli utenti");
});

$.validate({
	errorMessagePosition: 'top'
});
</script>