<div class="row space-bot">
	<div class="c12">
		<a href="/admin" class="back">Torna all'homepage</a>
	</div>
</div>
<h1><i class="fa fa-ambulance"></i> Modifica servizio</h1>
<form class="admin" action="/admin/edit_service" method="POST">
	<?php if (!empty($service)) { ?>
		<input type="hidden" name="id_service" value="<?=@$service['id']?>">
	<?php } ?>
	<input type="text" name="name" placeholder="Nome servizio" value="<?=@$service['name']?>" data-validation="required">
	<br>
	<button type="submit" class="blue">Salva</button>
</form>