<?php if (!empty($ret)) { ?>
	<script>
	$(function() {
		<?php if ($ret['alert_type'] == 'import_ok') { ?>
			Swal.fire({
				icon: 'success',
				title: 'Importazione completata',
				html: '<b>File esaminati</b>: <?=$ret['entries']?><hr>'+
					'<b>Fornitori esistenti</b>: <?=$ret['suppliers_ext']?><br>'+
					'<b>Fornitori inseriti</b>: <?=$ret['suppliers_ins']?><br>'+
					'<b>Fornitori falliti</b>: <?=$ret['suppliers_not']?><hr>'+
					'<b>Fornitori falliti:</b><br><?=($ret['suppliers_not_list'] != "" ? $ret['suppliers_not_list'] : '-')?><hr>'+
					'<b>Costi fornitore esistenti</b>: <?=$ret['suppliercosts_ext']?><br>'+
					'<b>Costi fornitore inseriti</b>: <?=$ret['suppliercosts_ins']?><br>'+
					'<b>Costi fornitore falliti</b>: <?=$ret['suppliercosts_not']?><hr>'+
					'<b>Costi fornitore esistenti:</b><br><?=($ret['suppliercosts_ext_list'] != "" ? $ret['suppliercosts_ext_list'] : '-')?><hr>'+
					'<b>Costi fornitore falliti:</b><br><?=($ret['suppliercosts_not_list'] != "" ? $ret['suppliercosts_not_list'] : '-')?>',
				width: '800px'
			});
		<?php } ?>
	});
	</script>
<?php } ?>

<h1 class="balance_list_title">
	<i class="fa fa-eur" aria-hidden="true"></i> Gestione costi fornitori
	<a href="#" class="balance_add_btn_link"><i class="fa fa-plus" aria-hidden="true"></i> Aggiungi costo fornitore</a>
</h1>
<div class="row balances_list_row">
	<div class="c9 main">
		<div class="row filter_row">
			<form action="/admin/view_suppliercosts" method="POST">
				<div class="c1">
					<label>Num. doc.</label>
					<input type="text" class="filter_control" name="filter_numdoc" value="<?=@isset($filter['filter_numdoc']) ? $filter['filter_numdoc'] : '' ?>">
				</div>
				<div class="c2">
					<label>Data documento</label>
					<input type="date" class="filter_control" name="filter_datadoc" value="<?=@isset($filter['filter_datadoc']) ? $filter['filter_datadoc'] : '' ?>">
				</div>
				<div class="c2">
					<label>Nome fornitore</label>
					<input type="text" class="filter_control" name="filter_name" value="<?=@isset($filter['filter_name']) ? $filter['filter_name'] : '' ?>">
				</div>
				<div class="c2">
					<label>Periodo competenza (mese - anno)</label>
					<select class="filter_control" name="filter_period_month" style="width: calc(60% - 10px); display: inline-block;">
						<option value="">- Tutti i mesi -</option>
						<?php for ($m = 1; $m <= 12; $m++) { echo '<option value="'.$m.'" '.($filter['filter_period_month'] == $m ? 'selected' : '').'>'.monthNameByNum($m).'</option>'; } ?>
					</select>
					<select class="filter_control" name="filter_period_year" style="width: calc(40% - 10px); display: inline-block;">
						<option value="">- Anno -</option>
						<?php for ($y = 2020; $y <= ((int)date('Y')+5); $y++) { echo '<option value="'.$y.'" '.($filter['filter_period_year'] == $y ? 'selected' : '').'>'.$y.'</option>'; } ?>
					</select>
				</div>
				<!-- <div class="c2">
					<label>Dal (data documento)</label>
					<input type="date" class="filter_control" name="filter_from" value="<?=@isset($filter['filter_from']) ? $filter['filter_from'] : date('Y-m').'-01' ?>">
				</div>
				<div class="c2">
					<label>Al (data documento)</label>
					<input type="date" class="filter_control" name="filter_to" value="<?=@isset($filter['filter_to']) ? $filter['filter_to'] : date('Y-m-d') ?>">
				</div> -->
				<div class="c2">
					<label>Centro di costo</label>
					<select class="filter_control" name="filter_costcenter">
						<option value="">- Tutti i centri di costo -</option>
						<?php foreach ($costcenters as $cc) { 
							echo '<option value="'.$cc['id'].'" '.($filter['filter_costcenter'] == $cc['id'] ? 'selected' : '').'>'.$cc['name'].' - '.$cc['location'].'</option>'; 
						} ?>
					</select>
				</div>
				<div class="c1">
					<label>Data creazione</label>
					<input type="date" class="filter_control" name="filter_date" value="<?=@isset($filter['filter_date']) ? $filter['filter_date'] : '' ?>">
				</div>
				<div class="c2">
					<label class="checkbox" for="filter_problems"><input type="checkbox" class="filter_control" name="filter_problems" id="filter_problems" <?=(!empty($filter['filter_problems']) ? 'checked' : '')?>> Solo con anomalie</label>
				</div>
				<div class="c12 filter_action_wrapper">
					<button type="submit" class="btn green"><i class="fa fa-search" aria-hidden="true"></i> Cerca</button>
					<a href="#" class="btn blue form_reset_btn" style="margin: 8px 0 0 0;"><i class="fa fa-ban" aria-hidden="true"></i> Azzera filtri</a>
					<a href="/admin/import_suppliercosts" class="btn yellow form_import_btn" style="margin: 8px 0 0 0;"><i class="fa fa-upload" aria-hidden="true"></i> Importa</a>
				</div>
			</form>
		</div>
		<div class="row">
			<div class="c12">
				Sono stati trovati <b class="numres"><?=count($suppliercosts)?></b> risultati con i filtri attuali - Totale imponibili <b class="numres"><?=number_format($find_total, 2, ",", ".")?> €</b>
			</div>
		</div>
		<div class="row" style="margin-top: 20px;">
			<div class="c12">
				<?php
				// echo '<pre>';
				// print_r($suppliercosts);
				// echo '</pre>';
				?>

				<table class="tab_list">
				<tr class="title_row">
					<th style="width: 50px;">Info</th>
					<th style="width: 50px;">Tipo</th>
					<th style="width: 100px;">Data</th>
					<th style="width: 130px;">Periodo</th>
					<th style="width: 220px;">Centro costo</th>
					<th>Fornitore</th>
					<th style="width: 100px;">Importo</th>
					<th style="width: 100px;">Imponibile</th>
					<th style="width: 100px;">Imposta</th>
					<th style="width: 50px;">Dett.</th>
					<th style="width: 100px;">Creato il</th>
					<th style="width: 110px;"></th>
				</tr>
				<?php foreach ($suppliercosts as $suppliercost) { 
					$missing_costcenter = false;
					$missing_costcat = false;
					$sum_is_ok = true;
					$sum = 0;
					$det_periods = [];
					$det_costcenters = [];
					$no_periods = false;
					foreach ($suppliercost['detail'] as $det) {
						// centro di costo
						if (!$det['id_costcenter'])
							$missing_costcenter = true;
						else {
							if (!in_array($det['id_costcenter'], $det_costcenters))
								$det_costcenters[] = $det['id_costcenter'];
						}
						// categoria di costo
						if (!$det['id_costcat'])
							$missing_costcat = true;
						// periodo di competenza
						if ($det['period_month'] AND $det['period_year']) {
							$p = monthNameByNum($det['period_month']).' '.$det['period_year'];
							if (!in_array($p, $det_periods))
								$det_periods[] = $p;
						} else {
							$no_periods = true;
						}
						// totale dettagli
						$sum += $det['amount'];
					}

					if (count($det_costcenters)) {
						if (count($det_costcenters) > 1)
							$cc_info = 'Vari';
						else {
							$tmp_cc = $this->costcenters_model->get_costcenter($det_costcenters[0]);
							$cc_info = $tmp_cc['name'];
							if ($tmp_cc['location'] && !strpos(strtolower($cc_info), trim(strtolower(preg_replace('/\(.+\)/', '', $tmp_cc['location'])))))
								$cc_info .= ' '.$tmp_cc['location'];
						}
					} else {
						$cc_info = '';
					}

					if (round($sum, 2) != round($suppliercost['imponibile'], 2))
						$sum_is_ok = false;
					?>
					<tr style="<?=(($missing_costcat OR $missing_costcenter OR !$sum_is_ok OR $no_periods) ? 'background: #fff9d3;' : '')?>" 
						class="<?=(($missing_costcat OR $missing_costcenter OR !$sum_is_ok OR $no_periods) ? 'danger' : '')?> <?=($suppliercost['doctype']['type'] == 'minus' ? 'minus' : '')?>">
						<td style="text-align: center; font-size: 18px;">
							<i class="fa fa-info-circle cost_info_icon" id="cost_info_icon_<?=$suppliercost['id']?>" aria-hidden="true"></i>
							<div class="cost_info_tooltip" style="display: none;">
								<table style="border: 0; box-shadow: none;">
									<tr>
										<td style="border: 0; margin: 0; padding: 0; color: #51c8ff;"><i class="fa fa-file-text" aria-hidden="true"></i><b>Tipo doc.:</b></td>
										<td style="border: 0; margin: 0; padding: 0 0 0 10px;"><?=substr($suppliercost['doctype']['description'], 0, 30).(strlen($suppliercost['doctype']['description']) > 30 ? '...' : '')?></td>
									</tr>
									<tr>
										<td style="border: 0; margin: 0; padding: 0; color: #51c8ff;"><i class="fa fa-file" aria-hidden="true"></i><b>Num. doc.:</b></td>
										<td style="border: 0; margin: 0; padding: 0 0 0 10px;"><?=$suppliercost['numero']?></td>
									</tr>
									<tr>
										<td style="border: 0; margin: 0; padding: 0; color: #51c8ff;"><i class="fa fa-code" aria-hidden="true"></i><b>Nome file:</b></td>
										<td style="border: 0; margin: 0; padding: 0 0 0 10px;"><?=$suppliercost['filename']?></td>
									</tr>
									<?php if ($missing_costcat OR $missing_costcenter OR !$sum_is_ok OR $no_periods) { ?>
										<tr>
											<td style="border: 0; margin: 0; padding: 0; color: #f45d8a;"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i><b>Problema:</b></td>
											<td style="border: 0; margin: 0; padding: 0 0 0 10px;">
												<?php
												if ($missing_costcat OR $missing_costcenter)
													echo 'Categ. o centro di costo mancante<br>';
												if (!$sum_is_ok)
													echo 'Somma dettagli errata<br>';
												if ($no_periods)
													echo 'Periodo di riferimento mancante<br>';
												?>
											</td>
										</tr>
									<?php } ?>
								</table>
							</div>
						</td>
						<td style="text-align: center;"><?=rvd($suppliercost['tipo_documento'])?></td>
						<td style="text-align: center;"><?=rvd($suppliercost['data'])?></td>
						<td style="text-align: center;"><?=(count($det_periods) ? (count($det_periods) > 1 ? 'Vari' : $det_periods[0]) : '')?></td>
						<td style="text-align: center; font-size: 13px; line-height: 1.2;"><?=$cc_info?></td>
						<td><?=$suppliercost['fornitore']?></td>
						<td style="text-align: right;" class="amount"><?=number_format($suppliercost['importo_totale'], 2, ",", ".")?> €</td>
						<td style="text-align: right;" class="amount"><?=number_format($suppliercost['imponibile'], 2, ",", ".")?> €</td>
						<td style="text-align: right;" class="amount"><?=number_format($suppliercost['imposta'], 2, ",", ".")?> €</td>
						<td style="text-align: center;"><?=count($suppliercost['detail'])?></td>
						<td style="text-align: center;"><?=rvd(substr($suppliercost['created_at'], 0, 10))?></td>
						<td class="actions">
							<a href="/admin/edit_supplier/<?=$suppliercost['id_supplier']?>" data-tippy-content="Vai al fornitore"><i class="fa fa-building"></i></a>
							<a href="/admin/edit_suppliercost/<?=$suppliercost['id']?>" data-tippy-content="Modifica costo fornitore"><i class="fa fa-pencil"></i></a>
							<a class="del_suppliercost" data-idsuppliercost="<?=$suppliercost['id']?>" data-namesuppliercost="<?=htmlentities($suppliercost['numero'])?>" href="#" data-tippy-content="Cancella costo fornitore"><i class="fa fa-trash"></i></a>
						</td>
					</tr>
				<?php } ?>
				</table>
			</div>
		</div>
	</div>
	<div class="c3 side">
		<h2 style="margin: 0 0 20px;">Nuovo costo fornitore</h2>
		<form class="admin detail_form" action="/admin/insert_suppliercost" method="POST">
			<label>Fornitore *</label>
			<select name="id_supplier" required>
				<option value="">- Seleziona un fornitore -</option>
				<?php
				foreach ($suppliers as $s)
					echo '<option value="'.$s['id'].'">'.($s['denominazione'] ? $s['denominazione'] : $s['cognome'].' '.$s['nome']).'</option>';
				?>
			</select>
			<div class="row" style="padding: 20px; background: #f6f6f6;">
				<div class="c12 first">
					<label><b>Periodo di competenza *</b></label><br>
					<select name="period_month" style="width: 45%; float: left; clear: none; margin: 0 10px 0 0;" required>
						<option value="">- Scegli il mese -</option>
						<?php for ($m = 1; $m <= 12; $m++) { echo '<option value="'.$m.'">'.monthNameByNum($m).'</option>'; } ?>
					</select>
					<select name="period_year" style="width: 45%; float: left; clear: none; margin: 0 10px 0 0;" required>
						<option value="">- Scegli l'anno -</option>
						<?php for ($y = 2020; $y <= ((int)date('Y')+5); $y++) { echo '<option value="'.$y.'">'.$y.'</option>'; } ?>
					</select>
				</div>
			</div>
			<br>
			<!-- <label>Tipo documento</label>
			<input type="text" name="tipo_documento" placeholder="Tipo documento" value=""> -->
			<!-- <label>Divisa</label> -->
			<input type="hidden" name="divisa" placeholder="Divisa" value="EUR">
			<label>Data</label>
			<input type="date" name="data" placeholder="Data">
			<label>Tipo documento *</label>
			<select name="tipo_documento" required>
				<option value="">- Scegli tipo -</option>
				<?php foreach ($doctypes as $type) { ?>
					<option value="<?=$type['cod']?>"><?=$type['description']?></option>
				<?php } ?>
			</select>
			<label>Num. documento</label>
			<input type="text" name="numero" placeholder="Numero documento" value="">
			<label>Importo totale *</label>
			<input type="number" step="0.01" name="importo_totale" placeholder="Importo costo fornitore" value="" required>
			<label>Importo imponibile</label>
			<input type="number" step="0.01" name="imponibile" placeholder="Importo imponibile" value="">
			<label>Importo imposta</label>
			<input type="number" step="0.01" name="imposta" placeholder="Importo imposta" value="">
			<!--
			<input type="text" name="aliquota_iva" placeholder="Aliquota IVA" value="">
			<input type="text" name="arrotondamento" placeholder="Arrotondamento" value="">
			<input type="text" name="esigibilita_iva" placeholder="Esigiblità IVA" value="">
			-->
			<br>
			<i class="advice">* campi obbligatori</i>
			<button type="submit" class="blue" style="float: right; margin-right: 20px;">Salva</button>
		</form>
	</div>
</div>

<div id="overlay"></div>
<div class="form_del_wrapper">
	<form id="del_suppliercost_form" class="admin" action="/admin/del_suppliercost" method="POST">
		<input name="id_suppliercost_to_del" id="id_suppliercost_to_del" type="hidden" value="">
		<h4>Stai per cancellare il costo fornitore [ <span id="suppliercost_to_del"></span> ]</h4>
		<a id="del_suppliercost_close" class="btn orange" href="#">Annulla</a>
		<a id="del_suppliercost_proceed" class="btn green" href="#" style="float: right;">Elimina costo fornitore</a>
	</form>
</div>

<script>
$(function() {
	handleProblematicRowsVisibility();

	tippy('[data-tippy-content]');

	$('.cost_info_icon').each(function() {
		var template = $(this).closest('td').find('.cost_info_tooltip').html();
		tippy('#'+$(this).attr('id'), {
			content(reference) {
				return template;
			},
			allowHTML: true
		});
	});
});

$.validate({
	errorMessagePosition: 'top'
});

function handleProblematicRowsVisibility() {
	if ($('#filter_problems').is(':checked')) {
		$('.tab_list tr:not(.title_row)').hide();
		$('.tab_list tr.danger').show();
		$.ajax({
			url: '/ajax/setSessionData',
			data: 'key=filter|usercosts|filter_problems&val=1',
			method: 'POST',
			success: function() {}
		});
	} else {
		$('.tab_list tr').show();
		$.ajax({
			url: '/ajax/delSessionData',
			data: 'key=filter|usercosts|filter_problems',
			method: 'POST',
			success: function() {}
		});
	}
}

$(document).ready(function() {
	$('#filter_problems').change(function() {
		handleProblematicRowsVisibility();
	});

	$(".del_suppliercost").click(function() {
		idsuppliercost = $(this).data("idsuppliercost");
		namesuppliercost = $(this).data("namesuppliercost");
		$("#overlay, .form_del_wrapper").fadeIn();
		$("#id_suppliercost_to_del").val(idsuppliercost);
		$("#suppliercost_to_del").html(namesuppliercost);
		$("#suppliercost_to_assoc option").show();
		$("#suppliercost_to_assoc option[value='"+idsuppliercost+"']").hide();
	});
	
	$("#del_suppliercost_proceed").click(function() {
		// if ($("#suppliercost_to_assoc").val()) {
			$("#del_suppliercost_form").submit();
		// } else {
			// alert("Devi selezionare un servizio a cui associare gli utenti!");
		// }
	});
	
	$("#del_suppliercost_close").click(function() {
		$("#overlay, .form_del_wrapper").fadeOut();
	});

	$('.form_reset_btn').click(function() {
		$(this).closest('form').find('input').val("");
		$(this).closest('form').find('select').val("");
		$(this).closest('form').find('input[type=checkbox]').prop('checked', false);
	});
});
</script>