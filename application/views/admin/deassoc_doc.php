<h1><i class="fa fa-unlink"></i> Rimuovi associazioni documenti</h1>

<div class="row">
	<div class="c6">
		<form class="admin" action="/admin/deassoc_doc" method="POST" id="deassoc_form">
			<label for="deassoc_from">Rimuovi associazioni dalla data/ora:</label><br>
			<input name="from" type="text" id="deassoc_from" class="datepicker"> 
			<br>
			<label for="deassoc_to">Fino alla data/ora:</label><br>
			<input name="to" type="text" id="deassoc_to" class="datepicker">
			
			<button type="submit" class="blue">Rimuovi associazioni</button>
		</form>
	</div>
	<div class="c6"></div>
</div>

<style>
label { margin-bottom: 10px; }
</style>

<script>
$(document).ready(function() {
	$(".datepicker").datetimepicker({
		timeText: 'Tempo',
		hourText: 'Ore',
		minuteText: 'Minuti',
		secondText: 'Secondi',
		currentText: 'Adesso',
		closeText: 'Chiudi'
	});
	
	$("#deassoc_form button").click(function(e) {
		e.preventDefault();
		if ($("#deassoc_from").val() != "" && $("#deassoc_to").val() != "") {
			if (confirm('Hai controllato bene l\'intervallo temporale selezionato? Sei sicuro di voler cancellare le associazioni?')) {
				$("#deassoc_form").submit();
			}
		} else {
			alert("Devi inserire le date/ore di inizio e di fine dell'intervallo temporale");
		}
		return false;
	});
});
</script>