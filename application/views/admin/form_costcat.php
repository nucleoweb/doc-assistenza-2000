<div class="row space-bot">
	<div class="c12">
		<a href="/admin" class="back">Torna all'homepage</a>
	</div>
</div>
<h1><i class="fa fa-folder-open" aria-hidden="true"></i> Modifica categoria di costo</h1>
<form class="admin" action="/admin/edit_costcat" method="POST">
	<input type="hidden" name="id_costcat" value="<?=@$costcat['id']?>">
	<input type="text" name="name" placeholder="Nome categoria di costo" value="<?=@htmlentities($costcat['name'])?>">
	<input type="text" name="note" placeholder="Note categoria di costo" value="<?=@htmlentities($supplier['note'])?>">
	<br>
	<button type="submit" class="blue">Salva</button>
</form>