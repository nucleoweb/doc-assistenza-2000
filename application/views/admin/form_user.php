<h1><i class="fa fa-user"></i> <?=((!empty($info)) ? 'Modifica utente' : 'Inserisci un nuovo utente')?></h1>
<form class="admin" action="<?=((!empty($info)) ? '/admin/edit_user' : '/admin/insert_user')?>" method="POST">
	<div class="row">
		<div class="c6">
			<?php if (!empty($info)) { ?>
				<input type="hidden" name="id_user" value="<?=@$info['id_user']?>">
			<?php } ?>
			<h4>Dati anagrafici</h4>
			<div class="row">
				<div class="c6 first">
					<label class="txtInput">Nome</label>
					<input type="text" name="first_name" placeholder="Nome" value="<?=@$info['first_name']?>" data-validation="required">
				</div>
				<div class="c6 last">
					<label class="txtInput">Cognome</label>
					<input type="text" name="last_name" placeholder="Cognome" value="<?=@$info['last_name']?>" data-validation="required">
				</div>
			</div>
			<div class="row">
				<div class="c12">
					<label class="txtInput">Genere</label>
					<select name="gender">
						<option value="">- Scegli -</option>
						<option value="M" <?=(@$info['gender'] == 'M' ? 'selected' : '')?>>Uomo</option>
						<option value="F" <?=(@$info['gender'] == 'F' ? 'selected' : '')?>>Donna</option>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="c12">
					<label class="txtInput">Comune di nascita</label>
					<input type="text" name="birth_city" placeholder="Comune di nascita" value="<?=@$info['birth_city']?>">
				</div>
			</div>
			<div class="row">
				<div class="c6 first">
					<label class="txtInput">Data di nascita</label>
					<input type="text" name="birth_date" placeholder="Data di nascita" value="<?=@rvd($info['birth_date'])?>" class="datepicker">
				</div>
				<div class="c6 last">
					<label class="txtInput calc" title="L'età viene calcolata a partire dalla data di nascita">Età</label>
					<input type="text" name="age" placeholder="Età" value="<?=@$info['age']?>" disabled>
				</div>
			</div>
			<div class="row">
				<div class="c6 first">
					<label class="txtInput">Nazionalità</label>
					<input type="text" name="nationality" placeholder="Nazionalità" value="<?=@$info['nationality']?>">
				</div>
				<div class="c6 last">
					<label class="txtInput">Straniero</label>
					<select name="foreigner">
						<option value="">- Scegli -</option>
						<option value="0" <?=(@$info['foreigner'] == '0' ? 'selected' : '')?>>No</option>
						<option value="1" <?=(@$info['foreigner'] == '1' ? 'selected' : '')?>>Sì</option>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="c6 first">
					<label class="txtInput">Indirizzo residenza</label>
					<input type="text" name="address" placeholder="Indirizzo" value="<?=@$info['address']?>">
				</div>
				<div class="c6 last">
					<label class="txtInput">Città residenza</label>
					<input type="text" name="city" placeholder="Città" value="<?=@$info['city']?>">
				</div>
			</div>
			<div class="row">
				<div class="c6 first">
					<label class="txtInput">Indirizzo e-mail</label>
					<input type="text" name="email" placeholder="Indirizzo e-mail" value="<?=@$info['email']?>" data-validation="required email">
				</div>
				<div class="c6 last">
					<label class="txtInput">Recapito telefonico</label>
					<input type="text" name="telephone" placeholder="Recapito telefonico" value="<?=@$info['telephone']?>">
				</div>
			</div>
			<label class="txtInput">Codice fiscale</label>
			<input type="text" name="fiscal_code" placeholder="Codice fiscale" value="<?=@$info['fiscal_code']?>">
			
			<input type="hidden" name="company" placeholder="Azienda" value="Assistenza 2000" readonly>			
			
			<br>
			<div class="accessdata_block">
				<h4><i class="fa fa-unlock-alt"></i> Accesso alla piattaforma</h4>
				<?php if (empty($info)) { ?>
					<input type="text" name="username" placeholder="Username" value="<?=@$info['username']?>" id="user_username" data-validation="length" data-validation-length="5-30">
					<input type="password" name="password" placeholder="Password" value="" data-validation="strength" data-validation-strength="2">  <a href="#" class="show_psw" data-fieldname="password"><i class="fa fa-eye"></i></a>
					<br class="clear">
				<?php } ?>
				<input type="radio" name="active" value="1" id="active_1" <?=((!empty($info) AND $info['active'] == 1) ? 'checked' : '')?> data-validation="required"> <label for="active_1">Abilitato</label>
				&nbsp; &nbsp; &nbsp;
				<input type="radio" name="active" value="0" id="active_0" <?=((!empty($info) AND $info['active'] == 0) ? 'checked' : '')?> data-validation="required"> <label for="active_0">Disabilitato</label>
				<br><br><p>Tipo di utente</p>
				<input type="radio" name="role" value="user" id="role_user" <?=((!empty($info) AND $info['role'] == 'user') ? 'checked' : '')?> data-validation="required"> <label for="role_user">User</label>
				&nbsp; &nbsp; &nbsp;
				<input type="radio" name="role" value="admin" id="role_admin" <?=((!empty($info) AND $info['role'] == 'admin') ? 'checked' : '')?> data-validation="required"> <label for="role_admin">Admin</label>
				<?php if (empty($info)) { ?>
					<br><br>
					<div class="infobox">
						Ricorda di salvare la password per questo utente in quanto d'ora in poi non sarà più visibile in chiaro ma potrà solo essere modificata.
						<br><br>
						<p>Vuoi inviare un'e-mail con i dati d'accesso all'utente?</p>
						<input type="radio" name="send_data" value="1" id="send_data_1" checked> <label for="send_data_1">Sì</label>
						&nbsp; &nbsp; &nbsp;
						<input type="radio" name="send_data" value="0" id="send_data_0"> <label for="send_data_0">No</label>
					</div>
				<?php } ?>
			</div>
		</div>
		<div class="c6"> 
			<h4>Dati lavorativi</h4>
			<div id="work_selects">
				<label class="txtInput">Mansione</label>
				<select name="id_job">
					<option value="">- Nessuna mansione -</option>
					<?php
					foreach ($jobs as $job)
						echo '<option value="'.$job['id'].'" '.(($info['job'] == $job['name']) ? 'selected' : '').'>'.$job['name'].'</option>';
					?>
				</select>

				<label class="txtInput">Livello</label>
				<select name="id_level">
					<option value="">- Nessun livello -</option>
					<?php
					foreach ($levels as $level)
						echo '<option value="'.$level['id'].'" '.(($info['level'] == $level['name']) ? 'selected' : '').'>'.$level['name'].'</option>';
					?>
				</select>

				<label class="txtInput">Area</label>
				<select name="id_area">
					<option value="">- Nessun'area -</option>
					<?php
					foreach ($areas as $area)
						echo '<option value="'.$area['id'].'" '.(($info['area'] == $area['name']) ? 'selected' : '').'>'.$area['name'].'</option>';
					?>
				</select>

				<label class="txtInput">Servizio</label>
				<select name="id_service">
					<option value="">- Nessun servizio -</option>
					<?php
					foreach ($services as $service)
						echo '<option value="'.$service['id'].'" '.(($info['service'] == $service['name']) ? 'selected' : '').'>'.$service['name'].'</option>';
					?>
				</select>

				<label class="txtInput">Cliente</label>
				<select name="id_customer">
					<option value="">- Nessun cliente -</option>
					<?php
					foreach ($customers as $customer)
						echo '<option value="'.$customer['id'].'" '.(($info['customer'] == $customer['name']) ? 'selected' : '').'>'.$customer['name'].'</option>';
					?>
				</select>
			</div>
			<div class="row">
				<div class="c12">
					<label class="txtInput">Data assunzione</label>
					<input type="text" name="date_hiring" placeholder="Data assunzione" value="<?=@rvd($info['date_hiring'])?>" class="datepicker">
				</div>
			</div>
			<div class="row">
				<div class="c6 first">
					<label class="txtInput calc" title="I mesi di servizio vengono calcolati in base alla data di assunzione">Mesi servizio</label>
					<input type="text" name="serv_months" placeholder="Mesi servizio" value="<?=@$info['serv_months']?>" disabled>
				</div>
				<div class="c6 last">
					<label class="txtInput">Mesi precedenti</label>
					<input type="text" name="prev_months" placeholder="Mesi precedenti" value="<?=@$info['prev_months']?>">
				</div>
			</div>
			<div class="row">
				<div class="c6 first">
					<label class="txtInput calc" title="I mesi di esperienza totali sono la somma dei mesi di servizio e dei mesi precedenti">Mesi esperienza totali</label>
					<input type="text" name="tot_months" placeholder="Mesi esperienza totali" value="<?=@$info['tot_months']?>" disabled>
				</div>
				<div class="c6 last">
					<label class="txtInput calc" title="Gli anni di esperienza totali dipendono dai mesi di esperienza totali">Anni esperienza totali</label>
					<input type="text" name="tot_years" placeholder="Anni esperienza totali" value="<?=@$info['tot_years']?>" disabled>
				</div>
			</div>
			<div class="row">
				<div class="c12">
					<label class="txtInput calc" title="Il rank dipende dal numero di mesi di esperienza totali e anche dal flag 'Temporaneo'">Rank</label>
					<select name="rank" disabled>
						<option value="">- Scegli -</option>
						<option value="Junior" <?=(@$info['rank'] == 'Junior' ? 'selected' : '')?>>Junior</option>
						<option value="Senior" <?=(@$info['rank'] == 'Senior' ? 'selected' : '')?>>Senior</option>
						<option value="Temp" <?=(@$info['rank'] == 'Temp' ? 'selected' : '')?>>Temp</option>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="c12">
					<div class="row">
						<div class="c4 first">
							<label style="display: block;">Costo orario</label>
							<input type="text" value="<?=@$info['hourly_cost']?>" style="width: calc(100% - 40px); display: inline-block;" disabled> €
						</div>
						<div class="c8 row">
							<div style="background: #FFF1C9; font-size: 14px; line-height: 1.2; font-weight: 600; padding: 10px;">
								<i class="fa fa-info-circle" aria-hidden="true" style="display: inline-block; margin-right: 5px; font-size: 18px;"></i> 
								Il costo orario del dipendente viene aggiornato automaticamente a ogni import dei costi dipendenti effettuato dalla sezione Bilanci
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="c4 first">
					<label class="txtInput">Temporaneo</label>
					<select name="temp">
						<option value="">- Scegli -</option>
						<option value="0" <?=(@$info['temp'] == '0' ? 'selected' : '')?>>No</option>
						<option value="1" <?=(@$info['temp'] == '1' ? 'selected' : '')?>>Sì</option>
					</select>
				</div>
				<div class="c4">
					<label class="txtInput">Libero professionista</label>
					<select name="freelance">
						<option value="">- Scegli -</option>
						<option value="0" <?=(@$info['freelance'] == '0' ? 'selected' : '')?>>No</option>
						<option value="1" <?=(@$info['freelance'] == '1' ? 'selected' : '')?>>Sì</option>
					</select>
				</div>
				<div class="c4 last">
					<label class="txtInput">Tirocinante</label>
					<select name="trainee">
						<option value="">- Scegli -</option>
						<option value="0" <?=(@$info['trainee'] == '0' ? 'selected' : '')?>>No</option>
						<option value="1" <?=(@$info['trainee'] == '1' ? 'selected' : '')?>>Sì</option>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="c6 first">
					<label class="txtInput">Prima assunzione (anno)</label>
					<input type="text" name="first_hiring" placeholder="Prima assunzione" value="<?=@$info['first_hiring']?>">
				</div>
				<div class="c6 last">
					<label class="txtInput">Ultima assunzione (anno)</label>
					<input type="text" name="last_hiring" placeholder="Ultima assunzione" value="<?=@$info['last_hiring']?>">
				</div>
			</div>
			<div class="row">
				<div class="c6 first">
					<label class="txtInput">Anno cessazione (anno) <span style="color: #d90000;">*</span></label>
					<input type="text" name="year_termination" placeholder="Anno cessazione" value="<?=@$info['year_termination']?>">
				</div>
				<div class="c6 last">
					<label class="txtInput calc" title="Questo flag dipende dalla valorizzazione o meno del campo 'Anno cessazione'">Attualmente in forza</label>
					<select name="is_hired" disabled>
						<option value="">- Scegli -</option>
						<option value="0" <?=(@$info['is_hired'] == '0' ? 'selected' : '')?>>No</option>
						<option value="1" <?=(@$info['is_hired'] == '1' ? 'selected' : '')?>>Sì</option>
					</select>
				</div>
			</div>
			<p class="note">* Se inserisci l'anno di cessazione del rapporto del dipendente, dovrai aggiornare manualmente anche il dato "Mesi precedenti" sommando il valore dei mesi di servizio appena conclusi. Puoi calcolare il valore dei mesi di servizio con l'utility presente nel menù Dipendenti</p>

			<hr class="bold">

			<!-- SEZIONE CONTRATTO A TERMINE -->
			<h6 id="fixed_term_title">Sezione contratto a termine <span><a href="#" id="add_fixed_term_row"><i class="fa fa-plus"></i> Aggiungi periodo</a></span></h6>
			<div id="fixed_term_wrapper">
				<div class="row fixed_term_row clone">
					<div class="c5 first start">
						<label class="txtInput">Inizio periodo</label>
						<input type="text" name="fixed_term_start[]" disabled>
					</div>
					<div class="c5 end">
						<label class="txtInput">Fine periodo</label>
						<input type="text" name="fixed_term_end[]" disabled>
					</div>
					<div class="c2"><a href="#" class="fixed_term_period_delete"><i class="fa fa-trash"></i></a></div>
				</div>
			</div>

			<script>
			<?php if (!empty($info['fixed_term'])) { ?>
				<?php $fixed_term = json_decode($info['fixed_term']); ?>
				$(function() {
					<?php foreach ($fixed_term as $period) { ?>
						new_row = $('.fixed_term_row.clone').clone().removeClass('clone');
						new_row.find('input').removeAttr('disabled');
						new_row.find('.start input').val("<?=$period->start?>");
						new_row.find('.end input').val("<?=$period->end?>");
						new_row.appendTo('#fixed_term_wrapper');
					<?php } ?>
				});
			<?php }	?>

			$(document).ready(function() {
				$('#add_fixed_term_row').click(function() {
					new_row = $('.fixed_term_row.clone').clone().removeClass('clone');
					new_row.find('input').removeAttr('disabled');
					new_row.appendTo('#fixed_term_wrapper');
					return false;
				});

				$(document).on('click', '.fixed_term_period_delete', function() {
					$(this).closest('.fixed_term_row').remove();
					return false;
				});
			});
			</script>
			<!-- FINE SEZIONE CONTRATTO A TERMINE -->
		</div>
	</div>
	<div class="row submit_row">
		<div class="c6">
			<button type="submit" class="blue">Salva</button>

			<?php if (!empty($info)) { ?>
				<a style="float: right;" href="/admin/delete_user/<?=$info['id_user']?>" class="btn red" onclick="return confirm('Sei sicuro di voler eliminare questo utente?');">Elimina</a>
				<a style="float: right; margin-right: 10px;" href="/admin/change_user_access/<?=$info['id_user']?>" class="btn blue">Modifica dati d'accesso</a>
			<?php } ?>
		</div>
		<div class="c6"></div>
	</div>
</form>
<?php if (!empty($docs)) { ?>
	<hr>
	<div class="row">
		<div class="c6">
			<h4><i class="fa fa-file-text-o"></i> File collegati all'utente</h4>
			<div id="docs_list">
				<form class="admin"><input type="text" class="search" placeholder="Cerca documento"></form>
				<ul class="list">
					<?php
					foreach ($docs as $d) {
						$cat = ucfirst(strtolower(str_replace("_", " ", ($d['cat']))));
						echo '<li style="margin-bottom: 10px;">
									<b><a class="name" href="/admin/edit_doc/'.$d['id_doc'].'" title="'.$d['filename'].'">'.$d['name'].'</a></b>
									<!--[ <span class="filename">'.$d['filename'].'</span> ]-->
									<i class="fa fa-tag"></i> Categoria: <span class="date">'.((strlen($cat) <= 16) ? $cat : substr($cat, 0, 16)."..").'</span><br>
									<i class="fa fa-upload"></i> Upload: <span class="date">'.date("d/m/Y", strtotime($d['date_upload'])).'</span><br>';
									// if (isset($d['users_assigned']) AND count($d['users_assigned']) == 1) {
										// $usr_assigned = reset($d['users_assigned']);
										// $datetime = $usr_assigned['last_download'];
										// if ($datetime)
											// echo '<i class="fa fa-cloud-download"></i> Download: <span class="date">'.date("d/m/Y", strtotime($datetime)).'</span><br>';
									// }
									echo '</li>';
					}
					?>
				</ul>
				<ul class="pagination"></ul>
			</div>
		</div>
	</div>
<?php } ?>

<style>
.accessdata_block input[type=text] {
	clear: none !important;
	display: inline-block !important;
	width: 80% !important;
}
</style>

<script>
var options = {
    valueNames: ['name', 'filename'],
	page: 30,
	pagination: true
};
var docsList = new List('docs_list', options);

$.validate({
	errorMessagePosition: 'top',
	modules: 'security',
	onModulesLoaded: function() {
		var optionalConfig = {
		  fontSize: '12pt',
		  padding: '4px',
		  bad: 'Pessimo',
		  weak: 'Debole',
		  good: 'Buono',
		  strong: 'Forte'
    };
    $('input[name="password"]').displayPasswordStrength(optionalConfig);
  }
});

$(document).ready(function() {
	$(".show_psw").click(function() {
		$("input[name='"+$(this).data('fieldname')+"']").attr('type', 'text');
		return false;
	});
});
</script>

