<div class="row space-bot">
	<div class="c12">
		<a href="/admin" class="back">Torna all'homepage</a>
	</div>
</div>
<h1><i class="fa fa-sort"></i> Modifica livello</h1>
<form class="admin" action="/admin/edit_level" method="POST">
	<?php if (!empty($level)) { ?>
		<input type="hidden" name="id_level" value="<?=@$level['id']?>">
	<?php } ?>
	<input type="text" name="name" placeholder="Nome livello" value="<?=@$level['name']?>" data-validation="required">
	<br>
	<button type="submit" class="blue">Salva</button>
</form>