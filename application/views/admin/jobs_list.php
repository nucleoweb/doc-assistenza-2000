<h1><i class="fa fa-briefcase"></i> Gestione mansioni</h1>
<div class="row">
	<div class="c6">
		<table>
		<tr>
			<th>ID</th>
			<th>Nome</th>
			<th>Num. utenti attivi</th>
			<th>Num. utenti totali</th>
			<th></th>
		</tr>
		<?php foreach ($jobs as $job) { ?>
			<tr>
				<td><?=$job['id']?></td>
				<td><?=$job['name']?></td>
				<td><?=$job['num_users_active']?></td>
				<td><?=$job['num_users']?></td>
				<td class="actions">
					<a href="/admin/edit_job/<?=$job['id']?>"><i class="fa fa-pencil"></i></a>
					<a class="del_job" data-idjob="<?=$job['id']?>" data-namejob="<?=$job['name']?>" href="#"><i class="fa fa-trash"></i></a>
				</td>
			</tr>
		<?php } ?>
		</table>
	</div>
	<div class="c6" style="padding-left: 40px;">
		<h2>Inserisci nuova mansione</h2>
		<form class="admin" action="/admin/insert_job" method="POST">
			<input type="text" name="name" placeholder="Nome mansione" value="<?=@$info['name']?>" data-validation="required">
			<br>
			<button type="submit" class="blue">Salva</button>
		</form>
	</div>
</div>

<div id="overlay"></div>
<div class="form_del_wrapper">
	<form id="del_job_form" class="admin" action="/admin/del_job" method="POST">
		<input name="id_job_to_del" id="id_job_to_del" type="hidden" value="">
		<h4>Stai per cancellare la mansione [ <span id="job_to_del"></span> ]</h4>
		<hr>
		Seleziona una mansione a cui associare gli utenti (se presenti):
		<br><br>
		<select name="job_to_assoc" id="job_to_assoc">
			<option value="">- Seleziona una mansione -</option>
			<?php foreach ($jobs as $job) { ?>
				<option value="<?=$job['id']?>"><?=$job['name']?></option>
			<?php } ?>
		</select>
		<br><br>
		<a id="del_job_close" class="btn orange" href="#">Annulla</a>
		<a id="del_job_proceed" class="btn green" href="#" style="float: right;">Elimina mansione</a>
	</form>
</div>

<script>
$.validate({
	errorMessagePosition: 'top'
});

$(document).ready(function() {
	$(".del_job").click(function() {
		idjob = $(this).data("idjob");
		namejob = $(this).data("namejob");
		$("#overlay, .form_del_wrapper").fadeIn();
		$("#id_job_to_del").val(idjob);
		$("#job_to_del").html(namejob);
		$("#job_to_assoc option").show();
		$("#job_to_assoc option[value='"+idjob+"']").hide();
	});
	
	$("#del_job_proceed").click(function() {
		if ($("#job_to_assoc").val()) {
			$("#del_job_form").submit();
		} else {
			alert("Devi selezionare una mansione a cui associare gli utenti!");
		}
	});
	
	$("#del_job_close").click(function() {
		$("#overlay, .form_del_wrapper").fadeOut();
	});
});
</script>