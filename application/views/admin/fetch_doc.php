<h1><i class="fa fa-link"></i> Associa documenti</h1>

<?php if (count($docs_to_assoc)) { ?> 
	<h4>Nella cartella ho trovato <?=count($docs_to_assoc)?> file da caricare</h4>
	<table style="max-width: 95%;">
		<tr>
			<th>Nome file</th>
			<th>Utente da associare</th>
			<th></th>
		</tr>
		<?php foreach ($docs_to_assoc as $doc) { ?>
		<tr>
			<td><?=$doc['filename']?></td>
			<td><?=$doc['user_name']?></td>
			<td style="width: 60px; font-size: 18px; text-align: center;">
				<a href="<?=$doc['link']?>" target="_blank"><i class="fa fa-download"></i></a>
			</td>
		</tr>
		<?php } ?>
	</table>
	
	<br>
	
	<form class="admin" action="/admin/fetch_doc" method="POST">
		<select name="cat" data-validation="required">
			<option value="">- Scegli una categoria -</option>
			<?php foreach ($doc_cats as $cod => $cat) { ?>
				<option value="<?=$cod?>"><?=$cat?></option>
			<?php } ?>
		</select>
			
		<div id="alert_box">
			<h4><i class="fa fa-bell"></i> Notifiche</h4>
			<input name="alert_mail" type="checkbox" id="alert_mail" checked> <label for="alert_mail">Invia notifica del documento via e-mail</label>
			<br><br>
			<input name="alert_sms" type="checkbox" id="alert_sms" checked> <label for="alert_sms">Invia notifica del documento via sms</label>
		</div>
		<button type="submit" class="blue">Salva</button>
	</form>
<?php } else { ?>
	<p>Non sono stati trovati file da caricare</p>
<?php } ?>

<script>
$.validate({
	errorMessagePosition: 'top'
});
</script>