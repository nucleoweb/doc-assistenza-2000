<h1><i class="fa fa-folder-open"></i> Gestione categorie</h1>
<div class="row">
	<div class="c6">
		<table>
		<tr>
			<th>ID</th>
			<th>Codice</th>
			<th>Nome</th>
			<th>Num. documenti</th>
			<th></th>
		</tr>
		<?php foreach ($cats as $cat) { ?>
			<tr>
				<td><?=$cat['id_cat']?></td>
				<td><?=$cat['code']?></td>
				<td><?=$cat['name']?></td>
				<td><?=$cat['num_docs']?></td>
				<td class="actions">
					<? if ($cat['code'] != "BUSTE_PAGA") { ?>
					<a href="/admin/edit_cat/<?=$cat['id_cat']?>"><i class="fa fa-pencil"></i></a>
					<a class="del_cat" data-idcat="<?=$cat['id_cat']?>" data-codecat="<?=$cat['code']?>" data-namecat="<?=$cat['name']?>" href="#"><i class="fa fa-trash"></i></a>
					<? } ?>
				</td>
			</tr>
		<?php } ?>
		</table>
	</div>
	<div class="c6" style="padding-left: 40px;">
		<h2>Inserisci nuova categoria</h2>
		<form class="admin" action="/admin/insert_cat" method="POST">
			<input type="text" name="code" placeholder="Codice categoria" value="<?=@$info['code']?>" data-validation="custom" data-validation-regexp="^([A-Z_]{3,})$">
			<input type="text" name="name" placeholder="Nome categoria" value="<?=@$info['name']?>" data-validation="required">
			<br>
			<button type="submit" class="blue">Salva</button>
		</form>
		<p><b>N.B.:</b> Il codice categoria accetta solo lettere maiuscole e trattini bassi [ _ ]</p>
	</div>
</div>

<div id="overlay"></div>
<div class="form_del_wrapper">
	<form id="del_cat_form" class="admin" action="/admin/del_cat" method="POST">
		<input name="id_cat_to_del" id="id_cat_to_del" type="hidden" value="">
		<input name="code_cat_to_del" id="code_cat_to_del" type="hidden" value="">
		<h4>Stai per cancellare la categoria [ <span id="cat_to_del"></span> ]</h4>
		<hr>
		Seleziona una categoria a cui associare i documenti (se presenti):
		<br><br>
		<select name="cat_to_assoc" id="cat_to_assoc">
			<option value="">- Seleziona una categoria -</option>
			<?php foreach ($cats as $cat) { ?>
				<option value="<?=$cat['code']?>"><?=$cat['name']?></option>
			<?php } ?>
		</select>
		<br><br>
		<a id="del_cat_close" class="btn orange" href="#">Annulla</a>
		<a id="del_cat_proceed" class="btn green" href="#" style="float: right;">Elimina categoria</a>
	</form>
</div>

<script>
$.validate({
	errorMessagePosition: 'top'
});

$(document).ready(function() {
	$(".del_cat").click(function() {
		idcat = $(this).data("idcat");
		codecat = $(this).data("codecat");
		namecat = $(this).data("namecat");
		$("#overlay, .form_del_wrapper").fadeIn();
		$("#id_cat_to_del").val(idcat);
		$("#code_cat_to_del").val(codecat);
		$("#cat_to_del").html(namecat);
		$("#cat_to_assoc option").show();
		$("#cat_to_assoc option[value='"+codecat+"']").hide();
	});
	
	$("#del_cat_proceed").click(function() {
		if ($("#cat_to_assoc").val()) {
			$("#del_cat_form").submit();
		} else {
			alert("Devi selezionare una categoria a cui associare i documenti!");
		}
	});
	
	$("#del_cat_close").click(function() {
		$("#overlay, .form_del_wrapper").fadeOut();
	});
});
</script>