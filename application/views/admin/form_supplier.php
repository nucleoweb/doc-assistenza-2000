<div class="row space-bot">
	<div class="c12">
		<a href="/admin" class="back">Torna all'homepage</a>
	</div>
</div>
<h1><i class="fa fa-building" aria-hidden="true"></i> Modifica fornitore</h1>
<form class="admin detail_form" action="/admin/edit_supplier" method="POST">
	<input type="hidden" name="id_supplier" value="<?=@$supplier['id']?>">
	<div class="row">
		<div class="c6">
			<label>Categoria di costo</label>
			<select name="id_costcat">
				<option value="">- Seleziona una categoria di costo -</option>
				<?php
				foreach ($costcats as $cat)
					echo '<option value="'.$cat['id'].'" '.($cat['id'] == $supplier['id_costcat'] ? 'selected' : '').'>'.$cat['name'].'</option>';
				?>
			</select>
		</div>
		<div class="c6">
			<label>Centro di costo</label>
			<select name="id_costcenter">
				<option value="">- Seleziona un centro di costo -</option>
				<?php
				foreach ($costcenters as $center)
					echo '<option value="'.$center['id'].'" '.($center['id'] == $supplier['id_costcenter'] ? 'selected' : '').'>'.$center['name'].' - '.$center['location'].'</option>';
				?>
			</select>
		</div>
	</div>
	<fieldset>
		<legend>Denominazione</legend>
		<div class="row">
			<div class="c4">
				<label>Denominazione</label>
				<input type="text" name="denominazione" placeholder="Ragione sociale fornitore" value="<?=@htmlentities($supplier['denominazione'])?>">
			</div>
			<div class="c4">
				<label>Nome</label>
				<input type="text" name="nome" placeholder="Nome fornitore" value="<?=@htmlentities($supplier['nome'])?>">
			</div>
			<div class="c4">
				<label>Cognome</label>
				<input type="text" name="cognome" placeholder="Cognome fornitore" value="<?=@htmlentities($supplier['cognome'])?>">
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>Info fiscali</legend>
		<div class="row">
			<div class="c6">
				<label>P.IVA</label>
				<input type="text" name="piva" placeholder="P.IVA fornitore" value="<?=@$supplier['piva']?>">
			</div>
			<div class="c6">
				<label>Cod. fiscale</label>
				<input type="text" name="codfisc" placeholder="Cod. fiscale fornitore" value="<?=@$supplier['codfisc']?>">
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>Indirizzo</legend>
		<div class="row">
			<div class="c4">
				<label>Indirizzo</label>
				<input type="text" name="indirizzo" placeholder="Indirizzo fornitore" value="<?=@$supplier['indirizzo']?>">
			</div>
			<div class="c4">
				<label>Num. civico</label>
				<input type="text" name="civico" placeholder="Civico fornitore" value="<?=@$supplier['civico']?>">
			</div>
			<div class="c4">
				<label>CAP</label>
				<input type="text" name="cap" placeholder="CAP fornitore" value="<?=@$supplier['cap']?>">
			</div>
		</div>
		<div class="row">
			<div class="c4">
				<label>Comune</label>
				<input type="text" name="comune" placeholder="Comune fornitore" value="<?=@$supplier['comune']?>">
			</div>
			<div class="c4">
				<label>Provincia</label>
				<input type="text" name="prov" maxLength="2" placeholder="Provincia fornitore" value="<?=@$supplier['prov']?>">
			</div>
			<div class="c4">
				<label>Nazione</label>
				<input type="text" name="nazione" placeholder="Nazione fornitore" value="<?=@$supplier['nazione']?>">
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>Contatti</legend>
		<div class="row">
			<div class="c6">
				<label>Telefono</label>
				<input type="text" name="telefono" placeholder="Telefono fornitore" value="<?=@$supplier['telefono']?>">
			</div>
			<div class="c6">
				<label>E-mail</label>
				<input type="text" name="email" placeholder="E-mail fornitore" value="<?=@$supplier['email']?>">
			</div>
		</div>
	</fieldset>
	<br>
	<button type="submit" class="blue">Salva</button>
</form>