<h1><i class="fa fa-money" aria-hidden="true"></i> Gestione centri di costo</h1>
<div class="row">
	<div class="c9">
		<table>
		<tr>
			<th>ID</th>
			<th>Nome</th>
			<th>Località</th>
			<th>Periodo</th>
			<th style="width: 80px;"></th>
		</tr>
		<?php foreach ($costcenters as $costcenter) { ?>
			<tr>
				<td><?=$costcenter['id']?></td>
				<td><?=$costcenter['name']?></td>
				<td><?=strtoupper($costcenter['location'])?></td>
				<td style="text-align: center;"><?=monthNameByNum($costcenter['from_month']).'-'.$costcenter['from_year'].' / '.monthNameByNum($costcenter['to_month']).'-'.$costcenter['to_year']?></td>
				<td class="actions">
					<a href="/admin/edit_costcenter/<?=$costcenter['id']?>"><i class="fa fa-pencil"></i></a>
					<a class="del_costcenter" data-idcostcenter="<?=$costcenter['id']?>" data-namecostcenter="<?=htmlentities($costcenter['name'])?>" href="#"><i class="fa fa-trash"></i></a>
				</td>
			</tr>
		<?php } ?>
		</table>
	</div>
	<div class="c3" style="padding-left: 40px;">
		<h2>Inserisci centro di costo</h2>
		<form class="admin detail_form" action="/admin/insert_costcenter" method="POST">
			<label>ID</label>
			<input type="number" name="id" placeholder="ID centro di costo" value="<?=@$info['id']?>" data-validation="required">
			<label>Nome</label>
			<input type="text" name="name" placeholder="Nome centro di costo" value="<?=@$info['name']?>" data-validation="required">
			<label>Località</label>
			<input type="text" name="location" placeholder="Località centro di costo" value="<?=@$info['location']?>">
			<div class="row" style="padding: 20px; background: #f6f6f6;">
				<div class="c12">
					<label><b>Inizio periodo</b></label><br>
					<select name="from_month" style="width: 45%; float: left; clear: none; margin: 0 10px 0 0;">
						<option value="">- Scegli il mese -</option>
						<?php for ($m = 1; $m <= 12; $m++) { echo '<option value="'.$m.'">'.monthNameByNum($m).'</option>'; } ?>
					</select>
					<select name="from_year" style="width: 45%; float: left; clear: none; margin: 0 10px 0 0;">
						<option value="">- Scegli l'anno -</option>
						<?php for ($y = 2020; $y <= ((int)date('Y')+5); $y++) { echo '<option value="'.$y.'">'.$y.'</option>'; } ?>
					</select>
				</div>
				<div class="c12" style="margin-top: 20px;">
					<label><b>Fine periodo</b></label><br>
					<select name="to_month" style="width: 45%; float: left; clear: none; margin: 0 10px 0 0;">
						<option value="">- Scegli il mese -</option>
						<?php for ($m = 1; $m <= 12; $m++) { echo '<option value="'.$m.'">'.monthNameByNum($m).'</option>'; } ?>
					</select>
					<select name="to_year" style="width: 45%; float: left; clear: none; margin: 0 10px 0 0;">
						<option value="">- Scegli l'anno -</option>
						<?php for ($y = 2020; $y <= ((int)date('Y')+5); $y++) { echo '<option value="'.$y.'">'.$y.'</option>'; } ?>
					</select>
				</div>
			</div>
			<br>
			<button type="submit" class="blue">Salva</button>
		</form>
	</div>
</div>

<div id="overlay"></div>
<div class="form_del_wrapper">
	<form id="del_costcenter_form" class="admin" action="/admin/del_costcenter" method="POST">
		<input name="id_costcenter_to_del" id="id_costcenter_to_del" type="hidden" value="">
		<h4>Stai per cancellare il centro di costo [ <span id="costcenter_to_del"></span> ]</h4>
		<a id="del_costcenter_close" class="btn orange" href="#">Annulla</a>
		<a id="del_costcenter_proceed" class="btn green" href="#" style="float: right;">Elimina centro di costo</a>
	</form>
</div>

<script>
$.validate({
	errorMessagePosition: 'top'
});

$(document).ready(function() {
	$(".del_costcenter").click(function() {
		idcostcenter = $(this).data("idcostcenter");
		namecostcenter = $(this).data("namecostcenter");
		$("#overlay, .form_del_wrapper").fadeIn();
		$("#id_costcenter_to_del").val(idcostcenter);
		$("#costcenter_to_del").html(namecostcenter);
		$("#costcenter_to_assoc option").show();
		$("#costcenter_to_assoc option[value='"+idcostcenter+"']").hide();
	});
	
	$("#del_costcenter_proceed").click(function() {
		// if ($("#costcenter_to_assoc").val()) {
			$("#del_costcenter_form").submit();
		// } else {
			// alert("Devi selezionare un servizio a cui associare gli utenti!");
		// }
	});
	
	$("#del_costcenter_close").click(function() {
		$("#overlay, .form_del_wrapper").fadeOut();
	});
});
</script>