<div class="row space-bot">
	<div class="c12">
		<a href="/admin" class="back">Torna all'homepage</a>
	</div>
</div>
<h1><i class="fa fa-external-link" aria-hidden="true"></i> Esporta bilancio</h1>
<form class="admin detail_form" action="/admin/export_balance" method="POST" enctype="multipart/form-data">
	<label>Selezione l'anno da esportare</label>
	<select name="year">
		<?php for ($y = (date("Y")-3); $y <= (date("Y")+1); $y++) { ?>
			<option value="<?=$y?>"><?=$y?></option>
		<?php } ?>
	</select>
	<br>
	<button type="submit" class="blue">Esporta</button>
</form>