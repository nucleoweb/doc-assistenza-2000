<h1><i class="fa fa-ambulance"></i> Gestione servizi</h1>
<div class="row">
	<div class="c6">
		<table>
		<tr>
			<th>ID</th>
			<th>Nome</th>
			<th>Num. utenti attivi</th>
			<th>Num. utenti totali</th>
			<th></th>
		</tr>
		<?php foreach ($services as $service) { ?>
			<tr>
				<td><?=$service['id']?></td>
				<td><?=$service['name']?></td>
				<td><?=$service['num_users_active']?></td>
				<td><?=$service['num_users']?></td>
				<td class="actions">
					<a href="/admin/edit_service/<?=$service['id']?>"><i class="fa fa-pencil"></i></a>
					<a class="del_service" data-idservice="<?=$service['id']?>" data-nameservice="<?=$service['name']?>" href="#"><i class="fa fa-trash"></i></a>
				</td>
			</tr>
		<?php } ?>
		</table>
	</div>
	<div class="c6" style="padding-left: 40px;">
		<h2>Inserisci nuovo servizio</h2>
		<form class="admin" action="/admin/insert_service" method="POST">
			<input type="text" name="name" placeholder="Nome servizio" value="<?=@$info['name']?>" data-validation="required">
			<br>
			<button type="submit" class="blue">Salva</button>
		</form>
	</div>
</div>

<div id="overlay"></div>
<div class="form_del_wrapper">
	<form id="del_service_form" class="admin" action="/admin/del_service" method="POST">
		<input name="id_service_to_del" id="id_service_to_del" type="hidden" value="">
		<h4>Stai per cancellare il servizio [ <span id="service_to_del"></span> ]</h4>
		<hr>
		Seleziona un'service a cui associare gli utenti (se presenti):
		<br><br>
		<select name="service_to_assoc" id="service_to_assoc">
			<option value="">- Seleziona un servizio -</option>
			<?php foreach ($services as $service) { ?>
				<option value="<?=$service['id']?>"><?=$service['name']?></option>
			<?php } ?>
		</select>
		<br><br>
		<a id="del_service_close" class="btn orange" href="#">Annulla</a>
		<a id="del_service_proceed" class="btn green" href="#" style="float: right;">Elimina servizio</a>
	</form>
</div>

<script>
$.validate({
	errorMessagePosition: 'top'
});

$(document).ready(function() {
	$(".del_service").click(function() {
		idservice = $(this).data("idservice");
		nameservice = $(this).data("nameservice");
		$("#overlay, .form_del_wrapper").fadeIn();
		$("#id_service_to_del").val(idservice);
		$("#service_to_del").html(nameservice);
		$("#service_to_assoc option").show();
		$("#service_to_assoc option[value='"+idservice+"']").hide();
	});
	
	$("#del_service_proceed").click(function() {
		if ($("#service_to_assoc").val()) {
			$("#del_service_form").submit();
		} else {
			alert("Devi selezionare un servizio a cui associare gli utenti!");
		}
	});
	
	$("#del_service_close").click(function() {
		$("#overlay, .form_del_wrapper").fadeOut();
	});
});
</script>