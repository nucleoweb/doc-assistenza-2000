<h1><i class="fa fa-envelope"></i> Invia un messaggio agli utenti</h1>

<form class="admin admin_users_list_form" id="write_users_form" action="/admin/write_to_users" method="POST">
	<div class="row">
		<div class="c6">
			<div id="sms_alertbox"><i class="fa fa-bell"></i> Hai <span><?=@number_format ($sms_left, 0, ",", ".")?></span> SMS rimasti da inviare</div>
		
			<div id="subject_field_wrapper"><input type="text" name="subject" placeholder="Oggetto messaggio" value="" data-validation="required"></div>
			<div id="message_field_wrapper">
				<textarea name="message" placeholder="Testo del messaggio"></textarea>
				<div id="message_char_counter" class="ok"></div>
			</div>
			<br>
			<div id="alert_box">
				<h4><i class="fa fa-bell"></i> Tipologie di invii</h4>
				<input name="alert_mail" type="checkbox" id="alert_mail" checked> <label for="alert_mail">Invia messaggio via e-mail</label>
				<br><br>
				<input name="alert_sms" type="checkbox" id="alert_sms" checked> <label for="alert_sms">Invia messaggio via sms<br><span style="font-size: 12px;">(L'oggetto verrà visualizzato come incipit dell'sms)</span></label>
			</div>
			<button type="submit" class="blue">Invia</button>
		</div>
		<div class="c6">
			<?php $this->load->view('admin/users_check_list.php', ['polls_list' => 'yes']); ?>
		</div>
	</div>
</form>

<script>
var charLimit = 900;	// 900 ok 

function checkCharNumber() {
	subject_ln = $("input[name='subject']").val().length;
	message_ln = $("textarea[name='message']").val().length;
	total_ln = subject_ln + message_ln;
	rest = charLimit - total_ln;
	$("#message_char_counter").html(rest);
	if (rest >= 0)
		$("#message_char_counter").removeClass("ko").addClass("ok");
	else
		$("#message_char_counter").removeClass("ok").addClass("ko");
}

$(function() {
	$("#message_char_counter").html(charLimit);
});

$(document).ready(function() {
	$("input[name='subject'], textarea[name='message']").keyup(function() {
		checkCharNumber();
	});
});

$.validate({
	errorMessagePosition: 'top'
});
</script>