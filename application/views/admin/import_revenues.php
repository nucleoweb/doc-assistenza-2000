<div class="row space-bot">
	<div class="c12">
		<a href="/admin" class="back">Torna all'homepage</a>
	</div>
</div>
<h1><i class="fa fa-upload" aria-hidden="true"></i> Importa ricavi</h1>
<form class="admin" action="/admin/import_revenues" method="POST" enctype="multipart/form-data">
	<input type="file" name="package">
	<br>
	<button type="submit" class="blue">Importa</button>
</form>