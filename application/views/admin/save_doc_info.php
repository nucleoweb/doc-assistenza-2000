<html>
    <head>
        <style>
            @font-face {
                font-family: 'Verdana';
                font-weight: normal;
                font-style: normal;
                font-variant: normal;
                src: url("/assets/fonts/Verdana.ttf") format("truetype");
            }
            body, div, .header {
                font-family: Verdana, sans-serif;
                font-size: 8px;
            }
            .row { clear: both; width: 100%; }
            .row > div { border: 1px solid #aaa; padding: 2px 0.5%; }
            .c05 { float: left; width: 3.16%; }     /* 4.16 */
            .c1 { float: left; width: 7.33%; }      /* 8.33 */
            .c15 { float: left; width: 11.49%; }    /* 12.49 */
            .c2 { float: left; width: 15.66%; }     /* 16.66 */
            .c3 { float: left; width: 24%; }        /* 25 */
            .c35 { float: left; width: 28.16%; }    /* 29.16 */
            .c4 { float: left; width: 32.33%; }     /* 33.33 */
            .c6 { float: left; width: 49%; }        /* 50 */
            .c8 { float: left; width: 65.66%; }     /* 66.66 */
            .label { background: #eee; }
            .header { border: 0 !important; text-align: center; }
            .space { border: 0 !important; }
        </style>
    </head>
    <body>
        <div class="row">
            <div class="c4 label"><b>ID</b></div>
            <div class="c8"><?=$docinfo['id_doc']?></div>
        </div>
        <div class="row">
            <div class="c4 label"><b>Nome</b></div>
            <div class="c8"><?=$docinfo['name']?></div>
        </div>
        <div class="row">
            <div class="c4 label"><b>Nome file</b></div>
            <div class="c8"><?=$docinfo['filename']?></div>
        </div>
        <div class="row">
            <div class="c4 label"><b>Categoria</b></div>
            <div class="c8"><?=$docinfo['cat']?></div>
        </div>
        <div class="row">
            <div class="c4 label"><b>Data caricamento</b></div>
            <div class="c8"><?=implode("/", array_reverse(explode("-", substr($docinfo['date_upload'], 0, 10)))).' '.substr($docinfo['date_upload'], 11, 5)?></div>
        </div>
        <div class="row">
            <div class="c4 label"><b>Note</b></div>
            <div class="c8"><?=$docinfo['note'] ? $docinfo['note'] : '-'?></div>
        </div>
        <div class="row">
            <div class="c4 label"><b>Num. utenti</b></div>
            <div class="c8"><?=count($docinfo['users'])?></div>
        </div>
        <div class="row">
            <div class="c05 header"><b>ID</b></div>
            <div class="c35 header"><b>Nome</b></div>
            <div class="c15 header"><b>Download</b></div>
            <div class="c1 header space"></div>
            <div class="c05 header"><b>ID</b></div>
            <div class="c35 header"><b>Nome</b></div>
            <div class="c15 header"><b>Download</b></div>
        </div>
        <?php
        $c = 0;
        foreach ($docinfo['users'] as $id => $user) { 
            ?>
            <?=(($c % 2) == 0 ? '<div class="row">' : '')?>
                <div class="c05"><?=$id?></div>
                <div class="c35"><?=$user['last_name'].' '.$user['first_name']?></div>
                <div class="c15"><?=($user['last_download'] != "" ? implode("/", array_reverse(explode("-", substr($user['last_download'], 0, 10)))).' '.substr($user['last_download'], 11, 5) : '-')?></div>
                <?php $c++; ?>
            <?=(($c % 2) == 0 ? '</div>' : '<div class="c1 space"></div>')?>
            <?php
        }
        ?>
        <?=(($c % 2) != 0 ? '</div>' : '')?>
    </body>
</html>