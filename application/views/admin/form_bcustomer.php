<div class="row space-bot">
	<div class="c12">
		<a href="/admin" class="back home">Torna all'homepage</a>
		<a href="/admin/view_bcustomers" class="back">Torna ai clienti</a>
	</div>
</div>
<h1><i class="fa fa-building" aria-hidden="true"></i> Modifica cliente</h1>
<form class="admin detail_form" action="/admin/edit_bcustomer" method="POST">
	<input type="hidden" name="id_customer" value="<?=@$customer['id']?>">
	<label>Centro di costo</label>
	<select name="id_costcenter">
		<option value="">- Seleziona un centro di costo -</option>
		<?php
		foreach ($costcenters as $center)
			echo '<option value="'.$center['id'].'" '.($center['id'] == $customer['id_costcenter'] ? 'selected' : '').'>'.$center['name'].' - '.$center['location'].'</option>';
		?>
	</select>
	<fieldset>
		<legend>Denominazione</legend>
		<div class="row">
			<div class="c4">
				<label>Denominazione</label>
				<input type="text" name="denominazione" placeholder="Ragione sociale fornitore" value="<?=@htmlentities($customer['denominazione'])?>">
			</div>
			<div class="c4">
				<label>Nome</label>
				<input type="text" name="nome" placeholder="Nome fornitore" value="<?=@htmlentities($customer['nome'])?>">
			</div>
			<div class="c4">
				<label>Cognome</label>
				<input type="text" name="cognome" placeholder="Cognome fornitore" value="<?=@htmlentities($customer['cognome'])?>">
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>Info fiscali</legend>
		<div class="row">
			<div class="c6">
				<label>P.IVA</label>
				<input type="text" name="piva" placeholder="P.IVA fornitore" value="<?=@$customer['piva']?>">
			</div>
			<div class="c6">
				<label>Cod. fiscale</label>
				<input type="text" name="codfisc" placeholder="Cod. fiscale fornitore" value="<?=@$customer['codfisc']?>">
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>Indirizzo</legend>
		<div class="row">
			<div class="c4">
				<label>Indirizzo</label>
				<input type="text" name="indirizzo" placeholder="Indirizzo fornitore" value="<?=@$customer['indirizzo']?>">
			</div>
			<div class="c4">
				<label>Num. civico</label>
				<input type="text" name="civico" placeholder="Civico fornitore" value="<?=@$customer['civico']?>">
			</div>
			<div class="c4">
				<label>CAP</label>
				<input type="text" name="cap" placeholder="CAP fornitore" value="<?=@$customer['cap']?>">
			</div>
		</div>
		<div class="row">
			<div class="c4">
				<label>Comune</label>
				<input type="text" name="comune" placeholder="Comune fornitore" value="<?=@$customer['comune']?>">
			</div>
			<div class="c4">
				<label>Provincia</label>
				<input type="text" name="prov" maxLength="2" placeholder="Provincia fornitore" value="<?=@$customer['prov']?>">
			</div>
			<div class="c4">
				<label>Nazione</label>
				<input type="text" name="nazione" placeholder="Nazione fornitore" value="<?=@$customer['nazione']?>">
			</div>
		</div>
	</fieldset>
	<fieldset>
		<legend>Contatti</legend>
		<div class="row">
			<div class="c6">
				<label>Telefono</label>
				<input type="text" name="telefono" placeholder="Telefono fornitore" value="<?=@$customer['telefono']?>">
			</div>
			<div class="c6">
				<label>E-mail</label>
				<input type="text" name="email" placeholder="E-mail fornitore" value="<?=@$customer['email']?>">
			</div>
		</div>
	</fieldset>
	<br>
	<button type="submit" class="blue">Salva</button>
</form>