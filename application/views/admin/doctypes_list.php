<h1><i class="fa fa-file-text" aria-hidden="true"></i> Gestione tipi documento</h1>
<div class="row">
	<div class="c9">
		<table>
		<tr>
			<th>ID</th>
			<th>Codice</th>
			<th>Descrizione</th>
			<th>Tipo</th>
			<th style="width: 50px;"></th>
		</tr>
		<?php foreach ($doctypes as $doctype) { ?>
			<tr>
				<td><?=$doctype['id']?></td>
				<td style="font-weight: 600;"><?=strtoupper($doctype['cod'])?></td>
				<td><?=$doctype['description']?></td>
				<td style="font-weight: 600; color: <?=($doctype['type'] == 'plus' ? 'green' : '#d90000')?>;"><?=$doctype['type']?></td>
				<td class="actions">
					<a href="/admin/edit_doctype/<?=$doctype['id']?>"><i class="fa fa-pencil"></i></a>
					<!-- <a class="del_doctype" data-iddoctype="<?=$doctype['id']?>" data-namedoctype="<?=htmlentities($doctype['name'])?>" href="#"><i class="fa fa-trash"></i></a> -->
				</td>
			</tr>
		<?php } ?>
		</table>
	</div>
	<div class="c3" style="padding-left: 40px;">
		<!-- <h2>Inserisci centro di costo</h2>
		<form class="admin detail_form" action="/admin/insert_doctype" method="POST">
			<label>ID</label>
			<input type="number" name="id" placeholder="ID centro di costo" value="<?=@$info['id']?>" data-validation="required">
			<label>Nome</label>
			<input type="text" name="name" placeholder="Nome centro di costo" value="<?=@$info['name']?>" data-validation="required">
			<label>Località</label>
			<input type="text" name="location" placeholder="Località centro di costo" value="<?=@$info['location']?>">
			<br>
			<button type="submit" class="blue">Salva</button>
		</form> -->
	</div>
</div>

<div id="overlay"></div>
<div class="form_del_wrapper">
	<form id="del_doctype_form" class="admin" action="/admin/del_doctype" method="POST">
		<input name="id_doctype_to_del" id="id_doctype_to_del" type="hidden" value="">
		<h4>Stai per cancellare il centro di costo [ <span id="doctype_to_del"></span> ]</h4>
		<a id="del_doctype_close" class="btn orange" href="#">Annulla</a>
		<a id="del_doctype_proceed" class="btn green" href="#" style="float: right;">Elimina centro di costo</a>
	</form>
</div>

<script>
$.validate({
	errorMessagePosition: 'top'
});

$(document).ready(function() {
	$(".del_doctype").click(function() {
		iddoctype = $(this).data("iddoctype");
		namedoctype = $(this).data("namedoctype");
		$("#overlay, .form_del_wrapper").fadeIn();
		$("#id_doctype_to_del").val(iddoctype);
		$("#doctype_to_del").html(namedoctype);
		$("#doctype_to_assoc option").show();
		$("#doctype_to_assoc option[value='"+iddoctype+"']").hide();
	});
	
	$("#del_doctype_proceed").click(function() {
		// if ($("#doctype_to_assoc").val()) {
			$("#del_doctype_form").submit();
		// } else {
			// alert("Devi selezionare un servizio a cui associare gli utenti!");
		// }
	});
	
	$("#del_doctype_close").click(function() {
		$("#overlay, .form_del_wrapper").fadeOut();
	});
});
</script>