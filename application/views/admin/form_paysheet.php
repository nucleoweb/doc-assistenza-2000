<h1><i class="fa fa-upload"></i> Carica buste paga</h1>
<form class="admin admin_users_list_form" id="edit_poll_form" action="/admin/upload_paysheets" method="POST" enctype="multipart/form-data">
	<div class="row">
		<div class="c12">
			<input type="file" name="pdf_paysheets">
            <input type="hidden" name="upload" value="1">
			<button type="submit" class="blue">Carica</button>
		</div>
	</div>
</form>