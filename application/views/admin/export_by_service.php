<h1><i class="fa fa-external-link" aria-hidden="true"></i> Esporta utenti per servizio</h1>
<h3>Seleziona i servizi da esportare</h3>

<form action="" method="POST">
    <div class="row">
        <div class="c12">
            <ul class="check_list">
            <?php foreach ($services as $serv) { ?>
                <li>
                    <label for="service_<?=$serv['id']?>">
                        <input type="checkbox" name="service_list[]" value="<?=$serv['id']?>|<?=$serv['name']?>" id="service_<?=$serv['id']?>"> <?=$serv['name']?> 
                    </label>
                    (<?=$serv['num_users']?> utenti)
                </li>
            <?php } ?>
            </ul>
        </div>
    </div>
    <div class="row space-top" id="only_active_check_row">
        <div class="c12">            
            <label for="only_hired_check"><input type="checkbox" name="only_hired" value="1" id="only_hired_check"> Esporta solo utenti attualmente in forza</label>
        </div>
        <div class="c12 space-top">            
            <label for="only_active_check"><input type="checkbox" name="only_active" value="1" id="only_active_check"> Esporta solo utenti attivi</label>
        </div>
    </div>
    <div class="row space-top">
		<div class="c12">
			<button type="submit" class="blue">Esporta</button>
        </div>
    </div>
</form>
