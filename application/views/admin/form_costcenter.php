<div class="row space-bot">
	<div class="c12">
		<a href="/admin" class="back">Torna all'homepage</a>
	</div>
</div>
<h1><i class="fa fa-money" aria-hidden="true"></i> Modifica centro di costo</h1>
<form class="admin detail_form" action="/admin/edit_costcenter" method="POST">
	<input type="hidden" name="id_costcenter" value="<?=@$costcenter['id']?>">
	<label>ID</label>
	<input type="number" name="new_id_costcenter" value="<?=@$costcenter['id']?>">
	<label>Nome</label>
	<input type="text" name="name" placeholder="Nome centro di costo" value="<?=@htmlentities($costcenter['name'])?>" data-validation="required">
	<label>Località</label>
	<input type="text" name="location" placeholder="Località centro di costo" value="<?=@$costcenter['location']?>">
	<div class="row" style="padding: 20px; background: #f6f6f6;">
		<div class="c6 first">
			<label><b>Inizio periodo</b></label><br><br>
			<select name="from_month" style="width: 45%; float: left; clear: none; margin: 0 10px 0 0;"><?php for ($m = 1; $m <= 12; $m++) { echo '<option value="'.$m.'" '.($m == $costcenter['from_month'] ? 'selected' : '').'>'.monthNameByNum($m).'</option>'; } ?></select>
			<select name="from_year" style="width: 45%; float: left; clear: none; margin: 0 10px 0 0;"><?php for ($y = 2020; $y <= ((int)date('Y')+6); $y++) { echo '<option value="'.$y.'" '.($y == $costcenter['from_year'] ? 'selected' : '').'>'.$y.'</option>'; } ?></select>
		</div>
		<div class="c6 last">
			<label><b>Fine periodo</b></label><br><br>
			<select name="to_month" style="width: 45%; float: left; clear: none; margin: 0 10px 0 0;"><?php for ($m = 1; $m <= 12; $m++) { echo '<option value="'.$m.'" '.($m == $costcenter['to_month'] ? 'selected' : '').'>'.monthNameByNum($m).'</option>'; } ?></select>
			<select name="to_year" style="width: 45%; float: left; clear: none; margin: 0 10px 0 0;"><?php for ($y = 2020; $y <= ((int)date('Y')+6); $y++) { echo '<option value="'.$y.'" '.($y == $costcenter['to_year'] ? 'selected' : '').'>'.$y.'</option>'; } ?></select>
		</div>
	</div>
	<br>
	<button type="submit" class="blue">Salva</button>
</form>