<div class="row space-bot">
	<div class="c12">
		<a href="/profile" class="back">Torna al tuo profilo</a>
	</div>
</div>

<?php
$file_parts = pathinfo($doc_info['filename']);
switch ($file_parts['extension']) {
	case "doc": 		$ext = "doc";		break;
	case "docx": 		$ext = "doc"; 	break;
	case "odt": 		$ext = "doc"; 	break;
	case "pdf": 		$ext = "pdf";		break;
	case "zip": 		$ext = "zip";		break;
	case "7z": 		$ext = "zip";		break;
	case "rar": 		$ext = "zip";		break;
	case "ppt": 		$ext = "ppt";		break;
	case "txt": 		$ext = "txt";		break;
	case "xls": 		$ext = "xls";		break;
	case "xlsx": 		$ext = "xls";		break;
	case "csv": 		$ext = "xls";		break;
	default: 			$ext = "boh";		break;
}
?>

<h1><img src="/assets/gfx/filetypes/<?=$ext?>.png"> </i> Scarica documento</h1>
<div class="row">
	<div class="c6">
		<table class="info">
			<tr><td><b>Nome documento</b></td><td><?=$doc_info['name']?></td></tr>
			<tr><td><b>Nome file</b></td><td><?=$doc_info['filename']?></td></tr>
			<tr><td><b>Dimensione</b></td><td><?=@round(filesize('./assets/docs/'.$doc_info['filename'])/1024)?> KB</td></tr>
			<tr><td><b>Data caricamento</b></td><td><?=date("d/m/Y", strtotime($doc_info['date_upload']))?></td></tr>
			<tr><td><b>Note</b></td><td><?=((!strpos($doc_info['note'], "FTP")) ? $doc_info['note'] : '')?></td></tr>
			<?php if (!empty($doc_info['users'][$id_user]['last_download'])) { ?>
				<tr class="evidence"><td>Hai già scaricato questo file</td><td> Ultimo download il <?=date("d/m/Y H:i", strtotime($doc_info['users'][$id_user]['last_download']))?></td></tr>
				<?php if (count($doc_info['users'][$id_user]['list_downloads'])) { ?>
				<tr class="evidence"><td>Num. complessivo di download *</td><td><?=count($doc_info['users'][$id_user]['list_downloads'])?> &nbsp; (<a href="#" id="view_downloads_list">Clicca per vedere l'elenco</a>)</td></tr>
				<?php } ?>
			<?php } ?>
		</table>
		<?php if (count($doc_info['users'][$id_user]['list_downloads'])) { ?>
			<span id="info_span">* il numero di download potrebbe non essere corretto sui dati anteriori a febbraio 2020</span>
		<?php } ?>
		<div id="downloads_list">
			<p>Hai scaricato questo file in queste date:</p>
			<ul>
				<?php 
				foreach ($doc_info['users'][$id_user]['list_downloads'] as $dwn_item) {
					echo '<li>'.date("d/m/Y H:i", strtotime($dwn_item['date_download'])).'</li>';
				}
				?>
			</ul>
		</div>
	</div>
	<div class="c6">
		<div class="infobox alert">
			I download dei file vengono tracciati e salvati nel sistema per assicurarci che gli utenti accedano correttamente alle informazioni pubblicate. Potrai proseguire con il download del documento solo una volta messa la spunta seguente.
			<br><br>
			<form action="" method="POST" id="download_form">
				<input type="checkbox" name="acceptance" id="doc_acceptance"> <label for="doc_acceptance">Confermo di aver preso visione di questo documento</label>
				<br><br>
				<a href="#" class="btn" style="background: #fff;" id="download_btn">Scarica il documento</a>
			</form>
		</div>
	</div>
</div>

<script>
$(document).ready(function() {
	$("#doc_acceptance").click(function() {
		if ($("#doc_acceptance").is(':checked'))
			$("div.infobox.alert a.btn").fadeIn();
		else
			$("div.infobox.alert a.btn").hide();
	});
	
	$("#download_btn").click(function() {
		if ($("#doc_acceptance").is(':checked'))
			$("#download_form").submit();
	});
	
	$("#view_downloads_list").click(function() {
		$("#downloads_list").fadeIn();
	});
});
</script>