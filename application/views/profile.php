<h1>Benvenuto nella tua area utente</h1>
<?php if (!isset($alert)) { ?>
<div class="row space-top">
	<div class="c12">
		<h4><i class="fa fa-question-circle"></i> Sondaggi</h4>
		<?php
		$show_polls = false;
		if (isset($polls) AND is_array($polls) AND count($polls))
			foreach ($polls as $poll)
				if ($poll['date_end'] >= date("Y-m-d"))
					$show_polls = true;

		if (!$show_polls) {
			echo '<p>Non sono presenti sondaggi al momento</p><hr>';
		} else {
			// echo '<pre>';
			// print_r($polls);
			// echo '</pre>';
			?>
			<div id="profile_polls_wrapper">
				<table id="profile_polls">
					<tr>
						<th>Titolo</th>
						<th>Data inizio</th>
						<th>Data fine</th>
						<th>Stato</th>
						<th>Votazione</th>
					</tr>
					<?php
					foreach ($polls as $p) {
						$date_vote = null;
						if (date("Y-m-d") <= $p['date_end']) {
							foreach ($p['users'] as $id_user => $usr_data)
								if ($id_user == $info['id_user'])
									$date_vote = $usr_data['date_vote'];
							?>
							<tr>
								<td><?=$p['title']?></td>
								<td style="width: 120px; text-align: center;"><?=implode("/", array_reverse(explode("-", $p['date_start'])))?></td>
								<td style="width: 120px; text-align: center;"><?=implode("/", array_reverse(explode("-", $p['date_end'])))?></td>
								<td style="width: 160px; text-align: center; font-weight: bold;">
									<?php
									$stato = null;
									$state_color = '#333';
									if (date("Y-m-d") < $p['date_start']) {
										$stato = 'Programmato';
										$state_color = '#0085C6';
									}
									elseif ($p['date_start'] <= date("Y-m-d") AND date("Y-m-d") <= $p['date_end']) {
										$stato = 'Aperto';
										$state_color = '#51A351';
									}
									else {
										$stato = 'Chiuso';
										$state_color = '#D90000';
									}
									echo '<span style="color: '.$state_color.';">'.$stato.'</span>';
									?>
								</td>
								<td class="actions" style="width: 200px; text-align: center;">
									<?php 
									if (date("Y-m-d") >= $p['date_start']) {
										if (!$date_vote) {
											?><a href="/vote/<?=$p['id']?>" title="Modifica sondaggio" style="background: #51A351; color: #fff; padding: 5px 10px;"><i class="fa fa-check-square-o" style="color: #fff;"></i> Vota</a><?php
										} else
											echo "Hai votato";
									} else
										echo "Non puoi ancora votare";
									?>
								</td>
							</tr>
							<?php 
						}
					}
					?>
				</table>
			</div>
			<?php
		}
		?>
	</div>
</div>
<br class="clear">
<?php } ?>

<div class="row">
	<div class="c4 profile_col" id="profile_info_col">
		<?php 
		if (isset($alert)) {
			if (isset($alert['psw_old'])) {
				echo '<p class="infobox alert" style="background: #ffbf00; font-size: 18px; line-height: 24px;">Devi cambiare la tua password per visualizzare le tue informazioni</p>';
			}
		} else {
			?>
			<h4><i class="fa fa-user"></i> Il tuo profilo</h4>
			<table class="info">
				<tr><td class="label"><i class="fa fa-key"></i> Il tuo ID</td><td><?=$info['id_user']?></td></tr>
				<tr><td class="label"><i class="fa fa-user-circle-o"></i> Nome</td><td><?=$info['first_name']?></td></tr>
				<tr><td class="label"><i class="fa fa-user-circle-o"></i> Cognome</td><td><?=$info['last_name']?></td></tr>
				<tr><td class="label"><i class="fa fa-road"></i> Indirizzo</td><td><?=$info['address']?></td></tr>
				<tr><td class="label"><i class="fa fa-building"></i> Città</td><td><?=$info['city']?></td></tr>
				<tr><td class="label"><i class="fa fa-briefcase"></i> Azienda</td><td><?=$info['company']?></td></tr>
				<tr><td class="label"><i class="fa fa-calendar-o"></i> Età</td><td><?=$info['age']?></td></tr>
				<tr><td class="label"><i class="fa fa-envelope"></i> Indirizzo e-mail</td><td><?=$info['email']?></td></tr>
				<tr><td class="label"><i class="fa fa-phone"></i> Recapito telefonico</td><td><?=$info['telephone']?></td></tr>
				<tr><td class="label"><i class="fa fa-code"></i> Codice fiscale</td><td><?=$info['fiscal_code']?></td></tr>
			</table>
			<?php
		}
		?>
		<br><br>
		<a style="float: left; margin-right: 10px;" href="/change_user_access" class="btn blue">Modifica dati d'accesso</a>
	</div>
	<div class="c8 profile_col" id="profile_docs_col">
		<?php if (!isset($alert)) { ?>
			<h4 style="color: green;"><i class="fa fa-file-text-o"></i> Documenti in arrivo</h4>
			<ul class="list" id="docs_list">
				<?php
				$all_docs = [];

				if (count($docs))
					foreach ($docs as $cat => $ar_docs)
						$all_docs = array_merge($all_docs, $ar_docs);

				if (count($all_docs)) {
					foreach ($all_docs as $key => $row)
						$date_upload[$key] = $row['date_upload'];
					array_multisort($date_upload, SORT_DESC, $all_docs);
				}

				// echo '<pre>';
				// print_r($all_docs);
				// echo '</pre>';
				// die();
				
				if (count($all_docs)) {
					foreach ($all_docs as $d) {
						if (!$d['downloaded'] AND !$d['date_download']) {
							$cat = ucfirst(strtolower(str_replace("_", " ", ($d['cat']))));
							echo '<li class="unread">
									<b><a class="name" title="'.$d['filename'].'" href="/download/'.str_replace('=', null, base64_encode($d['id_doc'].'_'.time())).'" style="font-size: 15px;">'.ucfirst(str_replace("_", " ",$d['name'])).'</a></b>
									<i class="fa fa-tag"></i> Categoria: <span class="date">'.((strlen($cat) <= 16) ? $cat : substr($cat, 0, 16)."..").'</span><br>
									<i class="fa fa-upload"></i> Upload: <span class="date">'.date("d/m/Y", strtotime($d['date_upload'])).'</span><br>
								</li>';
						}
					}
				} else {
					echo '<p style="margin-bottom: 60px;">Non sono presenti nuovi documenti da visualizzare</p>';
				}				
				?>
			</ul>
			<br class="clear"><br>
			
			<h4><i class="fa fa-folder-open"></i> Archivio documenti</h4>
			<ul id="folder-list">
			<?php			
			if (count($docs))
				foreach ($docs as $cat => $ar_docs) {
					?><li data-cat_name="<?=str_replace(" ", "_", strtolower($cat))?>"><h5><?=ucfirst(strtolower($cat))?></h5></li><?php
				}
			?>
			</ul>
			<?php
			if (count($docs)) {
				foreach ($docs as $cat => $ar_docs) {
					?>
					<div class="doc_folder" id="doc_folder_<?=str_replace(" ", "_", strtolower($cat))?>">
						<?php
						if (count($docs[$cat])) {
							?>
							<div class="docs_list_exp" id="docs_list_<?=str_replace(" ", "_", strtolower($cat))?>">
								<form class="admin"><input type="text" class="search" placeholder="Cerca documento"></form>
								<ul class="list">
									<?php
									foreach ($docs[$cat] as $d) {
										$cat = ucfirst(strtolower(str_replace("_", " ", ($d['cat']))));
										if ($d['downloaded'] OR $d['date_download']) {
											echo '<li>
														<b><a class="name" title="'.$d['filename'].'" href="/download/'.str_replace('=', null, base64_encode($d['id_doc'].'_'.time())).'">'.ucfirst(str_replace("_", " ",$d['name'])).'</a></b>
														<i class="fa fa-tag"></i> Categoria: <span class="date">'.((strlen($cat) <= 16) ? $cat : substr($cat, 0, 16)."..").'</span><br>
														<i class="fa fa-upload"></i> Upload: <span class="date">'.date("d/m/Y", strtotime($d['date_upload'])).'</span><br>
													</li>';
										}
									}
									?>
								</ul>
							</div>
							<?php
						} else {
							?><p style="margin-bottom: 20px; padding-bottom: 20px; border-bottom: 1px solid #ccc;">Non sono presenti documenti per questa categoria</p><?php
						}
						?>
					</div>
					<?php
				}
			}
			?>
			
			<?php if (0) { ?>
					<?php if (count($docs)) { ?>
						<?php foreach ($docs as $cat => $ar_docs) { ?>
							<?php if (count($ar_docs)) { // se la categoria non ha documenti, non la faccio proprio vedere ?>
									<h5><?=$cat?></h5>
									<?php if (count($docs[$cat])) { ?>
										<div id="docs_list_<?=str_replace(" ", "_", strtoupper($cat))?>">
											<form class="admin"><input type="text" class="search" placeholder="Cerca documento"></form>
											<ul class="list" style="margin-bottom: 20px; padding-bottom: 20px; border-bottom: 1px solid #ccc;">
												<?php
												foreach ($docs[$cat] as $d) {
													if ($d['downloaded'] OR $d['date_download']) {
														echo '<li>
																	<b><a class="name" href="/download/'.str_replace('=', null, base64_encode($d['id_doc'].'_'.time())).'">'.$d['name'].'</a></b>
																	[ <span class="date">'.date("d/m/Y", strtotime($d['date_upload'])).'</span> ]
																</li>';
													}
												}
												?>
											</ul>
										</div>
									<?php	} else {	?>
										<p style="margin-bottom: 20px; padding-bottom: 20px; border-bottom: 1px solid #ccc;">Non sono presenti documenti per questa categoria</p>
									<?php	} ?>
							<?php	} ?>
						<?php } ?>
					<?php } else {	?>
						<p>Non sono presenti documenti associati al tuo account</p>
					<?php } ?>
			<?php } ?>
		<?php } ?>
	</div>
</div>

<script>
var options = {
    valueNames: ['name', 'date', 'filename']
};
<?php 
foreach ($docs as $cat => $ar_docs) {
	?>
	var docsList = new List('docs_list_<?=str_replace(" ", "_", strtolower($cat))?>', options);
	<?php
}
?>

$(document).ready(function() {
	$("#folder-list li").click(function() {
		cat_name = $(this).data("cat_name");
		$("#folder-list li").removeClass("active");
		$(this).addClass("active");
		$(".doc_folder").hide();
		$("#doc_folder_"+cat_name).fadeIn();
	});
});

$(function() {
	$("#folder-list li:first-of-type").addClass('active');
	$(".doc_folder:first-of-type").fadeIn();
	
	$(".profile_col").matchHeight();
});
</script>