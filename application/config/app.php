<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// $config['doc_cats'] = array(
	// 'BUSTE_PAGA' => 'Buste paga',
	// 'CIRCOLARI' => 'Circolari',
	// 'CUD' => 'CUD',
	// 'ATTESTATI_FORMAZIONE' => 'Attestati di formazione',
	// 'DOCUMENTI_FISCALI' => 'Altri documenti fiscali'
// );
$config['month_names'] = array(
	1 => 'Gennaio',
	2 => 'Febbraio',
	3 => 'Marzo',
	4 => 'Aprile',
	5 => 'Maggio',
	6 => 'Giugno',
	7 => 'Luglio',
	8 => 'Agosto',
	9 => 'Settembre',
	10 => 'Ottobre',
	11 => 'Novembre',
	12 => 'Dicembre',
);