<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Suppliercosts_model extends CI_Model {

	const T_SUPPLIERCOSTS = 'b_suppliercosts';
	const T_SUPPLIERCOSTS_DETAILS = 'b_suppliercosts_details';
	const T_SUPPLIERS = 'b_suppliers';
	const T_DOCTYPES = 'b_doctypes';
	
	function __construct() {
		parent::__construct();
	}
	
	public function get_suppliercosts($default_data = NULL, $filter = null) {
		$query = $this->db->select(self::T_SUPPLIERS.'.denominazione, '.self::T_SUPPLIERCOSTS.'.*')
						  ->from(self::T_SUPPLIERCOSTS)
						  ->join(self::T_SUPPLIERS, self::T_SUPPLIERCOSTS.'.id_supplier = '.self::T_SUPPLIERS.'.id')
						  ->join(self::T_SUPPLIERCOSTS_DETAILS, self::T_SUPPLIERCOSTS.'.id = '.self::T_SUPPLIERCOSTS_DETAILS.'.id_suppliercost');
		# FILTRI - START
		// if (!$filter)
		// 	$filter = [
		// 		'filter_from' => date('Y-m').'-01',
		// 		'filter_to' => date('Y-m-d')
		// 	];

		// if (!empty($filter['filter_from']))
		// 	$query->where("data >= '".$filter['filter_from']."'");
		// if (!empty($filter['filter_to']))
		// 	$query->where("data <= '".$filter['filter_to']."'");
		if (!empty($filter['filter_numdoc'])) {
			$query->where(self::T_SUPPLIERCOSTS.".numero = '".$filter['filter_numdoc']."'");
		}
		if (!empty($filter['filter_datadoc'])) {
			$query->where(self::T_SUPPLIERCOSTS.".data = '".$filter['filter_datadoc']."'");
		}
		if (!empty($filter['filter_period_month'])) {
			$query->where(self::T_SUPPLIERCOSTS_DETAILS.'.period_month = '.$filter['filter_period_month']);
		}
		if (!empty($filter['filter_period_year'])) {
			$query->where(self::T_SUPPLIERCOSTS_DETAILS.'.period_year = '.$filter['filter_period_year']);
		}
		if (!empty($filter['filter_costcenter'])) {
			$query->where(self::T_SUPPLIERCOSTS_DETAILS.'.id_costcenter = '.$filter['filter_costcenter']);
		}
		if (!empty($filter['filter_name'])) {
			$query->where(self::T_SUPPLIERS.".denominazione LIKE '%".addslashes($filter['filter_name'])."%' 
			OR ".self::T_SUPPLIERS.".nome LIKE '%".addslashes($filter['filter_name'])."%'
			OR ".self::T_SUPPLIERS.".cognome LIKE '%".addslashes($filter['filter_name'])."%'");
		}
		if (!empty($filter['filter_date'])) {
			$query->where(self::T_SUPPLIERCOSTS.".created_at >= '".$filter['filter_date']." 00:00:00"."'");
			$query->where(self::T_SUPPLIERCOSTS.".created_at <= '".$filter['filter_date']." 23:59:59"."'");
		}
		# FILTRI - END
		$suppliercosts = $query->group_by(self::T_SUPPLIERCOSTS_DETAILS.'.id_suppliercost')->order_by('data', 'ASC')->order_by('created_at', 'ASC')->get()->result_array();

		// echo '<pre>'.$this->db->last_query().'</pre>';
		// die();

		foreach ($suppliercosts as $k => $cost) {
			if ($cost['id_supplier']) {
				$supplier = $this->suppliers_model->get_supplier($cost['id_supplier']);
				$suppliercosts[$k]['fornitore'] = $supplier['denominazione'] ? $supplier['denominazione'] : $supplier['nome'].' '.$supplier['cognome']; 
			}
			$suppliercosts[$k]['detail'] = $this->db->from(self::T_SUPPLIERCOSTS_DETAILS)->where('id_suppliercost = '.$cost['id'])->get()->result_array();
			$suppliercosts[$k]['doctype'] = $this->db->from(self::T_DOCTYPES)->where("cod = '".trim($cost['tipo_documento']."'"))->get()->row_array();
		}
		return $suppliercosts;
	}
	
	public function get_suppliercost($id_suppliercost = NULL, $numero = NULL, $id_supplier = NULL, $data = NULL, $importo_totale = NULL) {
		if ($id_suppliercost)
			$res = $this->db->where('id = '.$id_suppliercost)->get(self::T_SUPPLIERCOSTS)->row_array();
		elseif ($numero OR $id_supplier OR $data OR $importo_totale) {
			if ($numero)
				$tmp_res = $this->db->where("numero = '".$numero."'");
			if ($id_supplier)
				$tmp_res = $this->db->where("id_supplier = ".$id_supplier);
			if ($data)
				$tmp_res = $this->db->where("data = '".substr($data, 0, 10)."'");
			if ($importo_totale)
				$tmp_res = $this->db->where("importo_totale = ".$importo_totale);
			if ($tmp_res)
				$res = $tmp_res->get(self::T_SUPPLIERCOSTS)->row_array();
		}
		// echo '<pre style="background: #eee; padding: 10px;">'.$this->db->last_query().'</pre>';
		if ($res) {
			$res['detail'] = $this->db->from(self::T_SUPPLIERCOSTS_DETAILS)->where('id_suppliercost = '.$res['id'])->get()->result_array();
		}
		return $res;
	}
	
	public function insert_suppliercost($posted) {
		// Inserisco il costo fornitore
		$this->db->insert(self::T_SUPPLIERCOSTS, $posted);
		$id_suppliercost = $this->db->insert_id();
		// Recupero il record del fornitore per ottenere il centro di costo e la cat. di costo di default
		$supplier = $this->db->from(self::T_SUPPLIERS)->where('id = '.$posted['id_supplier'])->get()->row_array();
		// Inserisco il dettaglio del costo fornitore che in primis sarà 1 sola riga
		$detail_data = [
			'id_suppliercost' => $id_suppliercost,
			'id_costcenter' => $supplier['id_costcenter'],
			'id_costcat' => $supplier['id_costcat'],
			'period_month' => $posted['period_month'],
			'period_year' => $posted['period_year'],
			'amount' => $posted['imponibile'],
		];
		$this->db->insert(self::T_SUPPLIERCOSTS_DETAILS, $detail_data);
		// Ritorno l'ID del costo fornitore inserito
		return $id_suppliercost;
	}
	
	public function del_suppliercost($posted) {	
		$this->db->where('id = '.$posted['id_suppliercost_to_del'])->delete(self::T_SUPPLIERCOSTS);	
		$this->db->where('id_suppliercost = '.$posted['id_suppliercost_to_del'])->delete(self::T_SUPPLIERCOSTS_DETAILS);	
	}

	public function del_suppliercosts($posted) {
		$this->db->trans_start();
		$this->db->from(self::T_SUPPLIERCOSTS)->where("created_at LIKE '".$posted['upload_date']."%'");
		if ($posted['period_month'] AND $posted['period_year']) {
			$this->db->where("period_month = ".$posted['period_month']);
			$this->db->where("period_year = ".$posted['period_year']);
		}
		$this->db->delete();	// le righe di dettaglio vengono cancellate ON CASCADE
		$deleted_rows = $this->db->affected_rows();
		// $this->db->from($tab.'_details')->where("created_at LIKE '".$posted['upload_date']."%'")->delete();
		// $deleted_detail_rows = $this->db->affected_rows();
		$this->db->trans_complete();
		
		return [
			'deleted_rows' => $deleted_rows,
			'trans_status' => $this->db->trans_status()
		];
	}
	
	public function update_suppliercost($posted) {
		// Aggiorno il costo fornitore
		$fields = array(
			'id_supplier' => $posted['id_supplier'],
			'period_month' => $posted['period_month'],
			'period_year' => $posted['period_year'],
			'tipo_documento' => $posted['tipo_documento'],
			'divisa' => $posted['divisa'],
			'data' => $posted['data'],
			'numero' => $posted['numero'],
			'importo_totale' => $posted['importo_totale'],
			// 'aliquota_iva' => $posted['aliquota_iva'],
			// 'arrotondamento' => $posted['arrotondamento'],
			'imponibile' => $posted['imponibile'],
			'imposta' => $posted['imposta'],
			// 'esigibilita_iva' => $posted['esigibilita_iva'],
		);
		$this->db->where('id = '.$posted['id_suppliercost']);
		$this->db->update(self::T_SUPPLIERCOSTS, $fields);
		// Aggiorno i dettagli del costo fornitore che possono essere uno o più di uno
		$this->db->trans_start();
		$this->db->from(self::T_SUPPLIERCOSTS_DETAILS)->where('id_suppliercost = '.$posted['id_suppliercost'])->delete();
		foreach ($posted['detail']['amount'] as $k => $v) {
			$detail_data = [
				'id_suppliercost' => $posted['id_suppliercost'],
				'id_costcenter' => $posted['detail']['id_costcenter'][$k],
				'id_costcat' => $posted['detail']['id_costcat'][$k],
				'period_month' => $posted['detail']['period_month'][$k],
				'period_year' => $posted['detail']['period_year'][$k],
				'amount' => $posted['detail']['amount'][$k],
			];
			$this->db->insert(self::T_SUPPLIERCOSTS_DETAILS, $detail_data);
		}
		$this->db->trans_complete();

		// echo $this->db->last_query();
		// die();
	}

	public function get_last_imports($num = 30) {
		$suppliercosts_imports = [];
		
		$suppliercosts_dates = $this->db->query('SELECT DISTINCT DATE(created_at) AS date FROM '.self::T_SUPPLIERCOSTS.' ORDER BY date DESC LIMIT '.$num)->result_array();
		
		foreach ($suppliercosts_dates as $d)
			$suppliercosts_imports[$d['date']] = $this->db->query("SELECT COUNT(*) AS num FROM ".self::T_SUPPLIERCOSTS." WHERE DATE(created_at) = '".$d['date']."'")->row_array()['num'];

		return $suppliercosts_imports;
	}

	public function calcCCsumByMonthYear($id_center, $m, $y, $id_cat = null, $num_buste = null, $tot_buste = null) {
		// Calcolo la somma dei costi fornitori nei centri di costo standard
		$res = $this->db->query("SELECT 
								COALESCE (
								(SELECT SUM(".self::T_SUPPLIERCOSTS_DETAILS.".amount) 
								FROM ".self::T_SUPPLIERCOSTS_DETAILS." 
								JOIN ".self::T_SUPPLIERCOSTS." ON ".self::T_SUPPLIERCOSTS_DETAILS.".id_suppliercost = ".self::T_SUPPLIERCOSTS.".id
								JOIN ".self::T_DOCTYPES." ON ".self::T_SUPPLIERCOSTS.".tipo_documento = ".self::T_DOCTYPES.".cod
								WHERE ".self::T_DOCTYPES.".type = 'plus' 
								AND ".self::T_SUPPLIERCOSTS_DETAILS.".period_month = ".$m." 
								AND ".self::T_SUPPLIERCOSTS_DETAILS.".period_year = ".$y."
								AND ".self::T_SUPPLIERCOSTS_DETAILS.".id_costcenter = ".$id_center.
								($id_cat ? " AND ".self::T_SUPPLIERCOSTS_DETAILS.".id_costcat = ".$id_cat : '')."), 0)
								- COALESCE (
								(SELECT SUM(".self::T_SUPPLIERCOSTS_DETAILS.".amount) 
								FROM ".self::T_SUPPLIERCOSTS_DETAILS." 
								JOIN ".self::T_SUPPLIERCOSTS." ON ".self::T_SUPPLIERCOSTS_DETAILS.".id_suppliercost = ".self::T_SUPPLIERCOSTS.".id
								JOIN ".self::T_DOCTYPES." ON ".self::T_SUPPLIERCOSTS.".tipo_documento = ".self::T_DOCTYPES.".cod
								WHERE ".self::T_DOCTYPES.".type = 'minus' 
								AND ".self::T_SUPPLIERCOSTS_DETAILS.".period_month = ".$m." 
								AND ".self::T_SUPPLIERCOSTS_DETAILS.".period_year = ".$y."
								AND ".self::T_SUPPLIERCOSTS_DETAILS.".id_costcenter = ".$id_center.
								($id_cat ? " AND ".self::T_SUPPLIERCOSTS_DETAILS.".id_costcat = ".$id_cat : '')."), 0)
								AS tot")
								->row_array();
		$tot = $res['tot'];
		// Calcolo la somma dei costi fornitori del centro di costo *speciale*
		$res = $this->db->query("SELECT 
								COALESCE (
								(SELECT SUM(".self::T_SUPPLIERCOSTS_DETAILS.".amount) 
								FROM ".self::T_SUPPLIERCOSTS_DETAILS." 
								JOIN ".self::T_SUPPLIERCOSTS." ON ".self::T_SUPPLIERCOSTS_DETAILS.".id_suppliercost = ".self::T_SUPPLIERCOSTS.".id
								JOIN ".self::T_DOCTYPES." ON ".self::T_SUPPLIERCOSTS.".tipo_documento = ".self::T_DOCTYPES.".cod
								WHERE ".self::T_DOCTYPES.".type = 'plus' 
								AND ".self::T_SUPPLIERCOSTS_DETAILS.".period_month = ".$m." 
								AND ".self::T_SUPPLIERCOSTS_DETAILS.".period_year = ".$y."
								AND ".self::T_SUPPLIERCOSTS_DETAILS.".id_costcenter = 1000".
								($id_cat ? " AND ".self::T_SUPPLIERCOSTS_DETAILS.".id_costcat = ".$id_cat : '')."), 0)
								- COALESCE (
								(SELECT SUM(".self::T_SUPPLIERCOSTS_DETAILS.".amount) 
								FROM ".self::T_SUPPLIERCOSTS_DETAILS." 
								JOIN ".self::T_SUPPLIERCOSTS." ON ".self::T_SUPPLIERCOSTS_DETAILS.".id_suppliercost = ".self::T_SUPPLIERCOSTS.".id
								JOIN ".self::T_DOCTYPES." ON ".self::T_SUPPLIERCOSTS.".tipo_documento = ".self::T_DOCTYPES.".cod
								WHERE ".self::T_DOCTYPES.".type = 'minus' 
								AND ".self::T_SUPPLIERCOSTS_DETAILS.".period_month = ".$m." 
								AND ".self::T_SUPPLIERCOSTS_DETAILS.".period_year = ".$y."
								AND ".self::T_SUPPLIERCOSTS_DETAILS.".id_costcenter = 1000".
								($id_cat ? " AND ".self::T_SUPPLIERCOSTS_DETAILS.".id_costcat = ".$id_cat : '')."), 0)
								AS tot")
								->row_array();
		$tot_special = $res['tot'];
		if ($tot_special > 0) {
			if (!$num_buste)
				$num_buste = $this->usercosts_model->getNumBusteByCC($id_center, $m, $y);
			if (!$tot_buste)
				$tot_buste = $this->usercosts_model->getNumBusteByMonthYear($m, $y);
			
			if ($tot_buste > 0 AND $num_buste > 0)
				$tot = $tot + (($tot_special / $tot_buste) * $num_buste);
		}

		return ($tot ? $tot : 0);
	}

	public function calcAllsumByMonthYear($m, $y) {
		$active_costcenters = $this->costcenters_model->get_real_active_costcenters($m, $y);

		$tot = 0;
		foreach ($active_costcenters as $center)
			$tot += $this->calcCCsumByMonthYear($center['id'], $m, $y);

		return ($tot ? $tot : 0);
	}

	public function calcFullsumByMonthYear($m, $y) {
		$res = $this->db->query("SELECT 
								COALESCE (
								(SELECT SUM(".self::T_SUPPLIERCOSTS_DETAILS.".amount) 
								FROM ".self::T_SUPPLIERCOSTS_DETAILS." 
								JOIN ".self::T_SUPPLIERCOSTS." ON ".self::T_SUPPLIERCOSTS_DETAILS.".id_suppliercost = ".self::T_SUPPLIERCOSTS.".id
								JOIN ".self::T_DOCTYPES." ON ".self::T_SUPPLIERCOSTS.".tipo_documento = ".self::T_DOCTYPES.".cod
								WHERE ".self::T_DOCTYPES.".type = 'plus' 
								AND ".self::T_SUPPLIERCOSTS_DETAILS.".period_month = ".$m." 
								AND ".self::T_SUPPLIERCOSTS_DETAILS.".period_year = ".$y."), 0)
								- COALESCE (
								(SELECT SUM(".self::T_SUPPLIERCOSTS_DETAILS.".amount) 
								FROM ".self::T_SUPPLIERCOSTS_DETAILS." 
								JOIN ".self::T_SUPPLIERCOSTS." ON ".self::T_SUPPLIERCOSTS_DETAILS.".id_suppliercost = ".self::T_SUPPLIERCOSTS.".id
								JOIN ".self::T_DOCTYPES." ON ".self::T_SUPPLIERCOSTS.".tipo_documento = ".self::T_DOCTYPES.".cod
								WHERE ".self::T_DOCTYPES.".type = 'minus' 
								AND ".self::T_SUPPLIERCOSTS_DETAILS.".period_month = ".$m." 
								AND ".self::T_SUPPLIERCOSTS_DETAILS.".period_year = ".$y."), 0)
								AS tot")
								->row_array();
		return ($res['tot'] ? $res['tot'] : 0);
	}
	
}