<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Costcenters_model extends CI_Model {

	const T_COSTCENTERS = 'b_costcenters';
	
	function __construct() {
		parent::__construct();
	}
	
	public function get_costcenters($default_data = NULL) {
		$costcenters = $this->db->from(self::T_COSTCENTERS)->order_by('name', 'ASC')->get()->result_array();
		return $costcenters;
	}

	public function get_real_costcenters() {
		$costcenters = $this->db->from(self::T_COSTCENTERS)->where('id != 1000')->order_by('name', 'ASC')->get()->result_array();
		return $costcenters;
	}

	public function get_real_active_costcenters($m, $y) {
		$costcenters = $this->get_real_costcenters();
		$active_costcenters = [];
		foreach ($costcenters as $cc)
			if ($this->isCostCenterActiveByMonthYear($cc, $m, $y))
				$active_costcenters[] = $cc;
		return $active_costcenters;
	}
	
	public function get_costcenter($id_costcenter) {
		return $this->db->where('id = '.$id_costcenter)->get(self::T_COSTCENTERS)->row_array();
	}
	
	public function insert_costcenter($posted) {
		$this->db->insert(self::T_COSTCENTERS, $posted);		
		$id_costcenter = $this->db->insert_id();
		return $id_costcenter;
	}
	
	public function del_costcenter($posted) {		
		$this->db->where('id = '.$posted['id_costcenter_to_del'])->delete(self::T_COSTCENTERS);	
	}
	
	public function update_costcenter($posted) {
		$fields = array(
			'id' => $posted['new_id_costcenter'],
			'name' => $posted['name'],
			'location' => $posted['location'],
			'from_month' => $posted['from_month'],
			'to_month' => $posted['to_month'],
			'from_year' => $posted['from_year'],
			'to_year' => $posted['to_year']
		);
		$this->db->where('id = '.$posted['id_costcenter']);
		$this->db->update(self::T_COSTCENTERS, $fields);

		// echo $this->db->last_query();
		// die();
	}
	
	function isCostCenterActiveByMonthYear($center, $m, $y) {
		// se per il centro di costo ho passato solo l'ID, devo recuperare l'istanza
		if (is_numeric($center)) {
			$id_center = $center;
			$center = $this->get_costcenter($id_center);
		}
	
		$startDate = mktime(0, 0, 0, $center['from_month'], 1, $center['from_year']);   // Data di inizio del periodo del centro di costo
		$endDate = mktime(0, 0, 0, $center['to_month'], 1, $center['to_year']); // Data di fine del periodo del centro di costo
		$endDate = strtotime('+1 month', $endDate) - 1;     // Sposto la data di fine al termine del mese di chiusura del periodo del centro di costo
		$checkDate = mktime(0, 0, 0, $m, 1, $y);    // Data che devo controllare (il 1° del mese ovviamente)
	
		if ($checkDate >= $startDate && $checkDate <= $endDate)
			return true;
		return false;
	}
	
	function getCostCentersByMonthYear($m, $y) {
		// Recupero tutti i centri di costo tranne quello SPECIALE
		$centers = $this->get_real_costcenters();
		$m_costcenters = [];
		// Ciclo su tutti i centri di costo
		foreach ($centers as $c) {
			// Se il mese che sto considerando è compreso nell'intervanno DA - A del centro di costo, allora includo il centro di costo in quelli che devo considerare nei calcoli
			if (isWithinInterval($m, $y, $c['from_month'], $c['from_year'], $c['to_month'], $c['to_year']))
				$m_costcenters[] = $c;
		}
		return $m_costcenters;
	}

}