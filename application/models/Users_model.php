<?php

ini_set("display_errors", 1);
error_reporting(E_ALL);

defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once './application/libraries/PHPMailer/Exception.php';
require_once './application/libraries/PHPMailer/PHPMailer.php';
require_once './application/libraries/PHPMailer/SMTP.php';

require_once './application/libraries/aruba_sms.php';

class Users_model extends CI_Model {
	
	const T_USERS = 'users';
	const T_USERS_INFO = 'users_info';
	const T_USERS_WORK = 'users_work';
	const T_JOBS_USERS = 'jobs_users';
	const T_JOBS = 'jobs';
	const T_AREAS_USERS = 'areas_users';
	const T_AREAS = 'areas';
	const T_SERVICES_USERS = 'services_users';
	const T_SERVICES = 'services';
	const T_CUSTOMERS_USERS = 'customers_users';
	const T_CUSTOMERS = 'customers';
	const T_LEVELS_USERS = 'levels_users';
	const T_LEVELS = 'levels';
	const T_LOGS = 'logs';
	const T_MESS = 'messages';
	const T_DOCS_USERS = 'docs_users';
	const T_POLLS_USERS = 'polls_users';
	const T_MESS_USERS = 'messages_users';
	const F_MESS_SCHEDULE = './tosend_messages.csv';
	
	function __construct() {
		parent::__construct();
		$this->load->library('email');
		$this->email->initialize();
	}
	
	private function send_access_data($email, $name, $usr, $psw) {
		$mail = new PHPMailer(true);
		try {
			// $mail->SMTPDebug = 2;
			$mail->isSMTP();
			$mail->Host = 'smtps.aruba.it';
			$mail->CharSet = 'UTF-8';
			$mail->SMTPAuth = true;
			$mail->Username = 'doc-assistenza2000@gruppoyuma.it';
			$mail->Password = 'DOC.lcic$';
			$mail->SMTPSecure = 'ssl';
			$mail->Port = 465;
			$mail->setFrom('info@assistenza2000.it', 'Assistenza 2000');
			$mail->addAddress($email, $name);
			$mail->isHTML(true);
			$mail->Subject = "Accesso all'area riservata";
			$mail->Body = '<img src="http://www.assistenza2000.it/images/logo.jpg"><br>
									<p>È stato creato un utente a tuo nome o sono stati modificati i tuoi dati di accesso.<br>Da ora puoi accedere alla piattaforma con le seguenti credenziali:</p>
									<p>
										<b>Username</b>: '.$usr.'<br>
										<b>Password</b>: '.$psw.'
									</p>
									<p>Accedi alla tua area riservata cliccando su <a href="http://doc.assistenza2000.it">doc.assistenza2000.it</a></p>';
			$mail->send();
		} catch (Exception $e) {
			echo 'Message could not be sent.';
			echo 'Mailer Error: ' . $mail->ErrorInfo;
		}
	}
	
	public function list_messages($posted) {
		$result = $this->db->order_by('date_creation DESC')->get(self::T_MESS)->result_array();
		foreach ($result as $k => $row) {
			$result[$k]['recipients'] = array();
			$ar_users = explode(",", $row['users_selected']);
			foreach ($ar_users as $u)
				$result[$k]['recipients'][] = $this->db->where('id_user', $u)->get(self::T_USERS_INFO)->row_array();
		}
		return $result;
	}
	
	public function del_message($id) {
		$this->db->where('id = '.$id)->delete(self::T_MESS);
		$this->db->where('id_message = '.$id)->delete(self::T_MESS_USERS);
	}
	
	public function send_messages($posted) {
		$send_mail = FALSE;
		if (isset($posted['alert_mail']))
			$send_mail = TRUE;
		unset($posted['alert_mail']);
		 
		$send_sms = FALSE;
		if (isset($posted['alert_sms']))
			$send_sms = TRUE;
		unset($posted['alert_sms']);
		
		$subject = $posted['subject'];
		$text = $posted['message'];
		if (isset($posted['users']) AND count($posted['users'])) {
			$num_users = count($posted['users']);
			
			# Loggo il fatto che ho creato il messaggio da inviare
			$fields = array(
				'subject' => $subject,
				'body' => $text,
				'send_mail' => ($send_mail ? '1' : '0'),
				'send_sms' => ($send_sms ? '1' : '0'),
				'num_users' => $num_users,
				'users_selected' => implode(",", $posted['users'])
			);
			$this->db->insert(self::T_MESS, $fields);
			$id_message = $this->db->insert_id();
			
			foreach ($posted['users'] as $id_user) {
				if ($this->check_active($id_user)) {
					if ($num_users <= 20) {
						$this->send_message($id_user, $subject, $text, $send_mail, $send_sms, $id_message);
					} else {
						$this->schedule_message($id_user, $subject, $text, $send_mail, $send_sms, $id_message);
					}
				}
			}
		}
	}
	
	private function schedule_message($id_user, $subject, $text, $send_mail = TRUE, $send_sms = TRUE, $id_message = null) {
		# 0. NORMALIZZAZIONE MESSAGGIO
		$text = str_replace(array("\n", "\r"), ' ', $text);
		$text = trim(preg_replace('/\s+/', ' ', $text));
		
		# 1. AGGIUNGO IL MESSAGGIO AL FILE
		$data = $id_user.",".str_replace(",", "", $subject).",".str_replace(",", "", $text).",".(($send_mail) ? '1' : '0').",".(($send_sms) ? '1' : '0').",".$id_message."\n";
		write_file(self::F_MESS_SCHEDULE, $data, 'a');
	}
	
	public function send_message($id_user, $subject, $text, $send_mail, $send_sms, $id_message = null) {
		$user_info = $this->get_user_info($id_user);
		
		# 0. NORMALIZZAZIONE MESSAGGIO
		$text = str_replace(array("\n", "\r"), ' ', $text);
		$text = trim(preg_replace('/\s+/', ' ', $text));
		
		# 1. INVIO MESSAGGIO VIA EMAIL
		if ($send_mail) {
			$mail = new PHPMailer(true);
			try {
				// $mail->SMTPDebug = 2;
				$mail->isSMTP();
				$mail->Host = 'smtps.aruba.it';
				$mail->CharSet = 'UTF-8';
				$mail->SMTPAuth = true;
				$mail->Username = 'doc-assistenza2000@gruppoyuma.it';
				$mail->Password = 'DOC.lcic$';
				$mail->SMTPSecure = 'ssl';
				$mail->Port = 465;
				$mail->setFrom('info@assistenza2000.it', 'Assistenza 2000');
				$mail->addAddress($user_info['email'], $user_info['first_name'].' '.$user_info['last_name']);
				$mail->isHTML(true);
				$mail->Subject = $subject;
				$mail->Body = '<img src="http://www.assistenza2000.it/images/logo.jpg"><br>
										<p>'.$text.'</p>';
				$mail->send();
			} catch (Exception $e) {
				echo 'Message could not be sent.';
				echo 'Mailer Error: ' . $mail->ErrorInfo;
			}
		}
		
		# 2. INVIO MESSAGGIO VIA SMS
		if ($send_sms) {
			$aruba_sms = new ArubaSMS();

			$sms_recipient = ((strpos($user_info['telephone'], '+39') === FALSE) ? '+39' : '').str_replace(array('.','-',' ','/'), array('','','',''), $user_info['telephone']);
			$sms_message = $subject." > ".$text;
			
			$aruba_sms->send_sms($sms_message, $sms_recipient);

			/*
			require_once('./lib/sms/sendsms.php');
			$sms = new Sdk_SMS();
			$sms->sms_type = SMSTYPE_ALTA;
			$sms->add_recipient(((strpos($user_info['telephone'], '+39') === FALSE) ? '+39' : '').str_replace(array('.','-',' ','/'), array('','','',''), $user_info['telephone']));
			$sms->message = $subject." > ".$text;
			$sms->sender = '3711649736';
			//$sms->sender = 'Assist2000';
			$sms->set_immediate(); // or sms->set_scheduled_delivery($unix_timestamp)
			$sms->order_id = '69063344';
			// echo 'About to send a message '.$sms->count_smss().' SMSs long ';
			// echo 'to '.$sms->count_recipients().' recipients </br>';
			if ($sms->validate()) {
				$res = $sms->send();
				if ($res['ok']) {
					// echo $res['sentsmss'].' SMS sent, order id is '.$res['order_id'].' </br>';
				} else {
					// echo 'Error sending SMS: '.$sms->problem().' </br>';
				}
			}
			else {
				// echo 'invalid SMS: '.$sms->problem().' </br>';
			}
			*/
		}
		
		# Loggo il fatto che ho inviato il messaggio
		$fields = array(
			'id_message' => $id_message,
			'id_user' => $id_user
		);
		$this->db->insert(self::T_MESS_USERS, $fields);
	}
	
	public function get_sms_left() {
		$aruba_sms = new ArubaSMS();
		$credits = $aruba_sms->get_credit();
		foreach ($credits->sms as $obj_type)
			if ($obj_type->type == 'GP')
				return $obj_type->quantity;
		die();

		/*
		require_once('./lib/sms/credits.php');

		$credits = sdk_get_credits();
		if ($credits['ok']) {
			for ($i = 0; $i < $credits['count']; $i++) {
				if (!$credits[$i]->is_international()) {
					return $credits[$i]->availability;
					// echo 'You can send '.$credits[$i]->availability.' smss';
					// echo ' of type '.$credits[$i]->credit_type;
					// echo ' in '.$credits[$i]->nation.' </br>';
				} else {
					// if ($credits[$i]->credit_type == 'EE') {
						// echo 'You can send '.$credits[$i]->availability;
						// echo ' smss in foreign countries </br>';
					// }
				}
			}
		} else {
			echo 'Request failed: '.$status['errcode'].' - '.$status['errmsg'];
			die();
		}
		*/
	}
	
	public function can_login($ip) {
		$query = $this->db->from(self::T_LOGS)->where("ip_from = '".$ip."' AND date_action >= '".date("Y-m-d H:i:s", (time() - (60*30)))."'")->get();
		$num_tentativi = $query->num_rows();
		if ($num_tentativi >= 10)
			return FALSE;
		return TRUE;
	}

	public function login($usr, $psw) {
		if ($result = $this->db->select('id_user, role')->where("username = '".$usr."' AND password = '".md5($psw)."' AND active = 1")->get(self::T_USERS)) {
			$result = $result->row_array();
			// se è la prima volta che qualcuno si logga oggi, aggiorno i campi calcolabili di tutti gli utenti
			$today_login = $this->db->from(self::T_USERS)->where("date_lastlogin LIKE '".date('Y-m-d')."%'")->get();
			if ($today_login->num_rows() == 0)		
				$this->update_users_calc_fields();

			// aggiorno dati login utente
			$fields = array(
				'date_lastlogin' => date('Y-m-d H:i:s'),
				'ip_lastlogin' => $_SERVER['REMOTE_ADDR']
			);
			$this->db->where('id_user = '.$result['id_user']);
			$this->db->update(self::T_USERS, $fields);

			return $result;
		}
		return FALSE;
	}
	
	# "filters" is an array made by key (field) => value
	public function get_users($type = NULL, $filters = []) {
		$this->db->select(self::T_USERS_INFO.".*, ".self::T_USERS.".*, ".self::T_JOBS.".name AS job, ".self::T_AREAS.".name AS area, ".self::T_SERVICES.".name AS service, ".self::T_CUSTOMERS.".name AS customer, ".self::T_LEVELS.".name AS level, ".self::T_USERS_WORK.".rank, ".self::T_USERS_WORK.".freelance, ".self::T_USERS_WORK.".is_hired")
				 ->from(self::T_USERS_INFO)
				 ->join(self::T_USERS, self::T_USERS_INFO.'.id_user = '.self::T_USERS.'.id_user')
				 ->join(self::T_USERS_WORK, self::T_USERS_INFO.'.id_user = '.self::T_USERS_WORK.'.id_user', 'left')
				 ->join(self::T_JOBS_USERS, self::T_JOBS_USERS.'.id_user = '.self::T_USERS.'.id_user', 'left')
				 ->join(self::T_JOBS, self::T_JOBS_USERS.'.id_job = '.self::T_JOBS.'.id', 'left')
				 ->join(self::T_AREAS_USERS, self::T_AREAS_USERS.'.id_user = '.self::T_USERS.'.id_user', 'left')
				 ->join(self::T_AREAS, self::T_AREAS_USERS.'.id_area = '.self::T_AREAS.'.id', 'left')
				 ->join(self::T_SERVICES_USERS, self::T_SERVICES_USERS.'.id_user = '.self::T_USERS.'.id_user', 'left')
				 ->join(self::T_SERVICES, self::T_SERVICES_USERS.'.id_service = '.self::T_SERVICES.'.id', 'left')
				 ->join(self::T_CUSTOMERS_USERS, self::T_CUSTOMERS_USERS.'.id_user = '.self::T_USERS.'.id_user', 'left')
				 ->join(self::T_CUSTOMERS, self::T_CUSTOMERS_USERS.'.id_customer = '.self::T_CUSTOMERS.'.id', 'left')
				 ->join(self::T_LEVELS_USERS, self::T_LEVELS_USERS.'.id_user = '.self::T_USERS.'.id_user', 'left')
				 ->join(self::T_LEVELS, self::T_LEVELS_USERS.'.id_level = '.self::T_LEVELS.'.id', 'left');
		if (!empty($type))
			$this->db->where("role = '".$type."'");
		if (count($filters))
			foreach ($filters as $field => $val)
				$this->db->where($field." = '".$val."'");
		$result = $this->db->order_by("last_name ASC, first_name ASC")->get()->result_array();
		// echo '<pre>'.$this->db->last_query().'</pre>';
		return $result;
	}
	
	public function get_jobs() {
		return $this->db->from(self::T_JOBS)->order_by('name', 'ASC')->get()->result_array();
	}
	
	public function get_by_fiscode($fiscode) {
		return $this->db->from(self::T_USERS_INFO)->join(self::T_USERS, self::T_USERS_INFO.'.id_user = '.self::T_USERS.'.id_user')->where("fiscal_code = '".$fiscode."'")->get()->row_array();
	}

	public function getAreasByUserIds($ar_user_ids) {
		$str_user_ids = implode(',', $ar_user_ids);
		$result = $this->db->distinct()
						   ->select(self::T_AREAS.'.*')
						   ->from(self::T_AREAS_USERS)
						   ->join(self::T_AREAS, self::T_AREAS_USERS.'.id_area = '.self::T_AREAS.'.id', 'left')
						   ->where('id_user IN ('.$str_user_ids.')')
						   ->get()->result_array();
		// echo '<pre>'.$this->db->last_query().'</pre>';
		// die();
		return $result;
	}

	public function getJobsByUserIds($ar_user_ids) {
		$str_user_ids = implode(',', $ar_user_ids);
		$result = $this->db->distinct()
						   ->select(self::T_JOBS.'.*')
						   ->from(self::T_JOBS_USERS)
						   ->join(self::T_JOBS, self::T_JOBS_USERS.'.id_job = '.self::T_JOBS.'.id', 'left')
						   ->where('id_user IN ('.$str_user_ids.')')
						   ->get()->result_array();
		// echo '<pre>'.$this->db->last_query().'</pre>';
		// die();
		return $result;
	}
	
	public function get_user_info($id) {
		return $this->db->select(self::T_USERS_INFO.".*, ".self::T_USERS_WORK.".*, ".self::T_USERS.".*, ".self::T_JOBS.".name AS job, ".self::T_AREAS.".name AS area, ".self::T_SERVICES.".name AS service, ".self::T_CUSTOMERS.".name AS customer, ".self::T_LEVELS.".name AS level")
						->from(self::T_USERS_INFO)
						->join(self::T_USERS, self::T_USERS_INFO.'.id_user = '.self::T_USERS.'.id_user')
						->join(self::T_USERS_WORK, self::T_USERS_INFO.'.id_user = '.self::T_USERS_WORK.'.id_user', 'left')
						->join(self::T_JOBS_USERS, self::T_JOBS_USERS.'.id_user = '.self::T_USERS.'.id_user', 'left')
						->join(self::T_JOBS, self::T_JOBS_USERS.'.id_job = '.self::T_JOBS.'.id', 'left')
						->join(self::T_AREAS_USERS, self::T_AREAS_USERS.'.id_user = '.self::T_USERS.'.id_user', 'left')
						->join(self::T_AREAS, self::T_AREAS_USERS.'.id_area = '.self::T_AREAS.'.id', 'left')
						->join(self::T_SERVICES_USERS, self::T_SERVICES_USERS.'.id_user = '.self::T_USERS.'.id_user', 'left')
						->join(self::T_SERVICES, self::T_SERVICES_USERS.'.id_service = '.self::T_SERVICES.'.id', 'left')
						->join(self::T_CUSTOMERS_USERS, self::T_CUSTOMERS_USERS.'.id_user = '.self::T_USERS.'.id_user', 'left')
						->join(self::T_CUSTOMERS, self::T_CUSTOMERS_USERS.'.id_customer = '.self::T_CUSTOMERS.'.id', 'left')
						->join(self::T_LEVELS_USERS, self::T_LEVELS_USERS.'.id_user = '.self::T_USERS.'.id_user', 'left')
				 		->join(self::T_LEVELS, self::T_LEVELS_USERS.'.id_level = '.self::T_LEVELS.'.id', 'left')
						->where(self::T_USERS.'.id_user = '.$id)
						->get()->row_array();
	}
	
	public function save_user($posted) {
		# USER
		$access_data = array(
			'username' => $posted['username'],
			'password' => md5($posted['password']),
			'active' => $posted['active'],
			'role' => $posted['role']
		);
		if ($this->db->insert(self::T_USERS, $access_data)) {
			if ($posted['send_data']) {
				$this->send_access_data($posted['email'], $posted['first_name'].' '.$posted['last_name'], $posted['username'], $posted['password']);
			}
			$id_user = $this->db->insert_id();

			# USER INFO
			$info_data = array(
				'id_user' => $id_user,
				'first_name' => $posted['first_name'],
				'last_name' => $posted['last_name'],
				'address' => $posted['address'],
				'city' => $posted['city'],
				'company' => $posted['company'],
				'age' => (isset($posted['age']) AND $posted['age']) ? $posted['age'] : null,
				'email' => $posted['email'],
				'telephone' => $posted['telephone'],
				'fiscal_code' => $posted['fiscal_code'],
				'birth_date' => rvd($posted['birth_date']),
				'birth_city' => $posted['birth_city'],
				'nationality' => $posted['nationality'],
				'foreigner' => (isset($posted['foreigner']) AND $posted['foreigner']) ? $posted['foreigner'] : 0,
				'gender' => $posted['gender'],
			);
			$this->db->insert(self::T_USERS_INFO, $info_data);

			# USER WORK
			$work_data = array(
				'id_user' => $id_user,
				'date_hiring' => $posted['date_hiring'] ? rvd($posted['date_hiring']) : null,
				'serv_months' => (isset($posted['serv_months']) AND $posted['serv_months']) ? str_replace(",", ".", $posted['serv_months']) : null,
				'prev_months' => $posted['prev_months'] > 0 ? str_replace(",", ".", $posted['prev_months']) : $posted['prev_months'],
				'tot_months' => (isset($posted['tot_months']) AND $posted['tot_months']) ? str_replace(",", ".", $posted['tot_months']) : null,
				'tot_years' => (isset($posted['tot_years']) AND $posted['tot_years']) ? str_replace(",", ".", $posted['tot_years']) : null,
				'rank' => (isset($posted['rank']) AND $posted['rank']) ? $posted['rank'] : null,
				'temp' => $posted['temp'],
				'freelance' => $posted['freelance'],
				'trainee' => $posted['trainee'],
				'first_hiring' => $posted['first_hiring'] ? $posted['first_hiring'] : null,
				'last_hiring' => $posted['last_hiring'] ? $posted['last_hiring'] : null,
				'is_hired' => (isset($posted['is_hired']) AND $posted['is_hired']) ? $posted['is_hired'] : 0,
				'year_termination' => $posted['year_termination'] ? $posted['year_termination'] : null
			);

			// devo gestire il campo dell'eventuale contratto a termine
			$work_data['fixed_term'] = [];
			if (!empty($posted['fixed_term_start']) AND is_array($posted['fixed_term_start']) AND !empty($posted['fixed_term_end']) AND is_array($posted['fixed_term_end'])) {
				for ($c = 0; $c < count($posted['fixed_term_start']); $c++) {	// analizzo un periodo per volta
					$work_data['fixed_term'][] = [
						'start' => $posted['fixed_term_start'][$c],
						'end' => $posted['fixed_term_end'][$c]
					];
				}
			}
			$work_data['fixed_term'] = json_encode($work_data['fixed_term']);

			$this->db->insert(self::T_USERS_WORK, $work_data);

			# UPDATE CALC FIELDS
			$this->update_user_calc_fields($id_user);

			# JOB			
			if ($posted['id_job']) {
				$fields = array(
					'id_user' => $id_user,
					'id_job' => $posted['id_job']
				);
				$this->db->insert(self::T_JOBS_USERS, $fields);
			}

			# AREA
			if ($posted['id_area']) {
				$fields = array(
					'id_user' => $id_user,
					'id_area' => $posted['id_area']
				);
				$this->db->insert(self::T_AREAS_USERS, $fields);
			}

			# SERVICE
			if ($posted['id_service']) {
				$fields = array(
					'id_user' => $id_user,
					'id_service' => $posted['id_service']
				);
				$this->db->insert(self::T_SERVICES_USERS, $fields);
			}

			# CUSTOMER
			if ($posted['id_customer']) {
				$fields = array(
					'id_user' => $id_user,
					'id_customer' => $posted['id_customer']
				);
				$this->db->insert(self::T_CUSTOMERS_USERS, $fields);
			}

			# LEVEL
			if ($posted['id_level']) {
				$fields = array(
					'id_user' => $id_user,
					'id_level' => $posted['id_level']
				);
				$this->db->insert(self::T_LEVELS_USERS, $fields);
			}

			return $id_user;
		} else {
			echo "Si è verificato un errore durante l'inserimento dell'utente";
			die();
		}
	}

	public function update_fields($fields, $tab, $cond) {
		if (in_array($tab, ['users', 'users_info', 'users_work'])) {
			$this->db->where($cond);
			return $this->db->update($tab, $fields);
		}
		return false;
	}
	
	public function update_user($posted) {
		// echo '<pre>';
		// print_r($posted);
		// echo '</pre>';

		# USER
		$fields = array(
			'active' => $posted['active'],
			'role' => $posted['role']
		);
		$this->db->where('id_user = '.$posted['id_user']);
		$this->db->update(self::T_USERS, $fields);
		
		# USER INFO
		$info_data = array(
			'first_name' => $posted['first_name'],
			'last_name' => $posted['last_name'],
			'address' => $posted['address'],
			'city' => $posted['city'],
			'company' => $posted['company'],
			'age' => (isset($posted['age']) AND $posted['age']) ? $posted['age'] : null,
			'email' => $posted['email'],
			'telephone' => $posted['telephone'],
			'fiscal_code' => $posted['fiscal_code'],
			'birth_date' => rvd($posted['birth_date']),
			'birth_city' => $posted['birth_city'],
			'nationality' => $posted['nationality'],
			'foreigner' => (isset($posted['foreigner']) AND $posted['foreigner']) ? $posted['foreigner'] : 0,
			'gender' => $posted['gender'],
		);
		$this->db->where('id_user = '.$posted['id_user']);
		$this->db->update(self::T_USERS_INFO, $info_data);

		# USER WORK
		$this->db->where('id_user = '.$posted['id_user'])->delete(self::T_USERS_WORK);
		$work_data = array(
			'id_user' => $posted['id_user'],
			'date_hiring' => $posted['date_hiring'] ? rvd($posted['date_hiring']) : null,
			'serv_months' => (isset($posted['serv_months']) AND $posted['serv_months']) ? str_replace(",", ".", $posted['serv_months']) : null,
			'prev_months' => $posted['prev_months'] > 0 ? str_replace(",", ".", $posted['prev_months']) : $posted['prev_months'],
			'tot_months' => (isset($posted['tot_months']) AND $posted['tot_months']) ? str_replace(",", ".", $posted['tot_months']) : null,
			'tot_years' => (isset($posted['tot_years']) AND $posted['tot_years']) ? str_replace(",", ".", $posted['tot_years']) : null,
			'rank' => (isset($posted['rank']) AND $posted['rank']) ? $posted['rank'] : null,
			'temp' => $posted['temp'],
			'freelance' => $posted['freelance'],
			'trainee' => $posted['trainee'],
			'first_hiring' => $posted['first_hiring'] ? $posted['first_hiring'] : null,
			'last_hiring' => $posted['last_hiring'] ? $posted['last_hiring'] : null,
			'is_hired' => (isset($posted['is_hired']) AND $posted['is_hired']) ? $posted['is_hired'] : 0,
			'year_termination' => $posted['year_termination'] ? $posted['year_termination'] : null
		);

		// devo gestire il campo dell'eventuale contratto a termine
		$work_data['fixed_term'] = [];
		if (!empty($posted['fixed_term_start']) AND is_array($posted['fixed_term_start']) AND !empty($posted['fixed_term_end']) AND is_array($posted['fixed_term_end'])) {
			for ($c = 0; $c < count($posted['fixed_term_start']); $c++) {	// analizzo un periodo per volta
				$work_data['fixed_term'][] = [
					'start' => $posted['fixed_term_start'][$c],
					'end' => $posted['fixed_term_end'][$c]
				];
			}
		}
		$work_data['fixed_term'] = json_encode($work_data['fixed_term']);

		$this->db->insert(self::T_USERS_WORK, $work_data);

		# UPDATE CALC FIELDS
		$this->update_user_calc_fields($posted['id_user']);
		
		# JOB
		$this->db->where('id_user = '.$posted['id_user'])->delete(self::T_JOBS_USERS);
		if ($posted['id_job']) {
			$fields = array(
				'id_user' => $posted['id_user'],
				'id_job' => $posted['id_job']
			);
			$this->db->insert(self::T_JOBS_USERS, $fields);
		}

		# AREA
		$this->db->where('id_user = '.$posted['id_user'])->delete(self::T_AREAS_USERS);
		if ($posted['id_area']) {
			$fields = array(
				'id_user' => $posted['id_user'],
				'id_area' => $posted['id_area']
			);
			$this->db->insert(self::T_AREAS_USERS, $fields);
		}

		# SERVICE
		$this->db->where('id_user = '.$posted['id_user'])->delete(self::T_SERVICES_USERS);
		if ($posted['id_service']) {
			$fields = array(
				'id_user' => $posted['id_user'],
				'id_service' => $posted['id_service']
			);
			$this->db->insert(self::T_SERVICES_USERS, $fields);
		}

		# CUSTOMER
		$this->db->where('id_user = '.$posted['id_user'])->delete(self::T_CUSTOMERS_USERS);
		if ($posted['id_customer']) {
			$fields = array(
				'id_user' => $posted['id_user'],
				'id_customer' => $posted['id_customer']
			);
			$this->db->insert(self::T_CUSTOMERS_USERS, $fields);
		}

		# LEVEL
		$this->db->where('id_user = '.$posted['id_user'])->delete(self::T_LEVELS_USERS);
		if ($posted['id_level']) {
			$fields = array(
				'id_user' => $posted['id_user'],
				'id_level' => $posted['id_level']
			);
			$this->db->insert(self::T_LEVELS_USERS, $fields);
		}
	}

	/* METODO PER TUTTI GLI UTENTI */
	public function update_users_calc_fields() {
		$users = $this->get_users('user');
		foreach ($users as $u)
			$this->update_user_calc_fields($u['id_user']);
	}

	/* METODO PER IL SINGOLO UTENTE */
	public function update_user_calc_fields($id_user) {
		$calc_fields = $this->calc_user_calc_fields($id_user);

		if ($calc_fields) {
			$fields = [
				'age' => $calc_fields['age'],
			];
			$this->db->where('id_user = '.$id_user);
			$this->db->update(self::T_USERS_INFO, $fields);

			$fields = [
				'serv_months' => $calc_fields['serv_months'] ? $calc_fields['serv_months'] : null,
				'tot_months' => $calc_fields['tot_months'],
				'tot_years' => $calc_fields['tot_years'],
				'rank' => $calc_fields['rank'],
				'is_hired' => $calc_fields['is_hired'],
			];
			$this->db->where('id_user = '.$id_user);
			$this->db->update(self::T_USERS_WORK, $fields);
		}
	}

	public function calc_serv_months($date_start, $date_end) {
		$days_diff = null;
		$earlier = new DateTime(strpos($date_start, '/') ? rvd($date_start) : $date_start);
		$later = new DateTime(strpos($date_end, '/') ? rvd($date_end) : $date_end);
		$days_diff = $later->diff($earlier)->format("%a");
		return number_format((($days_diff / 365) * 12), 1);
	}
	
	public function calc_user_calc_fields($id_user, $date = null, $return_all_fields = false) {
		if ($id_user) {
			// recupero i dati dettagliati dell'utente in questione
			$user_info = $this->get_user_info($id_user);

			// se per l'utente in questione non ho né data assunzione, né mesi di servizio, né mesi precedenti, skippo e non modifico nulla
			// if (!$user_info['date_hiring'] AND !$user_info['serv_months'] AND !$user_info['prev_months'])
			// 	return null;

			// se la data non viene passata come parametro, utilizzo quella attuale
			// /* DEBUG */ $date = '2020-12-31';
			if (!$date)
				$date = date('Y-m-d');

			// mi ricavo i valori di tutti i campi calcolabili
			$age = $user_info['birth_date'] != '0000-00-00' ? calc_age($user_info['birth_date']) : null;
			$serv_months = ($user_info['year_termination'] OR !$user_info['date_hiring']) ? null : $this->calc_serv_months($user_info['date_hiring'], $date);	// se non c'è data fine rapporto e ho una data di assunzione
			$tot_months = $serv_months + $user_info['prev_months'];
			$tot_years = number_format(($tot_months / 12), 2);
			$rank = $user_info['temp'] ? 'Temp' : ($tot_months > 60 ? 'Senior' : 'Junior');
			// $active = $user_info['year_termination'] ? 0 : 1;	// il flag di accesso alla piattaforma non lo tocco per permettere di editarlo manualmente
			$is_hired = $user_info['year_termination'] ? 0 : 1;

			$user_calc_fields_updates = [
				'age' => $age,
				// 'active' => $active,
				'serv_months' => $serv_months,
				'tot_months' => $tot_months,
				'tot_years' => $tot_years,
				'rank' => $rank,
				'is_hired' => $is_hired
			];

			// if ($is_hired != $user_info['is_hired'])
				// echo '<span style="background: #d90000; color: #fff;">Dovrei cambiare lo stato dell\'utente sulla piattaforma</span>';

			// se ho scelto di far ritornare tutti i campi dell'array utente, devo sovrascrivere quelli appena calcolati a quelli presi dal db
			if ($return_all_fields)
				return array_merge($user_info, $user_calc_fields_updates);

			return $user_calc_fields_updates;
		}
		return null;
	}
	
	public function change_access($posted) {
		$access_data = array(
			'username' => $posted['username'],
			'password' => md5($posted['password']),
			'date_pswchange' => date('Y-m-d H:i:s')
		);
		$this->db->where('id_user = '.$posted['id_user']);
		$this->db->update(self::T_USERS, $access_data);
		
		if ($posted['send_data']) {
			$this->send_access_data($posted['email'], $posted['first_name'].' '.$posted['last_name'], $posted['username'], $posted['password']);
		}
	}
	
	public function delete_user($id) {
		$this->db->where('id_user = '.$id)->delete(self::T_USERS);
		$this->db->where('id_user = '.$id)->delete(self::T_USERS_INFO);
		$this->db->where('id_user = '.$id)->delete(self::T_USERS_WORK);

		$this->db->where('id_user = '.$id)->delete(self::T_JOBS_USERS);
		$this->db->where('id_user = '.$id)->delete(self::T_AREAS_USERS);
		$this->db->where('id_user = '.$id)->delete(self::T_SERVICES_USERS);
		$this->db->where('id_user = '.$id)->delete(self::T_CUSTOMERS_USERS);
		$this->db->where('id_user = '.$id)->delete(self::T_LEVELS_USERS);

		$this->db->where('id_user = '.$id)->delete(self::T_DOCS_USERS);
		$this->db->where('id_user = '.$id)->delete(self::T_MESS_USERS);
		$this->db->where('id_user = '.$id)->delete(self::T_POLLS_USERS);
	}
	
	public function check_psw_age($id) {
		$user_info = $this->get_user_info($id);
		return (strtotime($user_info['date_pswchange']) > (time()-7776000));
	}
	
	public function check_active($id) {
		$user = $this->db->from(self::T_USERS)->where(self::T_USERS.'.id_user = '.$id)->get()->row_array();
		return $user['active'];
	}

}