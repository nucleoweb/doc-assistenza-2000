<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Revenues_model extends CI_Model {

	const T_REVENUES = 'b_revenues';
	const T_REVENUES_DETAILS = 'b_revenues_details';
	const T_CUSTOMERS = 'b_customers';
	const T_DOCTYPES = 'b_doctypes';
	
	function __construct() {
		parent::__construct();
	}
	
	public function get_revenues($default_data = NULL, $filter = null) {
		$query = $this->db->select(self::T_CUSTOMERS.'.denominazione, '.self::T_CUSTOMERS.'.nome, '.self::T_CUSTOMERS.'.cognome, '.self::T_REVENUES.'.*')
						  ->from(self::T_REVENUES)
						  ->join(self::T_CUSTOMERS, self::T_REVENUES.'.id_customer = '.self::T_CUSTOMERS.'.id')
						  ->join(self::T_REVENUES_DETAILS, self::T_REVENUES.'.id = '.self::T_REVENUES_DETAILS.'.id_revenue');
		# FILTRI - START
		// if (!$filter)
		// 	$filter = [
		// 		'filter_from' => date('Y-m').'-01',
		// 		'filter_to' => date('Y-m-d')
		// 	];

		// if (!empty($filter['filter_from']))
		// 	$query->where("data >= '".$filter['filter_from']."'");
		// if (!empty($filter['filter_to']))
		// 	$query->where("data <= '".$filter['filter_to']."'");
		if (!empty($filter['filter_numdoc'])) {
			$query->where(self::T_REVENUES.".numero = '".$filter['filter_numdoc']."'");
		}
		if (!empty($filter['filter_datadoc'])) {
			$query->where(self::T_REVENUES.".data = '".$filter['filter_datadoc']."'");
		}
		if (!empty($filter['filter_period_month'])) {
			$query->where(self::T_REVENUES_DETAILS.'.period_month = '.$filter['filter_period_month']);
		}
		if (!empty($filter['filter_period_year'])) {
			$query->where(self::T_REVENUES_DETAILS.'.period_year = '.$filter['filter_period_year']);
		}
		if (!empty($filter['filter_costcenter'])) {
			$query->where(self::T_REVENUES_DETAILS.'.id_costcenter = '.$filter['filter_costcenter']);
		}
		if (!empty($filter['filter_name'])) {
			$query->where("CONCAT(".self::T_CUSTOMERS.".denominazione, ' ', ".self::T_CUSTOMERS.".nome, ' ', ".self::T_CUSTOMERS.".cognome) LIKE '%".addslashes($filter['filter_name'])."%'");
		}
		if (!empty($filter['filter_date'])) {
			$query->where(self::T_REVENUES.".created_at >= '".$filter['filter_date']." 00:00:00"."'");
			$query->where(self::T_REVENUES.".created_at <= '".$filter['filter_date']." 23:59:59"."'");
		}
		# FILTRI - END
		$revenues = $query->group_by(self::T_REVENUES_DETAILS.'.id_revenue')->order_by('id', 'ASC')->get()->result_array();
		// echo '<pre>';
		// print_r($this->db->last_query());
		// echo '</pre>';
		// die();
		foreach ($revenues as $k => $rev) {
			if ($rev['id_customer']) {
				$customer = $this->bcustomers_model->get_customer($rev['id_customer']);
				$revenues[$k]['cliente'] = $customer['denominazione'] ? $customer['denominazione'] : $customer['nome'].' '.$customer['cognome']; 
			}
			$revenues[$k]['detail'] = $this->db->from(self::T_REVENUES_DETAILS)->where('id_revenue = '.$rev['id'])->get()->result_array();
			$revenues[$k]['doctype'] = $this->db->from(self::T_DOCTYPES)->where("cod = '".trim($rev['tipo_documento']."'"))->get()->row_array();
		}
		return $revenues;
	}
	
	public function get_revenue($id_revenue = NULL, $numero = NULL, $id_customer = NULL, $data = NULL, $importo_totale = NULL) {
		if ($id_revenue)
			$res = $this->db->where('id = '.$id_revenue)->get(self::T_REVENUES)->row_array();
		elseif ($numero) {
			if ($numero)
				$tmp_res = $this->db->where("numero = '".$numero."'");
			if ($id_customer)
				$tmp_res = $this->db->where("id_customer = ".$id_customer);
			if ($data)
				$tmp_res = $this->db->where("data = '".substr($data, 0, 10)."'");
			if ($importo_totale)
				$tmp_res = $this->db->where("importo_totale = ".$importo_totale);
			if ($tmp_res)
				$res = $tmp_res->get(self::T_REVENUES)->row_array();
		}
		// echo '<pre style="background: #eee; padding: 10px;">'.$this->db->last_query().'</pre>';
		if ($res) {
			$res['detail'] = $this->db->from(self::T_REVENUES_DETAILS)->where('id_revenue = '.$res['id'])->get()->result_array();
		}
		return $res;
	}
	
	public function insert_revenue($posted) {
		$this->db->insert(self::T_REVENUES, $posted);
		$id_revenue = $this->db->insert_id();
		// Recupero il record del cliente per ottenere il centro di costo di default
		$customer = $this->db->from(self::T_CUSTOMERS)->where('id = '.$posted['id_customer'])->get()->row_array();
		// Inserisco il dettaglio del ricavo che in primis sarà 1 sola riga
		$detail_data = [
			'id_revenue' => $id_revenue,
			'id_costcenter' => $customer['id_costcenter'],
			'period_month' => $posted['period_month'],
			'period_year' => $posted['period_year'],
			'amount' => $posted['imponibile'],
		];
		$this->db->insert(self::T_REVENUES_DETAILS, $detail_data);
		// Ritorno l'ID del ricavo inserito
		return $id_revenue;
	}
	
	public function del_revenue($posted) {
		$this->db->where('id = '.$posted['id_revenue_to_del'])->delete(self::T_REVENUES);	
		$this->db->where('id_revenue = '.$posted['id_revenue_to_del'])->delete(self::T_REVENUES_DETAILS);	
	}

	public function del_revenues($posted) {
		$this->db->trans_start();
		$this->db->from(self::T_REVENUES)->where("created_at LIKE '".$posted['upload_date']."%'");
		if ($posted['period_month'] AND $posted['period_year']) {
			$this->db->where("period_month = ".$posted['period_month']);
			$this->db->where("period_year = ".$posted['period_year']);
		}
		$this->db->delete();	// le righe di dettaglio vengono cancellate ON CASCADE
		$deleted_rows = $this->db->affected_rows();
		// $this->db->from($tab.'_details')->where("created_at LIKE '".$posted['upload_date']."%'")->delete();
		// $deleted_detail_rows = $this->db->affected_rows();
		$this->db->trans_complete();
		
		return [
			'deleted_rows' => $deleted_rows,
			'trans_status' => $this->db->trans_status()
		];
	}
	
	public function update_revenue($posted) {
		$fields = array(
			'id_customer' => $posted['id_customer'],
			'period_month' => $posted['period_month'],
			'period_year' => $posted['period_year'],
			'tipo_documento' => $posted['tipo_documento'],
			'divisa' => $posted['divisa'],
			'data' => $posted['data'],
			'numero' => $posted['numero'],
			'importo_totale' => $posted['importo_totale'],
			// 'aliquota_iva' => $posted['aliquota_iva'],
			// 'arrotondamento' => $posted['arrotondamento'],
			'imponibile' => $posted['imponibile'],
			'imposta' => $posted['imposta'],
			// 'esigibilita_iva' => $posted['esigibilita_iva'],
		);
		$this->db->where('id = '.$posted['id_revenue']);
		$this->db->update(self::T_REVENUES, $fields);
		// Aggiorno i dettagli del ricavo che possono essere uno o più di uno
		$this->db->trans_start();
		$this->db->from(self::T_REVENUES_DETAILS)->where('id_revenue = '.$posted['id_revenue'])->delete();
		foreach ($posted['detail']['amount'] as $k => $v) {
			$detail_data = [
				'id_revenue' => $posted['id_revenue'],
				'id_costcenter' => $posted['detail']['id_costcenter'][$k],
				'period_month' => $posted['detail']['period_month'][$k],
				'period_year' => $posted['detail']['period_year'][$k],
				'amount' => $posted['detail']['amount'][$k],
			];
			$this->db->insert(self::T_REVENUES_DETAILS, $detail_data);
		}
		$this->db->trans_complete();

		// echo $this->db->last_query();
		// die();
	}

	public function get_last_imports($num = 30) {
		$revenues_imports = [];
		
		$revenues_dates = $this->db->query('SELECT DISTINCT DATE(created_at) AS date FROM '.self::T_REVENUES.' ORDER BY date DESC LIMIT '.$num)->result_array();
		
		foreach ($revenues_dates as $d)
			$revenues_imports[$d['date']] = $this->db->query("SELECT COUNT(*) AS num FROM ".self::T_REVENUES." WHERE DATE(created_at) = '".$d['date']."'")->row_array()['num'];

		return $revenues_imports;
	}

	public function calcCCsumByMonthYear($id_centercost, $m, $y, $num_buste = null, $tot_buste = null) {
		// Calcolo la somma dei ricavi dei centri di costo standard
		$res = $this->db->query("SELECT 
								COALESCE (
								(SELECT SUM(".self::T_REVENUES_DETAILS.".amount) 
								FROM ".self::T_REVENUES_DETAILS." 
								JOIN ".self::T_REVENUES." ON ".self::T_REVENUES_DETAILS.".id_revenue = ".self::T_REVENUES.".id
								JOIN ".self::T_DOCTYPES." ON ".self::T_REVENUES.".tipo_documento = ".self::T_DOCTYPES.".cod
								WHERE ".self::T_DOCTYPES.".type = 'plus' 
								AND ".self::T_REVENUES_DETAILS.".period_month = ".$m." 
								AND ".self::T_REVENUES_DETAILS.".period_year = ".$y."
								AND ".self::T_REVENUES_DETAILS.".id_costcenter = ".$id_centercost."), 0)
								- COALESCE (
								(SELECT SUM(".self::T_REVENUES_DETAILS.".amount) 
								FROM ".self::T_REVENUES_DETAILS." 
								JOIN ".self::T_REVENUES." ON ".self::T_REVENUES_DETAILS.".id_revenue = ".self::T_REVENUES.".id
								JOIN ".self::T_DOCTYPES." ON ".self::T_REVENUES.".tipo_documento = ".self::T_DOCTYPES.".cod
								WHERE ".self::T_DOCTYPES.".type = 'minus' 
								AND ".self::T_REVENUES_DETAILS.".period_month = ".$m." 
								AND ".self::T_REVENUES_DETAILS.".period_year = ".$y."
								AND ".self::T_REVENUES_DETAILS.".id_costcenter = ".$id_centercost."), 0) 
								AS tot")
								->row_array();
		$tot = $res['tot'];
		// Calcolo la somma dei ricavi del centro di costo *speciale*
		$res = $this->db->query("SELECT 
								COALESCE (
								(SELECT SUM(".self::T_REVENUES_DETAILS.".amount) 
								FROM ".self::T_REVENUES_DETAILS." 
								JOIN ".self::T_REVENUES." ON ".self::T_REVENUES_DETAILS.".id_revenue = ".self::T_REVENUES.".id
								JOIN ".self::T_DOCTYPES." ON ".self::T_REVENUES.".tipo_documento = ".self::T_DOCTYPES.".cod
								WHERE ".self::T_DOCTYPES.".type = 'plus' 
								AND ".self::T_REVENUES_DETAILS.".period_month = ".$m." 
								AND ".self::T_REVENUES_DETAILS.".period_year = ".$y."
								AND ".self::T_REVENUES_DETAILS.".id_costcenter = 1000), 0) 
								- COALESCE (
								(SELECT SUM(".self::T_REVENUES_DETAILS.".amount) 
								FROM ".self::T_REVENUES_DETAILS." 
								JOIN ".self::T_REVENUES." ON ".self::T_REVENUES_DETAILS.".id_revenue = ".self::T_REVENUES.".id
								JOIN ".self::T_DOCTYPES." ON ".self::T_REVENUES.".tipo_documento = ".self::T_DOCTYPES.".cod
								WHERE ".self::T_DOCTYPES.".type = 'minus' 
								AND ".self::T_REVENUES_DETAILS.".period_month = ".$m." 
								AND ".self::T_REVENUES_DETAILS.".period_year = ".$y."
								AND ".self::T_REVENUES_DETAILS.".id_costcenter = 1000), 0) 
								AS tot")
								->row_array();
		$tot_special = $res['tot'];
		// se il totale degli importi sul centro di costo SPECIALE è > 0, lo devo pesare in base al centro di costo e poi sommare al totale standard
		if ($tot_special > 0) {
			if (!$num_buste)
				$num_buste = $this->usercosts_model->getNumBusteByCC($id_centercost, $m, $y);
			if (!$tot_buste)
				$tot_buste = $this->usercosts_model->getNumBusteByMonthYear($m, $y);
			
			if ($tot_buste > 0 AND $num_buste > 0)
				$tot = $tot + (($tot_special / $tot_buste) * $num_buste);
		}

		return ($tot ? $tot : 0);
	}

	public function calcAllsumByMonthYear($m, $y) {
		$active_costcenters = $this->costcenters_model->get_real_active_costcenters($m, $y);

		$tot = 0;
		foreach ($active_costcenters as $center)
			$tot += $this->calcCCsumByMonthYear($center['id'], $m, $y);

		return ($tot ? $tot : 0);
	}

	public function calcFullsumByMonthYear($m, $y) {
		$res = $this->db->query("SELECT 
								COALESCE (
								(SELECT SUM(".self::T_REVENUES_DETAILS.".amount) 
								FROM ".self::T_REVENUES_DETAILS." 
								JOIN ".self::T_REVENUES." ON ".self::T_REVENUES_DETAILS.".id_revenue = ".self::T_REVENUES.".id
								JOIN ".self::T_DOCTYPES." ON ".self::T_REVENUES.".tipo_documento = ".self::T_DOCTYPES.".cod
								WHERE ".self::T_DOCTYPES.".type = 'plus' 
								AND ".self::T_REVENUES_DETAILS.".period_month = ".$m." 
								AND ".self::T_REVENUES_DETAILS.".period_year = ".$y."), 0)
								- COALESCE (
								(SELECT SUM(".self::T_REVENUES_DETAILS.".amount) 
								FROM ".self::T_REVENUES_DETAILS." 
								JOIN ".self::T_REVENUES." ON ".self::T_REVENUES_DETAILS.".id_revenue = ".self::T_REVENUES.".id
								JOIN ".self::T_DOCTYPES." ON ".self::T_REVENUES.".tipo_documento = ".self::T_DOCTYPES.".cod
								WHERE ".self::T_DOCTYPES.".type = 'minus' 
								AND ".self::T_REVENUES_DETAILS.".period_month = ".$m." 
								AND ".self::T_REVENUES_DETAILS.".period_year = ".$y."), 0)
								AS tot")
								->row_array();
		return ($res['tot'] ? $res['tot'] : '0');
	}
	
}