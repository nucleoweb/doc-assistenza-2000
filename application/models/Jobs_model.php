<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jobs_model extends CI_Model {

	const T_JOBS_USERS = 'jobs_users';
	const T_JOBS = 'jobs';
	const T_USERS = 'users';
	const T_USERS_WORK = 'users_work';
	
	function __construct() {
		parent::__construct();
	}
	
	public function get_jobs($default_data = NULL) {
		$result = $this->db->from(self::T_JOBS)->order_by('name', 'ASC')->get()->result_array();
		$jobs = array();
		foreach ($result as $job) {
			if ($default_data) {
				$job['num_users'] = $this->db->where('id_job', $job['id'])->get(self::T_JOBS_USERS)->num_rows();
				$job['num_users_active'] = $this->db->from(self::T_JOBS_USERS)
													->join(self::T_USERS, self::T_JOBS_USERS.'.id_user = '.self::T_USERS.'.id_user', 'left')
													->join(self::T_USERS_WORK, self::T_JOBS_USERS.'.id_user = '.self::T_USERS_WORK.'.id_user', 'left')
													->where(self::T_JOBS_USERS.'.id_job = '.$job['id'].' AND '.self::T_USERS.'.active = 1 AND '.self::T_USERS_WORK.'.is_hired = 1')
													->get()->num_rows();
				array_push($jobs, $job);
			}
			else
				$jobs[$job['id']] = $job['name'];
		}
		return $jobs;
	}
	
	public function get_job($id_job) {
		return $this->db->where('id = '.$id_job)->get(self::T_JOBS)->row_array();
	}
	
	public function insert_job($posted) {
		$this->db->insert(self::T_JOBS, $posted);		
		$id_job = $this->db->insert_id();
		return $id_job;
	}
	
	public function del_job($posted) {
		$fields = array(
			'id_job' => $posted['job_to_assoc']
		);
		$this->db->where('id_job = "'.$posted['id_job_to_del'].'"');
		$this->db->update(self::T_JOBS_USERS, $fields);
		
		$this->db->where('id = '.$posted['id_job_to_del'])->delete(self::T_JOBS);	
	}
	
	public function update_job($posted) {
		$fields = array(
			'name' => $posted['name']
		);
		$this->db->where('id = '.$posted['id_job']);
		$this->db->update(self::T_JOBS, $fields);
	}
	
}