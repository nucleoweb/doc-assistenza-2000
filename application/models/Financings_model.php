<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Financings_model extends CI_Model {

	const T_FINANCINGS = 'b_financings';
	
	function __construct() {
		parent::__construct();
	}
	
	public function get_financings($default_data = NULL) {
		$financings = $this->db->from(self::T_FINANCINGS)->order_by('id', 'ASC')->get()->result_array();
		return $financings;
	}
	
	public function get_financing($id_financing) {
		return $this->db->where('id = '.$id_financing)->get(self::T_FINANCINGS)->row_array();
	}
	
	public function insert_financing($posted) {
		$posted['costcenters'] = json_encode($posted['cc']);
		unset($posted['cc']);
		$this->db->insert(self::T_FINANCINGS, $posted);		
		$id_financing = $this->db->insert_id();
		return $id_financing;
	}
	
	public function del_financing($posted) {		
		$this->db->where('id = '.$posted['id_financing_to_del'])->delete(self::T_FINANCINGS);	
	}
	
	public function update_financing($posted) {
		$fields = array(
			'name' => $posted['name'],
			'description' => $posted['description'],
			'amount' => $posted['amount'],
			'from_month' => $posted['from_month'],
			'from_year' => $posted['from_year'],
			'to_month' => $posted['to_month'],
			'to_year' => $posted['to_year'],
			'costcenters' => json_encode($posted['cc']),
		);
		$this->db->where('id = '.$posted['id_financing']);
		$this->db->update(self::T_FINANCINGS, $fields);

		// echo $this->db->last_query();
		// die();
	}

	public function getFinancingsByYear($y) {
		return $this->db->from(self::T_FINANCINGS)->where('from_year <= '.$y.' AND to_year >= '.$y)->get()->result_array();
	}
	
	public function getFinancingsByMonthYear($m, $y) {
		$all_financings = $this->getFinancingsByYear($y);
		$financings = [];
		// Ciclo su tutti i finanziamenti che hanno attinenza con l'anno che sto considerando
		foreach ($all_financings as $fin) {
			// Se il mese che sto considerando è compreso nell'intervanno DA - A del finanziamento, allora includo il finanziamento in quelli che devo considerare nei calcoli
			if (isWithinInterval($m, $y, $fin['from_month'], $fin['from_year'], $fin['to_month'], $fin['to_year'])) {
				$fin['num_months'] = getMonthDiff($fin['from_month'], $fin['from_year'], $fin['to_month'], $fin['to_year']);
				$financings[] = $fin;
			}
		}
		return $financings;
	}

	public function calcCCsumByMonthYear($fin, $id_center, $m, $y, $num_buste = null, $tot_buste = null) {
		if (is_numeric($fin)) {
			$id_financing = $fin;
			$fin = $this->get_financing($id_financing);
		}

		$fin_centers = json_decode($fin['costcenters']);

		$tot = 0;
		$fin_month_quota = $fin['amount'] / $fin['num_months'];
		if (!$num_buste)
			$num_buste = $this->usercosts_model->getNumBusteByCC($id_center, $m, $y);
		
		if (in_array(1000, $fin_centers)) {
			// se ho messo il centro di costo *speciale* nel finanziamento, ripartisco tra tutti i centri di costo in modo proporzionale rispetto al numero di buste
			if (!$tot_buste)
				$tot_buste = $this->usercosts_model->getNumBusteByMonthYear($m, $y);		
		} elseif (in_array($id_center, $fin_centers)) {
			// se invece ho selezionato alcuni centri di costo specifici, ripartisco in modo pesato in base al numero di buste di quei centri - in questo caso ignoro l'eventuale parametro che potrei aver passato dal chiamante
			$tot_buste = 0;
			foreach ($fin_centers as $tmp_cc)
				$tot_buste += $this->usercosts_model->getNumBusteByCC($tmp_cc['id'], $m, $y);
		}

		if ($tot_buste > 0 AND $num_buste > 0)
			$tot = ($fin_month_quota / $tot_buste) * $num_buste;

		return $tot;
	}

	public function calcAllsumByMonthYear($m, $y) {
		$active_financings = $this->costcenters_model->get_real_active_costcenters($m, $y);
		$active_costcenters = $this->costcenters_model->get_real_active_costcenters($m, $y);

		$tot = 0;
		foreach ($active_costcenters as $center)
			foreach ($active_financings as $financing)
				$tot += $this->calcCCsumByMonthYear($financing['id'], $center['id'], $m, $y);

		return $tot;
	}
	
}