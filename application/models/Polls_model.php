<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once './application/libraries/PHPMailer/Exception.php';
require_once './application/libraries/PHPMailer/PHPMailer.php';
require_once './application/libraries/PHPMailer/SMTP.php';

require_once './application/libraries/aruba_sms.php';

class Polls_model extends CI_Model {

	const T_USERS = 'users';
	const T_USERS_INFO = 'users_info';
	const T_POLLS = 'polls';
	const T_QUESTIONS = 'polls_questions';
	const T_ANSWERS = 'polls_answers';
	const T_OPTIONS = 'polls_options';
	const T_POLLS_USERS = 'polls_users';
	const F_POLL_SCHEDULE = './tosend_polls.csv';
	const NUM_FROM_SCHEDULE = 5;
	
	function __construct() {
		parent::__construct();
	}

	public function schedule_poll_notify($id_user, $id_poll, $title, $send_mail, $send_sms) {
		$data = $id_user.",".$id_poll.",".$title.",".(($send_mail) ? '1' : '0').",".(($send_sms) ? '1' : '0')."\n";
		write_file(self::F_POLL_SCHEDULE, $data, 'a');
	}

	public function unschedule_poll_notify($user_ids, $id_poll) {	# $user_ids contiene gli id degli utenti di cui togliere la schedulazione
		$f = fopen(self::F_POLL_SCHEDULE, "r");
		$new_file_content = "";
		while (!feof($f)) {
			$file_row = fgets($f);
			if ($file_row != "") {
				$ar_row = explode(",", $file_row);
				$tmp_id_user = $ar_row[0];
				$tmp_id_poll = $ar_row[1];
				if ($tmp_id_poll != $id_poll OR !in_array($tmp_id_user, $user_ids))
					$new_file_content .= $file_row;
			}
		}
		fclose($f);
		write_file(self::F_POLL_SCHEDULE, $new_file_content);
	}

	public function send_poll_notify($id_user, $id_poll, $title, $send_mail, $send_sms) {
		$user_info = $this->users_model->get_user_info($id_user);
		$poll_info = $this->get_poll($id_poll);
		
		# 1. INVIO NOTIFICA VIA EMAIL
		if ($send_mail AND $poll_info['alert_mail']) {
			$mail = new PHPMailer(true);
			try {
				// $mail->SMTPDebug = 2;
				$mail->isSMTP();
				$mail->Host = 'smtps.aruba.it';
				$mail->CharSet = 'UTF-8';
				$mail->SMTPAuth = true;
				$mail->Username = 'doc-assistenza2000@gruppoyuma.it';
				$mail->Password = 'DOC.lcic$';
				$mail->SMTPSecure = 'ssl';
				$mail->Port = 465;
				$mail->setFrom('info@assistenza2000.it', 'Assistenza 2000');
				$mail->addAddress($user_info['email'], $user_info['first_name'].' '.$user_info['last_name']);
				$mail->isHTML(true);
				if ($poll_info['date_start'] < date("Y-m-d")) {
					$mail->Subject = 'Promemoria sondaggio';
					$mail->Body = '<img src="http://www.assistenza2000.it/images/logo.jpg"><br>
									<p>Un sondaggio in cui sei coinvolto aspetta ancora il tuo voto!</p>
									<p><b>Oggetto del sondaggio:</b> '.$poll_info['title'].'</p>
									<p><b>Descrizione del sondaggio:</b> '.$poll_info['info'].'</p>
									<p>Collegati a <a href="http://doc.assistenza2000.it">doc.assistenza2000.it</a> per partecipare</p>';
				} else {
					$mail->Subject = 'Nuovo sondaggio';
					$mail->Body = '<img src="http://www.assistenza2000.it/images/logo.jpg"><br>
									<p>Un nuovo sondaggio è stato indetto e sei coinvolto anche tu.</p>
									<p><b>Oggetto del sondaggio:</b> '.$poll_info['title'].'</p>
									<p><b>Descrizione del sondaggio:</b> '.$poll_info['info'].'</p>
									<p>Collegati a <a href="http://doc.assistenza2000.it">doc.assistenza2000.it</a> per partecipare</p>';
				}
				$mail->send();
			} catch (Exception $e) {
				echo 'Message could not be sent.';
				echo 'Mailer Error: ' . $mail->ErrorInfo;
			}
		}
		
		# 2. INVIO NOTIFICA VIA SMS
		if ($send_sms AND $poll_info['alert_sms']) {
			$aruba_sms = new ArubaSMS();

			$sms_recipient = ((strpos($user_info['telephone'], '+39') === FALSE) ? '+39' : '').str_replace(array('.','-',' ','/'), array('','','',''), $user_info['telephone']);
			$sms_message = "È stato indetto il sondaggio: ".$poll_info['title'].". Collegati alla piattaforma documentale per partecipare.\n\nASSISTENZA 2000";
			
			$aruba_sms->send_sms($sms_message, $sms_recipient);

			/*
			require_once('./lib/sms/sendsms.php');
			$sms = new Sdk_SMS();
			$sms->sms_type = SMSTYPE_ALTA;
			$sms->add_recipient(((strpos($user_info['telephone'], '+39') === FALSE) ? '+39' : '').str_replace(array('.','-',' ','/'), array('','','',''), $user_info['telephone']));
			$sms->message = "È stato indetto il sondaggio: ".$poll_info['title'].". Collegati alla piattaforma documentale per partecipare.\n\nASSISTENZA 2000";
			$sms->sender = '3711649736';
			//$sms->sender = 'Assist2000';
			$sms->set_immediate(); // or sms->set_scheduled_delivery($unix_timestamp)
			$sms->order_id = '69063344';
			// echo 'About to send a message '.$sms->count_smss().' SMSs long ';
			// echo 'to '.$sms->count_recipients().' recipients </br>';
			if ($sms->validate()) {
				$res = $sms->send();
				if ($res['ok']) {
					// echo $res['sentsmss'].' SMS sent, order id is '.$res['order_id'].' </br>';
				} else {
					// echo 'Error sending SMS: '.$sms->problem().' </br>';
				}
			}
			else {
				// echo 'invalid SMS: '.$sms->problem().' </br>';
			}
			*/
		}
	}
	
	public function get_polls($id_user = null, $date_end = null) {
		$this->db->from(self::T_POLLS)->order_by('date_creation', 'DESC');
		if ($id_user)
			$this->db->join(self::T_POLLS_USERS, self::T_POLLS.'.id = '.self::T_POLLS_USERS.'.id_poll')->where(self::T_POLLS_USERS.'.id_user = '.$id_user);
		if ($date_end)
			$this->db->where(self::T_POLLS.".date_end = '".$date_end."'");
		$polls = $this->db->get()->result_array();
		foreach ($polls as $k => $poll) {
			$polls[$k] = $this->get_poll(($id_user) ? $poll['id_poll'] : $poll['id']);
		}
		return $polls;
	}
	
	public function get_poll($id_poll) {
		$poll = $this->db->where('id = '.$id_poll)->get(self::T_POLLS)->row_array();

		$poll_questions = $this->db->where('id_poll = '.$id_poll)->get(self::T_QUESTIONS)->result_array();
		$poll['questions'] = $poll_questions;

		$answers = [];
		$poll_answers = $this->db->where('id_poll = '.$id_poll)->get(self::T_ANSWERS)->result_array();
		foreach ($poll_answers as $ans) {
			$answers[$ans['id_question']][] = $ans;
		}
		$poll['answers'] = $answers;

		$options = [];
		$poll_options = $this->db->where('id_poll = '.$id_poll)->get(self::T_OPTIONS)->result_array();
		foreach ($poll_options as $opt) {
			$options[$opt['id_question']][] = $opt;
		}
		$poll['options'] = $options;

		$result_users = $this->db->where('id_poll = '.$id_poll)->get(self::T_POLLS_USERS)->result_array();
		foreach ($result_users as $user_found) {
			$user_info = $this->users_model->get_user_info($user_found['id_user']);
			$poll['users'][$user_found['id_user']] = array(
				'date_vote' => $user_found['date_vote'],
				'name' => ucwords(strtolower(htmlentities($user_info['first_name']." ".$user_info['last_name'])))
			);
		}

		// echo '<pre>';
		// print_r($poll);
		// echo '</pre>';
		// die();
		

		if (!$poll['anonymous']) {
			$poll['users_voted'] = [];

			$voting_users = $this->db->select('first_name, last_name, answers')
							->from(self::T_POLLS_USERS)->where(self::T_POLLS_USERS.'.id_poll = '.$id_poll)
							->join(self::T_USERS_INFO, self::T_POLLS_USERS.'.id_user = '.self::T_USERS_INFO.'.id_user')
							->get()->result_array();

			// echo '<pre>';
			// print_r($voting_users);
			// echo '</pre>';
			// die();

			if (isset($voting_users) AND is_array($voting_users) AND count($voting_users)) {
				foreach ($voting_users as $k => $info) {
					$ar_answers = json_decode($info['answers']);
					$explicit_answers = [];
					if ($info['answers']) {
						foreach ($ar_answers as $id_question => $id_answer) {
							$question_label = null;
							$answer_label = null;
							// Recupero la domanda
							foreach ($poll['questions'] as $q)
								if ($q['id'] == $id_question)
									$question_label = $q['title'];
							// Recupero la risposta
							$answer_label = $id_answer;
							if (is_numeric($id_answer))
								foreach ($poll['options'][$id_question] as $opt)
									if ($opt['id'] == $id_answer)
										$answer_label = $opt['title'];
							$explicit_answers[$question_label] = $answer_label;
						}
					}
					$voting_users[$k]['answers'] = $explicit_answers;
				}
			}

			// echo '<pre>';
			// print_r($voting_users);
			// echo '</pre>';
			// die();

			$poll['users_voted'] = $voting_users;

			// foreach ($poll['options'] as $opt) {
			// 	$users_voted = $this->db->select('first_name, last_name')
			// 							->from(self::T_POLLS_USERS)
			// 							->join(self::T_USERS_INFO, self::T_POLLS_USERS.'.id_user = '.self::T_USERS_INFO.'.id_user')
			// 							->where(self::T_POLLS_USERS.'.id_poll = '.$id_poll.' AND '.self::T_POLLS_USERS.'.id_option = '.$opt['id'])
			// 							->get()->result_array();
			// 	foreach ($users_voted as $user_voted)
			// 		$poll['users_voted'][$opt['id']][] = $user_voted['first_name'].' '.$user_voted['last_name'];
			// }

		}

		$poll['num_voted'] = $this->db->from(self::T_POLLS_USERS)
									  ->where('id_poll', $id_poll)
									  ->where('date_vote is NOT NULL')
									  ->get()->num_rows();

		$poll['req_string'] = $this->get_req_string($id_poll);


		// echo '<pre>';
		// print_r($poll);
		// echo '</pre>';
		// die();

		return $poll;
	}
	
	public function save_poll($posted) {
		if (isset($posted['alert_mail'])) {
			$send_mail = TRUE;
			$posted['alert_mail'] = '1';
		} else {
			$send_mail = FALSE;
			$posted['alert_mail'] = '0';
		}

		if (isset($posted['alert_sms'])) {
			$send_sms = TRUE;
			$posted['alert_sms'] = '1';
		} else {
			$send_sms = FALSE;
			$posted['alert_sms'] = '0';
		}

		$questions = [];
		$options = [];
		foreach ($posted as $k => $v) {
			if (preg_match('/^question_([0-9]{1,})$/', $k, $matches)) {	// è una domanda
				$i = $matches[1];
				$questions[$i] = array('title' => $v, 'type' => 'O');
				unset($posted[$k]);
			} elseif (preg_match('/^question_([0-9]{1,})_options$/', $k, $matches)) {	// è una risposta
				$i = $matches[1];
				$questions[$i]['type'] = 'C';
				$options[$i] = $v;
				unset($posted[$k]);
			}
		}
		
		// echo '<pre>';
		// print_r($posted);
		// echo '</pre>';
		// echo '<pre>';
		// print_r($questions);
		// echo '</pre>';
		// echo '<pre>';
		// print_r($options);
		// echo '</pre>';
		// die();

		$posted['date_start'] = implode("-", array_reverse(explode("/", $posted['date_start'])));
		$posted['date_end'] = implode("-", array_reverse(explode("/", $posted['date_end'])));

		if (isset($posted['anonymous']))
			$posted['anonymous'] = 1;
		else
			$posted['anonymous'] = 0;

		$user_ids = ((isset($posted['users'])) ? $posted['users'] : array());
		$num_users = count($user_ids);
		if (isset($posted['users']))
			unset($posted['users']);
		if (isset($posted['select_all']))
			unset($posted['select_all']);

		foreach ($posted as $k => $v)		# rimuovo tutti i campi filtro dall'array POST
			if (strpos($k, 'filter'))
				unset($posted[$k]);
		
		if ($this->db->insert(self::T_POLLS, $posted)) {
			$id_poll = $this->db->insert_id();
			
			# SALVO LE DOMANDE
			foreach ($questions as $i => $qst) {
				if (trim($qst['title']) != "") {
					$fields = array(
						'id_poll' => $id_poll,
						'title' => $qst['title'],
						'type' => $qst['type']
					);
					$this->db->insert(self::T_QUESTIONS, $fields);
					$id_question = $this->db->insert_id();
					
					# SALVO LE OPZIONI RELATIVE ALLA DOMANDA
					if (isset($options[$i])) {
						foreach ($options[$i] as $opt) {
							if (trim($opt) != "") {
								$fields = array(
									'id_poll' => $id_poll,
									'id_question' => $id_question,
									'title' => $opt
								);
								$this->db->insert(self::T_OPTIONS, $fields);		
							}
						}
					}
				}
			}

			# SALVO LE ASSOCIAZIONI CON GLI UTENTI
			foreach ($user_ids as $id_user) {	// per ogni utente della lista che ho selezionato
				$user_tmp = $this->db->where('id_user', $id_user)->get(self::T_USERS)->row_array();
				$active = $user_tmp['active'];
				if ($active) {	// solo se l'utente che sto analizzando è abilitato
					$fields = array(
						'id_poll' => $id_poll,
						'id_user' => $id_user
					);
					if ($this->db->insert(self::T_POLLS_USERS, $fields)) {
						if ($num_users <= self::NUM_FROM_SCHEDULE) {
							$this->send_poll_notify($id_user, $id_poll, $posted['title'], $send_mail, $send_sms);
						} else {
							$this->schedule_poll_notify($id_user, $id_poll, $posted['title'], $send_mail, $send_sms);
						}
					} else {
						echo '<pre>';
						print_r($this->db->error());
						echo '</pre>';
					}
				}
			}
			// die();
			return $id_poll;
		} else {
			echo '<pre>';
			print_r($this->db->error());
			echo '</pre>';
		}
	}
	
	public function clone_poll($id_poll_to_clone) {
		# Clono il sondaggio
		$poll = $this->db->where('id', $id_poll_to_clone)->get(self::T_POLLS)->row_array();
		unset($poll['id']);
		$poll['title'] = "BOZZA | ".str_replace('"', '', $poll['title']);
		$poll['date_start'] = date('Y-m-d', strtotime('+1 month', strtotime(date("Y/m/d"))));
		$poll['date_end'] = date('Y-m-d', strtotime('+1 year', strtotime(date("Y/m/d"))));
		unset($poll['date_creation']);

		// echo '<pre>';
		// print_r($poll);
		// echo '</pre>';
		// die();

		$this->db->insert(self::T_POLLS, $poll);					// creo il nuovo sondaggio
		$id_new_poll = $this->db->insert_id();						// salvo l'id del nuovo sondaggio

		# Clono le domande
		$poll_questions = $this->db->where('id_poll', $id_poll_to_clone)->get(self::T_QUESTIONS)->result_array();
		foreach ($poll_questions as $question) {					// per ogni domanda
			$id_old_question = $question['id'];						// il vecchio id della domanda mi serve per recuperare le opzioni
			unset($question['id']);
			$question['id_poll'] = $id_new_poll;					// associo la domanda al nuovo sondaggio
			$this->db->insert(self::T_QUESTIONS, $question);		// salvo la nuova domanda
			$id_new_question = $this->db->insert_id();				// salvo l'id della nuova domanda, a cui dovrò associare tutte le opzioni

			# Clono le opzioni
			$question_options = $this->db->where('id_poll = '.$id_poll_to_clone.' AND id_question = '.$id_old_question)->get(self::T_OPTIONS)->result_array();
			foreach ($question_options as $option) {
				unset($option['id']);
				$option['id_poll'] = $id_new_poll;					// associo l'opzione al nuovo sondaggio
				$option['id_question'] = $id_new_question;			// associo l'opzione alla nuova domanda
				$this->db->insert(self::T_OPTIONS, $option);	// salvo la nuova opzione
			}
		}
	}

	public function update_poll($posted) {
		$send_mail = FALSE;
		if (isset($posted['alert_mail']))
			$send_mail = TRUE;
		unset($posted['alert_mail']);
		 
		$send_sms = FALSE;
		if (isset($posted['alert_sms']))
			$send_sms = TRUE;
		unset($posted['alert_sms']);
		
		$questions = [];
		$options = [];
		foreach ($posted as $k => $v) {
			if (preg_match('/^question_([0-9]{1,})$/', $k, $matches)) {	// è una domanda
				$i = $matches[1];
				$questions[$i] = array('title' => $v, 'type' => 'O');
				unset($posted[$k]);
			} elseif (preg_match('/^question_([0-9]{1,})_options$/', $k, $matches)) {	// è una risposta
				$i = $matches[1];
				$questions[$i]['type'] = 'C';
				$options[$i] = $v;
				unset($posted[$k]);
			}
		}
		
		$posted['date_start'] = implode("-", array_reverse(explode("/", $posted['date_start'])));
		$posted['date_end'] = implode("-", array_reverse(explode("/", $posted['date_end'])));

		if (isset($posted['anonymous']))
			$posted['anonymous'] = 1;
		else
			$posted['anonymous'] = 0;

		$user_ids = ((isset($posted['users'])) ? $posted['users'] : array());
		$num_users = count($user_ids);
		if (isset($posted['users']))
			unset($posted['users']);
		if (isset($posted['select_all']))
			unset($posted['select_all']);

		foreach ($posted as $k => $v)		# rimuovo tutti i campi filtro dall'array POST
			if (strpos($k, 'filter'))
				unset($posted[$k]);

		$this->db->where('id = '.$posted['id']);
		$this->db->update(self::T_POLLS, $posted);
		
		# AGGIORNO LE OPZIONI
		$this->db->where('id_poll = '.$posted['id'])->delete(self::T_QUESTIONS);	
		$this->db->where('id_poll = '.$posted['id'])->delete(self::T_OPTIONS);	
		
		# SALVO LE DOMANDE
		foreach ($questions as $i => $qst) {
			if (trim($qst['title']) != "") {
				$fields = array(
					'id_poll' => $posted['id'],
					'title' => $qst['title'],
					'type' => $qst['type']
				);
				$this->db->insert(self::T_QUESTIONS, $fields);
				$id_question = $this->db->insert_id();
				
				# SALVO LE OPZIONI RELATIVE ALLA DOMANDA
				if (isset($options[$i])) {
					foreach ($options[$i] as $opt) {
						if (trim($opt) != "") {
							$fields = array(
								'id_poll' => $posted['id'],
								'id_question' => $id_question,
								'title' => $opt
							);
							$this->db->insert(self::T_OPTIONS, $fields);		
						}
					}
				}
			}
		}
		
		# AGGIORNO LE ASSOCIAZIONI CON GLI UTENTI
		# Recupero la lista degli id utente che avevo associato in precedenza e che ora sono stati disassociati, perché devo fare l'unschedule
		if (count($user_ids)) {
			$unschedule_users = $this->db->select('id_user')->where('id_poll = '.$posted['id'].' AND id_user NOT IN ('.implode(",", $user_ids).')')->get(self::T_POLLS_USERS)->result_array('id_user');
			$unschedule_users_ids = array();
			if (is_array($unschedule_users) AND count($unschedule_users)) {
				foreach ($unschedule_users as $unschedule_user)
					$unschedule_users_ids[] = $unschedule_user['id_user'];
				$this->unschedule_poll_notify($unschedule_users_ids, $posted['id_poll']);
			}
		}
		# Cancello tutti quelli che ora non sono più associati
		if (count($user_ids))
			$this->db->where('id_poll = '.$posted['id'].' AND id_user NOT IN ('.implode(",", $user_ids).')')->delete(self::T_POLLS_USERS);
		else
			$this->db->where('id_poll = '.$posted['id'])->delete(self::T_POLLS_USERS);
		# Mi recupero le attuali associazioni e inserisco quelle nuove (!in_array)
		$connected_users = $this->db->select('id_user')->where('id_poll = '.$posted['id'])->get(self::T_POLLS_USERS)->result_array('id_user');
		$connected_users_ids = array();
		foreach ($connected_users as $conn_user)
			$connected_users_ids[] = $conn_user['id_user'];
		foreach ($user_ids as $id_user) {
			if (!in_array($id_user, $connected_users_ids)) {
				$fields = array(
					'id_poll' => $posted['id'],
					'id_user' => $id_user
				);
				if ($this->db->insert(self::T_POLLS_USERS, $fields)) {
					if ($num_users <= self::NUM_FROM_SCHEDULE) {
						$this->send_poll_notify($id_user, $posted['id'], $posted['title'], $send_mail, $send_sms);
					} else {
						$this->schedule_poll_notify($id_user, $posted['id'], $posted['title'], $send_mail, $send_sms);
					}
				} else {
					echo '<pre>';
					print_r($this->db->error());
					echo '</pre>';
				}
			}
		}
	}

	public function save_vote($posted) {
		$poll = $this->db->where('id = '.$posted['id_poll'])->get(self::T_POLLS)->row_array();

		$answers = [];
		foreach ($posted as $k => $ans) {
			if (preg_match('/^opt_([0-9]{1,})$/', $k, $matches)) {
				$qst_id = $matches[1];
				$this->db->where('id_poll = '.$posted['id_poll'].' AND id_question = '.$qst_id.' AND id = '.$ans)->set('votes', 'votes+1', FALSE)->update(self::T_OPTIONS);
				$answers[$qst_id] = $ans;
			}
			if (preg_match('/^txt_([0-9]{1,})$/', $k, $matches)) {
				$qst_id = $matches[1];
				$fields = array(
					'id_poll' => $posted['id_poll'],
					'id_question' => $qst_id,
					'answer' => $ans
				);
				$this->db->insert(self::T_ANSWERS, $fields);
				$answers[$qst_id] = $ans;
			}
		}
		$answers = json_encode($answers);

		$fields = array(
			'acceptance' => ($posted['acceptance'] == "on" ? '1' : '0'),
			'date_vote' => date("Y-m-d")
		);
		if (!$poll['anonymous'])
			$fields['answers'] = $answers;
		$this->db->where('id_poll = '.$posted['id_poll'].' AND id_user = '.$posted['id_user'])->update(self::T_POLLS_USERS, $fields);
		$this->send_vote_notify($posted['id_user'], $posted['id_poll'], NULL);
	}

	private function send_vote_notify($id_user, $id_poll, $chosen_option) {
		$user_info = $this->users_model->get_user_info($id_user);
		$poll_info = $this->get_poll($id_poll);
		// foreach ($poll_info['options'] as $opt)
		// 	if ($opt['id'] == $chosen_option)
		// 		$name_option = $opt['title'];
		
		$mail = new PHPMailer(true);
		try {
			$mail->isSMTP();
			$mail->Host = 'smtps.aruba.it';
			$mail->CharSet = 'UTF-8';
			$mail->SMTPAuth = true;
			$mail->Username = 'doc-assistenza2000@gruppoyuma.it';
			$mail->Password = 'DOC.lcic$';
			$mail->SMTPSecure = 'ssl';
			$mail->Port = 465;
			$mail->setFrom('info@assistenza2000.it', 'Assistenza 2000');
			$mail->addAddress($user_info['email'], $user_info['first_name'].' '.$user_info['last_name']);
			$mail->isHTML(true);
			$mail->Subject = 'Voto sondaggio ricevuto';
			$mail->Body = '<img src="http://www.assistenza2000.it/images/logo.jpg"><br>
							<p>Hai appena votato per un sondaggio</p>
							<table>
							<tr><td style="background: #2A317F; color: #fff; font-weight: bold; padding: 5px 20px;">Data voto</td><td style="padding: 5px 10px;">'.date("d/m/Y").'</td>
							<tr><td style="background: #2A317F; color: #fff; font-weight: bold; padding: 5px 20px;">Sondaggio</td><td style="padding: 5px 10px;">'.$poll_info['title'].'</td>
							</table>
							<p>Grazie per aver partecipato.</p>';
			$mail->send();
		} catch (Exception $e) {
			echo 'Message could not be sent.';
			echo 'Mailer Error: ' . $mail->ErrorInfo;
		}
	}
	
	public function delete_poll($id_poll) {		
		$this->db->where('id = '.$id_poll)->delete(self::T_POLLS);	
		$this->db->where('id_poll = '.$id_poll)->delete(self::T_QUESTIONS);	
		$this->db->where('id_poll = '.$id_poll)->delete(self::T_ANSWERS);	
		$this->db->where('id_poll = '.$id_poll)->delete(self::T_OPTIONS);	
		$this->db->where('id_poll = '.$id_poll)->delete(self::T_POLLS_USERS);	
	}
	
	public function get_req_string($id_poll) {
		$poll = $this->db->where('id', $id_poll)->get(self::T_POLLS)->row_array();
		return urlencode(base64_encode($id_poll."|".$poll['filename']));
	}
	
}