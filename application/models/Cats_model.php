<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cats_model extends CI_Model {

	const T_CATS = 'cats';
	const T_DOCS = 'docs';
	
	function __construct() {
		parent::__construct();
	}
	
	public function get_cats($default_data = NULL) {
		$result = $this->db->from(self::T_CATS)->order_by('name', 'ASC')->get()->result_array();
		$cats = array();
		foreach ($result as $cat) {
			if ($default_data) {
				$cat['num_docs'] = $this->db->where('cat', $cat['code'])->get(self::T_DOCS)->num_rows();
				array_push($cats, $cat);
			}
			else
				$cats[$cat['code']] = $cat['name'];
		}
		return $cats;
	}
	
	public function get_cat($id_cat) {
		return $this->db->where('id_cat = '.$id_cat)->get(self::T_CATS)->row_array();
	}
	
	public function insert_cat($posted) {
		$this->db->insert(self::T_CATS, $posted);		
		$id_cat = $this->db->insert_id();
		return $id_cat;
	}
	
	public function del_cat($posted) {
		$fields = array(
			'cat' => $posted['cat_to_assoc']
		);
		$this->db->where('cat = "'.$posted['code_cat_to_del'].'"');
		$this->db->update(self::T_DOCS, $fields);
		
		$this->db->where('id_cat = '.$posted['id_cat_to_del'])->delete(self::T_CATS);	
	}
	
	public function update_cat($posted) {
		$this->db->where('id_cat = '.$posted['id_cat']);
		$this->db->update(self::T_CATS, $posted);
	}
	
}