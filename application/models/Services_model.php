<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services_model extends CI_Model {

	const T_SERVICES_USERS = 'services_users';
	const T_SERVICES = 'services';
	const T_USERS = 'users';
	const T_USERS_WORK = 'users_work';
	
	function __construct() {
		parent::__construct();
	}
	
	public function get_services($default_data = NULL) {
		$result = $this->db->from(self::T_SERVICES)->order_by('name', 'ASC')->get()->result_array();
		$services = array();
		foreach ($result as $service) {
			if ($default_data) {
				$service['num_users'] = $this->db->where('id_service', $service['id'])->get(self::T_SERVICES_USERS)->num_rows();
				$service['num_users_active'] = $this->db->from(self::T_SERVICES_USERS)
													    ->join(self::T_USERS, self::T_SERVICES_USERS.'.id_user = '.self::T_USERS.'.id_user', 'left')
													    ->join(self::T_USERS_WORK, self::T_SERVICES_USERS.'.id_user = '.self::T_USERS_WORK.'.id_user', 'left')
													    ->where(self::T_SERVICES_USERS.'.id_service = '.$service['id'].' AND '.self::T_USERS.'.active = 1 AND '.self::T_USERS_WORK.'.is_hired = 1')
													    ->get()->num_rows();
				array_push($services, $service);
			}
			else
				$services[$service['id']] = $service['name'];
		}
		return $services;
	}
	
	public function get_service($id_service) {
		return $this->db->where('id = '.$id_service)->get(self::T_SERVICES)->row_array();
	}
	
	public function insert_service($posted) {
		$this->db->insert(self::T_SERVICES, $posted);		
		$id_service = $this->db->insert_id();
		return $id_service;
	}
	
	public function del_service($posted) {
		$fields = array(
			'id_service' => $posted['service_to_assoc']
		);
		$this->db->where('id_service = "'.$posted['id_service_to_del'].'"');
		$this->db->update(self::T_SERVICES_USERS, $fields);
		
		$this->db->where('id = '.$posted['id_service_to_del'])->delete(self::T_SERVICES);	
	}
	
	public function update_service($posted) {
		$fields = array(
			'name' => $posted['name']
		);
		$this->db->where('id = '.$posted['id_service']);
		$this->db->update(self::T_SERVICES, $fields);
	}
	
}