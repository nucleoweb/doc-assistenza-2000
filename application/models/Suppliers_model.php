<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Suppliers_model extends CI_Model {

	const T_SUPPLIERS = 'b_suppliers';
	
	function __construct() {
		parent::__construct();
	}
	
	public function get_suppliers($default_data = NULL) {
		$suppliers = $this->db->from(self::T_SUPPLIERS)->order_by('denominazione', 'ASC')->order_by('cognome', 'ASC')->get()->result_array();
		foreach ($suppliers as $k => $sup_info) {
			$suppliers[$k]['costcenter'] = $this->costcenters_model->get_costcenter($sup_info['id_costcenter']);
			$suppliers[$k]['costcat'] = $this->costcats_model->get_costcat($sup_info['id_costcat']);
		}
		return $suppliers;
	}
	
	public function get_supplier($id_supplier= NULL, $piva = NULL, $codfisc = NULL) {
		if ($id_supplier)
			return $this->db->where('id = '.$id_supplier)->get(self::T_SUPPLIERS)->row_array();
		elseif ($piva OR $codfisc) {
			if ($piva AND $piva != "")
				$res = $this->db->where("piva = '".$piva."'")->get(self::T_SUPPLIERS);
			else
				$res = $this->db->where("codfisc = '".$codfisc."'")->get(self::T_SUPPLIERS);
			if ($res)
				return $res->row_array();
		}
		return false;
	}
	
	public function insert_supplier($posted) {
		if (!$this->db->insert(self::T_SUPPLIERS, $posted)) {
			echo '<h1>Si è verificato un errore</h1>';
			echo '<pre>'.$this->db->last_query().'</pre>';
			die();
		}
		$id_supplier = $this->db->insert_id();
		return $id_supplier;
	}
	
	public function del_supplier($posted) {		
		$this->db->where('id = '.$posted['id_supplier_to_del'])->delete(self::T_SUPPLIERS);	
	}
	
	public function update_supplier($posted) {
		$id_supplier = $posted['id_supplier'];
		unset($posted['id_supplier']);

		$this->db->where('id = '.$id_supplier);

		foreach ($posted as $k => $v)
			if (preg_match('/^id_(.*)$/', $k) AND $v == "")
				$posted[$k] = NULL;
				
		$this->db->update(self::T_SUPPLIERS, $posted);
	}
	
}