<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers_model extends CI_Model {

	const T_CUSTOMERS_USERS = 'customers_users';
	const T_CUSTOMERS = 'customers';
	const T_USERS = 'users';
	const T_USERS_WORK = 'users_work';
	
	function __construct() {
		parent::__construct();
	}
	
	public function get_customers($default_data = NULL) {
		$result = $this->db->from(self::T_CUSTOMERS)->order_by('name', 'ASC')->get()->result_array();
		$customers = array();
		foreach ($result as $customer) {
			if ($default_data) {
				$customer['num_users'] = $this->db->where('id_customer', $customer['id'])->get(self::T_CUSTOMERS_USERS)->num_rows();
				$customer['num_users_active'] = $this->db->from(self::T_CUSTOMERS_USERS)
													     ->join(self::T_USERS, self::T_CUSTOMERS_USERS.'.id_user = '.self::T_USERS.'.id_user', 'left')
													     ->join(self::T_USERS_WORK, self::T_CUSTOMERS_USERS.'.id_user = '.self::T_USERS_WORK.'.id_user', 'left')
													     ->where(self::T_CUSTOMERS_USERS.'.id_customer = '.$customer['id'].' AND '.self::T_USERS.'.active = 1 AND '.self::T_USERS_WORK.'.is_hired = 1')
													     ->get()->num_rows();
				array_push($customers, $customer);
			}
			else
				$customers[$customer['id']] = $customer['name'];
		}
		return $customers;
	}
	
	public function get_customer($id_customer) {
		return $this->db->where('id = '.$id_customer)->get(self::T_CUSTOMERS)->row_array();
	}
	
	public function insert_customer($posted) {
		$this->db->insert(self::T_CUSTOMERS, $posted);		
		$id_customer = $this->db->insert_id();
		return $id_customer;
	}
	
	public function del_customer($posted) {
		$fields = array(
			'id_customer' => $posted['customer_to_assoc']
		);
		$this->db->where('id_customer = "'.$posted['id_customer_to_del'].'"');
		$this->db->update(self::T_CUSTOMERS_USERS, $fields);
		
		$this->db->where('id = '.$posted['id_customer_to_del'])->delete(self::T_CUSTOMERS);	
	}
	
	public function update_customer($posted) {
		$fields = array(
			'name' => $posted['name']
		);
		$this->db->where('id = '.$posted['id_customer']);
		$this->db->update(self::T_CUSTOMERS, $fields);
	}
	
}