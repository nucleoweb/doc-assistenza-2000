<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Costcats_model extends CI_Model {

	const T_COSTCATS = 'b_costcats';
	
	function __construct() {
		parent::__construct();
	}
	
	public function get_costcats($default_data = NULL) {
		$costcats = $this->db->from(self::T_COSTCATS)->order_by('name', 'ASC')->get()->result_array();
		return $costcats;
	}
	
	public function get_costcat($id_costcat) {
		return $this->db->where('id = '.$id_costcat)->get(self::T_COSTCATS)->row_array();
	}
	
	public function insert_costcat($posted) {
		$this->db->insert(self::T_COSTCATS, $posted);		
		$id_costcat = $this->db->insert_id();
		return $id_costcat;
	}
	
	public function del_costcat($posted) {		
		$this->db->where('id = '.$posted['id_costcat_to_del'])->delete(self::T_COSTCATS);	
	}
	
	public function update_costcat($posted) {
		$fields = array(
			'name' => $posted['name'],
			'note' => $posted['note']
		);
		$this->db->where('id = '.$posted['id_costcat']);
		$this->db->update(self::T_COSTCATS, $fields);

		// echo $this->db->last_query();
		// die();
	}
	
}