<?php
ini_set("display_errors", 1);
error_reporting(E_ALL);

defined('BASEPATH') OR exit('No direct script access allowed');

class Reports_model extends CI_Model {

	const T_DOCS = 'docs';
	const T_USERS = 'users';	
	const T_USERS_INFO = 'users_info';	
	const T_USERS_WORK = 'users_work';	
	
	function __construct() {
		parent::__construct();
	}

	private function init_query($db, $params) {
		$db->from(self::T_USERS)
			->join(self::T_USERS_INFO, self::T_USERS.'.id_user = '.self::T_USERS_INFO.'.id_user', 'left')
			->join(self::T_USERS_WORK, self::T_USERS_INFO.'.id_user = '.self::T_USERS_WORK.'.id_user', 'left')
			->where("role = 'user'");
		if ($params['is_hired'] != null)
			$db->where('is_hired = '.$params['is_hired']);
		if ($params['freelance'] != null)
			$db->where('freelance = '.$params['freelance']);
		return $db;
	}
	
	function get_data($params) {
		// echo '<pre>';
		// print_r($params);
		// echo '</pre>';
		switch ($params['type']) {
			case 'ita_for':
				$ar_cond = [
					'ita' => 0,
					'for' => 1
				];
				$data = null;
				foreach ($ar_cond as $k => $v) {
					$db = $this->init_query($this->db, $params);
					$data[$k] = $db->where('foreigner = '.$v)->count_all_results();
					// echo '<pre>'.$db->last_query().'</pre>';
				}
			break;
			case 'fasce_eta':
				$ar_cond = [
					['label' => 'Da 18 a 19 anni', 'min' => 18, 'max' => 19],
					['label' => 'Da 20 a 29 anni', 'min' => 20, 'max' => 29],
					['label' => 'Da 30 a 39 anni', 'min' => 30, 'max' => 39],
					['label' => 'Da 40 a 49 anni', 'min' => 40, 'max' => 49],
					['label' => 'Da 50 a 59 anni', 'min' => 50, 'max' => 59],
					['label' => 'Oltre 60 anni', 'min' => 60, 'max' => 100]
				];
				$data = null;
				$data['tot'] = 0;
				foreach ($ar_cond as $cond) {
					$db = $this->init_query($this->db, $params);
					$data[$cond['label']] = $db->where('age BETWEEN '.$cond['min'].' AND '.$cond['max'])->count_all_results();
					$data['tot'] += $data[$cond['label']];
					// echo '<pre>'.$db->last_query().'</pre>';
				}
			break;
			default:
				$data = null;
			break;
		}
		return $data;
	}
	
}