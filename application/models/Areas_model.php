<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Areas_model extends CI_Model {

	const T_AREAS_USERS = 'areas_users';
	const T_AREAS = 'areas';
	const T_USERS = 'users';
	const T_USERS_WORK = 'users_work';
	
	function __construct() {
		parent::__construct();
	}
	
	public function get_areas($default_data = NULL) {
		$result = $this->db->from(self::T_AREAS)->order_by('name', 'ASC')->get()->result_array();
		$areas = array();
		foreach ($result as $area) {
			if ($default_data) {
				$area['num_users'] = $this->db->where('id_area', $area['id'])->get(self::T_AREAS_USERS)->num_rows();
				$area['num_users_active'] = $this->db->from(self::T_AREAS_USERS)
													->join(self::T_USERS, self::T_AREAS_USERS.'.id_user = '.self::T_USERS.'.id_user', 'left')
													->join(self::T_USERS_WORK, self::T_AREAS_USERS.'.id_user = '.self::T_USERS_WORK.'.id_user', 'left')
													->where(self::T_AREAS_USERS.'.id_area = '.$area['id'].' AND '.self::T_USERS.'.active = 1 AND '.self::T_USERS_WORK.'.is_hired = 1')
													->get()->num_rows();
				array_push($areas, $area);
			}
			else
				$areas[$area['id']] = $area['name'];
		}
		return $areas;
	}
	
	public function get_area($id_area) {
		return $this->db->where('id = '.$id_area)->get(self::T_AREAS)->row_array();
	}
	
	public function insert_area($posted) {
		$this->db->insert(self::T_AREAS, $posted);		
		$id_area = $this->db->insert_id();
		return $id_area;
	}
	
	public function del_area($posted) {
		$fields = array(
			'id_area' => $posted['area_to_assoc']
		);
		$this->db->where('id_area = "'.$posted['id_area_to_del'].'"');
		$this->db->update(self::T_AREAS_USERS, $fields);
		
		$this->db->where('id = '.$posted['id_area_to_del'])->delete(self::T_AREAS);	
	}
	
	public function update_area($posted) {
		$fields = array(
			'name' => $posted['name']
		);
		$this->db->where('id = '.$posted['id_area']);
		$this->db->update(self::T_AREAS, $fields);
	}
	
}