<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bcustomers_model extends CI_Model {

	const T_CUSTOMERS = 'b_customers';
	
	function __construct() {
		parent::__construct();
	}
	
	public function get_customers($default_data = NULL) {
		$customers = $this->db->from(self::T_CUSTOMERS)->order_by('denominazione', 'ASC')->order_by('cognome', 'ASC')->get()->result_array();
		foreach ($customers as $k => $cust_info)
			$customers[$k]['costcenter'] = $this->costcenters_model->get_costcenter($cust_info['id_costcenter']);
		return $customers;
	}
	
	public function get_customer($id_customer = NULL, $piva = NULL, $codfisc = NULL) {
		if ($id_customer)
			return $this->db->where('id = '.$id_customer)->get(self::T_CUSTOMERS)->row_array();
		elseif ($piva OR $codfisc) {
			if ($piva AND $piva != "")
				$res = $this->db->where("piva = '".$piva."'")->get(self::T_CUSTOMERS);
			else
				$res = $this->db->where("codfisc = '".$codfisc."'")->get(self::T_CUSTOMERS);
			if ($res)
				return $res->row_array();
		}
		return false;
	}
	
	public function insert_customer($posted) {
		$this->db->insert(self::T_CUSTOMERS, $posted);		
		$id_customer = $this->db->insert_id();
		return $id_customer;
	}
	
	public function del_customer($posted) {		
		$this->db->where('id = '.$posted['id_customer_to_del'])->delete(self::T_CUSTOMERS);	
	}
	
	public function update_customer($posted) {
		$id_customer = $posted['id_customer'];
		unset($posted['id_customer']);

		$this->db->where('id = '.$id_customer);

		foreach ($posted as $k => $v)
			if (preg_match('/^id_(.*)$/', $k) AND $v == "")
				$posted[$k] = NULL;
				
		$this->db->update(self::T_CUSTOMERS, $posted);
	}
	
}