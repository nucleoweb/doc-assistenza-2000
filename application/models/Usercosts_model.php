<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usercosts_model extends CI_Model {

	const T_USERCOSTS = 'b_usercosts';
	const T_USERCOSTS_DETAILS = 'b_usercosts_details';
	const T_USERS = 'users';
	const T_USERS_INFO = 'users_info';
	const T_USERS_WORK = 'users_work';
	
	function __construct() {
		parent::__construct();
	}
	
	public function get_usercosts($default_data = NULL, $filter = null) {
		$query = $this->db->select(self::T_USERCOSTS.'.*')
						  ->from(self::T_USERCOSTS)
						  ->join(self::T_USERCOSTS_DETAILS, self::T_USERCOSTS.'.id = '.self::T_USERCOSTS_DETAILS.'.id_usercost');
		# FILTRI - START
		// if (!$filter)
		// 	$filter = [
		// 		'filter_from' => date('Y-m').'-01',
		// 		'filter_to' => date('Y-m-d')
		// 	];

		// if (!empty($filter['filter_from']))
		// 	$query->where("data >= '".$filter['filter_from']."'");
		// if (!empty($filter['filter_to']))
		// 	$query->where("data <= '".$filter['filter_to']."'");
		if (!empty($filter['filter_period_month'])) {
			$query->where(self::T_USERCOSTS_DETAILS.'.period_month = '.$filter['filter_period_month']);
		}
		if (!empty($filter['filter_period_year'])) {
			$query->where(self::T_USERCOSTS_DETAILS.'.period_year = '.$filter['filter_period_year']);
		}
		if (!empty($filter['filter_costcenter'])) {
			$query->where(self::T_USERCOSTS_DETAILS.'.id_costcenter = '.$filter['filter_costcenter']);
		}
		if (!empty($filter['filter_name'])) {
			$query->where("CONCAT(".self::T_USERCOSTS.".nome, ' ', ".self::T_USERCOSTS.".cognome) LIKE '%".addslashes($filter['filter_name'])."%'");
		}
		if (!empty($filter['filter_date'])) {
			$query->where(self::T_USERCOSTS.".created_at >= '".$filter['filter_date']." 00:00:00"."'");
			$query->where(self::T_USERCOSTS.".created_at <= '".$filter['filter_date']." 23:59:59"."'");
		}
		# FILTRI - END
		$usercosts = $query->group_by(self::T_USERCOSTS_DETAILS.'.id_usercost')->order_by(self::T_USERCOSTS.'.cognome', 'ASC')->order_by(self::T_USERCOSTS.'.nome', 'ASC')->order_by(self::T_USERCOSTS.'.created_at', 'ASC')->get()->result_array();

		// echo '<pre>'.$this->db->last_query().'</pre>';
		// die();

		foreach ($usercosts as $k => $cost) {
			if ($cost['id_user']) {
				$user = $this->users_model->get_user_info($cost['id_user']);
				$usercosts[$k]['utente'] = $user['last_name'].' '.$user['first_name']; 
			}
			$usercosts[$k]['detail'] = $this->db->from(self::T_USERCOSTS_DETAILS)->where('id_usercost = '.$cost['id'])->get()->result_array();
		}
		return $usercosts;
	}
	
	public function get_usercost($id_usercost = NULL, $codfisc = NULL, $data_elaborazione = NULL, $period_month = NULL, $period_year = NULL, $id_costcenter = NULL, $cod_dipendente = NULL) {
		$res = null;
		if ($id_usercost)
			$res = $this->db->where('id = '.$id_usercost)->get(self::T_USERCOSTS)->row_array();
		elseif ($codfisc AND $period_month AND $period_year AND $id_costcenter) {
			$tmp_res = $this->db->where("codfisc = '".$codfisc."'");
			$tmp_res = $this->db->where("data_elaborazione = '".$data_elaborazione."'");
			$tmp_res = $this->db->where("period_month = ".$period_month);
			$tmp_res = $this->db->where("period_year = ".$period_year);
			$tmp_res = $this->db->where("id_costcenter = ".$id_costcenter);
			$tmp_res = $this->db->where("cod_dipendente = ".$cod_dipendente);
			if ($tmp_res)
				$res = $tmp_res->get(self::T_USERCOSTS)->row_array();
		}
		if ($res) {
			$res['detail'] = $this->db->from(self::T_USERCOSTS_DETAILS)->where('id_usercost = '.$res['id'])->get()->result_array();
		}
		return $res;
	}
	
	public function insert_usercost($posted) {
		// Inserisco il costo dipendente
		$this->db->insert(self::T_USERCOSTS, $posted);
		$id_usercost = $this->db->insert_id();
		// Creo il dettaglio per il costo appena inserito, che in primis sarà solo 1 riga
		$ar_detail = [
			'id_usercost' => $id_usercost,
			'id_costcenter' => $posted['id_costcenter'],
			'period_month' => $posted['period_month'],
			'period_year' => $posted['period_year'],
			'amount' => $posted['amount'],
			'hours' => $posted['hours'],
			'hourly_cost' => $posted['hourly_cost']
		];
		$this->db->insert(self::T_USERCOSTS_DETAILS, $ar_detail);
		// Ritorno l'ID del costo dipendente inserito
		return $id_usercost;
	}
	
	public function del_usercost($posted) {	
		$this->db->where('id = '.$posted['id_usercost_to_del'])->delete(self::T_USERCOSTS);	
		$this->db->where('id_usercost = '.$posted['id_usercost_to_del'])->delete(self::T_USERCOSTS_DETAILS);	
	}

	public function del_usercosts($posted) {
		$this->db->trans_start();
		$this->db->from(self::T_USERCOSTS)->where("created_at LIKE '".$posted['upload_date']."%'");
		if ($posted['period_month'] AND $posted['period_year']) {
			$this->db->where("period_month = ".$posted['period_month']);
			$this->db->where("period_year = ".$posted['period_year']);
		}
		$this->db->delete();	// le righe di dettaglio vengono cancellate ON CASCADE
		$deleted_rows = $this->db->affected_rows();
		// $this->db->from($tab.'_details')->where("created_at LIKE '".$posted['upload_date']."%'")->delete();
		// $deleted_detail_rows = $this->db->affected_rows();
		$this->db->trans_complete();
		
		return [
			'deleted_rows' => $deleted_rows,
			'trans_status' => $this->db->trans_status()
		];
	}
	
	public function update_usercost($posted) {
		// Estraggo i dati delle voci di dettaglio
		$details = $posted['detail'];
		unset($posted['detail']);
		// Aggiorno il record principale del costo
		$this->db->where('id = '.$posted['id_usercost']);
		$this->db->update(self::T_USERCOSTS, $posted);
		// Aggiorno tutti i dettagli del costo
		$this->db->trans_start();
		$this->db->from(self::T_USERCOSTS_DETAILS)->where('id_usercost = '.$posted['id_usercost'])->delete();
		foreach ($details['amount'] as $k => $v) {
			$detail_data = [
				'id_usercost' => $posted['id_usercost'],
				'id_costcenter' => $details['id_costcenter'][$k],
				'period_month' => $details['period_month'][$k],
				'period_year' => $details['period_year'][$k],
				'amount' => $details['amount'][$k],
				'hours' => $details['hours'][$k],
				'hourly_cost' => $details['hourly_cost'][$k],
			];
			$this->db->insert(self::T_USERCOSTS_DETAILS, $detail_data);
		}
		$this->db->trans_complete();
	}

	public function get_last_imports($num = 30) {
		$usercosts_imports = [];
		
		$usercosts_dates = $this->db->query('SELECT DISTINCT DATE(created_at) AS date FROM '.self::T_USERCOSTS.' ORDER BY date DESC LIMIT '.$num)->result_array();
		
		foreach ($usercosts_dates as $d)
			$usercosts_imports[$d['date']] = $this->db->query("SELECT COUNT(*) AS num FROM ".self::T_USERCOSTS." WHERE DATE(created_at) = '".$d['date']."'")->row_array()['num'];

		return $usercosts_imports;
	}

	public function calcCCsumByMonthYear($id_centercost, $m, $y, $num_buste = null, $tot_buste = null) {
		// Calcolo la somma dei costi per il personale nei centri di costo standard
		$res = $this->db->query("SELECT SUM(amount) AS tot 
								FROM ".self::T_USERCOSTS_DETAILS." 
								WHERE period_month = ".$m." AND period_year = ".$y." AND id_costcenter = ".$id_centercost)->row_array();
		$tot = $res['tot'];
		// Calcolo la somma dei costi per il personale del centro di costo *speciale*
		$res = $this->db->query("SELECT SUM(amount) AS tot 
								FROM ".self::T_USERCOSTS_DETAILS." 
								WHERE period_month = ".$m." AND period_year = ".$y." AND id_costcenter = 1000")->row_array();
		$tot_special = $res['tot'];
		// se il totale degli importi sul centro di costo SPECIALE è > 0, lo devo pesare in base al centro di costo e poi sommare al totale standard
		if ($tot_special > 0) {
			if (!$num_buste)
				$num_buste = $this->getNumBusteByCC($id_centercost, $m, $y);
			if (!$tot_buste)
				$tot_buste = $this->getNumBusteByMonthYear($m, $y);
			
			if ($tot_buste > 0 AND $num_buste > 0)
				$tot = $tot + (($tot_special / $tot_buste) * $num_buste);
		}

		return ($tot ? $tot : 0);
	}

	public function calcAllsumByMonthYear($m, $y) {
		$active_costcenters = $this->costcenters_model->get_real_active_costcenters($m, $y);

		$tot = 0;
		foreach ($active_costcenters as $center)
			$tot += $this->calcCCsumByMonthYear($center['id'], $m, $y);

		return ($tot ? $tot : 0);
	}

	public function calcFullsumByMonthYear($m, $y) {
		$res = $this->db->query("SELECT SUM(amount) AS tot 
								FROM ".self::T_USERCOSTS_DETAILS."
								WHERE period_month = ".$m." AND period_year = ".$y)
								->row_array();
		return ($res['tot'] ? $res['tot'] : '0');
	}
	
	public function getNumBusteByCC($id_center, $m, $y) {
		$res = $this->db->query("SELECT id_user AS the_user, b_usercosts.period_month, b_usercosts.period_year, b_usercosts.hours AS cc_hours, (
									SELECT SUM(b_usercosts_details.hours) 
									FROM b_usercosts_details JOIN b_usercosts ON b_usercosts_details.id_usercost = b_usercosts.id
									WHERE id_user = the_user AND b_usercosts_details.period_month = ".$m." AND b_usercosts_details.period_year = ".$y."
								) AS tot_hours
								FROM b_usercosts 
								WHERE id_costcenter = ".$id_center." AND period_month = ".$m." AND period_year = ".$y."
								GROUP BY id_user")->result_array();
		// $res = $this->db->query("SELECT b_usercosts.id_user AS the_user, b_usercosts_details.period_month, b_usercosts_details.period_year, SUM(b_usercosts_details.hours) AS tot_hours,
		// 						(
		// 							SELECT SUM(b_usercosts_details.hours)
		// 							FROM b_usercosts_details JOIN b_usercosts ON b_usercosts_details.id_usercost = b_usercosts.id
		// 							WHERE b_usercosts.id_user = the_user AND b_usercosts_details.period_month = ".$m." AND b_usercosts_details.period_year = ".$y." AND b_usercosts_details.id_costcenter = ".$id_center."
		// 						) AS cc_hours
		// 						FROM b_usercosts_details JOIN b_usercosts ON b_usercosts_details.id_usercost = b_usercosts.id
		// 						GROUP BY b_usercosts.id_user
		// 						HAVING b_usercosts_details.period_month = ".$m." AND b_usercosts_details.period_year = ".$y."
		// 						ORDER BY b_usercosts.id_user ASC")->result_array();

		$num_buste = 0;
		foreach ($res as $row) {
			if ($row['cc_hours'] AND $row['cc_hours'] > 0) {
				$num_buste += round(($row['cc_hours'] / $row['tot_hours']), 2);
			}
		}

		return $num_buste;
	}

	public function getNumBusteByMonthYear($m, $y) {
		$costcenters = $this->costcenters_model->get_real_active_costcenters($m, $y);
		$num_buste = 0;
		foreach ($costcenters as $center)
			$num_buste += $this->getNumBusteByCC($center['id'], $m, $y);
		return $num_buste;
	}
}