<?php
ini_set("display_errors", 1);
error_reporting(E_ALL);

defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once './application/libraries/PHPMailer/Exception.php';
require_once './application/libraries/PHPMailer/PHPMailer.php';
require_once './application/libraries/PHPMailer/SMTP.php';

require_once './application/libraries/aruba_sms.php';

class Docs_model extends CI_Model {

	const T_DOCS = 'docs';
	const T_USERS = 'users';
	const T_USERS_INFO = 'users_info';
	const T_DOWNLOADS = 'downloads';
	const T_DOCS_USERS = 'docs_users';
	const F_DOC_FTP = './pdf_upload/';
	const F_DOC_STORAGE = './assets/docs/';
	const F_DOC_SCHEDULE = './tosend.csv';
	const NUM_FROM_SCHEDULE = 5;
	
	function __construct() {
		parent::__construct();
		$this->load->library('email');
		$this->email->initialize();

		/*
		if ($_SERVER['REMOTE_ADDR'] == "82.50.195.44") {
			echo 'test';
			$result = $this->db->where("`note` LIKE '%FTP%' AND `date_upload` LIKE '2020-06-19%'")->get(self::T_DOCS)->result_array();
			foreach ($result as $doc_info) {
				$assoc_user = $this->db->where("`id_doc` = ".$doc_info['id_doc'])->get(self::T_DOCS_USERS)->row_array();
				$id_user = $assoc_user['id_user'];
				$id_doc = $doc_info['id_doc'];
				$filename = $doc_info['filename'];
				$send_mail = 0;
				$send_sms = 1;
				// $this->send_doc_notify($id_user, $id_doc, $filename, $send_mail, $send_sms);
			}
			// die();
		}
		*/
	}
	
	private function schedule_doc_notify($id_user, $id_doc, $filename, $send_mail, $send_sms) {
		$data = $id_user.",".$id_doc.",".$filename.",".(($send_mail) ? '1' : '0').",".(($send_sms) ? '1' : '0')."\n";
		write_file(self::F_DOC_SCHEDULE, $data, 'a');
	}
	
	public function send_doc_notify($id_user, $id_doc, $filename, $send_mail, $send_sms) {
		$user_info = $this->users_model->get_user_info($id_user);
		$doc_info = $this->get_doc($id_doc);
		
		# 1. INVIO NOTIFICA VIA EMAIL
		if ($send_mail AND $doc_info['alert_mail']) {
			$mail = new PHPMailer(true);
			try {
				// $mail->SMTPDebug = 2;
				$mail->isSMTP();
				$mail->Host = 'smtps.aruba.it';
				$mail->CharSet = 'UTF-8';
				$mail->SMTPAuth = true;
				$mail->Username = 'doc-assistenza2000@gruppoyuma.it';
				$mail->Password = 'DOC.lcic$';
				$mail->SMTPSecure = 'ssl';
				$mail->Port = 465;
				$mail->setFrom('info@assistenza2000.it', 'Assistenza 2000');
				$mail->addAddress($user_info['email'], $user_info['first_name'].' '.$user_info['last_name']);
				$mail->isHTML(true);
				$mail->Subject = 'Nuovo documento disponibile';
				$mail->Body = '<img src="https://doc.assistenza2000.it/assets/gfx/logo.jpg"><br>
										<p>Un nuovo documento è stato caricato per te nell\'area riservata.</p>
										<p>Collegati a <a href="https://doc.assistenza2000.it">doc.assistenza2000.it</a> per visualizzarlo / scaricarlo.</p>';
				$mail->send();
			} catch (Exception $e) {
				echo 'Message could not be sent.';
				echo 'Mailer Error: ' . $mail->ErrorInfo;
			}
		}
		
		# 2. INVIO NOTIFICA VIA SMS
		if ($send_sms AND $doc_info['alert_sms']) {
			$aruba_sms = new ArubaSMS();

			$sms_recipient = ((strpos($user_info['telephone'], '+39') === FALSE) ? '+39' : '').str_replace(array('.','-',' ','/'), array('','','',''), $user_info['telephone']);
			$sms_message = "ASSISTENZA 2000\n\nAvviso: nuovo documento per te. Collegati alla piattaforma documentale per visualizzarlo.\n\nhttps://doc.assistenza2000.it";
			
			$aruba_sms->send_sms($sms_message, $sms_recipient);

			/*
			require_once('./lib/sms/sendsms.php');
			$sms = new Sdk_SMS();
			$sms->sms_type = SMSTYPE_ALTA;
			$sms->add_recipient(((strpos($user_info['telephone'], '+39') === FALSE) ? '+39' : '').str_replace(array('.','-',' ','/'), array('','','',''), $user_info['telephone']));
			$sms->message = "Un nuovo documento è stato caricato per te nell'area riservata. Collegati alla piattaforma documentale per scaricarlo.\n\nASSISTENZA 2000";
			$sms->sender = '3711649736';
			//$sms->sender = 'Assist2000';
			$sms->set_immediate(); // or sms->set_scheduled_delivery($unix_timestamp)
			$sms->order_id = '69063344';
			// echo 'About to send a message '.$sms->count_smss().' SMSs long ';
			// echo 'to '.$sms->count_recipients().' recipients </br>';
			if ($sms->validate()) {
				$res = $sms->send();
				if ($res['ok']) {
					// echo $res['sentsmss'].' SMS sent, order id is '.$res['order_id'].' </br>';
				} else {
					// echo 'Error sending SMS: '.$sms->problem().' </br>';
				}
			}
			else {
				// echo 'invalid SMS: '.$sms->problem().' </br>';
			}
			*/
		}
	}
	
	public function deassoc_doc($start, $end) {
		$start_date = implode("-", array_reverse(explode("/", substr($start, 0, 10))));
		$start_hour = substr($start, 11).":00";
		$start = $start_date." ".$start_hour;
		
		$end_date = implode("-", array_reverse(explode("/", substr($end, 0, 10))));
		$end_hour = substr($end, 11).":00";
		$end = $end_date." ".$end_hour;
		
		$result = $this->db->where("created_at >= '".$start."' AND created_at <= '".$end."'")->delete(self::T_DOCS_USERS);
		return TRUE;
		// echo $this->db->last_query();
	}
	
	public function get_req_string($id_doc) {
		$doc = $this->db->where('id_doc', $id_doc)->get(self::T_DOCS)->row_array();
		return urlencode(base64_encode($id_doc."|".$doc['filename']));
	}
	
	public function assoc_doc($filename, $id_user, $posted, $schedule = FALSE) {
		if (isset($posted['alert_mail'])) {
			$send_mail = TRUE;
			$posted['alert_mail'] = '1';
		} else {
			$send_mail = FALSE;
			$posted['alert_mail'] = '0';
		}

		if (isset($posted['alert_sms'])) {
			$send_sms = TRUE;
			$posted['alert_sms'] = '1';
		} else {
			$send_sms = FALSE;
			$posted['alert_sms'] = '0';
		}

		if (rename(self::F_DOC_FTP.$filename, self::F_DOC_STORAGE.$filename)) {
			$filename_clean = substr($filename, 0, strpos($filename, '.'));
			$ar_filename_clean = explode("_", $filename_clean);
			unset($ar_filename_clean[0]);
			$filename_clean = implode("_", $ar_filename_clean);
			
			$filedata = array(
				'name' => $filename_clean,
				'filename' => $filename,
				'note' => 'Caricato via FTP',
				'cat' => $posted['cat'],
				'year' => date("Y"),
				'month' => date("n"),
				'alert_mail' => $posted['alert_mail'],
				'alert_sms' => $posted['alert_sms']
			);
			// if (preg_match('/Tredicesima/', $filename)) {	// eccezione per mettere i file come già scaricati
				// $filedata['year'] = '2018';
				// $filedata['month'] = '12';
				// $filedata['date_upload'] = "2018-12-20 10:00:00";
			// }
			$this->db->insert(self::T_DOCS, $filedata);
			$id_doc = $this->db->insert_id();
			$fields = array(
				'id_doc' => $id_doc,
				'id_user' => $id_user
			);
			// if (preg_match('/Tredicesima/', $filename)) {	// eccezione per mettere i file come già scaricati
				// $fields['downloaded'] = '1';
				// $fields['date_download'] = "2018-12-20 10:00:00";
				// $fields['created_at'] = "2018-12-20 10:00:00";
			// }
			$this->db->insert(self::T_DOCS_USERS, $fields);
			if (!$schedule) {
				$this->send_doc_notify($id_user, $id_doc, $filename, $send_mail, $send_sms);
			} else {
				$this->schedule_doc_notify($id_user, $id_doc, ((!empty($posted['name'])) ? $posted['name'] : ''), $send_mail, $send_sms);
			}
			return $id_doc;
		}
		return FALSE;
	}
	
	public function save_doc($posted) {
		if (isset($posted['alert_mail'])) {
			$send_mail = TRUE;
			$posted['alert_mail'] = '1';
		} else {
			$send_mail = FALSE;
			$posted['alert_mail'] = '0';
		}

		if (isset($posted['alert_sms'])) {
			$send_sms = TRUE;
			$posted['alert_sms'] = '1';
		} else {
			$send_sms = FALSE;
			$posted['alert_sms'] = '0';
		}
		
		$user_ids = ((isset($posted['users'])) ? $posted['users'] : array());
		$num_users = count($user_ids);
		if (isset($posted['users']))
			unset($posted['users']);
		if (isset($posted['select_all']))
			unset($posted['select_all']);
		
		$posted['year'] = date("Y");
		$posted['month'] = date("n");
		
		foreach ($posted as $k => $v)		# rimuovo tutti i campi filtro dall'array POST
			if (strpos($k, 'filter'))
				unset($posted[$k]);
		
		if ($this->db->insert(self::T_DOCS, $posted)) {
			$id_doc = $this->db->insert_id();
			foreach ($user_ids as $id_user) {	// per ogni utente della lista che ho selezionato
				$user_tmp = $this->db->where('id_user', $id_user)->get(self::T_USERS)->row_array();
				$active = $user_tmp['active'];
				if ($active) {	// solo se l'utente che sto analizzando è abilitato
					$fields = array(
						'id_doc' => $id_doc,
						'id_user' => $id_user
					);
					if ($this->db->insert(self::T_DOCS_USERS, $fields)) {
						if ($num_users <= self::NUM_FROM_SCHEDULE) {
							$this->send_doc_notify($id_user, $id_doc, $posted['name'], $send_mail, $send_sms);
						} else {
							$this->schedule_doc_notify($id_user, $id_doc, $posted['name'], $send_mail, $send_sms);
						}
					} else {
						echo '<pre>';
						print_r($this->db->error());
						echo '</pre>';
					}
				}
			}
			return $id_doc;
		} else {
			echo '<pre>';
			print_r($this->db->error());
			echo '</pre>';
		}
	}
	
	public function update_doc($posted) {
		if (isset($posted['alert_mail'])) {
			$send_mail = TRUE;
			$posted['alert_mail'] = '1';
		} else {
			$send_mail = FALSE;
			$posted['alert_mail'] = '0';
		}

		if (isset($posted['alert_sms'])) {
			$send_sms = TRUE;
			$posted['alert_sms'] = '1';
		} else {
			$send_sms = FALSE;
			$posted['alert_sms'] = '0';
		}
		
		$user_ids = ((isset($posted['users'])) ? $posted['users'] : array());
		$num_users = count($user_ids);
		if (isset($posted['users']))
			unset($posted['users']);
		if (isset($posted['select_all']))
			unset($posted['select_all']);
		
		foreach ($posted as $k => $v)		# rimuovo tutti i campi filtro dall'array POST
			if (strpos($k, 'filter'))
				unset($posted[$k]);
		
		$this->db->where('id_doc = '.$posted['id_doc']);
		$this->db->update(self::T_DOCS, $posted);
		
		# AGGIORNO I COLLEGAMENTI CON GLI UTENTI
		# Recupero la lista degli id utente che avevo associato in precedenza e che ora sono stati disassociati, perché devo fare l'unschedule
		if (count($user_ids)) {
			$unschedule_users = $this->db->select('id_user')->where('id_doc = '.$posted['id_doc'].' AND id_user NOT IN ('.implode(",", $user_ids).')')->get(self::T_DOCS_USERS)->result_array('id_user');
			$unschedule_users_ids = array();
			if (is_array($unschedule_users) AND count($unschedule_users)) {
				foreach ($unschedule_users as $unschedule_user)
					$unschedule_users_ids[] = $unschedule_user['id_user'];
				$this->unschedule_doc_notify($unschedule_users_ids, $posted['id_doc']);
			}
		}
		# Cancello tutti quelli che ora non sono più associati
		if (count($user_ids))
			$this->db->where('id_doc = '.$posted['id_doc'].' AND id_user NOT IN ('.implode(",", $user_ids).')')->delete(self::T_DOCS_USERS);
		else
			$this->db->where('id_doc = '.$posted['id_doc'])->delete(self::T_DOCS_USERS);
		# Mi recupero le attuali associazioni e inserisco quelle nuove (!in_array)
		$connected_users = $this->db->select('id_user')->where('id_doc = '.$posted['id_doc'])->get(self::T_DOCS_USERS)->result_array('id_user');
		$connected_users_ids = array();
		foreach ($connected_users as $conn_user)
			$connected_users_ids[] = $conn_user['id_user'];
		foreach ($user_ids as $id_user) {
			if (!in_array($id_user, $connected_users_ids)) {
				$fields = array(
					'id_doc' => $posted['id_doc'],
					'id_user' => $id_user
				);
				if ($this->db->insert(self::T_DOCS_USERS, $fields)) {
					if ($num_users <= self::NUM_FROM_SCHEDULE) {
						$this->send_doc_notify($id_user, $posted['id_doc'], $posted['name'], $send_mail, $send_sms);
					} else {
						$this->schedule_doc_notify($id_user, $posted['id_doc'], $posted['name'], $send_mail, $send_sms);
					}
				} else {
					echo '<pre>';
					print_r($this->db->error());
					echo '</pre>';
				}
			}
		}
	}
	
	public function unschedule_doc_notify($user_ids, $id_doc) {	# $user_ids contiene gli id degli utenti di cui togliere la schedulazione
		$f = fopen(self::F_DOC_SCHEDULE, "r");
		$new_file_content = "";
		while (!feof($f)) {
			$file_row = fgets($f);
			if ($file_row != "") {
				$ar_row = explode(",", $file_row);
				$tmp_id_user = $ar_row[0];
				$tmp_id_doc = $ar_row[1];
				if ($tmp_id_doc != $id_doc OR !in_array($tmp_id_user, $user_ids))
					$new_file_content .= $file_row;
			}
		}
		fclose($f);
		// echo $new_file_content;
		// die();
		write_file(self::F_DOC_SCHEDULE, $new_file_content);
	}
	
	public function get_years() {
		$years = array();
		$result = $this->db->select('year')->distinct()->order_by('year', 'ASC')->get(self::T_DOCS)->result_array();
		foreach ($result as $row)
			$years[] = $row['year'];
		return $years;
	}
	
	public function get_docs($id_user = NULL, $year = NULL, $month = NULL, $cat = NULL) {
		$this->db->from(self::T_DOCS);
		if (!empty($id_user)) {
			$this->db->join(self::T_DOCS_USERS, self::T_DOCS.'.id_doc = '.self::T_DOCS_USERS.'.id_doc');
			$this->db->where('id_user = '.$id_user);
		} else {
			$this->db->where('year = '.((!empty($year)) ? $year : date("Y")));
			$this->db->where('month = '.((!empty($month)) ? $month : date("n")));
		}
		if (!empty($cat)) {
			$this->db->where('cat = "'.$cat.'"');
		}
		if (!empty($id_user)) {		/* MODIFICA NOVEMBRE 2018 */
			$this->db->order_by('date_upload', 'DESC');
			// $this->db->order_by('downloaded', 'ASC');
		}
		$result = $this->db->get()->result_array();
		
		/* MODIFICA MAGGIO 2018 */
		// if ($_SERVER['REMOTE_ADDR'] == '82.53.185.100') {
			foreach ($result as $k => $doc) {
				$tmp_doc = $this->get_doc($doc['id_doc']);
				$tmp_users = ((isset($tmp_doc['users'])) ? $tmp_doc['users'] : array());
				$result[$k]['users_assigned'] = $tmp_users;
			}
			// echo '<pre>';
			// print_r($result);
			// echo '</pre>';
			// die();
		// }
		
		return $result;
	}
	
	public function get_doc($id_doc, $sort_users_by_name = FALSE) {
		$result = $this->db->where('id_doc = '.$id_doc)->get(self::T_DOCS)->row_array();
		if ($sort_users_by_name) {
			$result_users = $this->db->select(self::T_DOCS_USERS.'.*, '.self::T_USERS_INFO.'.first_name, '.self::T_USERS_INFO.'.last_name')
									 ->from(self::T_DOCS_USERS)
									 ->join(self::T_USERS_INFO, self::T_DOCS_USERS.'.id_user = '.self::T_USERS_INFO.'.id_user')
									 ->where(self::T_DOCS_USERS.'.id_doc = '.$id_doc)
									 ->order_by('last_name ASC, first_name ASC')
									 ->get()
									 ->result_array();
		}
		else
			$result_users = $this->db->where('id_doc = '.$id_doc)->get(self::T_DOCS_USERS)->result_array();
		foreach ($result_users as $user_found) {
			$result_assoc = $this->db->where('id_doc = '.$id_doc.' AND id_user = '.$user_found['id_user'])->get(self::T_DOWNLOADS)->result_array();
			$result['users'][$user_found['id_user']] = array(
				'last_download' => $user_found['date_download'],
				'list_downloads' => $result_assoc
			);
			if ($sort_users_by_name) {
				$result['users'][$user_found['id_user']]['first_name'] = $user_found['first_name'];
				$result['users'][$user_found['id_user']]['last_name'] = $user_found['last_name'];
			}
		}
		$result['req_string'] = $this->get_req_string($id_doc);
		return $result;
	}
	
	public function user_has_doc($id_user, $id_doc) {
		$doc_info = $this->get_doc($id_doc);
		if (array_key_exists($id_user, $doc_info['users']))
			return $doc_info;	# dato che chiamo direttamente la funzione get_doc, se l'utente è autorizzato ritorno direttamente le informazioni
		return FALSE;
	}
	
	public function set_download($id_user, $id_doc) {
		/* SALVO IL DOWNLOAD ATTUALE */
		$fields = array(
			'downloaded' => '1',
			'date_download' => date("Y-m-d H:i:s")
		);
		$this->db->where('id_doc = '.$id_doc.' AND id_user = '.$id_user);
		$this->db->update(self::T_DOCS_USERS, $fields);
		/* DEVO AGGIORNARE ANCHE LO STORICO DI TUTTI I DOWNLOAD */
		$fields = array(
			'id_doc' => $id_doc,
			'id_user' => $id_user
		);
		$this->db->insert(self::T_DOWNLOADS, $fields);
	}
	
	public function delete_doc($id) {
		$this->db->where('id_doc = '.$id)->delete(self::T_DOCS);
		$this->db->where('id_doc = '.$id)->delete(self::T_DOCS_USERS);
	}
	
}