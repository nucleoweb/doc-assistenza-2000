<?php
ini_set("display_errors", 1);
error_reporting(E_ALL);

defined('BASEPATH') OR exit('No direct script access allowed');

class Reports_balance_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function get_data($posted) {
		// In base all'intervallo di tempo selezionato genero una lista di tutti i mesi-anno da analizzare
		$months = generateMonthsArray($posted);

		// Centro di costo che ho selezionato e che devo analizzare
		$centers = $posted['cc'];

		$data = [];

		// Ciclo sui mesi compresi nel periodo selezionato
		foreach ($months as $timeti) {	// time-item
			$m = $timeti['month'];
			$y = $timeti['year'];
			// Ciclo sui centri di costo selezionati
			foreach ($centers as $cc) {
				if ($cc == 'x') {
					$data[$y][$m]['COMPLESSIVO']['revenues'] = $this->revenues_model->calcFullsumByMonthYear($m, $y);
					$data[$y][$m]['COMPLESSIVO']['usercosts'] = $this->usercosts_model->calcFullsumByMonthYear($m, $y);
					$data[$y][$m]['COMPLESSIVO']['suppliercosts'] = $this->suppliercosts_model->calcFullsumByMonthYear($m, $y);
				} else {
					$cc_info = $this->costcenters_model->get_costcenter($cc);
					$data[$y][$m][$cc_info['name'].' - '.$cc_info['location']]['revenues'] = $this->revenues_model->calcCCsumByMonthYear($cc, $m, $y);
					$data[$y][$m][$cc_info['name'].' - '.$cc_info['location']]['usercosts'] = $this->usercosts_model->calcCCsumByMonthYear($cc, $m, $y);
					$data[$y][$m][$cc_info['name'].' - '.$cc_info['location']]['suppliercosts'] = $this->suppliercosts_model->calcCCsumByMonthYear($cc, $m, $y);
				}
			}
		}

		return $data;
	}
	
}