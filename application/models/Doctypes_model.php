<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doctypes_model extends CI_Model {

	const T_DOCTYPES = 'b_doctypes';
	
	function __construct() {
		parent::__construct();
	}
	
	public function get_doctypes($default_data = NULL) {
		$doctypes = $this->db->from(self::T_DOCTYPES)->order_by('cod', 'ASC')->get()->result_array();
		return $doctypes;
	}
	
	public function get_doctype($id_doctype) {
		return $this->db->where('id = '.$id_doctype)->get(self::T_DOCTYPES)->row_array();
	}
	
	public function insert_doctype($posted) {
		$this->db->insert(self::T_DOCTYPES, $posted);		
		$id_doctype = $this->db->insert_id();
		return $id_doctype;
	}
	
	public function del_doctype($posted) {		
		$this->db->where('id = '.$posted['id_doctype_to_del'])->delete(self::T_DOCTYPES);	
	}
	
	public function update_doctype($posted) {
		$fields = array(
			'cod' => $posted['cod'],
			'description' => $posted['description'],
			'type' => $posted['type']
		);
		$this->db->where('id = '.$posted['id_doctype']);
		$this->db->update(self::T_DOCTYPES, $fields);
	}

}