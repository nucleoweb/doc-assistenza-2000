<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Levels_model extends CI_Model {

	const T_LEVELS_USERS = 'levels_users';
	const T_LEVELS = 'levels';
	const T_USERS = 'users';
	const T_USERS_WORK = 'users_work';
	
	function __construct() {
		parent::__construct();
	}
	
	public function get_levels($default_data = NULL) {
		$result = $this->db->from(self::T_LEVELS)->order_by('name', 'ASC')->get()->result_array();
		$levels = array();
		foreach ($result as $level) {
			if ($default_data) {
				$level['num_users'] = $this->db->where('id_level', $level['id'])->get(self::T_LEVELS_USERS)->num_rows();
				$level['num_users_active'] = $this->db->from(self::T_LEVELS_USERS)
													  ->join(self::T_USERS, self::T_LEVELS_USERS.'.id_user = '.self::T_USERS.'.id_user', 'left')
													  ->join(self::T_USERS_WORK, self::T_LEVELS_USERS.'.id_user = '.self::T_USERS_WORK.'.id_user', 'left')
													  ->where(self::T_LEVELS_USERS.'.id_level = '.$level['id'].' AND '.self::T_USERS.'.active = 1 AND '.self::T_USERS_WORK.'.is_hired = 1')
													  ->get()->num_rows();
				array_push($levels, $level);
			}
			else
				$levels[$level['id']] = $level['name'];
		}
		return $levels;
	}
	
	public function get_level($id_level) {
		return $this->db->where('id = '.$id_level)->get(self::T_LEVELS)->row_array();
	}
	
	public function insert_level($posted) {
		$this->db->insert(self::T_LEVELS, $posted);		
		$id_level = $this->db->insert_id();
		return $id_level;
	}
	
	public function del_level($posted) {
		$fields = array(
			'id_level' => $posted['level_to_assoc']
		);
		$this->db->where('id_level = "'.$posted['id_level_to_del'].'"');
		$this->db->update(self::T_LEVELS_USERS, $fields);
		
		$this->db->where('id = '.$posted['id_level_to_del'])->delete(self::T_LEVELS);	
	}
	
	public function update_level($posted) {
		$fields = array(
			'name' => $posted['name']
		);
		$this->db->where('id = '.$posted['id_level']);
		$this->db->update(self::T_LEVELS, $fields);
	}
	
}